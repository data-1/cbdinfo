---
layout: post
title: "Activation of cannabinoid receptors in breast cancer cells improves osteoblast viability in cancer-bone interaction model while reducing breast cancer cell survival and migration"
date: 2024-08-25
tags: [Endocannabinoid system,Cannabinoid receptor 2,Bone,Anandamide,NF-κB,Metastasis,Apoptosis,Cannabinoid receptor 1,Cell biology,Neurochemistry,Cell signaling,Biochemistry,Biology]
author: 
---


While ECS was shown to regulate both cancer cell progression and bone homeostasis, it was not known whether this system also affected the interaction between cancer and bone cells during breast cancer bone metastasis. Interaction between cancer and bone cells appeared to be crucial during cancer bone metastasis when cancer cells regulated bone cell differentiation and activity to facilitate their colonization in bone. Both ACEA (CB1 agonist) and GW405833 (CB2 agonist) were shown to suppress the viability of both MDA-MB-231 and UMR-106 cells with MDA-MB-231 cells being more sensitive to ACEA and GW405833 than UMR-106 cells. With the opposite effects, i.e., growth promoting effect of low concentration of CB agonists on breast cancer and osteoblast cell growth, we hypothesized that the differential responses of MDA-MB-231 and UMR-106 cells to CB agonists was likely to result from different downstream signaling pathways. Inhibition of NF-κB activity in breast cancer was also shown to induce apoptosis corresponding to the reduced phosphorylated NF-κB level and enhanced apoptosis in MDA-MB-231 cells treated with CB agonists in this study56.

[Visit Link](https://www.nature.com/articles/s41598-022-11116-9){:target="_blank rel="noopener"}

