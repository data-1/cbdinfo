---
layout: post
title: "Green synthesis of AgNPs using Cannabis sativa leaf extract: Characterization, antibacterial, anti-yeast and α-amylase inhibitory activity"
date: 2024-09-16
tags: [Silver nanoparticle,Nanoparticle,Biology]
author: Sonam Chouhan | Sanjay Guleria
institutions: Sher-e-Kashmir University of Agricultural Sciences and Technology of Jammu
doi: "https://www.sciencedirect.com/science/article/pii/S2589299120300264?via%3Dihub"
---


The effect of silver nitrate (AgNO3) concentration, extract to AgNO3 ratio, pH and incubation time was also determined for optimization of green synthesis. The synthesized nanoparticles were characterized through UV–visible spectroscopy, FT-IR, SEM and TEM analysis. Surface plasmon resonance (SPR) peak was observed at 418 nm and AgNPs formed had average size of 26.52 nm. Three gram positive (Staphylococcus aureus, Micrococcus luteus, Bacillus subtilis) and two gram negative (Escherichia coli, Klebsiella pneumoniae) bacteria were used to determine the effect of AgNPs on their growth kinetics and integrity of cell membrane. Graphical abstract  Previous article in issue  Next article in issue  Keywords  Cannabis sativa  Silver nanoparticles  Antibacterial activity  Anti-yeast activity  α-Amylase inhibitory activity  References  Cited by (0)  © 2020 The Authors.