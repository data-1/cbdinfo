---
layout: post
title: "Plant Growth-Promoting Rhizobacteria for Cannabis Production: Yield, Cannabinoid Profile and Disease Resistance"
date: 2024-08-19
tags: [Rhizobacteria,Powdery mildew,Cannabis,Cannabidiol,Pseudomonas,Plant hormone,Cannabis cultivation,Plant,Cannabinoid,Hemp,Botany,Biology,Organisms,Plants]
author: W. George | Donald L
---


However, little research has been conducted regarding the response of yield and cannabinoid levels/composition to the application of plant-growth promoting rhizobacteria (PGPR), although research has already demonstrated the important role of PGPR on the production of many other crop species (Mabood et al., 2014; Smith et al., 2015). Plant-Growth Promoting Rhizobacteria for Cannabis Production  Plant growth-promoting rhizobacteria are microbes associated with plant roots that promote plant growth by (1) providing enhanced mineral nutrition, (2) producing plant hormones or other molecules that stimulate plant growth and prime plant defenses against biotic and abiotic stresses, or (3) protecting plants against pathogens by affecting survival of pathogenic microorganisms (Podile and Kishore, 2006; Ortíz-Castro et al., 2009; Bhattacharyya and Jha, 2012; Nandal and hooda, 2013; Vacheron et al., 2013; Ahemad and Kibret, 2014; Yan et al., 2016; Rosier et al., 2018). We hypothesize that future research will demonstrate that PGPR-based inoculants can alter (1) cannabinoid accumulation, (2) increase flower yield for marijuana cultivars and seed and fiber yield for hemp cultivars, (3) protect against plant pathogens by production of antimicrobial compounds and priming of plant immune responses, and (4) reduce the impact of abiotic stresses associated with intensive indoor marijuana cultivation (e.g., salinity stress) and challenges associated with climate change, for outdoor hemp cultivation (e.g., drought, high temperatures, flooding). Powdery Mildew Control in Indoor Cannabis Cultivation: An Example of Potential Plant-Growth Promoting Rhizobacteria Application  Cannabis can be infected by a plethora of phytopathogens, leading to reduced plant productivity from the seedling to harvest stages (McPartland, 1996; Kusari et al., 2013). Examples of Widely Prevalent Phytomicrobiome Members: Pseudomonas and Bacillus for Growth Promotion and Disease Control in Cannabis  Pseudomonas  In general, Pseudomonas spp.

[Visit Link](https://www.frontiersin.org/journals/microbiology/articles/10.3389/fmicb.2019.01761/full){:target="_blank rel="noopener"}

