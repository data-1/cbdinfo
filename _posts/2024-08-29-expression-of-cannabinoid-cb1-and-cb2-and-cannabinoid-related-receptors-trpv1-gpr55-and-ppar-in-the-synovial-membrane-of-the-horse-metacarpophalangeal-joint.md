---
layout: post
title: "Expression of cannabinoid (CB1 and CB2) and cannabinoid-related receptors (TRPV1, GPR55, and PPARα) in the synovial membrane of the horse metacarpophalangeal joint"
date: 2024-08-29
tags: [Macrophage,TRPV1,Endothelium,Angiogenesis,Cannabinoid,Inflammation,Cannabinoid receptor 2,Real-time polymerase chain reaction,Vasodilation,Endocannabinoid system,Blood vessel,Synovial membrane,Fibroblast-like synoviocyte,Monocyte,Tumor necrosis factor,Inflammatory cytokine,Palmitoylethanolamide,Interleukin 6,Cytokine,Extracellular matrix,Biochemistry,Biology,Cell biology]
author: Zamith Cunha | De Silva
---


Quantitative real-time PCR (RT-PCR) gene expression analysis  To evaluate gene expression profiles, quantitative real-time PCR (qPCR) was carried out in a CFX96 thermal cycler (Bio-Rad Laboratories Inc.) using SYBR green detection (Cat.172-5121, Bio-Rad laboratories Inc) for target genes. However, Cnr1 were not expressed in all the horses. The Expression of CB2R-IR has previously been observed in the vascular endothelial cells of humans and animals (57, 121, 122), including horses (44). Conclusion  The present study was the first study to demonstrate the mRNA presence and the protein cellular distribution of the cannabinoid receptors (CB1 and CB2) and three cannabinoid-related receptors (TRPV1, GPR55, and PPARα) in the horse synovial tissues of the metacarpophalangeal joint of the horse. Cannabinoid receptor 1 was identified in FLS and MLS, although it was not expressed in all the horses.

[Visit Link](https://www.frontiersin.org/journals/veterinary-science/articles/10.3389/fvets.2023.1045030/full){:target="_blank rel="noopener"}

