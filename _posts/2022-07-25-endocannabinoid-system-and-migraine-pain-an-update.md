---
layout: post
title: "Endocannabinoid System and Migraine Pain: An Update"
date: 2022-07-25
categories:
- Endocannabinoid
author: Greco, Laboratory Of Neurophysiology Of Integrative Autonomic Systems, Headache Science Centre, Irccs Mondino Foundation, Rosaria.Greco Mondino.It, Demartini, Zanaboni, Anna M., Department Of Brain, Behavioral Sciences
tags: [Anandamide,Cannabinoid receptor type 1,Endocannabinoid system,2-Arachidonoylglycerol,Neurotransmitter,Fatty acid amide hydrolase,Migraine,Cannabinoid receptor type 2,Cannabinoid receptor,Serotonin,Cannabinoid,Neuron,Nervous system,Physiology,Neurochemistry,Biochemistry,Neuroscience,Neurophysiology]
---


AEA is rapidly degraded by fatty acid amide hydrolase (FAAH) enzyme and its levels can be modulated in the peripheral and central nervous system (CNS) by FAAH inhibitors. Potential effects of endocannabinoids on migraine pain. The CB 1 receptor antagonist, AM251, reversed this inhibitory activity, suggesting that CB 1 receptors may be implicated in the relationship between headache and dural blood vessel dilation and migraine mediators. Cephalalgia 22, 107–111 doi: 10.1046/j.1468-2982.2002.00323.x PubMed Abstract | CrossRef Full Text | Google Scholar  Gouveia-Figueira, S., Goldin, K., Hashemian, S. A., Lindberg, A., Persson, M., Nording, M. L., et al. (2017). Pain 11, 1420–1428.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fnins.2018.00172/full){:target="_blank" rel="noopener"}


