---
layout: post
title: "Identification of Novel Antagonists Targeting Cannabinoid Receptor 2 Using a Multi-Step Virtual Screening Strategy"
date: 2022-09-01
categories:
- Cannabinoid
author: Wang, Hou, Liu, Li, Lin, Mukuo Wang, Shujing Hou, Ye Liu, Dongmei Li, Jianping Lin
tags: [Ligand (biochemistry),Virtual screening,Cannabinoid receptor type 2,Docking (molecular),Cannabinoid receptor type 1,G protein-coupled receptor,Receptor (biochemistry),Ligand binding assay,IC50,Biotechnology,Biochemistry,Cell signaling,Neurochemistry,Chemistry,Cell biology]
---


Introduction  i /G o protein) [ The G-protein-coupled receptors (GPCRs), containing about 800 members, are the largest membrane protein family known in the human body [ 1 ]. Although the expression of the CB1 receptor is low, both receptors exert a wide range of immune functions, such as regulating the release of cytokines [ 7 ]. In 2019, Li et al. [ 14 ] designed a high-affinity, CB2-receptor-specific antagonist AM10257 ( Figure 1 A) through systematic optimization of the CB1 receptor antagonist SR141716A (Rimonabant) [ 15 ], and the crystal structure of CB2 receptor in complex with AM10257 was resolved with a resolution of 2.80 Å. Markt et al. developed pharmacophore models based on CB2 receptor antagonists and identified seven compounds that showed Kvalues of <25 μM [ 19 ]. Chen et al. [ 20 ] constructed a 3D CB2 receptor homology structure model based on the crystal structure of bovine rhodopsin.

<hr>

[Visit Link](https://www.mdpi.com/1420-3049/26/21/6679/htm){:target="_blank" rel="noopener"}


