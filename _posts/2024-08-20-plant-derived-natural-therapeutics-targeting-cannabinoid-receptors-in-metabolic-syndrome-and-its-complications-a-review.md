---
layout: post
title: "Plant-derived natural therapeutics targeting cannabinoid receptors in metabolic syndrome and its complications: A review"
date: 2024-08-20
tags: [Cannabinoid,Endocannabinoid system,Cannabinol,Causes of death,Medical specialties,Diseases and disorders,Clinical medicine,Biochemistry,Neurochemistry]
author: 
---


The endocannabinoid system (ECS) is natural physiological system in the humans. The presence of the ECS system involves different roles in body. The endocannabinoid system involves regulation of most of the centers, which regulates the hunger and leads to changes in the weight. In the present article, we reviewed the role of natural cannabinoid compounds in metabolic disorders and related complications. We studied variety of a plant-derived cannabinoids in treating the metabolic syndrome including stoutness, fatty acid liver diseases, insulin obstruction, dementia, hypertension, lipid abnormalities, non-alcoholic steatohepatitis, endothelial damage, and polycystic ovarian syndrome and so on.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S0753332220310817){:target="_blank rel="noopener"}

