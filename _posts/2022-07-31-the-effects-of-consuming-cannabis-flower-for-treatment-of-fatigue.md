---
layout: post
title: "The Effects of Consuming Cannabis Flower for Treatment of Fatigue"
date: 2022-07-31
categories:
- Cannabis
author: Xiaoxue Li, Jegason P. Diviant, Sarah S. Stith, Franco Brockelman, Keenan Keeling, Https, Branden Hall, Jacob M. Vigil, Author Affiliations, Corresponding Author
tags: [Cannabidiol,Cannabis (drug),Medical cannabis,Vaporizer (inhalation device),Tetrahydrocannabinol,Terpene,Effects of cannabis,Cannabis sativa,Fatigue,Dose (biochemistry),Regression analysis,Medication,Signs and symptoms,Health,Medicine]
---


Usage sessions included real-time subjective changes in fatigue intensity levels prior to and following Cannabis consumption, Cannabis flower characteristics (labeled phenotype, cannabinoid potency levels), combustion method, and any potential experienced side effects. sativa,” or “hybrid”) did not differ in symptom relief, people that used joints to combust the flower reported greater symptom relief than pipe or vaporizer users. The study sample includes sessions using flower products with the fatigue symptom reported and with a starting fatigue intensity level greater than 0. Results  Table 1 presents descriptive statistics for the product characteristics (labeled plant phenotype, combustion method, and tetrahydrocannabinol [THC] and cannabidiol [CBD] potency levels), the starting and ending symptom intensity levels, and the prevalence of side effects. Table 2 shows the results for the effects of product characteristics on fatigue symptom relief.

<hr>[Visit Link](https://www.karger.com/Article/FullText/524057){:target="_blank" rel="noopener"}


