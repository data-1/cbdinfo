---
layout: post
title: "The Cannabinoid Receptor CB1 Interacts with the WAVE1 Complex and Plays a Role in Actin Dynamics and Structural Plasticity in Neurons"
date: 2024-08-12
tags: [Cannabinoid receptor 1,Dendritic spine,Synapse,Neuron,Receptor (biochemistry),Actin,Spinal cord,Synaptic plasticity,Dendrite,Cannabinoid,Growth cone,Cell signaling,Nervous system,G protein-coupled receptor,Endocannabinoid system,Neurotransmitter,Immunoprecipitation,Axon,Cannabinoid receptor,Biochemistry,Cell biology,Biology]
author: Christian Njoo | Nitin Agarwal | Beat Lutz | Rohini Kuner
---


Importantly, we demonstrate that cannabinoids structurally remodel synaptic spines by regulating the activity levels of WAVE1 and report a novel function for WAVE1 in mediating inflammatory pain via structural and functional plasticity of spinal neurons. Cannabinoid Agonists Destabilize F-Actin in Dendritic Spines in Mature Neurons and Specifically Decrease Density of Mature Spines  To address whether cannabinoids also regulate actin dynamics and thereby bring about structural remodeling in adult neurons via the WAVE1 complex, we then studied dendritic spines in wild-type cortical neurons matured over 3 wk in vitro and nucleofected with LifeAct-mCherry, thereby labelling F-actin in dendritic spines. The highly restricted nature of our manipulation of actin assembly in individual spines in FRAP experiments, which was rapidly altered upon cannabinoidergic modulation, coupled with the evidence for CB1 localization in dendritic spines makes it likely that the functional effects evoked by cannabinoids in this study indeed result from CB1 activation in dendritic spines. The endocannabinoid system and the brain. The endocannabinoid system and the brain.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4619884/){:target="_blank rel="noopener"}

