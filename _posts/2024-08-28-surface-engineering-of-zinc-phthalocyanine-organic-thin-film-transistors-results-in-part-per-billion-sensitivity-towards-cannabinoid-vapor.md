---
layout: post
title: "Surface engineering of zinc phthalocyanine organic thin-film transistors results in part-per-billion sensitivity towards cannabinoid vapor"
date: 2024-08-28
tags: [Thin film,Organic field-effect transistor,X-ray crystallography,Cannabidiol,Atomic force microscopy,Chemistry,Condensed matter physics,Physical chemistry,Applied and interdisciplinary physics,Materials,Physical sciences,Materials science]
author: Zachary J | Rosemary R | Halynne R | Cory S | Adam J | Benoît H
---


Thus, in addition to molecular tuning, Pc thin films can be morphologically tuned through film engineering by altering the deposition surface, deposition conditions, or post-deposition annealing to obtain more favorable spectral characteristics or charge transport conditions33,34. As previously established18, analytes, such as THC, when introduced to Pc thin films, in addition to the effects of intermolecular interactions, can also alter physical characteristics of the thin film, such as crystal packing and morphology, which can cause changes in OTFT performance. Increased thin-film crystallinity reduced OTFT sensing responses, with the most crystalline films demonstrating limited changes in charge transport characteristics and negligible changes in XRD intensity. Exposure to THC vapor showed a similar + ΔVT, increase in hysteresis, -ΔN, and a decrease in XRD peak intensity for OTFTs of both thicknesses. Additionally, where the most crystalline α-ZnPc thin-films demonstrated the least OTFT and XRD changes, highly crystalline β-ZnPc films demonstrated significant re-crystallization with exposure to THC vapor.

[Visit Link](https://www.nature.com/articles/s42004-022-00797-y){:target="_blank rel="noopener"}

