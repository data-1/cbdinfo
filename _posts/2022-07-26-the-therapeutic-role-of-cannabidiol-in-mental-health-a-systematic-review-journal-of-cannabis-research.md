---
layout: post
title: "The therapeutic role of Cannabidiol in mental health: a systematic review - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Khan, Dow University Of Health Science, Karachi, Naveed, Psychiatry, Behavioral Sciences, Kansas University Medical Center, Kansas City, Mian, Picacs Clinic
tags: [Cannabidiol,Mental disorder,Attention deficit hyperactivity disorder,Psychosis,Medical cannabis,Schizophrenia,Dose (biochemistry),Nabiximols,Randomized controlled trial,Bipolar disorder,Tetrahydrocannabinol,Mania,Major depressive disorder,Treatment-resistant depression,Evidence-based medicine,Clozapine,Anxiety disorder,Cannabis use disorder,Cannabis sativa,Cognitive behavioral therapy,Hierarchy of evidence,Tourette syndrome,Psychiatry,Post-traumatic stress disorder,Olanzapine,Cannabinoid receptor type 1,Clinical medicine,Health,Medicine,Health care,Diseases and disorders,Health sciences]
---


Irrespective of the study design, three studies reported that CBD alleviated psychotic symptoms and cognitive impairment in patients with chronic cannabis use and Parkinson’s disease (Leweke et al., 2012; Zuardi et al., 1995; Zuardi et al., 2009), while only two RCTs and one clinical trial provided evidence for the effectiveness of CBD among patients with schizophrenia, albeit with mixed results (Leweke et al., 2012; McGuire et al., 2018; Zuardi et al., 2009). Increased anandamide levels and improvements in the symptoms of psychosis were reported in another 4-week-long RCT comparing the efficacy of CBD to amisulpride for the treatment of schizophrenia (Leweke et al., 2012). Cannabis-related disorders  The present review included three RCTs (107 patients), two open-label trials (28 patients), one case series of four patients, and two case reports for cannabis-related disorders as summarized in Table 2 (Solowij et al., 2018; Crippa et al., 2013; Trigo et al., 2016a; Trigo et al., 2018; Trigo et al., 2016b; Allsop et al., 2014; Pokorski et al., 2017; Shannon & Opila-Lehman, 2015). The case series used self-titrated nabiximols at a dose of 77.5–113.4 mg THC and 71.5–105.0 mg CBD (Trigo et al., 2016b). Other disorders  The present review included two RCTs (54 patients), one open-label trial (53 patients), one retrospective chart review (72 patients), and four case reports for CBD and nabiximols use in the treatment of other psychiatric disorders.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-019-0012-y){:target="_blank" rel="noopener"}


