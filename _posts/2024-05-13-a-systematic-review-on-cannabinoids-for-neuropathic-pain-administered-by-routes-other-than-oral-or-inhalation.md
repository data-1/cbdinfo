---
layout: post
title: "A Systematic Review on Cannabinoids for Neuropathic Pain Administered by Routes Other than Oral or Inhalation"
date: 2024-05-13
categories:
- 
author: Quintero, Pulido, Giraldo, Leon, Diaz, Bustos, Jose-Manuel Quintero, German Pulido, Luis-Fernando Giraldo, Marta-Ximena Leon
tags: [Medical cannabis,Cannabinoid,Peripheral neuropathy,Neuropathic pain,Tetrahydrocannabinol,Cannabidiol,Pain,Analgesic,Levonantradol,Postherpetic neuralgia,Medicine,Health,Medical specialties,Clinical medicine,Health care]
---


Therefore, we aimed at evaluating the safety and effectiveness of cannabinoids used by routes other than oral or inhalation for neuropathic pain compared to placebo or other medications in terms of pain relief, quality of life and adverse events. There are recent studies [ 31 ], that suggest the use of new forms and administration vehicles of cannabinoid derivatives for the management of neuropathic pain, suppressing the possible adverse effects of their systemic administration. The characteristics of neuropathic pain are different from those of other types of chronic pain [ 12 ]. The risk of bias of the included study is summarized in Table 3 . In the oral group, phase 1 patients perceived a significant reduction in pain with oral THC on day 1 compared to baseline (= 0.047).

[Visit Link](https://www.mdpi.com/2223-7747/11/10/1357){:target="_blank" rel="noopener"}


