---
layout: post
title: "The Implications of Cannabinoid-Induced Metabolic Dysregulation for Cellular Differentiation and Growth"
date: 2024-09-02
tags: [MTORC1,Unfolded protein response,Mitochondrion,Endoplasmic reticulum,Endocannabinoid system,Cellular differentiation,Cannabinoid,MTOR,Wnt signaling pathway,Adipogenesis,Apoptosis,Cannabinoid receptor 2,Catenin beta-1,Stem cell,DNA damage-inducible transcript 3,Signal transduction,Cannabinoid receptor 1,Calcium in biology,Inositol trisphosphate,Autophagy,Cell potency,Cell signaling,DNM1L,Biology,Biotechnology,Biochemistry,Biological processes,Molecular biology,Cellular processes,Life sciences,Neurochemistry,Cell biology]
author: Tina Podinić | Geoff Werstuck | Sandeep Raha
---


Cell Stem Cell 2007, 1, 35–38. Cell Stem Cell 2012, 11, 589–595. Cell Stem Cell 2013, 12, 62–74. Cell Stem Cell 2016, 19, 232–247. Cell Stem Cell 2019, 25, 639–653.e7.

[Visit Link](https://www.mdpi.com/1422-0067/24/13/11003){:target="_blank rel="noopener"}

