---
layout: post
title: "Chronic Pain and the Endocannabinoid System: Smart Lipids – A Novel Therapeutic Option?"
date: 2022-08-05
categories:
- Cannabinoid
- Endocannabinoid
author: Walter Zieglgänsberger, Rudolf Brenneisen, Achim Berthele, Https, Carsten T. Wotjak, Borwin Bandelow, Thomas R. Tölle, Beat Lutz, Author Affiliations, Corresponding Author
tags: [Cannabidiol,Medical cannabis,Tetrahydrocannabinol,Chemical synapse,Cannabinoid,Cannabinoid receptor,NMDA receptor,TRPV1,Neuron,2-Arachidonoylglycerol,Cannabinoid receptor type 2,Neurotransmitter,Epithelium,Cell signaling,Astrocyte,Dendritic spine,Endocannabinoid system,Synapse,Physiology,Cell biology,Neurochemistry,Neurophysiology,Biochemistry,Neuroscience]
---


There is evidence that also other non-CB1R and non-CB2R (GPR18, GPR119, and GPR55) are involved in the signaling of the ECS [18]. AEA and 2-AG can also activate other receptors, such as TRPV1 and GABA A receptors, respectively [33-35]. ECS signaling is terminated by cellular uptake processes, which likely involve transporter proteins. When THC was either smoked or applied together with CBD (ratios of 2:1, 1:1, and 1:2 of CBD to THC), patients highlight the more favorable therapeutic effects and less side effects compared to taking pure THC orally [98]. CBD inhalation increased THC plasma concentrations and diminished THC-induced analgesic effects, revealing a synergistic pharmacokinetic but antagonistic pharmacodynamic interaction of THC and CBD.

<hr>

[Visit Link](https://www.karger.com/Article/FullText/522432){:target="_blank" rel="noopener"}


