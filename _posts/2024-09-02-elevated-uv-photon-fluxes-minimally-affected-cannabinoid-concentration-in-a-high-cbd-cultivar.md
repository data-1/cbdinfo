---
layout: post
title: "Elevated UV photon fluxes minimally affected cannabinoid concentration in a high-CBD cultivar"
date: 2024-09-02
tags: [Ultraviolet,Photosynthesis,Cannabinoid,Deviation (statistics),Errors and residuals,Linear regression,Canopy (grape),Photobiology]
author: F. Mitchell
---


All studies to date used relatively lower UV photon fluxes, but there is the potential that a higher UV flux would increase cannabinoids. UV was applied at the middle of the photoperiod for 0 (control), 1, 2.5, or 5 h. Weighting factors from Flint and Caldwell (2003) normalized at 300 nm were used to calculate daily biologically effect UV photon flux (UV-PFDBE; 280 to 399 nm). Cannabinoid yield was calculated by multiplying flower yield (g m-2) by cannabinoid concentration. There was no effect of rep on total or flower yield. Canopy photosynthesis at harvest was highest in the control treatment at 16.5 μmol m-2 s-1 and declined to 13.2, 11.5 and 11.8 μmol m-2 s-1 at a daily UV-PFDBE of 0.02, 0.05 and 0.11 mol m-2 d-1, respectively (Figure 3C).

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2023.1220585/full){:target="_blank rel="noopener"}

