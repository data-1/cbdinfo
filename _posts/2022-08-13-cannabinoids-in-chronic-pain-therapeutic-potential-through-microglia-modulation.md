---
layout: post
title: "Cannabinoids in Chronic Pain: Therapeutic Potential Through Microglia Modulation"
date: 2022-08-13
categories:
- Cannabinoid
author: Van Den Hoogen, Nynke J., Department Of Physiology, Pharmacology, University Of Calgary, Department Of Comparative Biology, Experimental Medicine, Hotchkiss Brain Institute, Harding, Erika K.
tags: [Cannabinoid receptor type 2,Endocannabinoid system,Microglia,Cannabinoid receptor,Pain,Chronic pain,Medical cannabis,Cannabinoid,Neuron,Pain management,Inflammation,Neuropathic pain,Neuroscience,Neurochemistry,Physiology,Neurophysiology,Cell biology,Biochemistry,Cell signaling,Signal transduction,Cell communication,Biotechnology]
---


2-AG activates both CB1R and CB2R. Pain 162, S1–S2. Pain 154, 160–174. Activation of dorsal horn cannabinoid CB2R receptor suppresses the expression of P2Y12 and P2Y13 receptors in neuropathic pain rats. Pain 162, S26–S44.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fncir.2021.816747/full){:target="_blank" rel="noopener"}


