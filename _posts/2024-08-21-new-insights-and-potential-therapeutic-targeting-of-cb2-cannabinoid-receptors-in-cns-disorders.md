---
layout: post
title: "New Insights and Potential Therapeutic Targeting of CB2 Cannabinoid Receptors in CNS Disorders"
date: 2024-08-21
tags: [Cannabinoid receptor 2,Neuroinflammation,Endocannabinoid system,Neurodegenerative disease,Experimental autoimmune encephalomyelitis,Microglia,Multiple sclerosis,Cannabinoid receptor 1,Addiction,Autism spectrum,Cannabinoid,Schizophrenia,Seizure,Inflammation,ALS,Anandamide,Huntingtin,Dizocilpine,Eating disorder,Major depressive disorder,Epilepsy,Neuroprotection,Mental disorder,Clinical medicine]
author: Berhanu Geresu Kibret | Hiroki Ishiguro | Yasue Horiuchi | Emmanuel S. Onaivi | Berhanu Geresu | Emmanuel S
---


; Gardner, E.; et al. Functional expression of brain neuronal CB2 cannabinoid receptors are involved in the effects of drugs of abuse and in depression. Progress in brain cannabinoid CB2 receptor research: From genes to behavior. Brain cannabinoid receptor 2: Expression, function and modulation. What we know and do not know about the cannabinoid receptor 2 (CB2). Brain Cannabinoid CB2 Receptor in Schizophrenia.

[Visit Link](https://www.mdpi.com/1422-0067/23/2/975){:target="_blank rel="noopener"}

