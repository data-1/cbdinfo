---
layout: post
title: "Response of essential oil hemp (Cannabis sativa L.) growth, biomass, and cannabinoid profiles to varying fertigation rates"
date: 2024-08-20
tags: [Cannabis,Cannabidiol,Cannabinoid,Hemp,Fertilizer,Tetrahydrocannabinolic acid,Tetrahydrocannabinol,Plant,Botany]
author: Roger Kjelgren | Zachary Brym | Steven L. Anderson II | Brian Pearson
---


Optimal fertilizer rates were observed at 50 ppm N, while increased fertilizer rates significantly reduced plant growth, biomass accumulation, and cannabinoid concentrations. Five essential oil hemp cultivars were subjected to six fertigation rates throughout a complete growth cycle (vegetative and flowering) to empirically test the genetic and abiotic response of essential oil hemp to fertilizer rates. Three of the most important findings were: (i) SPAD-502 measurements were highly correlated to leaf chlorophyll content, indicating that SPAD measurements can be utilized as a rapid, non-invasive tool to access nutrient deficiency in essential oil hemp, (ii) increased fertilizer rates and irrigation salinity at maintained rates significantly reduced plant growth, biomass, and cannabinoid profiles, and, (iii) maintaining constant low rates of fertilizer available in the growing media maximized cannabinoid concentrations. The gene controlling marijuana psychoactivity molecular cloning and heterologous expression of Δ1-tetrahydrocannabinolic acid synthase from Cannabis sativa L. Journal of Biological Chemistry. Development of Cannabinoids in Flowers of Industrial Hemp (Cannabis sativa L.)—a Pilot Study.

[Visit Link](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0252985){:target="_blank rel="noopener"}

