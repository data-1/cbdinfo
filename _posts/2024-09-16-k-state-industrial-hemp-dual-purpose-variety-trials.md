---
layout: post
title: "K-State Industrial Hemp Dual-Purpose Variety Trials"
date: 2024-09-16
tags: [Hemp,Agriculture,Primary sector of the economy,Crops,Plants and humans,Botany,Plants]
author: Jason Griffin | Kraig L. Roozeboom | L. Haag | Michael J. Shelton | Clint Wilson | Tami Myers
institutions: Arkansas Agricultural Experiment Station | Kansas State University
doi: "https://eupdate.agronomy.ksu.edu/article_new/k-state-industrial-hemp-dual-purpose-variety-trials-550-6"
---


Industrial hemp is marketed for oil, grain, and fiber (Figure 1). Values in bold are not significantly different from the maximum value within a column. Manhattan Trials  Yield results from only three of four years are also available for tests in Manhattan (Table 2). Varieties in the top yield group for either stover or grain, but with only one year of testing included CFX-2, CRS-1, Enecterol, Futura, Rigel, and Orion 33. Values in bold are not significantly different from the maximum value within a column.
