---
layout: post
title: "Is cannabidiol hepatotoxic or hepatoprotective: A review"
date: 2024-09-16
tags: [Cannabidiol,Dose (biochemistry),Hepatotoxicity,Valproate,Paracetamol,Medical specialties,Clinical medicine]
author: Sidney J. Stohs | SD Ray
institutions: Touro College
doi: "https://doi.org/10.1177/2397847320922944"
---


These results indicated that the hepatic effects of this extract in rats were mild and reversible at doses as high as 180 mg of CBD (720 mg of the extract)/kg for 90 days. The mice had been given CBD at a dose of 5 or 10 mg/kg i.p. Discussion  Concerns have been raised regarding the potential hepatotoxicity of CBD.9–13 In conjunction with assessing the efficacy of CBD in various neurological diseases, the potential hepatic effects have been determined in human subjects who received CBD in daily doses of 10–29 mg/kg.1–5 In addition to the high doses of CBD, essentially all patients in these studies were taking at least one antiepileptic medication, with the most common being valproic acid, a drug well known to be associated with hepatotoxicity.21,22  Many variables are involved, which impact the assessment and comparison of human and animal studies that have been conducted relative to hepatotoxicity versus hepatoprotection of CBD. in all of the above hepatoprotective studies.15–20 An effective CBD dose of 5 mg/kg/day reversed the adverse effects of bile duct ligation15,16 and thioacetamide-induced liver failure.17 A CBD dose of 30 mg/kg was most effective against the effects of cocaine,18 d-amphetamine, and ketamine,19 while a 5 and 10 mg/kg dose response effect of CBD was observed against alcohol-induced hepatic steatosis, metabolic dysregulation, inflammation, and neutrophil-mediated injury in mice.20  As compared to the i.p. administration are within the range of the oral dose used in the APAP study.12  It is important to know whether CBD causes hepatotoxicity at therapeutic doses.