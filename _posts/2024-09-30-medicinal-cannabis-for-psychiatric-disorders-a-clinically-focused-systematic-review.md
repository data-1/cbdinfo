---
layout: post
title: "Medicinal cannabis for psychiatric disorders: a clinically-focused systematic review"
date: 2024-09-30
tags: [Cannabis (drug),Medical cannabis,Cannabidiol,Attention deficit hyperactivity disorder,Post-traumatic stress disorder,Mental disorder,Cannabinoid,Tetrahydrocannabinol,Schizophrenia,Nabiximols,Dose (biochemistry),Psychosis,Major depressive disorder,Mood disorder,Dronabinol,Insomnia,Antipsychotic,Clinical medicine,Medicine,Health]
author: Jerome Sarris | Justin Sinclair | Diana Karamacoska | Margaret Davidson | Joseph Firth
pubdate: "2020-01-16"
institutions: Western Sydney University
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/6966847"
---


Cannabinoids for Medical Use. Medicinal cannabis in Australia, 2016: the Cannabis as Medicine Survey (CAMS-16) Med J Aust. Cannabidiol as a potential treatment for anxiety disorders. Cannabinoids for Medical Use. Cannabidiol as a potential treatment for anxiety disorders.