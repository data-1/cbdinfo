---
layout: post
title: "Cannabinoid levels description in a cohort of patients with chronic and neuropathic pain treated with Cannabis decoction: A possible role of TDM"
date: 2024-09-02
tags: [Medical cannabis,Cannabidiol,Tetrahydrocannabinol,Individual psychoactive drugs,Entheogens,Pharmacy,Clinical medicine,Health,Cannabaceae,Pharmaceutical sciences,Medicinal chemistry,Drugs,Pharmacology,Medical treatments,Chemicals in medicine,Cannabis,Psychoactive drugs,Medicine,Health care,Therapy,Medical research]
author: 
---


The phytocomplex of Cannabis is made up of approximately 500 substances: terpeno-phenols metabolites, including Δ-9-tetrahydrocannabinol and cannabidiol, exhibit pharmacological activity. In the literature, a few data are available concerning cannabis pharmacokinetics, efficacy and safety. Thus, aim of the present study was the evaluation of cannabinoid pharmacokinetics in a cohort of patients, with chronic and neuropathic pain, treated with inhaled medical cannabis and decoction, as a galenic preparation. In this study, 67 patients were enrolled. Statistically significant differences were found in cannabinoids plasma exposure between inhaled and oral administration of medical cannabis, between male and female and cigarette smokers.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S0753332224005705){:target="_blank rel="noopener"}

