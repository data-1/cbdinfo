---
layout: post
title: "The in vitro effect of delta-9-tetrahydrocannabinol and cannabidiol on whole blood viscosity, elasticity and membrane integrity - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
- THC
author: James, Tameika R., Department Of Basic Medical Sciences, Physiology Section, The University Of The West Indies, Mona Campus, Kingston, Richards, Andrea A., Lowe
tags: [Cannabidiol,Cannabinoid,Cannabis (drug),Erythrocyte deformability,Tetrahydrocannabinol,Cannabis sativa,Red blood cell,Hemorheology,Effects of cannabis,Circulatory system,Dose (biochemistry),Viscosity,Reactive oxygen species,Clinical medicine,Medical specialties]
---


The results indicate that there is a significant effect of increasing concentrations of THC and CBD on blood viscosity. Increased production of ROS can have a profound impact on the integrity of the red blood cell membrane with the possibility of haemoglobin degradation, decreased red cell deformability and haemolysis, all of which have been implicated in serious pathological effects on the cardiovascular system. Based on the results obtained in this study, the observed increased incidence of VOC from that study was likely due to consumption of cannabis which could have led to increased viscosity, elasticity and impaired membrane integrity and therefore decreased erythrocyte deformability. The increased viscosity and elasticity observed in this study indicate that as concentrations of THC and CBD in the blood increases, more energy is required for deformation and disaggregation of red blood cells. In conjunction with the increased viscosity and elasticity observed, there were increased morphological changes in the membrane of red blood cells with increasing concentrations of THC and CBD both separately and in combination.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00126-z){:target="_blank" rel="noopener"}


