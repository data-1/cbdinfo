---
layout: post
title: "Oral Tetrahydrocannabinol (THC):Cannabinoid (CBD) Cannabis Extract Adjuvant for Reducing Chemotherapy-Induced Nausea and Vomiting (CINV): A Randomized, Double-Blinded, Placebo-Controlled, Crossover Tr"
date: 2024-09-02
tags: [Chemotherapy-induced nausea and vomiting,Medical cannabis,Cannabidiol,Cannabinoid,Cannabis,Medicine,Clinical medicine,Health,Drugs,Medical specialties]
author: Apichaya Sukpiriyagul | Ratiporn Chartchaiyarerk | Paluekpon Tabtipwon | Buppa Smanchat | Sinart Prommas | Kornkarn Bhamarapravatana | Komsun Suwannarurk
---


Conclusion  The cannabinoid extract (THC:CBD) was an appropriate adjuvant agent to reduce CINV in patients with gynecologic cancer who received high-emetogenic chemotherapy. All participants received the CINV prophylaxis according to National Comprehensive Cancer Network Antiemesis Version 1.2022.4  The record form comprised a self-administered questionnaire including nausea score, side effects, and drug compliance. Subjects who received THC:CBD reported a lower nausea score than those who received only standard antiemetic agents during the first two days. During days 3 and 4, subjects who received THC:CBD or placebo reported a similar nausea score. The total THC:CBD doses of Duran’s work was higher than the present study.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10440684/){:target="_blank rel="noopener"}

