---
layout: post
title: "Virtual Screening of C. Sativa Constituents for the Identification of Selective Ligands for Cannabinoid Receptor 2"
date: 2024-08-20
tags: [Quantitative structure–activity relationship,Cannabinoid,Cannabinoid receptor 2,K-nearest neighbors algorithm,Ligand (biochemistry),Drug design,Training, validation, and test data sets,Biochemistry]
author: Mikołaj Mizera | Dorota Latek | Judyta Cielecka-Piontek | Cielecka-Piontek
---


These models showed a predictive performance of Q2 = 0.62 and Q2 = 0.72 for the CB1 and CB2 QSAR models, respectively. Our study aimed to predict CB2-selectivity for molecules identified in Cannabis Sativa by using a validated QSAR model. In this study, we identified potential selective cannabinoids among the constituents of Cannabis sativa. [Google Scholar] [CrossRef]  Marzo, V.D. [Google Scholar] [CrossRef]  Chen, J.-Z.

[Visit Link](https://www.mdpi.com/1422-0067/21/15/5308){:target="_blank rel="noopener"}

