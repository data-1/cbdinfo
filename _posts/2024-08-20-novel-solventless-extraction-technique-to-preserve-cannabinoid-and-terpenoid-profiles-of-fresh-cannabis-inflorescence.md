---
layout: post
title: "Novel Solventless Extraction Technique to Preserve Cannabinoid and Terpenoid Profiles of Fresh Cannabis Inflorescence"
date: 2024-08-20
tags: [Cannabinoid,Hashish,Gas chromatography–mass spectrometry,Cannabis,Cannabis (drug),Gas chromatography,Fragrance extraction,High-performance liquid chromatography]
author: Ethan B. Russo | Jeremy Plumb | Venetia L. Whiteley | Ethan B | Venetia L
---


Analysis was performed on each of six conditions for four cannabis chemovars: fresh flower, frozen-sifted flower by-product, Kryo-Kief™ dry ice process, dried flower, dried sifted flower and dried kief. Analysis of Treatment of Ursa Major Type I Chemovar  This sample was also treated with dry ice vapor for an extended 48 h and 20 min of pollinator extraction. This was undertaken at Lightscale Labs, Portland, OR, USA, https://lightscale.com/, accessed on 22 August 2021. Analytical results were tabulated to allow comparison of each of the six different conditions: fresh flower, frozen-sifted flower by-product, Kryo-Kief™ (dry ice kief), dried flower, dried sifted flower and dried kief (vide supra, Results). Novel Solventless Extraction Technique to Preserve Cannabinoid and Terpenoid Profiles of Fresh Cannabis Inflorescence Molecules 26, no.

[Visit Link](https://www.mdpi.com/1420-3049/26/18/5496){:target="_blank rel="noopener"}

