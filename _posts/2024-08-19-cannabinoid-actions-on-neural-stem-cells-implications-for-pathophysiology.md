---
layout: post
title: "Cannabinoid Actions on Neural Stem Cells: Implications for Pathophysiology"
date: 2024-08-19
tags: [Subventricular zone,Subgranular zone,Neurogenesis,Neuroblast,Oligodendrocyte progenitor cell,Neurulation,Cannabinoid receptor 1,Dentate gyrus,2-Arachidonoylglycerol,Neuroepithelial cell,Cannabinoid receptor 2,Cerebral cortex,Endocannabinoid system,Chemical synapse,Development of the nervous system,Multiple sclerosis,Adult neurogenesis,Cannabinoid,Rostral migratory stream,Epilepsy,Neurotrophin,Hippocampus,Cell biology,Neuroscience,Branches of neuroscience]
author: Rui S. Rodrigues | Diogo M. Lourenço | Sara L. Paulo | Joana M. Mateus | Miguel F. Ferreira | Francisco M. Mouro | João B. Moreira | Filipa F. Ribeiro | Ana M. Sebastião | Sara Xapelli
---


One of these signals has been shown to come through the action of endocannabinoids (eCBs), mainly via activation of cannabinoid receptors type 1 and 2 (CB1R and CB2R). Regulators of Neurogenesis  Several signals are key factors in the regulation of NSC proliferation, differentiation, migration and survival. Cannabinoid Regulation of Neurogenesis  4.1. In fact, alterations in the mechanisms behind NSC regulation in the embryonic and adult brain have been reported in both patients and animal models of AD, Parkinson’s disease (PD), MS, epilepsy and mood disorders [247,248,249,250,251]. Cannabinoids and Brain Disorders  5.2.1.

[Visit Link](https://www.mdpi.com/1420-3049/24/7/1350){:target="_blank rel="noopener"}

