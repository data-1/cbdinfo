---
layout: post
title: "Modulation of Astrocyte Activity by Cannabidiol, a Nonpsychoactive Cannabinoid"
date: 2024-08-18
tags: [Gliosis,Astrogliosis,Astrocyte,Neuroinflammation,Cannabinoid receptor 2,Cell biology,Biology,Medical specialties]
author: Ewa Kozela | Ana Juknat | Zvi Vogel
---


Neuronal CB1 activation was shown to exert neuroprotective effects, while CB2 activation on T cells has been described to have a direct immunosuppressive effect [68]. [Google Scholar] [CrossRef] [PubMed]  Rao, V.L.R. [Google Scholar] [CrossRef] [PubMed]  Guimarães, F.S. [Google Scholar] [CrossRef] [PubMed]  Gomes, F.V. ; et al. Neurotoxic reactive astrocytes are induced by activated microglia.

[Visit Link](https://www.mdpi.com/1422-0067/18/8/1669){:target="_blank rel="noopener"}

