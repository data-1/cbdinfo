---
layout: post
title: "N-Methyl-D-aspartate (NMDA) and cannabinoid CB2 receptors form functional complexes in cells of the central nervous system: insights into the therapeutic potential of neuronal and microglial NMDA rece"
date: 2024-05-13
categories:
- 
author: Rivas-Santisteban, Centro De Investigación Biomédica En Red Enfermedades Neurodegenerativas, Ciberned, National Institute Of Health Carlos Iii, Madrid, Departament De Bioquímica I Biomedicina Molecular, Universitat De Barcelona, Barcelona, Lillo, Institut De Neurociències
tags: [Cannabinoid receptor 2,NMDA receptor,Ligand-gated ion channel,Microglia,Receptor antagonist,Cannabinoid receptor,Glutamate receptor,Signal transduction,Cell communication,Cell signaling,Biology,Neurochemistry,Cell biology,Biochemistry,Neurophysiology]
---


Bioluminescence resonance energy transfer (BRET) assay  For BRET assay, HEK-293T cells were transiently cotransfected with a constant amount of cDNA encoding for GluN1-RLuc (0.25 μg) and GluN2B (0.15 μg) and with increasing amounts of cDNA corresponding to CB 2 R-YFP (0.25 to 1.25 μg). Cells growing in a medium containing 50 μM zardaverine were distributed in 384-well microplates (2000 HEK-293T cells or 4000 hippocampal neurons or microglial cells per well) followed by the stimulation with the NMDA and/or CB 2 R agonists (NMDA (15 μM) and/or JWH-133 (100 nM)) for 15 min before adding 0.5 μM forskolin or vehicle for an additional 15 min period. MAP kinase pathway activation is measured by ERK1/2 phosphorylation  Hippocampal neurons, microglial cells, or HEK-293T cells cotransfected with the cDNA for the protomers of the NMDA receptor, GluN1 (1 μg) and GluN2B (0.75 μg), and/or with the cDNA for CB 2 R (1 μg) were plated in transparent Deltalab 96-well microplates. Primary microglial cells were activated by incubating cells with 1 μM LPS and 200 U/mL IFN-γ during 48 h. Two hours before the experiment, the medium was substituted by serum-starved DMEM medium. Cells were then washed twice with cold PBS before the addition of lysis buffer (15 min treatment).

[Visit Link](https://alzres.biomedcentral.com/articles/10.1186/s13195-021-00920-6#Sec11){:target="_blank" rel="noopener"}


