---
layout: post
title: "Inhibition of Nociception in a Preclinical Episodic Migraine Model by Dietary Supplementation of Grape Seed Extract Involves Activation of Endocannabinoid Receptors"
date: 2022-08-27
categories:
- Endocannabinoid
author: Woodman, Sara E., Department Of Biology, Missouri State University, Jordan Valley Innovation Center-Center For Biomedical, Life Sciences, United States, Antonopoulos, Sophia R., Durham
tags: [Calcitonin gene-related peptide,Endocannabinoid system,Allodynia,Cannabinoid receptor type 2,Cannabinoid receptor type 1,Migraine,Cannabinoid,Neuroscience]
---


These results demonstrate that GSE supplementation inhibits trigeminal pain signaling in an injury-free model of migraine-like pain via activation of endocannabinoid receptors and repression of CGRP expression centrally. Results  Effects of Restraint Stress, Pungent Odor, and GSE Administration on Nocifensive Responses  Initially, the level of trigeminal nociception in response to mechanical stimulation was determined in the orofacial region with von Frey filaments in male and female animals (Figure 2). Similar to the results in the orofacial region (Figure 2), a significant increase in the average number of nocifensive withdrawals was observed in the sensitized animals 2 h post odor exposure when compared to naïve levels (P = 0.032). To investigate changes in CGRP expression in the STN, immunohistochemical analysis was performed on tissues obtained from female naïve animals and animals subjected to restraint stress and odor exposure and animals that received daily supplementation of GSE (Figure 5). The ability of the enriched polyphenolic GSE to prevent latent sensitization induced by repetitive stress and repress trigeminal pain signaling is in agreement with the significant cognitive benefits and neuroprotective potential of polyphenols (70).

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fpain.2022.809352/full){:target="_blank" rel="noopener"}


