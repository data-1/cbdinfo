---
layout: post
title: "A Crosstalk between the Cannabinoid Receptors and Nociceptin Receptors in Colitis—Clinical Implications"
date: 2024-08-27
tags: [Cannabinoid receptor 2,Cannabinoid receptor 1,Inflammatory bowel disease,NF-κB,Nociceptin,Cell signaling,Endocannabinoid system,Signal transduction,Cannabinoid,Receptor (biochemistry),Opioid receptor,Cell biology,Biochemistry,Neurochemistry]
author: Maria Wołyniak | Ewa Małecka-Wojciesko | Marta Zielińska | Adam Fabisiak | Małecka-Wojciesko
---


Similar to CB receptors and other opioid receptors, NOP receptors were associated with the group of G protein-coupled receptors (GPCRs), which regulate adenylate cyclase activity. and NOP receptor antagonist JTC-801 (1 mg/kg, i.p.) Novel cannabinoid receptors. Receptors and Channels Targeted by Synthetic Cannabinoid Receptor Agonists and Antagonists. [Google Scholar] [CrossRef] [PubMed]  Sobczak, M.; Sałaga, M.; Storr, M.; Fichna, J. Nociceptin / Orphanin FQ (NOP) Receptors as Novel Potential Target in the Treatment of Gastrointestinal Diseases.

[Visit Link](https://www.mdpi.com/2077-0383/11/22/6675){:target="_blank rel="noopener"}

