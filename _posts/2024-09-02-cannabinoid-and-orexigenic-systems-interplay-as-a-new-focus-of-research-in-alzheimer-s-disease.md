---
layout: post
title: "Cannabinoid and Orexigenic Systems Interplay as a New Focus of Research in Alzheimer’s Disease"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Orexin,Cannabinoid receptor 1,Endocannabinoid system,Anandamide,G protein-coupled receptor,Chemical synapse,Cannabinoid,Synapse,Cell signaling,Amyloid beta,Neurotransmitter,Cell surface receptor,Biology,Signal transduction,Basic neuroscience research,Neurochemistry,Biochemistry,Neurophysiology,Cell biology]
author: Joan Biel Rebassa | Toni Capó | Jaume Lillo | Iu Raïch | Irene Reyes-Resina | Gemma Navarro | Joan Biel | Reyes-Resina
---


In parallel, it has been demonstrated that the internalization of the cannabinoid CB1 receptor can be induced by the peptide orexin A when the orexin OX1 receptor is co-expressed along with the cannabinoid CB1 receptor. The potential role of GPCR heteromers in Alzheimer’s disease has been described [92,93,94]. To demonstrate the physiological role of the heteromer formed between cannabinoid and orexin receptors and validate the idea that orexin receptors regulate cannabinoid receptors within these complexes, it would be interesting to see how this regulation is lost in an orexin receptor KO animal. [Google Scholar] [CrossRef] [PubMed]  Liguori, C. Orexin and Alzheimer’s Disease. [Google Scholar] [CrossRef] [PubMed]  Aso, E.; Ferrer, I. CB2 Cannabinoid Receptor As Potential Target against Alzheimer’s Disease.

[Visit Link](https://www.mdpi.com/1422-0067/25/10/5378){:target="_blank rel="noopener"}

