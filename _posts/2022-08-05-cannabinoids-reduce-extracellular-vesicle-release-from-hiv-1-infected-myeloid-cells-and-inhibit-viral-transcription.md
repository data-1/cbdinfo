---
layout: post
title: "Cannabinoids Reduce Extracellular Vesicle Release from HIV-1 Infected Myeloid Cells and Inhibit Viral Transcription"
date: 2022-08-05
categories:
- Cannabinoid
author: Demarino, Cowen, Khatkar, Cotto, Branscome, Kim, Sharif, Sarah Al, Agbottah, Emmanuel T.
tags: [HIV,ESCRT,Autophagy,RNA polymerase II,Virus,HIV-associated neurocognitive disorder,Inflammation,Bafilomycin,Cannabidiol,Tandem mass spectrometry,Nef (protein),Neuroinflammation,Mass spectrometry,Immunoprecipitation,Env (gene),Endosome,Cannabis (drug),Exosome (vesicle),Chromatin immunoprecipitation,Transcription (biology),Cell biology,Life sciences,Biochemistry,Biotechnology,Biology,Medical specialties,Molecular biology]
---


The following supporting information can be downloaded at: https://www.mdpi.com/article/10.3390/cells11040723/s1 , Figure S1: Cell viability analysis of CBD on HIV-1 infected monocytes; Figure S2: Normalized densitometry analysis of CBD lowering of EV-associated viral cargo from HIV-1 infected monocytes; Figure S3: CBD lowers EV-associated viral RNAs from HIV-1 infected primary macrophages treated with cART; Figure S4: CBD lowers EV-associated viral RNAs from HIV-1 infected primary T-cells; Figure S5: THC lower EVs released from HIV-1 infected monocytes; Figure S6: THC decrease the amount of viral cargo carried inside EVs released from infected monocytes; Figure S7: CBD lowers EV-associated viral proteins from HIV-1 infected individuals; Figure S8: Normalized densitometry analysis of CBD lowering intracellular viral cargo in HIV-1 infected monocytes; Figure S9: Normalized densitometry analysis of CBD treatment of HIV-1 infected monocytes over time and autophagy; Figure S10: Cell viability analysis of autophagy drugs on HIV-1 infected monocytes and secreted EVs; Figure S11: Normalized densitometry analysis of CBD and autophagy drug treatment on HIV-1 infected monocytes; Figure S12: Normalized densitometry analysis of cART on HIV-1 infected 3D neurospheres; Figure S13: Expression of glial receptors in 3D neurospheres; Figure S14: Mass spectrometry analysis of biotinylated CBD pull down from HIV-1 infected lysates. RNA was isolated from the samples, followed by RT-qPCR analysis for HIV-1 viral transcripts, TAR and (C) env. RNA was isolated from the samples, followed by RT-qPCR analysis for HIV-1 viral transcripts, TAR and (C) env. Cannabinoids, CBD can inhibit the abundance of EVs secreted from HIV-1 infected myeloid cells, as well as the viral RNA and protein cargo incorporated inside the EVs potentially through inhibition of intracellular transcription processes and restoration of the autophagy pathway. Cannabinoids, CBD can inhibit the abundance of EVs secreted from HIV-1 infected myeloid cells, as well as the viral RNA and protein cargo incorporated inside the EVs potentially through inhibition of intracellular transcription processes and restoration of the autophagy pathway.

<hr>

[Visit Link](https://www.mdpi.com/2073-4409/11/4/723/htm){:target="_blank" rel="noopener"}


