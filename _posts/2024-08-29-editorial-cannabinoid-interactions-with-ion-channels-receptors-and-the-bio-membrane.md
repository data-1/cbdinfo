---
layout: post
title: "Editorial: Cannabinoid interactions with ion channels, receptors, and the bio-membrane"
date: 2024-08-29
tags: [Cannabinoid,Cannabidiol,Cannabinoid receptor,Cannabinoid receptor 1,Neurochemistry,Biochemistry,Neurophysiology,Drugs,Pharmacology,Psychoactive drugs,Drugs acting on the nervous system]
author: Mohammad-Reza | Jonathon C | Peter C
---


Thus, its mechanism of efficacy, especially against seizure-related disorders, is mainly attributed to targets independent of CB receptors. Articles published in Research Topic  A total of five papers appeared in our Research Topic, including both literature reviews and original research articles. The first paper published this Research Topic by Oz et al. provides a literature review on the effects of cannabinoids on ligand-gated ion channels. In the second paper, Schmiedhofer et al. contribute a systematic review on the interactions of cannabinoids with Cys-loop receptors. The third paper by Ghovanloo et al. is a mini-review on the interactions of two important non-psychoactive compounds, cannabigerol (CBG) and CBD, with voltage-gated sodium (Nav) channels.

[Visit Link](https://www.frontiersin.org/journals/physiology/articles/10.3389/fphys.2023.1211230/full){:target="_blank rel="noopener"}

