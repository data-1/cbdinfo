---
layout: post
title: "CBD and THC in Special Populations: Pharmacokinetics and Drug–Drug Interactions"
date: 2024-05-24
categories:
- 
author: Lixuan Qian, Jessica L. Beers, Klarissa D. Jackson, Zhu Zhou, Jessica L, Klarissa D
tags: [Cannabidiol,Website,Tetrahydrocannabinol,HTTP cookie,Drug interaction,Clinical medicine,Pharmacology,Medical specialties]
---


This review summarizes the published studies on CBD and THC–drug interactions, outlines the mechanisms involved, discusses the physiological considerations in pharmacokinetics (PK) and DDI studies in special populations (including pregnant and lactating women, pediatrics, older adults, patients with hepatic or renal impairments, and others), and presents modeling approaches that can describe the DDIs associated with CBD and THC in special populations. CBD/THC Studies in Special Populations  In this section, studies on the effects of CBD and THC in special populations, including pregnant and lactating women, pediatrics, older adults, patients with hepatic or renal impairment, cancer patients, patients with neurological disorders, and others, are reviewed. Additionally, the t1/2 of CBD and THC increased with the dose. Cannabis Cannabinoids 2022, 5, 199–206. [Google Scholar] [CrossRef]  U.S. Food and Drug Administration.

[Visit Link](https://www.mdpi.com/1999-4923/16/4/484){:target="_blank" rel="noopener"}


