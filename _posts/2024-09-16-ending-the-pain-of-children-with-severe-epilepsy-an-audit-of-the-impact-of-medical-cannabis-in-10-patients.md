---
layout: post
title: "Ending the pain of children with severe epilepsy? An audit of the impact of medical cannabis in 10 patients"
date: 2024-09-16
tags: [Cannabidiol,Medical cannabis,Cannabinoid,Epilepsy,Tetrahydrocannabinol,Seizure,Health care,Clinical medicine,Health,Medicine]
author: RR Zafar | AK Schlag | DJ Nutt
institutions: Imperial College London
doi: "https://doi.org/10.1177/2050324520974487"
---


Another study employing the same dosing regimen in 46 patients with various epilepsy etiologies found 56% of patients to have a ≤50% reduction in mean monthly seizure frequency (Hausman-Kedem et al., 2018). With an estimated 600,000 epilepsy sufferers in the UK that is an estimated 72,000 individuals who are currently using a CBMP in the UK, of which only 20 have been prescribed by the NHS since November 2018 (Conservative Drug Policy Reform Group, 2020; Joint Epilepsy Council of the UK and Ireland, 2011). According to the recent survey by the Centre for Medical Cannabis (Centre for Medical Cannabis, 2020), it was found that average yearly black market purchased medical cannabis equated to £3732·00 contrasted with an average annual cost of £21,794·40 for the private prescriptions for participants within our study. Conservative Drug Policy Reform Group (2020) UK Review of Medicinal Cannabis. Health and Social Care Committee (2019) Drugs Policy: Medicinal Cannabis.