---
layout: post
title: "Impact of Cannabinoid Compounds on Skin Cancer"
date: 2024-08-21
tags: [Cannabinoid receptor 2,Cannabinoid,Anandamide,Cannabinoid receptor 1,2-Arachidonoylglycerol,Programmed cell death protein 1,Melanoma,Apoptosis,Metastasis,Endocannabinoid system,Kaposi's sarcoma-associated herpesvirus,Angiogenesis,Cancer,Alcohol and cancer,Skin cancer,Clinical medicine,Medical specialties,Cell biology,Biochemistry,Biology]
author: Robert Ramer | Franziska Wendt | Felix Wittig | Mirijam Schäfer | Lars Boeckmann | Steffen Emmert | Burkhard Hinz
---


Other efforts to study the effect of cannabinoids and the endocannabinoid system on cancer progression have included analyses of cannabinoid receptor expression levels in cancer tissue and their association with malignancy and survival in patients with squamous cell carcinoma. Inhibition of Tumour Growth of Melanomas and Squamous Cell Carcinomas by Cannabinoids  3.1. It was later shown that cannabinoids in squamous cell carcinomas lead to the death of the cancer cells not only through their effect on the cannabinoid receptors, but also through other mechanisms of action. Clinical Trials on Cannabinoid Use in Cancer Patients  The data currently available on the systemic effects of cannabinoids on carcinogenesis in cancer patients, and thus their benefits, are very limited. However, the systemic effects on skin cancer were not investigated in this study.

[Visit Link](https://www.mdpi.com/2072-6694/14/7/1769){:target="_blank rel="noopener"}

