---
layout: post
title: "The Effect of Harvest Date on Temporal Cannabinoid and Biomass Production in the Floral Hemp (Cannabis sativa L.) Cultivars BaOx and Cherry Wine"
date: 2024-08-27
tags: [Cannabidiol,Cannabis,Tetrahydrocannabinolic acid,Akaike information criterion,Regression analysis,Cannabinoid,High-performance liquid chromatography,Cannabinol,Tetrahydrocannabinol,Δ-8-Tetrahydrocannabinol]
author: Eric R. Linder | Sierra Young | Xu Li | Shannon Henriquez Inoa | David H Suchoff | Eric R | Henriquez Inoa | David H
---


Linear regression for the effect of cultivar and total potential THC (A) and total THC (B) on total CBD. Plants 2022, 11, 140. The Effect of Harvest Date on Temporal Cannabinoid and Biomass Production in the Floral Hemp (Cannabis sativa L.) Cultivars BaOx and Cherry Wine. The Effect of Harvest Date on Temporal Cannabinoid and Biomass Production in the Floral Hemp (Cannabis sativa L.) Cultivars BaOx and Cherry Wine. The Effect of Harvest Date on Temporal Cannabinoid and Biomass Production in the Floral Hemp (Cannabis sativa L.) Cultivars BaOx and Cherry Wine Horticulturae 8, no.

[Visit Link](https://www.mdpi.com/2311-7524/8/10/959){:target="_blank rel="noopener"}

