---
layout: post
title: "Positive Effect of Cannabis sativa L. Herb Extracts on Skin Cells and Assessment of Cannabinoid-Based Hydrogels Properties"
date: 2024-08-20
tags: [Cannabinoid,Cytotoxicity,Cannabinol,Cannabis,Resazurin,Superoxide dismutase,Antioxidant,Cannabidiol]
author: Martyna Zagórska-Dziok | Tomasz Bujak | Aleksandra Ziemlewska | Zofia Nizioł-Łukaszewska | Zagórska-Dziok | Nizioł-Łukaszewska
---


Our results obtained with the Neutral Red test have found that Cannabis sativa L. extract at all tested concentrations (1–1000 μg/mL) showed no cytotoxic effect on keratinocytes and fibroblasts, and thus does not affect the integrity of cell membranes. Analysis of results indicated that the influence of the hemp extraction method and concentration of the extract on hydrogels properties was not significant. Assessment of Antioxidant Activity  3.3.1. The analysis confirmed the ultrasonic extraction resulted in a higher concentration of cannabinoids, phenolic compounds, flavonoids and chlorophyll in extracts of Cannabis sativa L. In addition to the previously known antioxidant properties of the tested extracts, which can have a positive effect on the structure and condition of skin cells, this work also shows other benefits of hemp extracts. Positive Effect of Cannabis sativa L. Herb Extracts on Skin Cells and Assessment of Cannabinoid-Based Hydrogels Properties Molecules 26, no.

[Visit Link](https://www.mdpi.com/1420-3049/26/4/802){:target="_blank rel="noopener"}

