---
layout: post
title: "Systematic combinations of major cannabinoid and terpene contents in Cannabis flower and patient outcomes: a proof-of-concept assessment of the Vigil Index of Cannabis Chemovars"
date: 2024-08-29
tags: [Cannabidiol,Cannabis,Cannabinoid,Tetrahydrocannabinol]
author: Jacob Miguel | Sarah See
---


Only the sessions using flower products (60.4% of total sessions) were included in the dataset, and 6.7% of the flower sessions included laboratory-provided information on the product’s terpene levels. To create the index system and the associated treatment variables, distinct plant chemovars were categorized according to a 4-character coding system that broadly describes the relative magnitudes of the primary and secondary terpenes detected and THC and CBD potency levels. Study outcomes  The study objectives are to identify common examples of unique chemovars and evaluate whether differences exist in their effectiveness at reducing the severity of patients’ symptoms, and their associations with experienced side effects. We also included the product’s total terpene contents as a covariate to control for the volume of additional terpenes not represented in the primary or secondary indexing position, and we included the total number of side effects recorded as a covariate for examining each side effect category. The indexing system is validated as showing statistically and clinically significant differences in reported symptom relief for specific health conditions and differences in the side effects experienced across the modeled chemovars.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00170-9){:target="_blank rel="noopener"}

