---
layout: post
title: "A Phase I Trial to Determine the Pharmacokinetics, Psychotropic Effects, and Safety Profile of a Novel Nanoparticle-Based Cannabinoid Spray for Oromucosal Delivery"
date: 2022-08-07
categories:
- THC
author: Stefan Lorenzl, A B C, Franz Gottwald, Angelika Nistler, Laura Brehm, Renate Grötsch, Georg Haber, Christian Bremm, Christiane Weck, A C
tags: [Dose (biochemistry),Medical cannabis,Tetrahydrocannabinol,Pharmacokinetics,Cannabidiol,Bioavailability,Absorption (pharmacology),Cannabinoid,Nabiximols,Pharmacy,Chemistry,Clinical medicine,Medical research,Health sciences,Medicinal chemistry,Health,Medical treatments,Health care,Drugs,Medicine,Pharmaceutical sciences,Pharmacology]
---


The total duration of the study was 30 h from administration of the study preparation to the last blood sample taken. Assessments  Plasma samples were collected on Study day 1 pre-dose and at the following time points after the first actuation of the spray in the application of the study preparation: 10, 15, 20, 30, 45 min, and 1, 1.5, 2, 3, 4, 5, 6, 7, 8, 24, 26, 28, and 30 h. The participants were continuously monitored for the occurrence of AEs. The single dose of the applied THC spray (12 actuations, 3.96 mg THC) showed a mean maximum C max of 2.23 ng/mL at t max 1.54 h with t 1/2 at 3.5 h and a mean overall plasma concentration (AUC 0–t ) of 7.74 h × ng/mL for THC (Table 3). For example, the THC plasma concentration AUC 0–t of the oromucosal administered THC/CBD spray Sativex® (5.4 mg THC, 5.0 mg CBD) in healthy male volunteers resulted in 2.99 h × ng/mL, at a C max of 1.48 ng/mL and t 1/2 at 1.94 h [22] despite a 36% higher THC dosage compared to the tested nanoparticle THC spray AP701. There is possibly a relation between this ratio and the occurrence of psychotropic adverse effects, which might be useful for the assessment of drug effect profiles of medical cannabis preparations.

<hr>

[Visit Link](https://www.karger.com/Article/FullText/521352){:target="_blank" rel="noopener"}


