---
layout: post
title: "Fragment-based drug discovery and biological evaluation of novel cannabinol-based inhibitors of oxytosis/ferroptosis for neurological disorders"
date: 2024-05-24
categories:
- 
author: 
tags: [Cannabinoid,Mitochondrion,Cannabinol,Ferroptosis,Adenosine triphosphate,Cellular respiration,Biology]
---


This was followed by the addition of test compounds (5 μM). For HT22 cells, 2,000 cells/well were seeded onto the Seahorse XFe96 plates under normal culture condition as described above. The next day, cells were treated with test compounds at the desired concentrations and incubated for 16 h. Immediately before the assay, the culture medium in the plates was replaced with complete Seahorse XF DMEM assay medium. Among them, CP1 is the best with EC50 values ranging from 31 nM to 2.8 μM in the different neuroprotection assays. Importantly, the trauma assay indicated that CP1 was more effective than CBN, while the two compounds had very similar profiles in cell-based assays.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2213231724001149){:target="_blank" rel="noopener"}


