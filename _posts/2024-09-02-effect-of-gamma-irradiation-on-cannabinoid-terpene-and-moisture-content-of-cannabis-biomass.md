---
layout: post
title: "Effect of Gamma Irradiation on Cannabinoid, Terpene, and Moisture Content of Cannabis Biomass"
date: 2024-09-02
tags: [Food irradiation,Medical cannabis,Sterilization (microbiology),Gas chromatography,Cannabinoid,Cannabidiol,High-performance liquid chromatography]
author: Chandrani G. Majumdar | Mahmoud A. ElSohly | Elsayed A. Ibrahim | Mostafa A. Elhendawy | Donald Stanford | Suman Chandra | Amira S. Wanas | Mohamed M. Radwan | Chandrani G | Mahmoud A
---


Effect of Gamma Irradiation on Cannabinoid, Terpene, and Moisture Content of Cannabis Biomass. Molecules 2023, 28, 7710. https://doi.org/10.3390/molecules28237710  AMA Style  Majumdar CG, ElSohly MA, Ibrahim EA, Elhendawy MA, Stanford D, Chandra S, Wanas AS, Radwan MM. Effect of Gamma Irradiation on Cannabinoid, Terpene, and Moisture Content of Cannabis Biomass. 2023; 28(23):7710. https://doi.org/10.3390/molecules28237710  Chicago/Turabian Style  Majumdar, Chandrani G., Mahmoud A. ElSohly, Elsayed A. Ibrahim, Mostafa A. Elhendawy, Donald Stanford, Suman Chandra, Amira S. Wanas, and Mohamed M. Radwan. Effect of Gamma Irradiation on Cannabinoid, Terpene, and Moisture Content of Cannabis Biomass Molecules 28, no.

[Visit Link](https://www.mdpi.com/1420-3049/28/23/7710){:target="_blank rel="noopener"}

