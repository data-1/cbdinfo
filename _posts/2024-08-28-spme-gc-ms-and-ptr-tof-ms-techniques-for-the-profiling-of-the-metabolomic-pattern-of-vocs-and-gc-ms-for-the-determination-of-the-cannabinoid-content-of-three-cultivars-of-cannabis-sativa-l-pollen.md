---
layout: post
title: "SPME-GC-MS and PTR-ToF-MS Techniques for the Profiling of the Metabolomic Pattern of VOCs and GC-MS for the Determination of the Cannabinoid Content of Three Cultivars of Cannabis sativa L. Pollen"
date: 2024-08-28
tags: [Cannabis sativa,Principal component analysis,Cannabidiol,Cannabis,Gas chromatography,Metabolomics]
author: Cosimo Taiti | Elisa Masi | Vittoria Cicaloni | Vittorio Vinciguerra | Laura Salvini | Stefania Garzoli
---


Chemical volatile percentage composition of GEL, SMC, and SCBD of C. sativa pollen samples. Chemical volatile percentage composition of GEL, SMC, and SCBD of C. sativa pollen extracts. PTR-ToF-MS: Determination of Volatile Compounds from C. sativa Pollen Extracts  Headspace analysis by PTR-ToF-MS of the three C. sativa pollen extracts allowed the identification of 85 different peaks, which are listed in . Multivariate Metabolomics Data Analysis  The obtained results from all analyses simultaneously described the volatile profile of untreated pollen samples and pollen extracts of SMC, SCBD, and GEL cultivars of C. sativa L. To better compare the large amount of data obtained from chemical analyses performed by PTR-ToF-MS technique and to identify similarities and/or differences in chemical constituents, a multivariate statistical investigation was carried out. The other cannabinoids were instead detected only in the GEL extract sample.

[Visit Link](https://www.mdpi.com/1420-3049/27/24/8739){:target="_blank rel="noopener"}

