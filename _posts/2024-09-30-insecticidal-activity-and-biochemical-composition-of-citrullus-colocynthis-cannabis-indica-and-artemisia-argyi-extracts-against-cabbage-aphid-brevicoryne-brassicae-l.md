---
layout: post
title: "Insecticidal activity and biochemical composition of Citrullus colocynthis, Cannabis indica and Artemisia argyi extracts against cabbage aphid (Brevicoryne brassicae L.)"
date: 2024-09-30
tags: [Insecticide,Pest (organism),Aphid,Essential oil,Cabbage,Gas chromatography–mass spectrometry,Myzus persicae]
author: Maqsood Ahmed | Pei-Wen Qin | Zumin Gu | Yuyang Liu | Aatika Sikandar | Dilbar Hussain | Ansar Javeed | Jamil Shafi | Mazher Farid Iqbal | Ran An | Hongxia Guo | Du Ying | Weijing Wang | Yumeng Zhang | Mingshan Ji
pubdate: "2020-01-16"
institutions: Shenyang Agricultural University
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/6965137"
---


Plant pathology51 (2002). Plant. Insecticidal activity of five medicinal plant essential oils against the cabbage aphid, Brevicoryne brassicae. Plant pathology51 (2002). Insecticidal activity of five medicinal plant essential oils against the cabbage aphid, Brevicoryne brassicae.