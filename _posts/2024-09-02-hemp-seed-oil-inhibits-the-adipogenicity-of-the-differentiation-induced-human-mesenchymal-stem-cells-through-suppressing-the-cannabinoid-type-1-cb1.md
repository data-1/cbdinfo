---
layout: post
title: "Hemp Seed Oil Inhibits the Adipogenicity of the Differentiation-Induced Human Mesenchymal Stem Cells through Suppressing the Cannabinoid Type 1 (CB1)"
date: 2024-09-02
tags: [Endocannabinoid system,Anandamide,Fatty-acid amide hydrolase 1,Cannabinoid,Cannabinoid receptor 2,Adipogenesis,Cannabinoid receptor 1,2-Arachidonoylglycerol,Biochemistry,Biology,Cell biology]
author: Albatul S. Almousa | Pandurangan Subash-Babu | Ibrahim O. Alanazi | Ali A. Alshatwi | Huda Alkhalaf | Eman Bahattab | Atheer Alsiyah | Mohammad Alzahrani | Albatul S | Subash-Babu
---


Hemp seed extracted oil GC–MS analysis. CBD concentration in hemp seed extracted oils using HPLC. HSO, CBD, and THC Treatments Regulated the ECS  Since HSO and its active compounds (CBD and THC) initiated the adipogenic differentiation, we sought to examine whether this effect was related to the activation of the ECS. Cells 2021, 10, 1279. Cell.

[Visit Link](https://www.mdpi.com/1420-3049/29/7/1568){:target="_blank rel="noopener"}

