---
layout: post
title: "Cannabidiol binding and negative allosteric modulation at the cannabinoid type 1 receptor in the presence of delta-9-tetrahydrocannabinol: An In Silico study"
date: 2024-08-19
tags: [Allosteric regulation,G protein-coupled receptor,Docking (molecular),Alpha helix,Solvation,Ligand (biochemistry),Arrestin,Protein structure,Proteins,Neurochemistry,Cell signaling,Biophysics,Molecular biophysics,Biomolecules,Macromolecules,Biochemistry,Nutrients,Cell biology,Structural biology,Molecular biology]
author: C. David Pessoa-Mahana | Hery Chung | Angélica Fierro
---


The specific goals of this simulation were to: (1) observe if the interactions between the receptor, the orthosteric antagonist and CBD, as proposed by the docking studies persisted throughout the simulation and; (2) observe changes in the orthosteric binding site S1 and the possible formation of an allosteric site S3. CBD (cyan) and water molecules within ≥3 Å in the inactive conformation of the CB1R at 0 and 25 ns of simulation. More movement and changes were observed in the active conformation, so the simulation time was extended to 50 ns. An interaction network between the disulfide bond, CBD and water molecules that guides CBD entry into the binding site could explain the importance of polar residues in positions 98 and 107 for the modulating effects described by Laprairie et al.  THC maintains mainly hydrophobic and aromatic interactions; the tricyclic core forms aromatic interactions with P177, P189 and P268 while the pentyl side chain extends towards TM 3, 5 and 7. The aromatic interaction between F200 and W356 contributes to stabilization of the receptor in the inactive state but is disrupted in the active conformation [10].

[Visit Link](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0220025){:target="_blank rel="noopener"}

