---
layout: post
title: "An Evaluation of Regulatory Regimes of Medical Cannabis: What Lessons Can Be Learned for the UK?"
date: 2024-09-27
tags: [Medical cannabis,Cannabidiol,Cannabis (drug),Prescription drug,Evidence-based medicine,National Institute for Health and Care Excellence,Medication,Pharmacy,Cannabinoid,Health insurance,Nabiximols,Medicine,Physician,Indication (medicine),Medical prescription,Health,Health care,Clinical medicine,Health sciences]
author: Anne Katrin Schlag
pubdate: "2020-01-15"
institutions: King's College London
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/8489334"
---


In 2018, there were over 185,000 prescriptions for medicinal cannabis in Germany, with around 60,000–80,000 patients using medicinal cannabis products [10]. The Israel Medical Cannabis Agency is a special regulatory agency within the Ministry of Health providing detailed criteria for a wide list of indications that cannabis can be authorized for (https://www.health.gov.il). The Benefits of Establishing a Medical Cannabis Office  A special medical cannabis government office (as in the Netherlands and Israel) can ensure responsible production of cannabis for medical and scientific purposes and for the supply to pharmacies, universities, and research institutes. Health Canada Market data under the Access to Cannabis for Medical Purposes Regulations. Health Canada Market data under the Access to Cannabis for Medical Purposes Regulations.