---
layout: post
title: "Cannabinoid compounds in combination with curcumin and piperine display an anti-tumorigenic effect against colon cancer cells"
date: 2024-08-29
tags: [Apoptosis,ATM serine/threonine kinase,Ataxia telangiectasia and Rad3 related,Cancer,P53,Cannabinoid,Colorectal cancer,Cannabinoid receptor 2,YAP1,Cannabinoid receptor 1,Reverse transcription polymerase chain reaction,Hippo signaling pathway,Chemotherapy,Directionality (molecular biology),Carcinogenesis,Receptor (biochemistry),Apoptosis regulator BAX,Cell biology,Biotechnology,Biology,Biochemistry,Life sciences]
author: Hızlı Deniz | Ayşen Aslı
---


3.1.1 Effect of cannabinoid compounds together with curcumin and piperine on cell survival  HT29 and HCT116 colon cancer cell lines were treated with CBD, CBG, curcumin, piperine either alone or in triple combinations at a selected range of concentrations for 72 h (Figures 1, 2). However, there were no significant changes in the levels of p53 when these cells were treated with the triple combination containing CBD and curcumin/piperine (Figure 3B). However, while CBG mono-treatment induced a significant increase in the expression levels of both ATR and ATM genes compared to the negative control, triple treatment with any cannabinoid compounds resulted in significant decrease in both ATR and ATM gene expression levels (Figure 4B). Furthermore, combinations of curcumin, piperine and CBG showed more profound effect in HCT116 cells, whereas the same combinations were not effective in HT29 cells regarding cytotoxicity (Figures 1F, 2F). Therefore, we conclude that although cannabinoid compounds were effective as a single anti-cancer agents on HT29 cells, they are not suitable for combinatorial treatment with curcumin and piperine and, therefore, they have to be further assessed for their usage with other anti-cancer agents.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2023.1145666/full){:target="_blank rel="noopener"}

