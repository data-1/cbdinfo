---
layout: post
title: "Cannabinoid Receptor 1/miR-30b-5p Axis Governs Macrophage NLRP3 Expression and Inflammasome Activation in Liver Inflammatory Disease"
date: 2024-08-20
tags: [NLRP3,Cannabinoid receptor 1,Inflammasome,Inflammation,Macrophage,Carbon tetrachloride,Molecular biology,Signal transduction,Human genes,Human biology,Nutrients,Cell signaling,Biology,Neurochemistry,Cell communication,Biochemistry,Cell biology,Medical specialties]
author: 
---


Nod-like receptor (NLR) family pyrin domain containing 3 (NLRP3) has been regarded as an important initiator or promoter in multiple inflammatory diseases. CB1 expression was increased in liver tissue and macrophages of CCl4- and MCDHF-treated mice, positively correlated with NLRP3. CB1 agonist ACEA (Arachiodonyl-2’-Chloroethylamide) promoted NLRP3 expression and NLRP3 inflammasome activation in macrophages. CB1 blockade with its antagonist AM281 reduced NLRP3 expression, inflammasome activation, and liver inflammation in CCl4- and MCDHF-treated mice. Altogether, CB1/miR-30b-5p axis modulates NLRP3 expression and NLPR3 inflammasome activation in macrophages during liver inflammation, which provides a potential target for liver disease.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2162253120301189){:target="_blank rel="noopener"}

