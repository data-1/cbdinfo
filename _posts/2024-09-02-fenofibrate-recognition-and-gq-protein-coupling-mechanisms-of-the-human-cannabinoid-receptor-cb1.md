---
layout: post
title: "Fenofibrate Recognition and Gq Protein Coupling Mechanisms of the Human Cannabinoid Receptor CB1"
date: 2024-09-02
tags: [His-tag,Protein purification,Biology,Biochemistry,Life sciences,Molecular biology,Cell biology,Biotechnology]
author: B. Jiang | Y.-J. Wang | H. Wang | L. Song | C. Huang | Q. Zhu | F. Wu | W. Zhang | J. K. Barbiero | R. Santiago
---


Finally, the CB1-LgBiT protein was concentrated to ≈1 mg mL−1 for complex assembly. The assembled complex was further purified in the buffer (20 mm HEPES pH 7.5, 100 mm NaCl, 0.00075% (w/v) LMNG, 0.00025% (w/v) CHS, 0.00025% (w/v) GDN, 100 µm TCEP, 50 µm fenofibrate) by Superdex 200 Increase 10/300 GL column (Cytiva). The remaining 2753 pictures were subjected to blob picking with a size range from 110 to 160 Å. The final model was subjected to refinement on the composite map using phenix.real_space_refine in Phenix. CHARMM-GUI membrane builder[36] was used to generate the system.

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1002/advs.202306311){:target="_blank rel="noopener"}

