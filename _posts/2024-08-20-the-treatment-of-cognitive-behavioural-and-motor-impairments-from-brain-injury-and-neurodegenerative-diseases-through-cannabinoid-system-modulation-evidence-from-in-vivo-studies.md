---
layout: post
title: "The Treatment of Cognitive, Behavioural and Motor Impairments from Brain Injury and Neurodegenerative Diseases through Cannabinoid System Modulation—Evidence from In Vivo Studies"
date: 2024-08-20
tags: [Anandamide,Cannabinoid,Cannabinoid receptor 2,Cannabinoid receptor 1,Cannabinol,Experimental autoimmune encephalomyelitis,Neurochemistry,Biochemistry,Cell biology]
author: Daniela Calina | Ana Maria Buga | Mihaela Mitroi | Aleksandra Buha | Constantin Caruntu | Cristian Scheau | Abdelhakim Bouyahya | Nasreddine El Omari | Naoual El Menyiy | Anca Oana Docea
---


Cannabinoids Effects in Cognitive and Motor Impairment in Traumatic Brain Injury (TBI)  In are presented in vivo studies published between 2009 and 2019 that evaluate the cannabinoids protective effects in TBI models ( ). Cannabinoids Effects in Cognitive and Motor Impairment in Multiple Sclerosis (MS)  In are presented in vivo studies published between 2009 and 2019 that evaluate the cannabinoids protective effects in MS models ( ). Acute effects of a selective cannabinoid-2 receptor agonist on neuroinflammation in a model of traumatic brain injury. Decreased endocannabinoid levels in the brain and beneficial effects of agents activating cannabinoid and/or vanilloid receptors in a rat model of multiple sclerosis. The Treatment of Cognitive, Behavioural and Motor Impairments from Brain Injury and Neurodegenerative Diseases through Cannabinoid System Modulation—Evidence from In Vivo Studies.

[Visit Link](https://www.mdpi.com/2077-0383/9/8/2395){:target="_blank rel="noopener"}

