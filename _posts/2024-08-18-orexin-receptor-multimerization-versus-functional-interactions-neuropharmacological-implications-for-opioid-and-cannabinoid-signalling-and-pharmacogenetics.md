---
layout: post
title: "Orexin Receptor Multimerization versus Functional Interactions: Neuropharmacological Implications for Opioid and Cannabinoid Signalling and Pharmacogenetics"
date: 2024-08-18
tags: [Orexin,Cannabinoid receptor 1,Corticotropin-releasing hormone receptor 1,Endocannabinoid system,Cell signaling,Cell biology,Biology,Neurophysiology,Neurochemistry,Signal transduction,Biochemistry]
author: Miles D. Thompson | Takeshi Sakurai | Innocenzo Rainero | Mary C. Maj | Jyrki P. Kukkonen | Miles D | Mary C | Jyrki P
---


Orexin Receptor Variants  Genetic variants of the orexin system have been identified. Confirmation that naturally occurring orexin variants are functional, let alone pathogenic, is debatable; however, study of artificially created orexin receptor variants has provided insight into orexin receptor signalling that may be relevant to functional interactions and/or heterodimerization with other receptors, such as the cannabinoid CB1 receptor. Orexin Signalling  The potential for orexin receptors to functionally interact with other GPCRs or to homo- and/or heterodimerize or oligomerize will be presented after we review the structure and function of wild-type and variant the OX1 ( ) and OX2 ( ) receptors with respect to their signalling properties. The results suggested that OX2 Pro10Ser variant may be an example of a pharmacogenetic variant, though we cannot be sure that decreases in efficacy and potency of orexin peptides at these variant OX2 receptors result from altered receptor expression levels. Since studies of the functional interactions between human orexin receptor variants are not available, data on mouse orexin receptors may provide insight into the human system.

[Visit Link](https://www.mdpi.com/1424-8247/10/4/79){:target="_blank rel="noopener"}

