---
layout: post
title: "The attitudes, knowledge and confidence of healthcare professionals about cannabis-based products"
date: 2024-09-03
tags: [Cannabidiol,Cannabinoid,Medical cannabis,Dronabinol,Medicine,Health,Health care]
author: Emilio Russo | Paula Martinez Agredano | Peter Flachenecker | Charlotte Lawthom | Duncan Munro | Chandni Hindocha | Makarand Bagul | Eugen Trinka
---


Keywords: Healthcare professional education, Survey, Cannabis, Cannabinoids, Cannabis-based product, Cannabis-based medicine  Introduction  Cannabis refers to the Cannabis sativa L plant and its products (Morales et al. 2017), and although cannabis has many applications, its potential use in healthcare has attracted substantial research interest in recent years. Survey questions and data  The data reported in the main body of this publication are from survey questions relating to the attitudes, confidence, and knowledge of HCPs on the use of cannabis-based products in healthcare. HCPs from the medical specialities included in this study were surveyed based on the understanding that they were most likely to receive questions from patients about cannabis-based products in a healthcare setting. 10.1016/j.yebeh.2020.107102 [PubMed] [CrossRef] [Google Scholar]  UK National Health Service Research Authority. 10.1177/0269881120926677 [PMC free article] [PubMed] [CrossRef] [Google Scholar] [Ref list]  European Monitoring Centre for Drugs and Drug Addiction.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11267914/){:target="_blank rel="noopener"}

