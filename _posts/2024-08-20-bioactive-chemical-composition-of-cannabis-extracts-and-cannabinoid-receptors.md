---
layout: post
title: "Bioactive Chemical Composition of Cannabis Extracts and Cannabinoid Receptors"
date: 2024-08-20
tags: [Cannabinoid,Receptor antagonist,Tetrahydrocannabinol,Quantitative structure–activity relationship,High-performance liquid chromatography,Cannabidiol,Cannabis (drug),Cannabinoid receptor 2]
author: Yi Yang | Rupali Vyawahare | Melissa Lewis-Bakker | Hance A. Clarke | Albert H. C. Wong | Lakshmi P. Kotra | Lewis-Bakker | Hance A | Albert H. C | Lakshmi P
---


Phytocannabinoid concentrations and corresponding receptor potencies of cannabis oil samples. Using a similar strategy, we correlated the CB1 and CB2 receptor potencies of the medical cannabis samples to the four cannabinoid concentrations using PLS regression analysis to obtain QSAR-like prediction models (Table S2). Extraction of Cannabis for Cannabinoids  Cannabis samples in their oil form were analyzed for their chemical content without further processing. This is the first study disclosing a relationship between the major phytocannabinoids concentrations in cannabis extracts and the corresponding cannabinoid receptors activities. Supplementary Materials  The following are available online, Table S1: LLD and LLQ values for the phytocannabinoids quantified in this study; Table S2: Categorization of data prior to regression analysis; Table S3: Preliminary prediction models generated using all combinations of linear and quadratic explanatory variables, describing the relationship between chemical composition and potency at CB2 as agonists for extracts obtained from dried plant samples; Table S4: Preliminary prediction models generated using all combinations of linear and quadratic explanatory variables, describing the relationship between chemical composition and potency at CB1 as agonists for cannabis-derivative samples; Table S5: Preliminary prediction models generated using all combinations of linear and quadratic explanatory variables, describing the relationship between chemical composition and potency at CB1 as agonists for extracts obtained from dried plant samples; Figure S1: Dose-response curves for CBD, CBDA, Δ9-THC and Δ9-THCA as agonists and antagonists at CB1 receptor; Figure S2: Dose-response curves for CBD, CBDA, Δ9-THC and Δ9-THCA as agonists and antagonists at CB2 receptor.

[Visit Link](https://www.mdpi.com/1420-3049/25/15/3466){:target="_blank rel="noopener"}

