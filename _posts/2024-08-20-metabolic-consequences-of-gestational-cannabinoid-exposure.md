---
layout: post
title: "Metabolic Consequences of Gestational Cannabinoid Exposure"
date: 2024-08-20
tags: [Fatty-acid amide hydrolase 1,Cannabinoid receptor 2,Tetrahydrocannabinol,Cannabinoid,Cannabinoid receptor 1,Anandamide,Mitochondrion,Cannabis (drug),Fetus,Decidualization,Intrauterine growth restriction,Prenatal development,Placenta,Endocannabinoid system,2-Arachidonoylglycerol,Cannabinoid receptor]
author: Kendrick Lee | Daniel B. Hardy | Daniel B
---


Moreover, clinical studies indicate that cannabis consumption during pregnancy leads to fetal growth restriction (FGR), which is associated with an increased risk of obesity, type II diabetes (T2D), and cardiovascular disease in the offspring. [Google Scholar] [CrossRef]  Day, N.L. [Google Scholar] [CrossRef]  Fonseca, B.M. The Endocannabinoid System and Heart Disease: The Role of Cannabinoid Receptor Type 2. [Google Scholar] [CrossRef]

[Visit Link](https://www.mdpi.com/1422-0067/22/17/9528){:target="_blank rel="noopener"}

