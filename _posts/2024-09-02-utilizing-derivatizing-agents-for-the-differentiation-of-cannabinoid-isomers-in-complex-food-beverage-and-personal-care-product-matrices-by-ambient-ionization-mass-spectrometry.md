---
layout: post
title: "Utilizing derivatizing agents for the differentiation of cannabinoid isomers in complex food, beverage and personal-care product matrices by ambient ionization mass spectrometry"
date: 2024-09-02
tags: [Cannabidiol,Cannabinoid,Cannabis,Tetrahydrocannabinol,Cannabaceae,Entheogens,Individual psychoactive drugs]
author: 
---


Recent legalization and decriminalization of marijuana at the state level has not only contributed to a rise in the recreational use of Cannabis sativa, also known simply as Cannabis, but also to an increase in the range of matrices into which cannabinoids derived from it are infused. Traditional methods for analyzing these products, which are typically chromatography-based, are often matrix-dependent and demand time-consuming and resource-intensive sample preparation protocols that are highly nuanced and not readily applicable to multiple matrix types. In this study, foods and personal-care products under the categories of sweets, spreads, condiments/toppings, beverages, oils, and commercial body products were spiked with cannabinoids including Δ9-tetrahydrocannabinol (THC), cannabidiol (CBD), cannabicitran (CBT), and cannabigerol (CBG). Chemical derivatization of the samples with N-methyl-N-(trimethylsilyl)trifluoroacetamide (MSTFA) followed by direct analysis in real time – high-resolution mass spectrometry (DART-HRMS) analysis readily revealed the presence of cannabinoids in the products despite the matrix complexity (i.e., the contributions from matrix-derived peaks did not interfere with the differentiation of cannabinoids). However, due to the different number of hydroxyl (-OH) groups (zero in CBT, one in THC, and two in CBD) that engage with the derivatizing agent, the cannabinoids are differentiated based on the mass disparities of the protonated adducts formed (m/z 315.2324, 387.2719 and 459.3119 for CBT, THC, and CBD, respectively), which is readily revealed by DART-HRMS.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2666831924000420){:target="_blank rel="noopener"}

