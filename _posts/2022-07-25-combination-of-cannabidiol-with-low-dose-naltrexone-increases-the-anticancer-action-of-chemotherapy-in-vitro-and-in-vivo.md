---
layout: post
title: "Combination of cannabidiol with low‑dose naltrexone increases the anticancer action of chemotherapy in vitro and in vivo"
date: 2022-07-25
categories:
- CBD
author: Liu Wai M., Institute For Infection, Immunity, St George'S University Of London, London, Hall Nadine K., Liu Harry S.Y., Hood Francis L., Ldn Pharma Ltd., Dalgleish Angus G.
tags: [Cannabinoid,Chemotherapy,Cancer,Apoptosis,MAPKERK pathway,Bcl-2,Clinical medicine,Cell biology,Cell signaling,Biotechnology,Biochemistry,Neurochemistry,Cellular processes,Biology,Cell communication,Medical specialties,Signal transduction]
---


These receptors feed into and interact with central signalling cascades that determine the ease by which cells engage in apoptosis, and can be used as a way to prime cancer cells to other treatments. The present study was designed to investigate the effect of combining these two agents on cancer cell lines in vitro and in a mouse model, and focused on how the sequence of administration may affect the overall action. The results showed both agents had minimal effect on cell numbers when used simultaneously; however, the combination of LDN and CBD, delivered in this specific sequence, significantly reduced the number of cells, and was superior to the regimen where the order of the agents was reversed. For example, there was a 35% reduction in cell numbers when using LDN before CBD compared to a 22% reduction when using CBD before LDN. The two agents also sensitised cells to chemotherapy as significant decreases in cell viability were observed when they were used before chemotherapy.

<hr>[Visit Link](https://www.spandidos-publications.com/10.3892/or.2022.8287){:target="_blank" rel="noopener"}


