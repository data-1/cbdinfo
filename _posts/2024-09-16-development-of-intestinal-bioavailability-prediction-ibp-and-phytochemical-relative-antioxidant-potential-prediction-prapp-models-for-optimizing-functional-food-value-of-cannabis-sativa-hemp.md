---
layout: post
title: "Development of intestinal bioavailability prediction (IBP) and phytochemical relative antioxidant potential prediction (PRAPP) models for optimizing functional food value of Cannabis sativa (hemp)"
date: 2024-09-16
tags: [Antioxidant,Bioavailability,Area under the curve (pharmacokinetics),Pharmacokinetics,Quantitative structure–activity relationship,Partition coefficient]
author: Kimber Wise | Sophie Selby-Pham | Jamie Selby‐Pham | Harsharn Gill
institutions: RMIT University
doi: "https://doi.org/10.1080/10942912.2020.1797783"
---


Herein two quantitative structure–activity relationship (QSAR) predictive models are presented: the intestinal absorption prediction (IBP) model for predicting compound bioavailability (r2 = 0.93), and the phytochemical relative antioxidant potential prediction (PRAPP) model for predicting antioxidant capacity (r2 = 0.89). Herein two QSAR models are presented: the intestinal bioavailability prediction (IBP) model and the phytochemical relative antioxidant potential prediction (PRAPP) model. [Citation16–Citation18] Whilst this relationship between physicochemical properties and F is well established, the IBP model is the first QSAR model which predicts F from these properties. Predicting hemp meal phytochemical absorption and antioxidant capacity  Industrial cannabis (hemp) is used for the production of material, [Citation24] food,[Citation25] and medicinal products. Phytochem.