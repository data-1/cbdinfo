---
layout: post
title: "Cannabidiol-Loaded Mixed Polymeric Micelles of Chitosan/Poly(Vinyl Alcohol) and Poly(Methyl Methacrylate) for Trans-Corneal Delivery"
date: 2022-08-27
categories:
- CBD
author: Sosnik, Shabo, Ronya Ben, Halamish, Hen Moshe, Alejandro Sosnik, Ronya Ben Shabo, Hen Moshe Halamish
tags: [Cannabidiol,Copolymer,Poly(methyl methacrylate),Differential scanning calorimetry,Spray drying,Nanoparticle drug delivery,Nanocarriers,Cornea,Dynamic light scattering,Micelle,Cell membrane,Nanoparticle,Chemical substances,Chemistry,Physical sciences,Materials,Physical chemistry]
---


This copolymer is named CS-PMMA30, where 30 represents the relative PMMA weight content in the copolymer (%PMMA), as determined by proton-nuclear magnetic resonance (1H-NMR, see below) [ g -PMMA copolymer with a %PMMA of 16%, namely PVA-PMMA16 [® 4–88, weight-average MW of 31,000 g/mol, 87–89% hydrolysis, Sigma-Aldrich, St. Louis, MO, USA) was dissolved in distilled water (100 mL) at RT, and TEMED (0.18 mL in 50 mL of degassed water) was dissolved in nitric acid 70% (0.45 mL). The purged CS solution was magnetically stirred, heated to 35 °C and 0.142 mL of purified MMA pre-treated with aluminum oxide (5 g, pore size 58 Å, ~150 mesh, Sigma-Aldrich, St. Louis, MO, USA) to remove the free radical inhibitor, was dispersed in degassed water (48 mL) and added to the reaction mixture. For this, PVA (0.4 g, Mowiol4–88, weight-average MW of 31,000 g/mol, 87–89% hydrolysis, Sigma-Aldrich, St. Louis, MO, USA) was dissolved in distilled water (100 mL) at RT, and TEMED (0.18 mL in 50 mL of degassed water) was dissolved in nitric acid 70% (0.45 mL). CBD-free and CBD-loaded mixed CS--PMMA30:PVA--PMMA16 (1:1 weight ratio) PMs were prepared by using a microfluidics system designed and fabricated in our laboratory [ 47 ]. For the preparation of the mixed PMs (1:1 weight ratio between the graft copolymers), 16 mg CS-PMMA30 was dissolved in water (1 mL) supplemented with 10 µL of glacial acetic acid (pH of 5.5, Gadot, Netanya, Israel).

<hr>

[Visit Link](https://www.mdpi.com/1999-4923/13/12/2142/htm){:target="_blank" rel="noopener"}


