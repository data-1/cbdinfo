---
layout: post
title: "Cannabinoids and COVID-19"
date: 2022-07-25
categories:
- Cannabinoid
author: Lauren Robertson, The Time I Finished My Degree In Microbiology I Had Come To One Conclusion, I Did Not Want To Work In A Lab. Instead, I Decided To Move To The South Of Spain To Teach English. After Two Brilliant Years, I Realized That I Missed Science, What I Really Enjoyed Was Communicating Scientific Ideas, Whether That Be To Four-Year-Olds Or Mature Professionals. On Returning To England I Landed A Role In Science Writing, Found It Combined My Passions Perfectly. Now At Texere, I Get To Hone These Skills Every Day Writing About The Latest Research In An Exciting, Creative Way.
tags: [Tetrahydrocannabinolic acid,Ligand (biochemistry),Combinatorial chemistry,Allosteric regulation,Severe acute respiratory syndrome coronavirus 2,Biology,Biochemistry,Chemistry,Biotechnology,Medicinal chemistry,Life sciences]
---


The team’s research focused specifically on the role of cannabinoid acids – CBD-A, CBG-A, and THC-A. You’ve been studying hemp for years. Think of the mass spectrometer as the magnet that finds the needle in the haystack. It was surprising that CBD-A, CBG-A, and THC-A were the best ligands for the spike protein. It was very exciting to find that not only could these acidic forms bind, but they could function to block infection at the cell entry step.

<hr>[Visit Link](https://thecannabisscientist.com/research-development/cannabinoids-and-covid-19){:target="_blank" rel="noopener"}


