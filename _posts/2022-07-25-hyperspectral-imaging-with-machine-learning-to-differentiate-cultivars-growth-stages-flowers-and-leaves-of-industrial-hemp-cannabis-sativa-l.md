---
layout: post
title: "Hyperspectral Imaging With Machine Learning to Differentiate Cultivars, Growth Stages, Flowers, and Leaves of Industrial Hemp (Cannabis sativa L.)"
date: 2022-07-25
categories:
- Cannabis
- Hemp
author: Lu, Department Of Agricultural, Biological Engineering, Mississippi State University, United States, Young, Department Of Biological, Agricultural Engineering, North Carolina State University, Linder
tags: [Cannabis,Cannabidiol,Hemp,Confusion matrix,Hyperspectral imaging,Statistical classification,Accuracy and precision,Image segmentation,Principal component analysis,Hyperparameter optimization,Cross-validation (statistics)]
---


Specifically, in this research we acquired hyperspectral images from freshly harvested leaf and flower materials of five cultivars of CBD hemp at five growth stages using a benchtop hyperspectral reflectance imaging system, developed an image processing pipeline for segmenting the plant parts from background and extracting spectra from sample segments, performed exploratory analysis of spectral features of hemp samples, and built classification models to differentiate the cultivars, growth stages, and plant parts. At the time of harvest or growth stage, four plants were randomly chosen per cultivar row, corresponding to four replications, and both leaves and flowers, as shown in Figure 1 (right), were sampled for the differentiation of hemp cultivars and growing stages. Hyperspectral Image Acquisition  A portable, benchtop hyperspectral reflectance imaging system (Figure 2) under controlled lighting was assembled for acquiring images from hemp samples. Classification  The machine learning models based on rLDA were first developed for differentiating the hemp cultivars using the spectral data of leaf and flower samples separately as well as their combination, at each growth stage (harvest dates). The rLDA models, using leaf or flower samples at individual growth stages, achieved the classification accuracies of 96.8%-99.6% in the differentiation of hemp cultivars.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fpls.2021.810113/full){:target="_blank" rel="noopener"}


