---
layout: post
title: "Endocannabinoid administration affects taste preference and the expression of cannabinoid and opioid receptors in the amygdala of early lactating cows"
date: 2024-08-29
tags: [Taste,Sweetness,Cannabinoid receptor 1,Δ-opioid receptor,Reward system,Rumen,Taste receptor,Real-time polymerase chain reaction,Endocannabinoid system,Κ-opioid receptor,Cannabinoid,Amygdala]
author: Mazzuoli-Weber
---


Thus, the aims of the present study were (i) to investigate the taste preference of late-pregnant dairy cows by offering sweet-tasting, umami-tasting and taste-unaltered feed and water and (ii) to examine whether AEA compared to NaCl (CON) administration in early lactation modifies taste preference, the expression of endocannabinoid receptors and taste-related genes in the tongue and the expression of opioid receptors in the amygdala and nucleus accumbens of dairy cows. Although we observed a preference for sweet-tasting feed, the lack of changes in taste receptor expression after AEA administration might be because the tongue sampling was performed several days after the preference test when cows had no access to taste-altered feed. It was loaded with either drinking bowls or feeding bins containing the taste-altered water or feed, respectively, on the right and left side and a feed bin in between containing taste-unaltered feed (control feed). The FPT followed the WPT and was performed on Day 19 (± 9) a.p. for one animal (cow 3) and the 1st FPT Day p.p.

[Visit Link](https://www.nature.com/articles/s41598-023-31724-3){:target="_blank rel="noopener"}

