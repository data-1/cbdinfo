---
layout: post
title: "Terminal type-specific cannabinoid CB1 receptor alterations in patients with schizophrenia: A pilot study"
date: 2024-09-02
tags: [Cannabinoid receptor 1,Schizophrenia,Neuroscience,Diseases and disorders,Neurophysiology,Basic neuroscience research,Branches of neuroscience,Mental disorders]
author: 
---


Background  Individuals with schizophrenia are at elevated genetic risks for comorbid cannabis use, and often experience exacerbations of cognitive and psychotic symptoms when exposed to cannabis. These findings have led a number of investigators to examine cannabinoid CB1 receptor (CB1R) alterations in schizophrenia, though with conflicting results. We recently demonstrated the presence of CB1R in both excitatory and inhibitory boutons in the human prefrontal cortex, with differential levels of the receptor between bouton types. We hypothesized that the differential enrichment of CB1R between bouton types – a factor previously unaccounted for when examining CB1R changes in schizophrenia – may resolve prior discrepant reports and increase our insight into the effects of CB1R alterations on the pathophysiology of schizophrenia. Terminal type-specific analyses identified significantly higher CB1R levels within excitatory boutons in samples from individuals with schizophrenia relative to comparisons.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S0969996123002772){:target="_blank rel="noopener"}

