---
layout: post
title: "Endocannabinoid Metabolism and Traumatic Brain Injury"
date: 2022-09-01
categories:
- Endocannabinoid
author: Zhu, Gao, Chen, Dexiao Zhu, Fei Gao, Chu Chen
tags: [Neuroinflammation,Tau protein,2-Arachidonoylglycerol,Amyloid beta,Long-term potentiation,Traumatic brain injury,TAR DNA-binding protein 43,Neurodegenerative disease,Nutrients,Neuroscience,Biochemistry,Molecular biology,Biotechnology,Cell biology]
---


The inflammatory response is one of the major features of brain damage in the case of secondary injury. The anti-inflammatory effects of LXs and ATLs in TBI appear to be via binding to FPR2 to suppress cytokines, including IL1β, IL6, and TNF, in mice [ 49 ]. Increased tau phosphorylation has been demonstrated in models of mild and severe TBI [ 58 ]. A previous study demonstrated that pharmacological inhibition of MAGL reduced the levels of phosphorylated tau as well as of P25 and phosphorylated GSK3β, key players in tau phosphorylation, 8 and 30 days after the first injury in a mouse model of repetitive mild closed-head injury [ 40 ], suggesting that inhibition of 2-AG metabolism is capable of suppressing tau phosphorylation. Importantly, this study revealed that pharmacological inactivation of MAGL robustly reduced TDP-43 production, providing the first evidence that inhibition of 2-AG metabolism prevents TBI-induced excessive formation of TDP-43, which, in turn, promotes recovery from the secondary injury, thus preventing cognitive decline [ 40 ].

<hr>

[Visit Link](https://www.mdpi.com/2073-4409/10/11/2979/htm){:target="_blank" rel="noopener"}


