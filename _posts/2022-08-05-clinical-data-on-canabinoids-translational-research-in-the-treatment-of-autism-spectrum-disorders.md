---
layout: post
title: "Clinical Data on Canabinoids: Translational Research in the Treatment of Autism Spectrum Disorders"
date: 2022-08-05
categories:
- Cannabinoid
author: Carreira, Laura D., Matias, Francisca C., Campos, Maria G., Laura D. Carreira, Francisca C. Matias, Maria G. Campos
tags: [Dronabinol,Cannabidiol,Nabilone,Cannabinoid,Autism spectrum,Cannabinoid receptor type 2,Medical cannabis,Diseases and disorders,Neuroscience,Medicine,Health,Clinical medicine]
---


The number of participants (patients included) shows great variability among the analyzed studies. Another study [ 61 ] (p. 1286) published in 2018 evaluated that 60 children with autism showed a 47% improvement in communication problems. However, regarding GABA levels, there are no changes at the cortical and subcortical levels after administration of CBDV [ 40 ] (pp. Most studies are observational and without a control group and, consequently, it is not possible to establish causality between cannabinoid therapy and the improvement in ASDs symptoms. The high number of drugs that are administered concomitantly with cannabinoid therapy also contributes to this factor.

<hr>

[Visit Link](https://www.mdpi.com/2227-9059/10/4/796/htm){:target="_blank" rel="noopener"}


