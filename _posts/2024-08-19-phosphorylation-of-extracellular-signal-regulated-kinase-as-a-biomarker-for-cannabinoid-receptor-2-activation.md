---
layout: post
title: "Phosphorylation of extracellular signal-regulated kinase as a biomarker for cannabinoid receptor 2 activation"
date: 2024-08-19
tags: [Cannabinoid receptor 2,Cannabinoid receptor 1,Cannabinoid,Cell signaling,Neurochemistry,Biochemistry,Biology,Cell communication,Signal transduction,Cell biology]
author: 
---


To this end, in this study we evaluated all the downstream signaling of CB2 and developed a method for quantifying CB2R activation in primary mouse and human blood cells based on the findings that (1) CB2R activation results in phosphorylation of extracellular signal-regulated kinase (ERK) in blood cells; and (2) CB2R agonist (e.g. Cell-based quantitative detection of AKT and ERK phosphorylation  CHO-K1 cells transiently expressing human or mouse CB2 receptor were seeded in 96-well plates and cultured overnight. 4370), antibody was diluted in the stain/wash buffer (400×) and the cells were incubated in this solution overnight at 4 °C. In the present study, we detected CB2R activation in human primary cells by evaluating AKT and ERK phosphorylation. However, since the pERK signal declines so rapidly following receptor activation, using ERK phosphorylation as a measure of CB2R activation is impractical in vivo.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2405844018331013){:target="_blank rel="noopener"}

