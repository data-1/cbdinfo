---
layout: post
title: "A content analysis of internet information sources on medical cannabis - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabis
author: Kruger, Daniel J., Population Studies Center, Institute For Social Research, University Of Michigan, Ann Arbor, Moffet, Ilana M., Literature, Science
tags: [Medical cannabis,Cannabidiol,Harm reduction,Research,Cannabis (drug),Dose (biochemistry),Drug,Health,Substance abuse,Tetrahydrocannabinol,Cannabinoid,Adverse effect,Pharmacology,Medication,Controlled Substances Act,Food and Drug Administration,Health sciences,Health care,Medicine]
---


The Internet is a frequent source for information, including for those seeking information on the medical use of cannabis (Kruger et al. 2020b). The current study found that information related to medical cannabis on the Internet is abundant, generally positive, but lacking in the depth and detail that would ideally inform medical treatment. Over 150 different health and medical conditions were suggested for treatment by cannabis, yet only 4% of the conditions mentioned have substantial empirical support (NASEM 2017). Few websites mentioned even the most commonly known cannabinoids, THC and CBD, and only a handful of these sites recommended specific dosages for particular conditions. Systematic research is needed on treatment effectiveness for various health conditions; the effective dosage levels for the numerous cannabinoids by health condition; optimal administration forms and schedules; health and other risks; effectiveness of harm reduction techniques; and the effectiveness of health education techniques custom-tailored for cannabis users.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-020-00041-1){:target="_blank" rel="noopener"}


