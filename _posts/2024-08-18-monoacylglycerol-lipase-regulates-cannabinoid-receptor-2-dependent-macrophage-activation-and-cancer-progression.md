---
layout: post
title: "Monoacylglycerol lipase regulates cannabinoid receptor 2-dependent macrophage activation and cancer progression"
date: 2024-08-18
tags: [Tumor-associated macrophage,Cannabinoid receptor 2,Interleukin 10,Toll-like receptor 4,Macrophage,Tumor necrosis factor,Interferon gamma,Cell biology,Biochemistry,Biology]
author: 
---


To further verify the role of MGLL in lipid accumulation in TAMs, we constructed a transgenic mouse model with specific overexpression of MGLL (TgMGLL) in myeloid cells (Supplementary Fig. We also verified that CB2 did not regulate MGLL expression in MC-38 TAMs (Supplementary Fig. The pellets were resuspended in 5 ml of ACK Lysing Buffer (#C3702, Beyotime, Shanghai, China), kept still for 5 min at room temperature, diluted to 15 ml with 10 ml of DMEM and centrifuged at 1000 × g for 5 min. These cells were collected for further isolation of macrophages and other immune cells. These cells were collected for further isolation of T cells.

[Visit Link](https://www.nature.com/articles/s41467-018-04999-8){:target="_blank rel="noopener"}

