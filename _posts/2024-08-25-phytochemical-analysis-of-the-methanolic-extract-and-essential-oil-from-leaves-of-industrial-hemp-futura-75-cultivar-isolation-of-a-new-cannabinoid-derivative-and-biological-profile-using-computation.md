---
layout: post
title: "Phytochemical Analysis of the Methanolic Extract and Essential Oil from Leaves of Industrial Hemp Futura 75 Cultivar: Isolation of a New Cannabinoid Derivative and Biological Profile Using Computation"
date: 2024-08-25
tags: [Thrombin,Caryophyllene,Gas chromatography,Biochemistry]
author: Simona De Vita | Claudia Finamore | Maria Giovanna Chini | Gabriella Saviano | Vincenzo De Felice | Simona De Marino | Gianluigi Lauro | Agostino Casapullo | Francesca Fantasma | Federico Trombetta
---


Previous studies have reported antioxidant, anti-inflammatory, and antimycotic properties of the certified hemp variety “Futura 75” [42], whose essential oil from inflorescences showed antibacterial, anti-proliferative [43], and insecticidal effects [44]. From our analysis of the resulting binding poses, 3 shows slightly better binding towards the protein target interacting with Arg288 and Ser 342 through the carboxyl portion of the molecule, while 2 forms a π-π interaction between its aromatic ring and Phe264 side chain ( B,C). [Google Scholar] [CrossRef] [PubMed]  ElSohly, M.A. [Google Scholar] [CrossRef] [Green Version]  Jian, Y.; He, Y.; Yang, J.; Han, W.; Zhai, X.; Zhao, Y.; Li, Y. Molecular modeling study for the design of novel Peroxisome Proliferator-Activated Receptor Gamma agonists using 3D-QSAR and molecular docking. Phytochemical Analysis of the Methanolic Extract and Essential Oil from Leaves of Industrial Hemp Futura 75 Cultivar: Isolation of a New Cannabinoid Derivative and Biological Profile Using Computational Approaches Plants 11, no.

[Visit Link](https://www.mdpi.com/2223-7747/11/13/1671){:target="_blank rel="noopener"}

