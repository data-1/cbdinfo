---
layout: post
title: "The Cytotoxic Effect of Isolated Cannabinoid Extracts on Polypoid Colorectal Tissue"
date: 2024-08-27
tags: [Colorectal cancer,Cannabinoid,Colorectal polyp,Polyp (medicine),Cannabidiol,Clinical medicine,Medical specialties]
author: Dana Ben-Ami Shor | Ilan Hochman | Nathan Gluck | Oren Shibolet | Erez Scapa | Ben-Ami Shor
---


by  Dana Ben-Ami Shor  1,* ,  Ilan Hochman  2,  Nathan Gluck  1 ,  Oren Shibolet  1 and  Erez Scapa  1  1  Department of Gastroenterology, Tel-Aviv Sourasky Medical Center, Tel-Aviv, Israel, Affiliated to Sackler Faculty of Medicine, Tel-Aviv University, Tel Aviv 6997801, Israel  2  CNBX Pharmaceuticals Ltd., Rehovot 7608801, Israel  *  Author to whom correspondence should be addressed. To assess the cytotoxic effect of cannabinoid extracts and purified cannabinoids on both colorectal polyps and normal colonic cells, as well as their synergistic interaction. The differential toxic effect of the various cannabinoids extracts as well as of the isolated cannabinoids was examined. Evaluation of the Effect of Combinations of Cannabinoids on Colon Cancer Cell Viability  Various isolated cannabinoids were evaluated for synergistic interaction in inhibiting colon-polyp-derived cell viability. The effect of combinations of cannabinoids on polyp cells’ viability.

[Visit Link](https://www.mdpi.com/1422-0067/23/19/11366){:target="_blank rel="noopener"}

