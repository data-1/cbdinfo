---
layout: post
title: "Cannabinoids and Cannabinoid Receptors: The Story so Far"
date: 2024-08-20
tags: [Cannabinoid,Cannabinoid receptor 2,Cannabinoid receptor,Receptor (biochemistry),Neurochemistry,Biochemistry,Cell biology,Cell signaling,Signal transduction,Receptors,Neurophysiology,Cell communication,Biology,Biotechnology,Molecular biology,Life sciences]
author: 
---


Like most modern molecular biology and natural product chemistry, understanding cannabinoid pharmacology centers around molecular interactions, in this case, between the cannabinoids and their putative targets, the G-protein coupled receptors (GPCRs) cannabinoid receptor 1 (CB1) and cannabinoid receptor 2 (CB2). Understanding the complex structure and interplay between the partners in this molecular dance is required to understand the mechanism of action of synthetic, endogenous, and phytochemical cannabinoids. This review, with 91 references, surveys our understanding of the structural biology of the cannabinoids and their target receptors including both a critical comparison of the extant crystal structures and the computationally derived homology models, as well as an in-depth discussion about the binding modes of the major cannabinoids. The aim is to assist in situating structural biochemists, synthetic chemists, and molecular biologists who are new to the field of cannabis research.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2589004220304880){:target="_blank rel="noopener"}

