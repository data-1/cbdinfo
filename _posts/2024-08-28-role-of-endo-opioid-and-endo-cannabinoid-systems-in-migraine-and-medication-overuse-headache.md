---
layout: post
title: "Role of Endo-opioid and Endo-cannabinoid Systems in Migraine and Medication-overuse Headache"
date: 2024-08-28
tags: [Nociceptin,Endocannabinoid system,Migraine,Fatty-acid amide hydrolase 1,Anandamide,Neurochemistry]
author: Babür Dora | Gökçen Hatipoğlu | Devrim Demir-Dora | Sebahat Özdem
---


Materials and Methods: Patients with (episodic migraine; n=29), (CM; n=15), MOH (n=16) and 31 healthy controls were recruited and blood levels of nociceptin and anandamide (AEA) were compared between groups, as well as their levels with headache parameters. There are only a few studies of nociceptin in migraine and in MOH (31,32). Median values for monthly headache frequency, headache years, headache score and MIDAS score were higher in the CM and MOH groups compared to EM, but did not differ between CM and MOH groups (Table 1). Discussion  We found significantly lower AEA levels in patients with migraine and high nociceptin levels in the CM group and very low levels of nociceptin in the MOH group, although we were unable to demonstrate significance. There are only a few studies of nociceptin in migraine.

[Visit Link](https://tjn.org.tr/full-text/74/eng){:target="_blank rel="noopener"}

