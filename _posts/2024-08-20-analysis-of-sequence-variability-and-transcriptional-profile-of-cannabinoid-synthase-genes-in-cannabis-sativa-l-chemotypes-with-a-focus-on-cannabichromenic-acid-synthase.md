---
layout: post
title: "Analysis of Sequence Variability and Transcriptional Profile of Cannabinoid synthase Genes in Cannabis sativa L. Chemotypes with a Focus on Cannabichromenic acid synthase"
date: 2024-08-20
tags: [Polymerase chain reaction,Real-time polymerase chain reaction,Primer (molecular biology),Tetrahydrocannabinolic acid synthase,Branches of genetics,Molecular biology,Biology,Biotechnology,Genetics,Life sciences,Biochemistry]
author: Flavia Fulvio | Roberta Paris | Massimo Montanari | Cinzia Citti | Vincenzo Cilento | Laura Bassolino | Anna Moschella | Ilaria Alberti | Nicola Pecchioni | Giuseppe Cannazza
---


Diversity in CBCAS Sequences  Specific primers were designed following a search for the “fiber-type THCAS” reference sequence, AB212830 [8], in the genome assemblies available on NCBI in 2019, listed in Supplementary Table S1. Primer design and amplification reactions were optimized to distinguish the different cannabinoid synthases sequences, developing highly specific assays for gene detection and transcriptional analyses. Specificity of primers for CBCAS genes. Primer pairs can be used as markers on DNA or to verify if the specific target gene is expressed using cDNA as template, as indicated in . Plants.

[Visit Link](https://www.mdpi.com/2223-7747/10/9/1857){:target="_blank rel="noopener"}

