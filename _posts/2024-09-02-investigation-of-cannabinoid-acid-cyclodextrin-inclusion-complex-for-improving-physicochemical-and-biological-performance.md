---
layout: post
title: "Investigation of Cannabinoid Acid/Cyclodextrin Inclusion Complex for Improving Physicochemical and Biological Performance"
date: 2024-09-02
tags: [High-performance liquid chromatography,Tetrahydrocannabinolic acid,Detection limit,Cannabinoid,Differential scanning calorimetry,Tetrahydrocannabinol,Nuclear magnetic resonance spectroscopy,Nuclear magnetic resonance,Solubility,Spray drying,Cannabidiol,Resazurin,X-ray crystallography,Freeze drying,PH,Chemistry,Physical sciences]
author: Chulhun Park | Jieyu Zuo | Myung-Chul Gil | Raimar Löbenberg | Beom-Jin Lee | Myung-Chul | Beom-Jin
---


Regardless of preparation methods for CD inclusion complexes, the solubilities of the cannabinoid acids decreased when the M-β-CD had a relatively higher molar ratio of 1 cannabinoid acid to 5 CD molecules. At the same molar ratio (1:2 = cannabinoid acid: M-β-CD molecule), the THCA solubility in the spray-freeze-dried CD complex showed 4.2-fold higher than that of the freeze-dried CD complex. As a result, the spray-freeze-drying method was the most effective technique for stabilizing cannabinoid acids and improving their aqueous solubility in the CD inclusion complex. The IC50 (mean concentration that indicated 50% cell inhibition) of cannabinoid acid was 17.1 ± 1.2 μg/mL (THCA) and 14.7 ± 1.1 μg/mL (CBDA), whereas the IC50 of cannabinoid acid/M-β-CD complex decreased to 3.5 ± 1.1 μg/mL (THCA) and 4.3 ± 1.1 μg/mL (CBDA), respectively. pH-dependent solubility of (A) THCA and (B) CBDA with M-β-cyclodextrin complex; Figure S1: Enlarged version of 1H NMR spectra with (A) M-β-CD, (B) THCA/M-β-CD Spray-freeze-dried inclusion complex, (C) THCA/M-β-CD freeze dried inclusion complex, (D) CBDA/M-β-CD Spray-freeze-dried inclusion complex, and (E) CBDA/M-β-CD freeze dried inclusion complex.

[Visit Link](https://www.mdpi.com/1999-4923/15/11/2533){:target="_blank rel="noopener"}

