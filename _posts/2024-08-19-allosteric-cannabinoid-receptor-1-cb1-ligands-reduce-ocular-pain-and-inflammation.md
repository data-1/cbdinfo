---
layout: post
title: "Allosteric Cannabinoid Receptor 1 (CB1) Ligands Reduce Ocular Pain and Inflammation"
date: 2024-08-19
tags: [Allosteric regulation,Allosteric modulator,Cannabinoid receptor 1,Cornea,Cannabinoid,TRPV1,Cannabinoid receptor 2,Endocannabinoid system,Agonist]
author: Dinesh Thapa | Elizabeth A. Cairns | Anna-Maria Szczesniak | Pushkar M. Kulkarni | Alex J. Straiker | Ganesh A. Thakur | Melanie E. M. Kelly | Elizabeth A | Anna-Maria | Pushkar M
---


Therefore, together with previous data [28], we have further provided evidence to support that activation CB1 may be a good target for corneal neuropathic pain management by direct modulation of the sensation of pain, and the inflammatory response which may lead to sensitization over time. [Google Scholar] [CrossRef] [PubMed]  Howlett, A.C. Cannabinoid Receptor Signaling. Molecules 2020, 25, 417. https://doi.org/10.3390/molecules25020417  AMA Style  Thapa D, Cairns EA, Szczesniak A-M, Kulkarni PM, Straiker AJ, Thakur GA, Kelly MEM. 2020; 25(2):417. https://doi.org/10.3390/molecules25020417  Chicago/Turabian Style  Thapa, Dinesh, Elizabeth A. Cairns, Anna-Maria Szczesniak, Pushkar M. Kulkarni, Alex J. Straiker, Ganesh A. Thakur, and Melanie E. M. Kelly. Allosteric Cannabinoid Receptor 1 (CB1) Ligands Reduce Ocular Pain and Inflammation Molecules 25, no.

[Visit Link](https://www.mdpi.com/1420-3049/25/2/417){:target="_blank rel="noopener"}

