---
layout: post
title: "Self-Assembly System Based on Cyclodextrin for Targeted Delivery of Cannabidiol"
date: 2022-09-05
categories:
- CBD
author: Zhu, Faculty Of Life Science, Technology, Kunming University Of Science, Lv, Industrial Crop Research Institute, Yunnan Academy Of Agricultural Sciences, Zhang, Liao, Pharmacy Department
tags: [X-ray crystallography,Cannabidiol,Differential scanning calorimetry,Chemical shift,Nuclear magnetic resonance,Nuclear magnetic resonance spectroscopy,Two-dimensional nuclear magnetic resonance spectroscopy,PH,Hostguest chemistry,Solubility,Titration,Cannabinoid,Biotin,Cancer,Tetrahydrocannabinol,Buffer solution,Experiment,Infrared spectroscopy,Cell (biology),Solid,Coordination complex,Cell culture,Chemistry,Physical sciences,Physical chemistry]
---


The characterization and inclusion behavior of inclusion complex of amantadine conjugated cannabinoid (AD-CBD) and biotin-ethylenediamine-β-cyclodextrin (BIO-CD) in solution and solid state were studied by various methods, while the water solubility and anti-tumor activity of the inclusion complex were tested in our study. MTT (3-(4,5-dimethylthiazole)-2-yl)-2,5-diphenyltetrazolium bromide) analysis was performed using HeLa, HepG2, A549, and LO2 cell lines to evaluate the cytotoxicity of BIO-CD, AD-CBD, and the inclusion complex of BIO-CD and AD-CBD in vitro. Then new culture medium containing different concentrations of CBD, AD-CBD, BIO-CD, AD-CBD/BIO-CD were added to the cells and continued to cultivate for 48 h. Finally, we used the previous mentioned method to measure the OD value and calculate the IC 50 value of CBD, AD-CBD, BIO-CD, and AD-CBD/BIO-CD. Nevertheless, there was no characteristic peak of AD-CBD, or BIO-CD was observed in the pattern of the inclusion complex, which indicated that the AD-CBD/BIO-CD inclusion complex was formed (Figure 5D). The two cell lines of HeLa cervical cancer and A549 human lung cancer have different biotin receptor expression levels on the cell membranes, thence, they are used as cell models.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fchem.2021.754832/full){:target="_blank" rel="noopener"}


