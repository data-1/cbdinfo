---
layout: post
title: "Therapeutic Potential of Cannabinoid Profiles Identified in Cannabis L. Crops in Peru"
date: 2024-09-02
tags: [Medical cannabis,Cannabinoid,Cannabidiol,Tetrahydrocannabinol,Cannabis]
author: Pedro Wong-Salgado | Fabiano Soares | Jeel Moya-Salazar | José F. Ramírez-Méndez | Marcia M. Moya-Salazar | Alfonso Apesteguía | Americo Castro | Wong-Salgado | Moya-Salazar | Ramírez-Méndez
---


by  Pedro Wong-Salgado  1,2,3,* ,  Fabiano Soares  2,3,4 ,  Jeel Moya-Salazar  5,* ,  José F. Ramírez-Méndez  1,3 ,  Marcia M. Moya-Salazar  3,6 ,  Alfonso Apesteguía  7 and  Americo Castro  2  1  CANNAVITAL, Clínica Especializada en Terapias con Cannabinoides, Lima 15022, Peru  2  RENATU Research Group, Faculty of Pharmacy and Biochemistry, Universidad Nacional Mayor de San Marcos, Lima 15039, Peru  3  Centro de Estudios del Cannabis del Perú, Lima 15022, Peru  4  REAJA Laboratory, Curitiba 80045-180, Brazil  5  Faculties of Health Science, Universidad Privada del Norte, Lima 15001, Peru  6  Cannabis and Stone Unit, Nesh Hubbs, Lima 15001, Peru  7  Centro de Información, Control Toxicológico y Apoyo a la Gestión Ambiental CICOTOX, Faculty of Pharmacy and Biochemistry, Universidad Nacional Mayor de San Marcos, Lima 15039, Peru  *  Authors to whom correspondence should be addressed. In this study, samples were only accessible from 4 out of the 24 regions in Peru. Conclusions  Cannabis cultivars, that existed before Peruvian medicinal regulation, have potential therapeutic effects. Due to the variety in cannabinoid chemotypes found in cannabis samples from four different regions, these potential therapeutic effects can vary according to the concentrations of the cannabinoids that show major evidence of therapeutic effects, like THC, CBD, and CBG. Therapeutic Potential of Cannabinoid Profiles Identified in Cannabis L. Crops in Peru Biomedicines 12, no.

[Visit Link](https://www.mdpi.com/2227-9059/12/2/306){:target="_blank rel="noopener"}

