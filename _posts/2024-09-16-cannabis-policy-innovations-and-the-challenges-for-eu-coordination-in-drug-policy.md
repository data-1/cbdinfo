---
layout: post
title: "Cannabis Policy Innovations and the Challenges for EU Coordination in Drug Policy"
date: 2024-09-16
tags: [European Union,Politics]
author: Constanza Sánchez Avilés
institutions: International Council for the Exploration of the Sea
doi: "https://doi.org/10.1017/aju.2020.60"
---


In the past decades, Europe has been the site of multiple drug policy innovations and cannabis policy experimentation. Most of the policy reforms in this area are taking place at the national and subnational levels. They have not resulted from agreements or harmonization initiatives at the EU level, but rather from bottom-up adoption of cannabis policies and practices that deviate from punitive approaches. Cannabis regulation has been only marginally debated by European institutions and is not discussed in depth in any EU document. On the one hand, the diversity of European drug policies is a positive development: it serves as a policy laboratory and reflects adaptations to local contexts in light of drug market-related challenges.