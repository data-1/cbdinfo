---
layout: post
title: "Cannabis use and the sperm epigenome: a budding concern?"
date: 2024-09-16
tags: [Epigenetics,DNA methylation,Cannabis (drug),Genomic imprinting,Reprogramming,Cannabidiol,CpG site,Gene expression,Substance abuse,Genetics,Biology]
author: Rose Schrott | Susan K. Murphy
institutions: Duke Medical Center | Duke University | Duke University Hospital
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7081939"
---


Even in states without legalized cannabis use, CBD is available in enormous numbers of consumer products. Effects of Male Cannabis Use on DNA Methylation: Human Studies  To date, only one study, published by our group, has reported on the effects of adult male cannabis use on the human sperm DNA methylome [55]. There were significant effects of cannabis use on sperm DNA methylation, with 3979 CpG sites that were differentially methylated in the sperm of cannabis users compared to the nonuser controls [55]. A significant loss of methylation was detected at the same CpG sites in the nucleus accumbens as in the sperm of the exposed dads, beginning to suggest the potential for a paternal exposure that alters the epigenetic profile of sperm to contribute to the epigenetic profile of the offspring [61]. Little information is known about the potential effects of e-cigarette use on the health, development and epigenetic profile of the individual and the potential effects on their offspring.