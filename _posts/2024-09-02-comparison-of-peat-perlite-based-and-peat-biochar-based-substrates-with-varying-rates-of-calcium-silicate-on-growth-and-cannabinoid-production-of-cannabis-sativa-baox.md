---
layout: post
title: "Comparison of Peat–Perlite-based and Peat–Biochar-based Substrates with Varying Rates of Calcium Silicate on Growth and Cannabinoid Production of Cannabis sativa ‘BaOx’"
date: 2024-09-02
tags: [Biochar,Cutting (plant),Plant nutrition,Hydroponics,Hemp,Botrytis cinerea,Soil,Cannabidiol,Horticulture,Cannabis,Cannabis sativa,Botany]
author: Patrick Veazie | M. Seth Balance | Brian E. Whipker | Ka Yeon Jeong
---


However, the pH values of biochar materials reportedly range from 3.5 to 10.3 (Fornes et al. 2015; Khodadad et al. 2011; Nemati et al. 2015; Spokas et al. 2012) and may potentially neutralize acidity caused by peat and root growth (Bedussi et al. 2015). Silicon supplementation in greenhouse crops can be achieved in multiple ways, including foliar applications (Kamenidou et al. 2009; Whitted-Haag et al. 2014), incorporation of Si in hydroponic nutrient solution (Bolt and Altland 2021; Mattson and Leatherwood 2010), or Si substrate amendments (Boldt et al. 2018; Kamenidou et al. 2010). Substrate and plant analysis. Foliar nutrient concentrations. HR 2: Agriculture Improvement Act of 2018. https://www.congress.gov/bill/115th-congress/house-bill/ 2/text.

[Visit Link](https://journals.ashs.org/hortsci/view/journals/hortsci/58/10/article-p1250.xml){:target="_blank rel="noopener"}

