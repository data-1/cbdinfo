---
layout: post
title: "Interaction of Synthetic Cannabinoid Receptor Agonists with Cannabinoid Receptor I: Insights into Activation Molecular Mechanism"
date: 2024-09-02
tags: [G protein-coupled receptor,Ligand (biochemistry),Receptor antagonist,Receptor (biochemistry),Docking (molecular),Cannabinoid receptor 1,Protein structure,Alpha helix,Allosteric regulation,Cannabinoid receptor 2,Protein,Allosteric modulator,Cannabinoid,Agonist,Molecular dynamics,Nutrients,Neurochemistry,Molecular biology,Cell biology,Biochemistry]
author: Sergei Gavryushov | Anton Bashilov | Konstantin V. Cherashev-Tumanov | Nikolay N. Kuzmich | Tatyana I. Burykina | Boris N. Izotov | Cherashev-Tumanov | Konstantin V | Nikolay N | Tatyana I
---


They include the existence of a single active conformation of CB1 for the different agonists bound, a crucial role of the stacking interactions of hydrophobic residues of TM2 with the ligand, a deep hydrophobic pocket in the protein structure to bind the ligand aliphatic chain, a possible role of the GPCR “toggle switch” for CB1, and a list of the receptor’s residues interacting with the ligand [8]. If we built a stable model of the 7TM domain of CB1 in an inactive apo from which can be transformed into the receptor’s active conformation at the computer simulations of the agonist ligand binding, it would be possible to explore the nature of the allosteric modulation mechanism via ligand modifications or mutations of the receptor residues. Results of MD simulations of CB1 in modeled apo-receptor conformation with docked agonist AM11542. The residues of this “basement” interact with the tail of the ligand. RMSD between simulated conformations of the receptor and its conformations in active and inactive states from crystal complexes.

[Visit Link](https://www.mdpi.com/1422-0067/24/19/14874){:target="_blank rel="noopener"}

