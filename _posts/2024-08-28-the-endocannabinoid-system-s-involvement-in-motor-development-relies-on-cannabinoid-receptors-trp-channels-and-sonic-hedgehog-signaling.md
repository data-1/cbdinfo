---
layout: post
title: "The endocannabinoid systemʼs involvement in motor development relies on cannabinoid receptors, TRP channels, and Sonic Hedgehog signaling"
date: 2024-08-28
tags: [Endocannabinoid system,Cannabinoid receptor 1,Fatty-acid amide hydrolase 1,Sonic hedgehog protein,Cannabinoid,Cannabinoid receptor 2,2-Arachidonoylglycerol,Branches of neuroscience,Neuroscience,Biology,Cell communication,Signal transduction,Basic neuroscience research,Neurochemistry,Cell biology,Biochemistry,Cell signaling,Neurophysiology]
author: T. M | G. J | K. P | S. E | E. W | L. B | K. E | Boa-Amponsem | Mendoza-Romero | H. N
---


However, cannabinoids interact with receptors other than CB1R and CB2R and thus motor development may be impacted through the interactions of eCBs with non-canonical cannabinoid receptor systems. For instance, the CB1R is important for the development of hindbrain reticulospinal neurons in zebrafish, which are crucial for sensorimotor function (Watson et al., 2008). Therefore, it is clear that while the eCS and SHH pathways play important roles in the neurobiology of locomotor activities, our findings here suggest that their influences likely converge when it comes to functional locomotor development. Overall, the recent findings suggesting that SHH signaling is a mechanism of action for cannabinoids support our observations of an eCS-SMO interaction in the motor development of zebrafish (Boa-Amponsem et al., 2019; Fish et al., 2019). Given the locomotor behavioral recoveries that were mediated through certain receptor systems in the current study, it will also be useful to ascertain the involvement of GPR55 and FGF signaling when studying the role of the eCS in neurobiological and motor development.

[Visit Link](https://physoc.onlinelibrary.wiley.com/doi/10.14814/phy2.15565){:target="_blank rel="noopener"}

