---
layout: post
title: "Influence of Cannabinoid Treatment on Trajectories of Patient-Related Outcomes in Chronic Pain: Pain Intensity, Emotional Distress, Tolerability and Physical Disability"
date: 2024-08-29
tags: [Cannabinoid receptor 2,Pain,Pain management,Cannabidiol,Cannabinoid receptor 1,Mental disorder,Chronic condition,Chronic pain,Receptor (biochemistry),Opioid,Major depressive disorder,Health,Clinical medicine,Medicine,Health care]
author: Anna Marie Balestra | Katharina Chalk | Claudia Denke | Nashwan Mohammed | Thomas Fritzsche | Sascha Tafelski | Anna Marie
---


Pain 2003, 106, 337–345. Pain 1992, 50, 133–149. Pain 2022, 26, 1221–1233. Pain 2016, 17, 739–744. Pain 2022, 26, 310–335.

[Visit Link](https://www.mdpi.com/2076-3425/13/4/680){:target="_blank rel="noopener"}

