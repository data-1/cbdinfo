---
layout: post
title: "Research progress on the cannabinoid type-2 receptor and Parkinson’s disease"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Endocannabinoid system,Anandamide,Cannabinoid,Neurodegenerative disease,Neuroinflammation,Cannabinoid receptor 1,Mitochondrion,Microglia,Biology,Neurochemistry,Biochemistry,Cell biology]
author: 
---


Activation of this receptor in animal models demonstrate disease-modifying effects against the process of neurodegeneration, suggesting CB2 receptor is a promising therapeutic target for the treatments of such disease. Further research demonstrates neuroprotective potentials of CB2 receptor in PD. These results indicate that activation of CB2 receptor can inhibit oxidative stress and protect neuronal cells. It has been reported that iron levels in the SN of PD patients increase significantly. Selective activation of CB2 receptor regulates mitochondrial function, inhibits oxidative stress, suppresses the release of inflammatory factors, and involves in various regulations such as iron transport, electrophysiology, and autophagy.

[Visit Link](https://www.frontiersin.org/journals/aging-neuroscience/articles/10.3389/fnagi.2023.1298166/full){:target="_blank rel="noopener"}

