---
layout: post
title: "High Expression of Cannabinoid Receptor 2 on Cytokine-Induced Killer Cells and Multiple Myeloma Cells"
date: 2024-08-20
tags: [Cytokine-induced killer cell,Natural killer cell,T cell,Cannabinoid receptor 2,Lymphocyte,Cannabinoid,Natural killer T cell,NKG2D,Cell signaling,Neurochemistry,Cell biology,Immune system,Biology,Medical specialties,Immunology,Biochemistry]
author: Francesca Garofano | Schmidt-Wolf | Ingo G. H
---


CB2 is Detectable by Flow Cytometry on Days 7 and 14 in CIK Cells  Initial studies were performed to assess the differential expression of CB2 receptor in the main cell subsets of human CIK cells, i.e., CD3+ T lymphocytes (CD4+ and CD8+), CD56+ NK cells. When CIK cells were activated at day 14, the CB2 expression was dramatically upregulated within the natural killer T (NKT) population CD3+CD56+T cells ( B) 98.1%. CBD Inhibits the Growth of NKT Cells  We performed some studies to assess the differential expression of NKG2D receptor in the main cell subsets of human CIK cells, i.e., CD3+ T lymphocytes (CD4+ and CD8+) and CD56+ NK cells for surface NKG2D expression by flow cytometry. High Expression of Cannabinoid Receptor 2 on Cytokine-Induced Killer Cells and Multiple Myeloma Cells. High Expression of Cannabinoid Receptor 2 on Cytokine-Induced Killer Cells and Multiple Myeloma Cells.

[Visit Link](https://www.mdpi.com/1422-0067/21/11/3800){:target="_blank rel="noopener"}

