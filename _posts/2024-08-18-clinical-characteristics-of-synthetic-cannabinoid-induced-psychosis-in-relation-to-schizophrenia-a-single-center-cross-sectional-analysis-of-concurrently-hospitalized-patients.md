---
layout: post
title: "Clinical characteristics of synthetic cannabinoid-induced psychosis in relation to schizophrenia: a single-center cross-sectional analysis of concurrently hospitalized patients"
date: 2024-08-18
tags: [Psychosis,Schizophrenia,Mental disorder,Synthetic cannabinoids,Cannabis (drug),Cannabidiol,Suicidal ideation,Signs and symptoms,Health,Mental health,Mental disorders,Abnormal psychology,Clinical medicine]
author: Merih Altintas | Leman Inanc | Gamze Akcay Oruc | Selim Arpacioglu | Huseyin Gulec
---


Hence, adolescent cannabinoid exposure is considered likely to be a risk factor for developing psychosis symptoms in adulthood.45  Hence, our findings support the evidence regarding the link between cannabis use and psychosis along with similarities between SC and schizophrenia-induced cognitive impairment.22,42  Presence of suicidal ideation in one-third and involuntary hospitalizations in more than half of SC-induced psychosis patients in our cohort seems consistent with the nature of SC-induced psychosis and the fact that involuntary hospitalization is mostly caused by the circumstances where the patient has lost his/her insight and discernment, the severity of psychosis is relatively stronger, and suicidal or homicidal ideations or rejection of treatment are evident. Synthetic cannabinoid intoxication: a case series and review. European Monitoring Centre for Drugs and Drug Addiction (EMCDDA) Understanding the “Spice” Phenomenon. Turk J Psychiatry Psychol Psychopharmacol. Turk J Psychiatry Psychol Psychopharmacol.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4977070/){:target="_blank rel="noopener"}

