---
layout: post
title: "Associations Between Prenatal Cannabis Exposure and Childhood Outcomes"
date: 2024-09-16
tags: [Prenatal development,Cannabis (drug),Pregnancy,Cannabis in pregnancy,Dependent and independent variables,Propensity score matching,Confounding,National Institute of Mental Health,Psychosis,Birth weight,Fetus,Preterm birth,PubMed Central,Health]
author: Sarah E. Paul
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7512132/"
---


Of those with reported data, 87.0% of those in the exposure only before maternal knowledge group (287 of 330) and 92.7% of those in the exposure after maternal knowledge group (140 of 151) reported using cannabis at least once per day; groups did not differ in reported frequency of use during pregnancy (t = 0.43; P = .67). Collectively, these findings suggest that prenatal exposure after maternal knowledge of pregnancy may plausibly be independently associated with child outcomes, while associations with exposure only before maternal knowledge of pregnancy may be attributable to confounding variables, such as familial and pregnancy-related factors correlated with cannabis use and/or offspring outcomes. The risks of marijuana use during pregnancy. 2019;76(4):729-743. doi: 10.1007/s00018-018-2955-0 [PMC free article] [PubMed] [CrossRef] [Google Scholar]  Articles from JAMA Psychiatry are provided here courtesy of American Medical Association  1. The risks of marijuana use during pregnancy.