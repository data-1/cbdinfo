---
layout: post
title: "Spatial distribution of cannabinoid receptor type 1 (CB1) in normal canine central and peripheral nervous system"
date: 2024-08-18
tags: [Cannabinoid receptor 1,Dentate gyrus,Cerebral cortex,Cerebellum,Schwann cell,Endocannabinoid system,Cannabinoid receptor 2,Synapse,Hippocampus proper,Astrocyte,Oligodendrocyte progenitor cell,Anandamide,Trigeminal nerve,Oligodendrocyte,Granule cell,Central nervous system,Grey matter,Cannabinoid receptor,Cannabinoid,Hippocampus,Neuron,Basic neuroscience research,Neuroscience]
author: Jessica Freundt-Revilla | Kristel Kegler | Wolfgang Baumgärtner | Andrea Tipold
---


In mammalian tissues, two main subtypes of cannabinoid receptors, the cannabinoid receptor 1 (CB1) and cannabinoid receptor 2 (CB2), which are G protein-coupled receptors, have been recognized [5, 8] and are responsible for the transduction of different effects of ECs [9]. Pharmacology of cannabinoid CB1 and CB2 receptors. Immunohistochemical distribution of cannabinoid CB1 receptors in the rat central nervous system. Pharmacology of cannabinoid CB1 and CB2 receptors. Immunohistochemical distribution of cannabinoid CB1 receptors in the rat central nervous system.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5507289/){:target="_blank rel="noopener"}

