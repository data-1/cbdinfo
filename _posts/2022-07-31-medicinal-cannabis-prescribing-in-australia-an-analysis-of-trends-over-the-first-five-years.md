---
layout: post
title: "Medicinal Cannabis Prescribing in Australia: An Analysis of Trends Over the First Five Years"
date: 2022-07-31
categories:
- Cannabis
author: Macphail, Sara L., The Lambert Initiative For Cannabinoid Therapeutics, School Of Psychology, Bedoya-Pérez, Miguel A., Cohen, School Of Social Sciences, Cannabis Consulting Australia, Kotsirilos
tags: [Cannabidiol,Errors and residuals,Regression analysis,Therapeutic Goods Administration,Disease,Medical prescription,Data analysis,Medical cannabis,Health,Signs and symptoms,Cannabinoid,Academic degree,Categorical variable,Tetrahydrocannabinol,Curve fitting,Akaike information criterion,Medical diagnosis,Prescription drug,Medicine,Physician,Pain]
---


Most patient access, however, is through the Special Access Scheme Category B (SAS-B) which allows practitioners to make an application to the TGA to allow an individual patient to be prescribed a certain type of MC product to treat a specific condition (Therapeutic Goods Administration, 2020b). The data received via FOI request were supplemented (where specified) with data from the new TGA Medicinal Cannabis Access Data Dashboard, which contains publicly available SAS-A and SAS-B data summaries (Therapeutic Goods Administration, 2021a). 3.1 Approved SAS-B Applications Over Time  The number of SAS-B approvals has been increasing in a non-linear pattern over time (3rd degree polynomial, R2 = 0.998, Δm = 44.398) with a dramatic increase in the last 2 years of analysis. Nine ICD-10 indication categories had over 1,000 cumulative approvals in the SAS-B dataset (Figure 3), representing 94.1% of total approvals. Potential Reforms to Medicinal Cannabis Manufacturing, Labelling and Packaging Requirements [Online].

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2022.885655/full){:target="_blank" rel="noopener"}


