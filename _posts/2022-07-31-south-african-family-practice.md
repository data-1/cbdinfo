---
layout: post
title: "South African Family Practice"
date: 2022-07-31
categories:
- cannabinoid
author: Lehlohonolo Makhakhe, Department Of Dermatology, Faculty Of Health Science, University Of The Free State, Bloemfontein, South Africa, The South African Institute Of Dermatology
tags: [Cannabinoid,Tetrahydrocannabinol,Sebaceous gland,Cannabidiol,Cannabinoid receptor,Cannabinoid receptor type 2,Inflammation,Epidermis,Keratinocyte,Wound healing,Medicine,Clinical medicine,Medical specialties]
---


Keywords: cannabis; dermatology; new modality of treatment; anti-inflammatory; topical usage. FIGURE 1: Benefits of cannabidiol for your skin.9  Sebaceous gland related disorders Acne and rosacea Acne is a common skin disease characterised by elevated sebum production and inflammation of the sebaceous glands. Endocannabinoids seem to have a double effect on human melanocytes: (1) to induce melanogenesis at low concentrations through CB 1 receptors (vitiligo and other depigmentation skin disorders) and (2) to induce apoptosis at high concentrations through TRPV1 receptors in treating melanomas and melasma.5,15,16,17 Wound healing properties Considering that cannabinoid signalling regulates fibroblast functions, proliferation and differentiation of epidermal keratinocytes, as well as cutaneous inflammation, it is not surprising that it also influences the complex process of cutaneous wound healing. Cannabinoid receptor 2 is expressed mainly on leukocytes and is the receptor implicated in mediating many of the effects of cannabinoids on immune processes. https://doi.org/10.1016/S0304-3959(97)00213-3

<hr>[Visit Link](https://safpj.co.za/index.php/safpj/article/view/5493/7379){:target="_blank" rel="noopener"}


