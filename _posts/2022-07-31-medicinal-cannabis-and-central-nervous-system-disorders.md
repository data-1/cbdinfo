---
layout: post
title: "Medicinal Cannabis and Central Nervous System Disorders"
date: 2022-07-31
categories:
- Cannabinoid
author: Ortiz, Yuma T., Department Of Pharmacodynamics, United States, Mcmahon, Lance R., Department Of Pharmaceutical Sciences, Wilkerson, Jenny L., Shimon Ben -Shabat
tags: [Cannabidiol,Medical cannabis,Cannabinoid,Tetrahydrocannabinol,Amyotrophic lateral sclerosis,Opioid,Cannabinoid receptor type 2,Addiction,Multiple sclerosis,Nabiximols,Pain,Self-administration,Neuropathic pain,Neurodegenerative disease,Peripheral neuropathy,Serotonin,Dose (biochemistry),Alcohol (drug),Anxiety disorder,Pain management,Cannabinoid receptor,Route of administration,Bioavailability,Mental disorder,Opioid use disorder,Nasal administration,Epilepsy,Nicotine,5-HT1A receptor,Substance dependence,Dronabinol,Clinical trial,Pharmaceutical formulation,Experimental autoimmune encephalomyelitis,Substantia nigra,Dopamine,Analgesic,Selective serotonin reuptake inhibitor,Hydromorphone,Placebo,Generalized anxiety disorder,Morphine,Striatum,Neuroscience,Health sciences,Medical specialties,Medicine,Health,Diseases and disorders,Clinical medicine]
---


As medicinal cannabis related clinical research has predominately focused on CNS-related diseases, such as neurodegeneration and neurological disorders, pain, substance use disorders, and anxiety disorders, this review will first examine evidence that supports or refutes the therapeutic utility of cannabinoids for the treatment of neurodegenerative disease, pain, mood disorders, and substance use disorders. Preclinical research studying the effects of THC in PD models have reported potential neuroprotective effects. 2.2.3 Clinical Trials in Pain  Despite these preclinical studies suggesting therapeutic potential of cannabinoids as analgesics, review of recent clinical trials in various pain pathologies suggests an inconclusive viability of cannabinoids as a therapeutic for pain. Clinical research utilizing cannabinoids within instances of neurodegenerative disease, pain, addiction, and anxiety suggest both tolerability and therapeutic potential either alone or in combination with current therapeutics. Abbreviations  AD, alzheimer’s disease; ALS, amyotrophic lateral sclerosis; CBD, cannabidiol; CB1R, cannabinoid 1 receptor; CB2R, cannabinoid 2 receptor; CNS, central nervous system; GAD, generalized anxiety disorder; HD, Huntington’s disease; MS, multiple sclerosis; OCD, obsessive-compulsive disorder; PD, Parkinson’s disease; PTSD, post-traumatic stress disorder; 5-HT1A, serotonin 1a; SEDDS, Self-emulsifying drug delivery system; SSRIs, selective serotonin reuptake inhibitors; SAD, social anxiety disorder; THC, Δ9-tetrahydrocannabinol.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2022.881810/full){:target="_blank" rel="noopener"}


