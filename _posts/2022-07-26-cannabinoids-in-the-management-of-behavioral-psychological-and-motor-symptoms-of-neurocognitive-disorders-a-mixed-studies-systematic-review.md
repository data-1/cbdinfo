---
layout: post
title: "Cannabinoids in the management of behavioral, psychological, and motor symptoms of neurocognitive disorders: a mixed studies systematic review"
date: 2022-07-26
categories:
- Cannabinoid
author: Bahji, Department Of Psychiatry, University Of Calgary, University Drive Nw, Calgary, Breward, Cannabinoid Research Initiative Of Saskatchewan, Cris, Saskatchewan, College Of Pharmacy
tags: [Sensitivity and specificity,Machine learning,Cross-validation (statistics),Assay,Receiver operating characteristic,Cannabinoid receptor type 1,Tetrahydrocannabinol,Prediction,Cannabinoid]
---


Ultimately, 25 studies (Ahmed et al. 2015; Balash et al. 2017; Bruce et al. 2018; Carroll et al. 2004; Chagas et al. 2014a; Chagas et al. 2014b; Consroe et al. 1991; Curtis et al. 2009; Herrmann et al. 2019; Lopez-Sendon Moreno et al. 2016; Lotan et al. 2014; Mahlberg and Walther 2007; Mesnage et al. 2004; Shelef et al. 2016; Shohet et al. 2017; Sieradzan et al. 2001; van den Elsen et al. 2015a; van den Elsen et al. 2015b; van den Elsen et al. 2017; Venderova et al. 2004; Volicer et al. 1997; Walther et al. 2006; Walther et al. 2011; Woodward et al. 2014; Zuardi et al. 2009) met inclusion criteria for the review (Fig. Of these, none reported safety as the primary outcome, and only one of the PD studies reported dementia symptoms, measured using the Brief Psychiatric Rating Scale (BPRS), which was initially developed to assess symptom domains in schizophrenia, but has been used in AD/dementia clinical trials (e.g., (Sultzer et al. 2008)). Finally, a ‘fair’ quality, open-label study indicated four weeks of CBD improved the BPRS score (improved psychotic symptoms, without any effect on motor symptoms) in six PD patients (Zuardi et al. 2009). Two of these studies reported AEs, and two reported on the Neuropsychiatric Inventory (NPI) as the primary outcome. The remaining two studies included patients with dementia and patients with chronic diseases that use medical cannabis.

<hr>[Visit Link](https://academic.oup.com/clinchem/article/68/7/906/6528321){:target="_blank" rel="noopener"}


