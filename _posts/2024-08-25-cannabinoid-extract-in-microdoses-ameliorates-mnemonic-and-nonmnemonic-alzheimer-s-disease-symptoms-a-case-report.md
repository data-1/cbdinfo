---
layout: post
title: "Cannabinoid extract in microdoses ameliorates mnemonic and nonmnemonic Alzheimer’s disease symptoms: a case report"
date: 2024-08-25
tags: [Cannabinoid,Dementia,Dose (biochemistry),Cannabidiol,Tetrahydrocannabinol,Alzheimer's disease,Endocannabinoid system,Cannabinoid receptor 2,Neurology,Neuroinflammation,Medicine,Medical specialties,Health,Clinical medicine]
author: Ruver-Martins | Ana Carolina | Maíra Assunção | de Araujo | Fabiano Soares | de Noronha Sales Maia | Beatriz Helena Lameiro | Fabrício Alano | da Silva | Elton Gomes
---


The endocannabinoid system consists of endocannabinoid molecules, enzymes, and CB1R and CB2R (Gi-coupled) receptors. Herein, we describe the beneficial effect of an orally administered phytocannabinoids extract (8:1; THC:CBD ratio) on mnemonic and nonmnemonic symptoms in one patients with AD, as evaluated by Mini-Mental State Examination (MMSE) and Alzheimer’s Disease Assessment Scale-Cognitive Subscale (ADAS-Cog). AD history  The patient was diagnosed with AD 2 years prior to the start of this experimental treatment, according to brain magnetic resonance imaging, anamnesis, and clinical assessment, which includes and is not limited to the use of National Institute of Neurologic and Communicative Disorders and Stroke, and the Alzheimer Disease and Related Disorders Association (NINCDS-ADRDA) criteria. This case report describes the therapeutic effect of cannabinoids microdosing using a THC-rich extract for the treatment of mnemonic and nonmnemonic symptoms of a patient with AD. It is remarkable how a dose so significantly lower than those previously reported is able to consistently improve cognitive and noncognitive AD symptoms.

[Visit Link](https://jmedicalcasereports.biomedcentral.com/articles/10.1186/s13256-022-03457-w){:target="_blank rel="noopener"}

