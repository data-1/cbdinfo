---
layout: post
title: "Effect of augmented nutrient composition and fertigation system on biomass yield and cannabinoid content of medicinal cannabis (Cannabis sativa L.) cultivation"
date: 2024-09-02
tags: [Plant nutrition,Cannabinoid,Soil,Phosphate,Hydroponics,Cannabis,Root,Medical cannabis]
author: Josef Baltazar
---


Comparing the leaves and flowers of CN and ET plants under the augmented nutritional regime in the 1C cycle, statistically significant differences in N concentrations were observed only twice in the leaves (6th and 13th week) and twice in the stems (8th and 13th week). Notably, the most significant variation in P concentration between nutritional treatments was observed in the leaves during the 13th week, with a difference of 63% (CN: 63.48 mg·g−1, ET: 38.9 mg·g−1; Figure 3A). In contrast to the 1C cycle, the concentration of Fe in the stems of CN and ET plants in the 2C cycle exhibited significant differences only in week 10, in the leaves merely in weeks 6 and 10, and in the flowers from the 7th until the 8th week. In the 1C cycle, the biomass increases in stems, leaves, and flowers for both CN and ET plants was nearly identical until week 4. When comparing CN with ET in the 2C fertigation system, statistically significant differences were observed only during the 11th week of cultivation.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2024.1322824/full){:target="_blank rel="noopener"}

