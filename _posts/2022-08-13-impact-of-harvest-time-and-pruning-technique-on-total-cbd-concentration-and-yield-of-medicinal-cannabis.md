---
layout: post
title: "Impact of Harvest Time and Pruning Technique on Total CBD Concentration and Yield of Medicinal Cannabis"
date: 2022-08-13
categories:
- Cannabis
author: Crispim Massuela, Hartung, Munz, Erpenbach, Graeff-Hönninger, Danilo Crispim Massuela, Jens Hartung, Sebastian Munz, Federico Erpenbach, Simone Graeff-Hönninger
tags: [High-performance liquid chromatography,Cannabis sativa,Cannabidiol,Cannabis,Biomass,Cannabinoid,Cannabis (drug),Medical cannabis,F-test,Plant,Meristem,Tetrahydrocannabinolic acid,Chromatography,Inflorescence,Cannabis cultivation,Botany]
---


In both references, topping generated a higher number of side shoots, thus more top terminal inflorescence biomass than the control plants (not pruned). The significantly higher biomass of inflorescences and leaves from T plants ( Table 2 ) did not result in significantly higher total CBD yield ( Table 3 ). For the total CBD concentration, the inflorescence position showed significant differences for the top fraction (9.9%), which was significantly higher than mid (8.2%) and low (7.4%) fractions ( Table 5 ). In our study, the observed significantly higher total CBD concentrations in the top fractions are not confirmed for total CBD yield, mainly due to a higher biomass of inflorescences in the mid-fraction. Management intensity may be reduced with unpruned plants; however, cultivators need to keep in mind the accumulation of inflorescences in the lower fractions, which contain lower levels of total CBD concentration and yield.

<hr>

[Visit Link](https://www.mdpi.com/2223-7747/11/1/140/htm){:target="_blank" rel="noopener"}


