---
layout: post
title: "Omega-6 and omega-3 fatty acids: Endocannabinoids, genetics and obesity"
date: 2024-09-16
tags: [Omega-3 fatty acid,Free fatty acid receptor 4,Adipose tissue,Eicosapentaenoic acid,Polyunsaturated fat,Obesity,Endocannabinoid system,Fat,Cannabinoid,Fatty acid desaturase,Adipocyte,Docosahexaenoic acid,Insulin resistance,Anandamide,Cannabinoid receptor 2,Fatty acid,2-Arachidonoylglycerol,Biochemistry,Nutrients]
author: Artemis P. Simopoulos
institutions: 
doi: "https://www.ocl-journal.org/articles/ocl/full_html/2020/01/ocl190046s/ocl190046s.html"
---


Unfortunately, the study did not include the omega-6 fatty acid intake or the ratio of LA/ALA, AA/EPA or AA/EPA and DHA. Kabir et al. (2007), in 2007, in studying the effect of fish oil on adipose tissue, noted that 1.8 g of EPA and DHA decreased the total omega-6/omega-3 ratio in the plasma phospholipids from 12.9 ± 1.1 to 5.6 ± 0.7; total fat mass (P < 0.019) and subcutaneous adipocyte diameter (P < 0.0018) were lower in the group receiving 1.8 g of EPA and DHA than in the placebo group. Increasing the dietary LA from 1 to 8% of energy increased liver endocannabinoid levels, which increased the risk of developing obesity, even in a low-fat diet. Considering the high omega-6/omega-3 fatty acid ratio of Western diets and the role of AA in adipose cell differentiation, proliferation, and decreasing browning of white adipose tissue, further research should include studies on the effects of omega-3 fatty acids in blocking the effects of the risk allele (rs 1421085), which appears to be responsible for the association between the first intron of FTO gene and obesity in humans. LA metabolizes to AA, which produces AEA and 2-AG that lead to hyperactivity of the endocannabinoid system increasing appetite and food intake.