---
layout: post
title: "Activation of Cannabinoid Receptor 2 Prevents Colitis-Associated Colon Cancer through Myeloid Cell De-activation Upstream of IL-22 Production"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Dendritic cell,Antigen-presenting cell,Regulatory T cell,Clinical medicine,Human biology,Biology,Cell biology,Immune system,Immunology,Medical specialties]
author: 
---


Intestinal disequilibrium leads to inflammatory bowel disease (IBD), and chronic inflammation predisposes to oncogenesis. Antigen-presenting dendritic cells (DCs) and macrophages can tip the equilibrium toward tolerance or pathology. Working through cannabinoid receptor 2 (CB2), THC increases CD103 expression on DCs and macrophages and upregulates TGF-β1 to increase T regulatory cells (Tregs). By examining tissues from multiple sites, we confirmed that THC affects DCs, especially in mucosal barrier sites in the colon and lungs, to reduce DC CD86. Using models of colitis and systemic inflammation we show that THC, through CB2, is a potent suppressor of aberrant immune responses by provoking coordination between APCs and Tregs.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2589004220306969){:target="_blank rel="noopener"}

