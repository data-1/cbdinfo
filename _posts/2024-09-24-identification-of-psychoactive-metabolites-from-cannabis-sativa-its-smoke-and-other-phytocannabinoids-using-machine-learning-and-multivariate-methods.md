---
layout: post
title: "Identification of Psychoactive Metabolites from Cannabis sativa, Its Smoke, and Other Phytocannabinoids Using Machine Learning and Multivariate Methods"
date: 2024-09-24
tags: [Hydrogen bond,Cannabinoid,Tetrahydrocannabinol,Cannabidiol,Cannabis,Cannabis sativa,Training, validation, and test data sets,Cannabinoid receptor 2,Medical cannabis]
author: Jagannathan Ramesh
institutions: Ontario Medical Association
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/6964292"
---


[PMC free article] [PubMed] [CrossRef] [Google Scholar]  U.S. Department of Health and Human Services . [PMC free article] [PubMed] [CrossRef] [Google Scholar]  Safwat A. [PMC free article] [PubMed] [CrossRef] [Google Scholar] [Ref list]  U.S. Department of Health and Human Services . [PubMed] [CrossRef] [Google Scholar] [Ref list]  Lowry J. [Google Scholar] [Ref list]