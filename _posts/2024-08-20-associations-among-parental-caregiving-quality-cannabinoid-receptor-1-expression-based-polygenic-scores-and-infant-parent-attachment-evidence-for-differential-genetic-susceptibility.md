---
layout: post
title: "Associations Among Parental Caregiving Quality, Cannabinoid Receptor 1 Expression-Based Polygenic Scores, and Infant-Parent Attachment: Evidence for Differential Genetic Susceptibility?"
date: 2024-08-20
tags: [Attachment theory,Attachment in children,Differential susceptibility,Endocannabinoid system,Reward system,Maternal sensitivity,Dependent and independent variables,Memory,Regression analysis]
author: Potter-Dickey | Patricia P | Gerald F | de Koning | A. P. Jason
---


To the best of our knowledge, this is the first study that seeks to investigate if infant genetic susceptibility interacts with the quality of parental caregiving in predicting attachment patterns using observational measures. The final list included 343 genes for the CNR1 prefrontal gene network, 12 genes for the striatal network, and 175 genes for the hippocampal network. Discussion  This study set out to analyze if parental caregiving qualities (i.e., sensitivity, controlling, and unresponsiveness) interacted with the ePRS for the CNR1 gene networks in the prefrontal cortex, striatum, and hippocampus in predicting the probability of secure or disorganized attachment patterns. However, in the case of the significant interactions between hippocampal CNR1 ePRS and maternal unresponsiveness and controlling in predicting the probability of disorganization, the analyses carried out to confirm differential susceptibility were more suggestive of the diathesis-stress model. Conclusion  To the best of our knowledge, this is the first study that examines the interaction between maternal parental caregiving qualities (i.e., sensitivity, controlling, and unresponsiveness) and children’s ePRS for the CNR1 gene networks in the prefrontal cortex, striatum, and hippocampus in predicting the probability of secure and disorganized attachment patterns in young children.

[Visit Link](https://www.frontiersin.org/journals/neuroscience/articles/10.3389/fnins.2021.704392/full){:target="_blank rel="noopener"}

