---
layout: post
title: "The synthetic cannabinoid 5-fluoro ABICA upregulates angiogenic markers and stimulates tube formation in human brain microvascular endothelial cells"
date: 2024-09-02
tags: [Angiogenesis,Angiopoietin,Vascular endothelial growth factor,MTT assay,Apoptosis,Cannabinoid receptor 2,Western blot,Biology,Cell biology,Biochemistry,Life sciences,Molecular biology,Biological processes,Biotechnology]
author: 
---


The culture medium was supplemented with various components in the endothelial cell growth kit (PCS-110-040) from the American Type Culture Collection. Cells were plated on 96-well microtiter plates at a density of 5 × 103 cells/well and allowed to attach for 24 h. The cells were then exposed to five concentrations of 5-fluoro ABICA (ranging from 0.0001 μM to 1 μM) in triplicate for 24 h. The culture medium was then replaced with serum-free medium containing MTT solution (5 mg/ml) obtained from (HiMedia, Mumbai, India) and incubated for 4 h. Finally, DMSO was added to the cells, which were then agitated for 10 min to dissolve the formazan crystals. Protein quantification was performed according to the instructions of a protein assay kit from Bio-Rad, Hercules, CA, USA. Each experimental condition was replicated three times, and each concentration was applied to triplicate wells. Therefore, 5-fluoro ABICA has the potential to induce brain angiogenesis.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S1658361224000027){:target="_blank rel="noopener"}

