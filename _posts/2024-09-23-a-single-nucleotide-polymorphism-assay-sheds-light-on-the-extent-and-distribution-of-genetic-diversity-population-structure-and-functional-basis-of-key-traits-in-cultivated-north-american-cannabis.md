---
layout: post
title: "A Single Nucleotide Polymorphism assay sheds light on the extent and distribution of genetic diversity, population structure and functional basis of key traits in cultivated North American Cannabis"
date: 2024-09-23
tags: [Cannabis,Branches of genetics,Biology,Genetics,Life sciences,Biotechnology]
author: Philippe Henry
institutions: 
doi: "https://doi.org/10.6084/m9.figshare.11778936"
---


dataset  posted on 2020-01-31, 18:24 authored by Philippe HenryPhilippe Henry  A set of 22 highly informative and polymorphic SNP markers typed in 420 accessions with metadata associated with important traits such as cannabinoid and terpenoid expression as well as fibre and resin production. The assay offers insight into cannabis population structure, phylogenetic relationship, population genetics and correlation to secondary metabolite concentrations and demonstrate the utility of this assay for rapid, repeatable and cost-efficient genotyping of commercial and industrial cannabis accessions for use in product traceability, breeding programs and consumer education.