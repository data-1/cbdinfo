---
layout: post
title: "Towards A Molecular Understanding of The Cannabinoid Related Orphan Receptor GPR18: A Focus on Its Constitutive Activity"
date: 2024-08-19
tags: [Alpha helix,G protein-coupled receptor,Amino acid,Conformational isomerism,Salt bridge (protein and supramolecular),Cannabinoid receptor 2,2-Arachidonoylglycerol,Cannabinoid receptor,Receptor (biochemistry),Molecular dynamics,Biochemistry,Cell biology]
author: Noori Sotudeh | Paula Morales | Dow P. Hurst | Diane L. Lynch | Patricia H. Reggio | Dow P | Diane L | Patricia H
---


Conformational differences in specific transmembrane helices were investigated using the simulated annealing/Monte Carlo (MC) conformational analysis method of conformational memories (CM) as described in the Methods. Figure 9. χ1 rotamer conformational change in D7.49 in WT GPR18 (black) and the A3.39N mutant (red) due to sodium ion movement. [Google Scholar] [CrossRef]  Ballesteros, J.; Weinstein, H. Integrated methods for the construction of three-dimensional models and computational probing of structure-function relations in G protein-coupled receptors. Comparative Protein Structure Modeling Using Modeller. Modeling Loops in Protein Structures.

[Visit Link](https://www.mdpi.com/1422-0067/20/9/2300){:target="_blank rel="noopener"}

