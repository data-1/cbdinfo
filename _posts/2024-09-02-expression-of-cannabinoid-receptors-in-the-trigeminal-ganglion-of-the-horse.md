---
layout: post
title: "Expression of Cannabinoid Receptors in the Trigeminal Ganglion of the Horse"
date: 2024-09-02
tags: [Cannabinoid receptor 2,TRPV1,Anandamide,Endocannabinoid system,Cannabinoid receptor 1,Satellite glial cell,Cell biology]
author: Rodrigo Zamith Cunha | Alberto Semprini | Giulia Salamanca | Francesca Gobbo | Maria Morini | Kirstie J. Pickles | Veronica Roberts | Roberto Chiocchetti | Zamith Cunha | Kirstie J
---


Primary antibodies used in the study. [Google Scholar] [CrossRef] [PubMed]  Roberts, V.L. Pain. Neuron 1998, 21, 531–543. ; Roberts, V.; Chiocchetti, R. Expression of Cannabinoid Receptors in the Trigeminal Ganglion of the Horse.

[Visit Link](https://www.mdpi.com/1422-0067/24/21/15949){:target="_blank rel="noopener"}

