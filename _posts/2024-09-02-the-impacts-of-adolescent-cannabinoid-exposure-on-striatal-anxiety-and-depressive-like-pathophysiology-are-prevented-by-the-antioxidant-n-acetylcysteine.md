---
layout: post
title: "The Impacts of Adolescent Cannabinoid Exposure on Striatal Anxiety- and Depressive-Like Pathophysiology Are Prevented by the Antioxidant N-Acetylcysteine"
date: 2024-09-02
tags: [Nucleus accumbens,Striatum,Anxiety,Mental disorders,Neuroscience]
author: 
---


Under a Creative Commons license  open access  Abstract  Background  Exposure to Δ9-tetrahydrocannabinol (THC) is an established risk factor for later-life neuropsychiatric vulnerability, including mood- and anxiety-related symptoms. THC may increase neuroinflammatory responses via the redox system and dysregulate inhibitory and excitatory neural balance in various brain circuits, including the striatum. Thus, interventions that can induce antioxidant effects may counteract the neurodevelopmental impacts of THC exposure. Methods  In the current study, we used an established preclinical adolescent rat model to examine the impacts of adolescent THC exposure on various behavioral, molecular, and neuronal biomarkers associated with increased mood and anxiety disorder vulnerability. Conclusions  The preventive effects of this antioxidant intervention highlight the critical role of redox mechanisms underlying cannabinoid-induced neurodevelopmental pathology and identify a potential intervention strategy for the prevention and/or reversal of these pathophysiological sequelae.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2667174324000740){:target="_blank rel="noopener"}

