---
layout: post
title: "Endothelial dysfunction and angiogenesis: what is missing from COVID-19 and cannabidiol story? - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Ashtar Nakhaei, School Of Medicine, Tehran University Of Medical Sciences, Tehran, Najarian, Medical Biology Research Center, Health Technology Institute, Kermanshah University Of Medical Sciences, Kermanshah, Farzaei
tags: [Cannabidiol,Angiogenesis,Endothelium,Cannabinoid,Tumor necrosis factor,COVID-19,Cannabinoid receptor type 2,Angiotensin-converting enzyme 2,Cytokine,Interleukin 6,Inflammation,Vascular endothelial growth factor,Neurochemistry,Cell signaling,Biochemistry,Medicine,Clinical medicine,Cell biology,Medical specialties]
---


Angiogenesis and endothelial dysfunction in COVID-19  Recently, upon our review research on the role of angiogenesis and endothelial dysfunction in COVID-19 (Norooznezhad and Mansouri 2021), we found that among many EC surface receptors, angiotensin-converting enzyme 2 (ACE2) receptor associates most with COVID-19 infection. Cannabidiol, angiogenesis, and endothelial cells  As mentioned, cannabinoids have been studied for their strong anti-inflammatory potentials and ability to inhibit/decrease various important pro-inflammatory cytokines such as IL-1β, IL-6, and TNF-α. Endothelial cell dysfunction and some of the pro-angiogenic factors are directly associated with ARDS (Ackermann et al. 2020; Norooznezhad and Mansouri 2021). 1 Short review on the potential of cannabidiol on pathologic angiogenesis and endothelialitis. A significant decrease was observed in TNF-α levels from their PBMCs upon previous exposure to CBD compared to the cells collected before CBD treatment (Hobbs et al. 2020).

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00129-w){:target="_blank" rel="noopener"}


