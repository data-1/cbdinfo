---
layout: post
title: "Medical cannabis for the treatment of fibromyalgia syndrome: a retrospective, open-label case series - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabis
author: Mazza, Department Of Anaesthesiology, Critical Care Medicine, Pain Medicine, Nuovo Ospedale Degli Infermi, Biella, Via Dei Ponderanesi, Ponderano, Manuela Mazza, You Can Also Search For This Author In
tags: [Medical cannabis,Cannabidiol,Dose (biochemistry),Fibromyalgia,Pain,Neuropathic pain,Box plot,Mental disorder,Major depressive disorder,Antidepressant,Health,Medicine,Clinical medicine,Health care,Health sciences,Causes of death,Diseases and disorders,Medical specialties]
---


The findings of this study could be compared with the results from the few previous studies of MC in patients with FMS. One strength of the current study was evaluation of the effects of long-term therapy (12 months) in a controlled hospital setting. Other studies evaluated the effects of MC in patients who had already received therapy with MC with a mean treatment duration of approximately 10.4 months (Habib and Artul 2018). Reductions in pain have been reported in other studies of long-term MC treatment (Sagy et al. 2019; Habib and Artul 2018). In a trial on nabilone use in patients with FMS, a significant reduction in anxiety was observed by FIQR (Skrabek et al. 2008), and similar results were observed for MC in patients with FMS (Habib and Artul 2018).

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00060-6){:target="_blank" rel="noopener"}


