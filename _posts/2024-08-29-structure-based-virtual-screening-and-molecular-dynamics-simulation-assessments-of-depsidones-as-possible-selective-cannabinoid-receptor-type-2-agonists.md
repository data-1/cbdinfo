---
layout: post
title: "Structure-Based Virtual Screening and Molecular Dynamics Simulation Assessments of Depsidones as Possible Selective Cannabinoid Receptor Type 2 Agonists"
date: 2024-08-29
tags: [Docking (molecular),Ligand (biochemistry),Cannabinoid receptor 2,Hydrogen bond,Concepts in chemistry,Biochemistry,Chemistry]
author: Gamal A. Mohamed | Abdelsattar M. Omar | Dana F. AlKharboush | Mona A. Fallatah | Ikhlas A. Sindi | Dina S. El-Agamy | Sabrin R. M. Ibrahim | Gamal A | Abdelsattar M | Dana F
---


In our continual interest to explore new bioactivities of the reported natural metabolites, 235 were depsidone derivatives, and 217 of them were reported to have naturally originated mainly from lichens (Figures S1–S9) inn addition, 18 nornidulin semisynthetic derivatives (Figure S10). Molecular dynamic simulations were performed on the native agonist and the best binding agonists to validate the docking scores. Supplementary Materials  The following supporting information can be downloaded at: https://www.mdpi.com/article/10.3390/molecules28041761/s1, Figures S1–S10: Chemical structures of all tested depsidone derivatives: title; Table S1: ADMET properties table for all tested depsidone derivatives; Table S2: Docking results for all tested compounds against native ligands of CB2 agonist (PDB: 6KPF) Details of molecular dynamics simulation for E3R, simplicildone J (10), lobaric acid (110), mollicellin Q (101), garcinisidone E (215), mollicellin P (100), paucinervin Q (149), and boremexin C (161); ADMET properties table for all tested depsidone derivatives; docking results for all tested compounds against native ligands of CB2 agonist (PDB: 6KPF). Molecules 2023, 28, 1761. https://doi.org/10.3390/molecules28041761  AMA Style  Mohamed GA, Omar AM, AlKharboush DF, Fallatah MA, Sindi IA, El-Agamy DS, Ibrahim SRM. 2023; 28(4):1761. https://doi.org/10.3390/molecules28041761  Chicago/Turabian Style  Mohamed, Gamal A., Abdelsattar M. Omar, Dana F. AlKharboush, Mona A. Fallatah, Ikhlas A. Sindi, Dina S. El-Agamy, and Sabrin R. M. Ibrahim.

[Visit Link](https://www.mdpi.com/1420-3049/28/4/1761){:target="_blank rel="noopener"}

