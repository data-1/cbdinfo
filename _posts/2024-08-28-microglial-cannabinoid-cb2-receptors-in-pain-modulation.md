---
layout: post
title: "Microglial Cannabinoid CB2 Receptors in Pain Modulation"
date: 2024-08-28
tags: [Cannabinoid receptor 2,Microglia,Cannabinoid receptor 1,2-Arachidonoylglycerol,Interleukin 10,Cannabinoid,Endocannabinoid system,Fatty-acid amide hydrolase 1,Macrophage polarization,Gliosis,Cannabinoid receptor,Cell signaling,Biochemistry,Neurochemistry,Cell biology]
author: Kangtai Xu | Yifei Wu | Zhuangzhuang Tian | Yuanfan Xu | Chaoran Wu | Zilong Wang
---


The biological effects of these cannabinoids are mainly mediated by two endocannabinoid receptors, cannabinoid receptors 1 (CB1R) and 2 (CB2R). 2-AG can not only activate CB1R, but also activate CB2R expressed on microglia. The activation of CB2R can reduce pain signaling by regulating the activity of spinal microglia and inhibiting neuroinflammation. [Google Scholar] [CrossRef] [PubMed]  Stella, N. Endocannabinoid signaling in microglial cells. [Google Scholar] [CrossRef] [PubMed]  Xu, J.; Tang, Y.; Xie, M.; Bie, B.; Wu, J.; Yang, H.; Foss, J.F.

[Visit Link](https://www.mdpi.com/1422-0067/24/3/2348){:target="_blank rel="noopener"}

