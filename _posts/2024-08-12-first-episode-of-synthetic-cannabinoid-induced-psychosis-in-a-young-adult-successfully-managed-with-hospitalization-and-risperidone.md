---
layout: post
title: "First-Episode of Synthetic Cannabinoid-Induced Psychosis in a Young Adult, Successfully Managed with Hospitalization and Risperidone"
date: 2024-08-12
tags: [Psychosis,Cannabinoid receptor 2,Cannabinoid receptor,Synthetic cannabinoids,Antipsychotic,Cannabinoid,Human diseases and disorders,Health,Mental disorders,Clinical medicine,Diseases and disorders,Abnormal psychology,Mental health]
author: Fattore L | Fratta W | Znaleziona J | Ginterová P | Petr J | Ondra P | Válka I | Ševčík J | Chrastina J | Maier V
---


Synthetic cannabinoids mainly interact with cannabinoid receptors (CB) [2–5, 7, 10]. Seizures are less common but have also been reported [3]. At the next appointment, 2 weeks after discharge, Mr. A presented with recurrence of catatonia, stiffness, mutism, auditory hallucinations, disorganized speech and thought process, and blunted affect. One case-series included 10 male subjects, aged 21 through 25, without prior psychiatric history (though 1 had a family history of psychosis), who developed psychotic symptoms after prolonged use of SCs and were hospitalized on a psychiatric unit [16]. These patients’ symptoms included paranoia, thought disorders, and suicidal ideation [16].

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1155/2016/7257489){:target="_blank rel="noopener"}

