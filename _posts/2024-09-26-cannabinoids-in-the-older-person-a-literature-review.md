---
layout: post
title: "Cannabinoids in the Older Person: A Literature Review"
date: 2024-09-26
tags: [Nabilone,Nabiximols,Tetrahydrocannabinol,Cannabidiol,Dronabinol,Cannabis (drug),Cannabinoid,Cannabinoid receptor 2,Medical cannabis,Medicine,Health care,Health,Clinical medicine,Medical specialties]
author: William Beedham | Magda Sbai | Isabel Allison | Roisin Coary | David Shipway
pubdate: "2020-01-13"
institutions: University of Bristol | North Bristol NHS Trust
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7151062"
---


Keywords: cannabinoids, elderly, THC, CBD, effectiveness, safety, literature review  1. MeSH terms included: “Cannabinoid OR Cannabis OR Medical marijuana” AND “Aged or Geriatrics”. In particular, its ability to reduce other prescriptions, such as opioids, is advantageous.Agornyo P. [12]2018SurveyRegular cannabis use in older patients resulted in reduced pain, reduced medication side effects and the discontinuation of other pain medications.Briscoe [13]2018ReviewThe volume of current research is increasing, but there is still insufficient evidence to make robust recommendations.Mücke M. [14]2018Systematic reviewThe risks of cannabinoid treatments for neuropathic pain appear to outweigh any potential benefits.Torres-Moreno M. [15]2018Systematic ReviewResults favored cannabinoids over the placebo. Narang et al. reported the effect of dronabinol on pain in a placebo-controlled trial [39]. Potential Hazards of Medical Cannabinoids in Older Adults  14.1.