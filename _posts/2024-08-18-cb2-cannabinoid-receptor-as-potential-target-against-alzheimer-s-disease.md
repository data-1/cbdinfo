---
layout: post
title: "CB2 Cannabinoid Receptor As Potential Target against Alzheimer's Disease"
date: 2024-08-18
tags: [Cannabinoid receptor 2,Endocannabinoid system,Amyloid beta,Microglia,Anandamide,Cannabinoid,Inflammation,Tau protein,Neurodegenerative disease,Cannabinoid receptor,Dementia,Cannabinoid receptor 1,Neuroinflammation,Biochemistry,Cell biology,Biology]
author: 
---


TABLE 1  TABLE 2  Anti-Inflammatory Effects of CB2 Receptor Activity  Inflammation is common in most neurodegenerative diseases including AD, and it may contribute to progressive neuronal damage. These findings have been corroborated in vivo by the administration of selective CB2 and mixed CB1–CB2 receptor agonists to rats and mice inoculated with Aβ into the brain, resulting in reduced levels of several pro-inflammatory cytokines and decreased microglia reactivity to the Aβ insult (Ramírez et al., 2005; Esposito et al., 2007; Martín-Moreno et al., 2011; Fakhfouri et al., 2012; Wu et al., 2013). Modulation of Aβ and Hyper-Phosphorylated Tau Processing  A number of studies have proposed a direct role for CB2 receptors in the modulation of Aβ peptide levels in brain. Early studies performed in cell cultures demonstrated that the mixed CB1–CB2 agonist WIN55,212-2 inhibited tau protein hyper-phosphorylation in Aβ-stimulated PC12 neuronal cells, but that this effect was mediated mainly by CB1 receptors (Esposito et al., 2006). It is worth noting that in vivo experiments have demonstrated that chronic treatment with the specific CB2 agonist JWH-133 significantly reduces tau hyper-phosphorylation at the Thr181 site in the vicinity of Aβ plaques in APP/PS1 mice (Aso et al., 2013).

[Visit Link](https://www.frontiersin.org/journals/neuroscience/articles/10.3389/fnins.2016.00243/full){:target="_blank rel="noopener"}

