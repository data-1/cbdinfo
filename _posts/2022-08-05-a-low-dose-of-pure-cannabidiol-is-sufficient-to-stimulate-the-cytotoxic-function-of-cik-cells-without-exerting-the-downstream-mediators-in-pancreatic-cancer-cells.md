---
layout: post
title: "A Low Dose of Pure Cannabidiol Is Sufficient to Stimulate the Cytotoxic Function of CIK Cells without Exerting the Downstream Mediators in Pancreatic Cancer Cells"
date: 2022-08-05
categories:
- CBD
author: Garofano, Sharma, Abken, Gonzalez-Carmona, Maria A., Schmidt-Wolf, Ingo G. H., Francesca Garofano, Amit Sharma, Hinrich Abken
tags: [Cytokine-induced killer cell,Cannabinoid receptor type 2,Interleukin 2,Interleukin 10,Interleukin 15,T cell,Cannabinoid receptor,Medical specialties,Clinical medicine,Biotechnology,Biology,Immunology,Cell biology,Biochemistry]
---


The uniqueness of CIK cells is their encouraging synergetic effect with cancer associated inhibitors/compounds in preclinical models since cannabinoids have been the subject of intensive cancer research, in particular cannabinoid receptor 2 (CB2) due to its expression in cells of the immune system where CIK cells also play an important role. To date, no study has thoroughly investigated the combinatorial impact of CBD and CIK cells, particularly in pancreatic cancer (PC). This clearly indicated that CBDs (mainly at low concentration) are sufficient to stimulate the cytotoxic function of CIK cells without exerting the downstream mediators like p38 and/or CREB, particularly in the pancreatic adenocarcinoma cell line. It is worth noting that cell viability and cytotoxicity of PANC-1 cells were also found to be significantly decreased when they were exposed to various concentrations of CBD (1–20 μM) for 24 h at 37 °C. Of interest, like PC cells, a significant difference was observed when effector cells were cultured with target cells (U-266) E:T ratio of 10:1 and exposed to different CBD concentrations (1–20 μM)—hence confirming that a low dose of CBD impacted in a similar way in both PC and myeloma cells, presumably via CIK cells.

<hr>

[Visit Link](https://www.mdpi.com/1422-0067/23/7/3783/htm){:target="_blank" rel="noopener"}


