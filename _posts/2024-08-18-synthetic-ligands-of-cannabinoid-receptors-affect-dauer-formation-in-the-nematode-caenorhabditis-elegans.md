---
layout: post
title: "Synthetic Ligands of Cannabinoid Receptors Affect Dauer Formation in the Nematode Caenorhabditis elegans"
date: 2024-08-18
tags: [Cannabinoid receptor 2,Cell signaling,AMP-activated protein kinase,Daf-2,Signal transduction,G protein-coupled receptor,Cannabinoid receptor 1,Serotonin,Transforming growth factor beta,Receptor (biochemistry),Caenorhabditis elegans,Receptor antagonist,Insulin,Dauer larva,G protein,Biology,Life sciences,Biotechnology,Cell communication,Molecular biology,Neurochemistry,Biochemistry,Cell biology]
author: Reis Rodrigues, Pedro | Kaul, Tiffany K | Ho, Jo-Hao | Lucanic, Mark | Burkewitz, Kristopher | Mair, William B | Held, Jason M | Bohn, Laura M | Gill, Matthew S | Reis Rodrigues
---


Interestingly, we found that other antagonists of mammalian CB receptors also suppress dauer entry, while the nonselective CB receptor agonist, O-2545, not only inhibited the activity of AM251, but also was able to promote dauer entry when administered alone. We found that AM251, an inverse agonist/antagonist of the mammalian CB receptor (Gatley et al. 1996), suppressed dauer formation in daf-2 insulin receptor mutants, by acting through G-protein signaling to activate TGF-β and insulin peptide pathways in the ASI chemosensory neuron. AM251 was not able to promote reproductive growth in daf-11 mutants (Figure 5A), suggesting that it requires functional DAF-11 to promote secretion of neuropeptides from the ASI neuron. The ability of O-2545 to induce dauer formation did not require srbc-64 or srbc-66, suggesting that this CB receptor agonist was not acting via these ascaroside receptors (Figure 6H). Although we have yet to identify the molecular target of AM251 in the worm, our data support a model in which AM251 acts upstream of SER-5 and requires TGF-β and insulin peptides in the ASI neuron to promote reproductive growth programs throughout the whole animal.

[Visit Link](https://academic.oup.com/g3journal/article/6/6/1695/6029944){:target="_blank rel="noopener"}

