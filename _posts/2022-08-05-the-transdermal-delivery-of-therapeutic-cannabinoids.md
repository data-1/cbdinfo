---
layout: post
title: "The Transdermal Delivery of Therapeutic Cannabinoids"
date: 2022-08-05
categories:
- Cannabinoid
author: Mahmoudinoodezh, Telukutla, Srinivasa Reddy, Bhangu, Sukhvir Kaur, Bachari, Cavalieri, Mantri, Haleh Mahmoudinoodezh, Srinivasa Reddy Telukutla
tags: [Transient receptor potential channel,Cannabinoid,Bioavailability,Pharmaceutical sciences,Pharmacology,Clinical medicine,Medicine,Biochemistry,Health]
---


Transdermal Delivery of Cannabinoids and Challenges  All advantages mentioned above of transdermal drug delivery over other formulations and considering dermatologists are interested in recommending topical formulations of cannabinoids to patients [ 93 ]. Additionally, the potential promising benefits of applying nanoformulation and nanoencapsulation systems can be improving the effective doses delivery of highly lipophilic drugs (e.g., cannabinoids), protecting poor stable therapeutic agents from aggressive environments, and targeted and controlled delivery [ 96 ]. Table 3 depicts several reports of the transcutaneous application of cannabinoids for various purposes like reaching a steady-state plasma cannabinoids concentration by using patches and gels, applying different techniques to enhance cannabinoids permeability through the skin, and also devising different formulations to encapsulate these therapeutic agents to improve physicochemical properties and penetration via skin layers. This report shows that after six months of storing cannabinoids-loaded microemulsion at 4 ℃ and 25 ℃, over 95% stable acidic cannabinoids content was maintained, indicating microemulsion system is a promising strategy for improving the stability and permeability of cannabinoids [ As is summarised in Table 3 , to improve physicochemical properties and the skin permeability of cannabinoids, various methods were proposed to encapsulate phytocannabinoids for different purposes. Furthermore, an ex-vivo skin study on Porcine skin samples confirmed that after 24 h of topical administration, these CBD-loaded CH/CP nanoparticles had low permeability into the deeper layers of skin, and CBD was kept in high concentration in the.

<hr>

[Visit Link](https://www.mdpi.com/1999-4923/14/2/438/htm){:target="_blank" rel="noopener"}


