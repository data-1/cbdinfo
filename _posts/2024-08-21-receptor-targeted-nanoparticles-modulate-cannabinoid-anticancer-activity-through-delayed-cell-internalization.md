---
layout: post
title: "Receptor-targeted nanoparticles modulate cannabinoid anticancer activity through delayed cell internalization"
date: 2024-08-21
tags: [MTT assay,Endocytosis,Flow cytometry,Drug metabolism,Endosome,Nanoparticle,Cell signaling,Receptor-mediated endocytosis,Neptunium,Lysosome,Cytometry,Biology]
author: Durán-Lobato | Álvarez-Fuentes | Fernández-Arévalo | Martín-Banderas
---


Finally, Fig. 4g,h present fluorescence intensity values near 0% for both NPs and the encapsulated molecules corresponding to cell cultures incubated at 4 °C to inhibit energy-dependent mechanisms. The similar values of size and ZP of plain blank and plain THC-loaded PLGA formulations (PLGA NPs and THC-PLGA NPs) indicate a negligible drug adsorption of Δ9-THC onto the surface of NPs, being the majority of drug molecules incorporated into the NPs matrix11. In addition, the higher intensity of fluorescence obtained in cells incubated with plain formulations vs. Tf-modified formulations (Fig. Cell studies  Cell culture  Caco-2 cells were cultivated as previously reported11,17,18.

[Visit Link](https://www.nature.com/articles/s41598-022-05301-z){:target="_blank rel="noopener"}

