---
layout: post
title: "A nutraceutical product, extracted from Cannabis sativa, modulates voltage-gated sodium channel function - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Milligan, Carol J., Florey Institute Of Neuroscience, Mental Health, The University Of Melbourne, Melbourne, Anderson, Lyndsey L., Brain, Mind Centre
tags: [Cannabidiol,Nav11,SCN8A,Nav18,Nav15,Cannabis,Cannabinoid,Voltage-gated ion channel,Nav17,Neurochemistry,Biochemistry,Neuroscience]
---


Many clinically effective anticonvulsants inhibit Na V channels, and purified CBD non-selectively inhibits these channels (Ghovanloo et al. 2018). Our study shows that a CBD-dominant nutraceutical product that contains a mixture of phytocannabinoids and terpenes potently modulated various functional properties of the sodium channel subtypes Na V 1.1–Na V 1.8. In adult brain, Na V 1.3 expression is present in both excitatory neurons and inhibitory interneurons (Whitaker et al. 2001). However, purified CBD is well tolerated in humans with no clinically significant ECG changes even at peak plasma concentrations >2 μM, consistent with purified CBD only modestly inhibiting Na V 1.5 (IC 50 of 3.8 μM) (Ghovanloo et al. 2018). NP treatment resulted in hyperpolarizing shifts in the midpoint of inactivation for all channel subtypes, except Na V 1.2.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00136-x){:target="_blank" rel="noopener"}


