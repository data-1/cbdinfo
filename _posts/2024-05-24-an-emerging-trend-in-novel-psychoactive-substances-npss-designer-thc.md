---
layout: post
title: "An emerging trend in Novel Psychoactive Substances (NPSs): designer THC"
date: 2024-05-24
categories:
- 
author: Maria Angela
tags: [-8-Tetrahydrocannabinol,Cannabinoid,Tetrahydrocannabinolic acid,Tetrahydrocannabinol,Gas chromatography,Cannabidiol,Chromatography,HTTP cookie,Cannabis sativa,High-performance liquid chromatography,Organic synthesis,Synthetic cannabinoids,Cannabis,THC-O-acetate,Chemistry]
---


Tetrahydrocannabinolic acid (THCA) diamonds  (6aR,10aR)-1-Hydroxy-6,6,9-trimethyl-3-pentyl-6a,7,8,10a-tetrahydro-6H-benzo[c]chromene-2-carboxylic acid. It is also reported that even the active epimer (9R) is less active that the active epimer of Δ6a,10a-THC (9S) (see next paragraph), which in turn is less active than Δ9-THC (Järbe et al. 1988). An informative website reports the effects produced by HHCB including stress reduction, relaxing effect, euphoria, appetite stimulating effect, sleep-inducing effect, and analgesic effect (TSM 2023). This cannabinoid produces sensations similar to natural cannabinoids like Δ9-THC. However, the compound found on the recreational market is not even pure octyl-Δ8-THC, but probably a mixture of cannabinoids with only 17% of the octyl-Δ8-THC (reddit.com.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-024-00226-y){:target="_blank" rel="noopener"}


