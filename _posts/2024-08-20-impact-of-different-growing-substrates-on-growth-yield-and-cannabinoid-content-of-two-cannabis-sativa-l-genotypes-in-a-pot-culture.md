---
layout: post
title: "Impact of Different Growing Substrates on Growth, Yield and Cannabinoid Content of Two Cannabis sativa L. Genotypes in a Pot Culture"
date: 2024-08-20
tags: [Cannabidiol,Cannabis,Coir]
author: Lisa Burgel | Jens Hartung | Simone Graeff-Hönninger | Graeff-Hönninger
---


The aim of this study was to evaluate the growth performance, such as the plant height, biomass yield, root growth and cannabinoid content, of cannabis plants grown in different substrates, substituted with peat alternatives in an indoor pot cultivation system. Root DW was measured after drying root samples at 110 °C for 24 h.  2.5. The genotype KAN of the plants grown in G30 and CC fibre had lower DW yields of flowers compared to 0.2x, whereas PM treatments showed no genotype-specific growth differences ( ; ). Cannabis Cannabinoids 2018, 1, 19–27. Plant J.

[Visit Link](https://www.mdpi.com/2311-7524/6/4/62){:target="_blank rel="noopener"}

