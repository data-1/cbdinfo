---
layout: post
title: "An overview on plants cannabinoids endorsed with cardiovascular effects"
date: 2024-05-13
categories:
- cbd
author: 
tags: []
---


Nowadays cardiovascular diseases (CVDs) are the major causes for the reduction of the quality of life. The endocannabinoid system is an attractive therapeutic target for the treatment of cardiovascular disorders due to its involvement in vasomotor control, cardiac contractility, blood pressure and vascular inflammation. Alteration in cannabinoid signalling can be often related to cardiotoxicity, circulatory shock, hypertension, and atherosclerosis. Plants have been the major sources of medicines until modern eras in which researchers are experiencing a rediscovery of natural compounds as novel therapeutics. The aim of this review is to collect and investigate several less studied plants rich in cannabinoid-like active compounds able to interact with cannabinoid system; these plants may play a pivotal role in the treatment of disorders related to the cardiovascular system.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S0753332221007459){:target="_blank" rel="noopener"}


