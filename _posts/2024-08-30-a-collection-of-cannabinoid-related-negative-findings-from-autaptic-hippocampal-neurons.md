---
layout: post
title: "A collection of cannabinoid-related negative findings from autaptic hippocampal neurons"
date: 2024-08-30
tags: [Cannabinoid,Cannabinoid receptor 1,Cannabinoid receptor,Endocannabinoid system,Synapse,Cell signaling,2-Arachidonoylglycerol,Excitatory postsynaptic potential,Anandamide,Chemical synapse,Fatty acid-binding protein,Receptor (biochemistry),Allosteric regulation,Allosteric modulator,Neurophysiology,Biochemistry,Cell biology,Basic neuroscience research,Neurochemistry]
author: Bosquez-Berger
---


We tested the effect of SGIP1 deletion on CB1 signaling in autaptic cultures. Autaptic hippocampal neurons were cultured as described previously34,35. To address these questions, we tested the efficacy of 1-AG to engage CB1 receptors in autaptic hippocampal neurons, recording from neurons to obtain baseline EPSCs, and then treating the cells with 1-AG at various concentrations (Fig. 1-AG (5 µM) did not significantly inhibit EPSCs in CB1−/− neurons, indicating that 1-AG inhibition was CB1-dependent (Fig. To evaluate the role of SGIP1 in neuronal CB1 function, we examined depolarization induced suppression of excitation (DSE) in neurons cultured from mice lacking SGIP1.

[Visit Link](https://www.nature.com/articles/s41598-023-36710-3){:target="_blank rel="noopener"}

