---
layout: post
title: "Synthesis and In Vitro Characterization of Selective Cannabinoid CB2 Receptor Agonists: Biological Evaluation against Neuroblastoma Cancer Cells"
date: 2024-08-25
tags: [Cannabinoid receptor 2,MAPK/ERK pathway,Extracellular signal-regulated kinases,Ligand (biochemistry),Cannabinoid receptor 1,Endocannabinoid system,Cannabinoid]
author: Francesca Gado | Rebecca Ferrisi | Sarah Di Somma | Fabiana Napolitano | Kawthar A. Mohamed | Lesley A. Stevenson | Simona Rapposelli | Giuseppe Saccomanni | Giuseppe Portella | Roger G. Pertwee
---


The effectiveness of the novel 1,8-naphthyridine-3-carboxamide derivatives and of compound LV62 were determined against neuroblastoma cells. Cell. Cell. ; et al. Synthesis and In Vitro Characterization of Selective Cannabinoid CB2 Receptor Agonists: Biological Evaluation against Neuroblastoma Cancer Cells. Synthesis and In Vitro Characterization of Selective Cannabinoid CB2 Receptor Agonists: Biological Evaluation against Neuroblastoma Cancer Cells Molecules 27, no.

[Visit Link](https://www.mdpi.com/1420-3049/27/9/3019){:target="_blank rel="noopener"}

