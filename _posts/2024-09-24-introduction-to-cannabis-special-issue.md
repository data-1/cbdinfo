---
layout: post
title: "Introduction to Cannabis Special Issue"
date: 2024-09-24
tags: [Medical cannabis,Cannabinoid,Cannabidiol,Cannabis,Effects of cannabis,Human diseases and disorders,Health,Clinical medicine,Diseases and disorders,Mental disorders,Medicine,Psychoactive drugs,Causes of death,Mental health]
author: Francesca M. Filbey
institutions: The University of Texas at Dallas
doi: "https://doi.org/10.1080/15504263.2019.1708551"
---


The papers in this Special Issue address important questions surrounding the medicinal use of cannabis by reviewing the literature on (1) the impact of cannabis on neurocognition across the lifespan, (2) the therapeutic potential of cannabis, and (3) putative harms associated with cannabis use. Of those who support legalization, an astounding majority (86%) do so for its potential therapeutic benefits. In populations who use cannabinoids for medicinal purposes, a large majority (i.e., 40%) utilize cannabis for pain (Gallup, June 19–July 12, 2019). To date, the unresolved and complex relationship between the analgesic effects of cannabinoids and their psychoactive effects limit the application of cannabinoids for the treatment and management of pain. As in other therapeutic compounds, unwanted side effects are important to consider.