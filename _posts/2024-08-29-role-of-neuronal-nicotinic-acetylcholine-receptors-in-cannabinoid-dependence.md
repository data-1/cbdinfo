---
layout: post
title: "Role of neuronal nicotinic acetylcholine receptors in cannabinoid dependence ☆"
date: 2024-08-29
tags: [Nicotinic acetylcholine receptor,Cannabinoid,Cannabinoid receptor 1,Anandamide,Endocannabinoid system,Fatty-acid amide hydrolase 1,Cannabis use disorder,Self-administration,Cannabinoid receptor 2,Tetrahydrocannabinol,Varenicline,Nicotine,Neurophysiology,Neurochemistry]
author: 
---


Preclinical studies suggest that the ɑ7 nAChR subtype may play a role in modulating the reinforcing and discriminative stimulus effects of cannabinoids, while the ɑ4β2 * nAChR subtype may be involved in modulating the motor and sedative effects of cannabinoids. Nicotinic receptors containing α3, α5 and β4 subunits were investigated for their role in cannabis withdrawal. Additionally, other studies showed that the ɑ6 nAChR subunit, expressed in dopaminergic neurons which co-assembles with β4 and β2 containing receptor subtypes [17], [45] may play a role in THC withdrawal. As nicotine is often co-used with cannabis and with the overlapping distribution of CBRs and nAChRs in brain regions involved in substance abuse, the role that the nicotinic system plays in cannabis use and abuse represents a high area of research interest. While the ɑ7 nAChR has been implicated in the rewarding effects of cannabinoids, via endocannabinoid release, the ɑ4β2 * nAChR has been implicated in the attenuation of cannabinoid induced motor impairments, via increasing NO levels, suggesting that there may be differential roles of the neuronal nicotinic acetylcholine system in the behavioral and neurochemical modulation of the effects of cannabinoids.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S1043661823001020){:target="_blank rel="noopener"}

