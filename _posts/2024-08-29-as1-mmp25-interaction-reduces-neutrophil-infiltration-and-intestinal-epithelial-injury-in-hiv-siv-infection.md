---
layout: post
title: "AS1/MMP25 interaction reduces neutrophil infiltration and intestinal epithelial injury in HIV/SIV infection"
date: 2024-08-29
tags: [Gene expression,Long non-coding RNA,Protein isoform,Interferon gamma,Transcription (biology),Base pair,Inflammation,Transfection,Formyl peptide receptor 2,Messenger RNA,Epigenetics,Complementarity (molecular biology),Protein,Neutrophil,HIV,Inflammatory bowel disease,Vector (molecular biology),Antisense RNA,Intestinal epithelium,Alpha-1 antitrypsin,Downregulation and upregulation,Immunostaining,Simian immunodeficiency virus,Regulation of gene expression,Exon,CD47,Real-time polymerase chain reaction,Sense (molecular biology),Three prime untranslated region,Health sciences,Genetics,Life sciences,Cell biology,Biology,Biotechnology,Molecular biology,Biochemistry]
author: JCI Insight | Lakmini S. Premadasa | Eunhee Lee | Marina McDew-White | Xavier Alvarez | Sahana Jayakumar | Binhua Ling | Chioma M. Okeoma | Siddappa N. Byrareddy | Smita Kulkarni
---


To determine the role of lncRNAs in HIV/SIV-induced intestinal epithelial dysfunction, we profiled and identified differentially expressed (DE) lncRNAs based on a log2 fold-change (FC) cutoff of >2.0 for upregulated lncRNAs and FC < –2.0 for downregulated lncRNAs with P < 0.05. Image quantification (HALO) confirmed significantly elevated MMP25 protein expression in the CE of VEH/SIV compared with both THC/SIV (P < 0.0001) and uninfected control RMs (~2.0-fold) (P < 0.0001) (Figure 3C). MMP25-AS1 overexpression significantly reduces MMP25 mRNA and protein expression in in vitro–cultured primary colon and small intestinal epithelial cells. Further, relative to VEH/SIV/cART RMs, expression of MMP25-AS1 was significantly higher in JE of THC/SIV/cART RMs, and this increase was also associated with significantly reduced MMP25 mRNA expression (P < 0.0001) (Figure 7B). In contrast to the findings in the colon of cART-naive SIV-infected RMs (Figure 6, A and C), no significant difference in the number of MPO/CD11b++ neutrophils was detected in the jejunal lamina propria of VEH/SIV/cART compared with THC/SIV/cART RMs despite significantly high MMP25 protein expression.

[Visit Link](https://insight.jci.org/articles/view/167903){:target="_blank rel="noopener"}

