---
layout: post
title: "Flipping the GPCR Switch: Structure-Based Development of Selective Cannabinoid Receptor 2 Inverse Agonists"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Ligand (biochemistry),MAPK/ERK pathway,Functional selectivity,Time-resolved fluorescence energy transfer,Drug design,Agonist,Extracellular signal-regulated kinases,Cell signaling,Receptor antagonist,Cyclic adenosine monophosphate,G protein-coupled receptor,Arrestin,EC50,Docking (molecular),G protein,Receptor (biochemistry),Pharmacology,Förster resonance energy transfer,Protein structure,Cannabinoid receptor 1,Biochemistry,Cell biology,Biology,Neurochemistry,Cell communication]
author: Miroslav Kosar | Roman C. Sarott | David A. Sykes | Alexander E. G. Viray | Rosa Maria Vitale | Nataša Tomašević | Xiaoting Li | Rudolf L. Z. Ganzoni | Bilal Kicin | Lisa Reichert
---


[PubMed] [CrossRef] [Google Scholar]  Copeland R. A. [PubMed] [CrossRef] [Google Scholar] [Ref list]  Copeland R. A. [PubMed] [CrossRef] [Google Scholar] [Ref list]  Olajide O. [PMC free article] [PubMed] [CrossRef] [Google Scholar] [Ref list]  Filipek S. Molecular switches in GPCRs. [PubMed] [CrossRef] [Google Scholar] [Ref list]

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11117691/){:target="_blank rel="noopener"}

