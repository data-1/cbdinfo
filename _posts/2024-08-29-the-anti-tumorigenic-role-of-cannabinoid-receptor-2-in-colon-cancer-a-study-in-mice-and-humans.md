---
layout: post
title: "The Anti-Tumorigenic Role of Cannabinoid Receptor 2 in Colon Cancer: A Study in Mice and Humans"
date: 2024-08-29
tags: [Cannabinoid receptor 2,Myeloid-derived suppressor cell,Endocannabinoid system,Tumor microenvironment,Single-nucleotide polymorphism,Colorectal cancer,Anandamide,Laboratory mouse,Carcinogenesis,Cancer,Medical specialties,Cell biology,Clinical medicine,Biology]
author: Jennifer Ana Iden | Bitya Raphael-Mizrahi | Zamzam Awida | Aaron Naim | Dan Zyc | Tamar Liron | Melody Kasher | Gregory Livshits | Marilena Vered | Yankel Gabet
---


Cancer 2021, 9, e001643. Cancer 2014, 134, 2853–2864. Cell. The Anti-Tumorigenic Role of Cannabinoid Receptor 2 in Colon Cancer: A Study in Mice and Humans. The Anti-Tumorigenic Role of Cannabinoid Receptor 2 in Colon Cancer: A Study in Mice and Humans.

[Visit Link](https://www.mdpi.com/1422-0067/24/4/4060){:target="_blank rel="noopener"}

