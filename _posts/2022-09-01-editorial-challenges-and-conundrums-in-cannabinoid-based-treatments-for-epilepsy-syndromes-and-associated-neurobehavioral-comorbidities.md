---
layout: post
title: "Editorial: Challenges and Conundrums in Cannabinoid-Based Treatments for Epilepsy Syndromes and Associated Neurobehavioral Comorbidities"
date: 2022-09-01
categories:
- Cannabinoid
author: Gómez-Nieto, Institute Of Neuroscience Of Castilla Y León, University Of Salamanca, Institute For Biomedical Research Of Salamanca, Ibsal, Department Of Cell Biology, Pathology, Faculty Of Medicine, López, Dolores E.
tags: [Epilepsy,Seizure,Cannabinoid,Cannabidiol,Anticonvulsant,Comorbidity,Postictal state,Endocannabinoid system,Electroencephalography,Major depressive disorder,Cannabinoid receptor type 1,Cannabinoid receptor,Hippocampus,Brain,Disease,Mental disorder,Pharmacology,Syndrome,Model organism,Health,Medical specialties,Medicine,Clinical medicine,Neuroscience]
---


Thus, epilepsy syndromes include not only seizures, but also several comorbid conditions related to behavioral and psychiatric disorders such as cognitive impairments, depression, and anxiety. This article collection is meant to provide a comprehensive insight into the therapeutic use of cannabinoids, including the anticonvulsant effects, the involvement of the endocannabinoid system, pharmacoresistance mechanisms, potential interactions with classic antiseizure medications and possible side effects, making a wide variety of key information available to clarify the relevance of cannabinoid-based treatments in refractory epilepsy as well as comorbid anxiety- and depression-like behavior (Figure 1). Since there are strong indications that the endocannabinoid system modulates epileptic seizures by regulating neuronal excitability, it is vital to investigate the expression of cannabinoid receptors in pre-clinical animal models of epilepsy. The review article by Lazarini-Lopes et al. provides an overview on the pharmacological modulation of the endocannabinoid system in audiogenic seizure susceptibility as well as the effects of Cannabis-derived compounds, with special attention to CBD. This Topic collectively provides a comprehensive insight into the anticonvulsant effects of cannabidiol (CBD), role of the endocannabinoid system, pharmacoresistance mechanisms, potential interactions with classic antiseizure medications, possible side effects, with a view to tackle the use of cannabinoids in refractory epilepsy as well as comorbid anxiety and depression in epilepsy.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fnbeh.2021.781852/full){:target="_blank" rel="noopener"}


