---
layout: post
title: "Fatty Acid Amides Suppress Proliferation via Cannabinoid Receptors and Promote the Apoptosis of C6 Glioma Cells in Association with Akt Signaling Pathway Inhibition"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Apoptosis,Protein kinase B,Cannabinoid,MTT assay,Akt/PKB signaling pathway,Fatty-acid amide hydrolase 1,Biochemistry,Biology,Cell biology]
author: Nágila Monteiro da Silva | Izabella Carla Silva Lopes | Adan Jesus Galué-Parra | Irlon Maciel Ferreira | Edilene Oliveira da Silva | Barbarella de Matos Macchi | Fábio Rodrigues de Oliveira | Nágila Monteiro da | Izabella Carla Silva | Galué-Parra
---


Treatment with FAAs Inhibits C6 Glioma Cell Growth  This is example 1 of an experiment: In order to evaluate the effects of FAAs on cell viability, C6 glioma cells were tested using the MTT assay for mitochondrial activity. For the analysis of the effects of cannabinoid receptor, cells were incubated with the CB1 antagonist, 10 μM LY3210135, and/or the CB2 antagonist, AM630, 30 min before the treatment with fatty acid amides for 12 h; 120 µg/mL FAA1 and FAA2. Cells 2019, 8, 863. Pharmaceuticals 2024, 17, 873. https://doi.org/10.3390/ph17070873  AMA Style  Silva NMd, Lopes ICS, Galué-Parra AJ, Ferreira IM, Sena CBCd, Silva EOd, Macchi BdM, Oliveira FRd, do Nascimento JLM. 2024; 17(7):873. https://doi.org/10.3390/ph17070873  Chicago/Turabian Style  Silva, Nágila Monteiro da, Izabella Carla Silva Lopes, Adan Jesus Galué-Parra, Irlon Maciel Ferreira, Chubert Bernardo Castro de Sena, Edilene Oliveira da Silva, Barbarella de Matos Macchi, Fábio Rodrigues de Oliveira, and José Luiz Martins do Nascimento.

[Visit Link](https://www.mdpi.com/1424-8247/17/7/873){:target="_blank rel="noopener"}

