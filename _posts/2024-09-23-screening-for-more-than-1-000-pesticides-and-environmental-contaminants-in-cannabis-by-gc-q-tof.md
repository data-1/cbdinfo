---
layout: post
title: "Screening for More than 1,000 Pesticides and Environmental Contaminants in Cannabis by GC/Q-TOF"
date: 2024-09-23
tags: [Gas chromatography–mass spectrometry,Tandem mass spectrometry,Cannabis (drug),Gas chromatography,Mass spectrometry,Electron ionization,High-performance liquid chromatography,Chromatography,Pesticide,Liquid chromatography–mass spectrometry,Legalization of non-medical cannabis in the United States,Detection limit,Analytical chemistry]
author: Philip Wylie | J. Westland | Maggie Haitian Wang | Mohamed M. Radwan | Chandrani G. Majumdar | Mahmoud A. ElSohly
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8489331/"
---


McPartland has published two comprehensive reviews on the diseases [3] and pests [4] that attack cannabis plants. The GC/Q-TOF together with a pesticide PCDL have been used for the detection of pesticides in aquatic environments [15] and in various foods [16, 17], but it has never been used to analyze pesticide residues in cannabis. Suspect Screening for All of the PCDL Compounds  The FbF screening tool in the qualitative analysis software was used to screen data files for all 1,020 compounds included in the P&EP PCDL. Column G indicates how many of the six extracted ions were qualified for each compound listed. Analysis of Confiscated Cannabis Samples  Cannabis that is confiscated by authorities is most likely purchased through the unregulated market or grown by the user, so one does not know which, if any, pesticides were used on the plants.