---
layout: post
title: "A population-based survey to assess the association between cannabis and quality of life among colorectal cancer survivors"
date: 2024-09-24
tags: [Cannabis (drug),Colorectal cancer,Cancer,Cannabidiol,Medical diagnosis,Signs and symptoms,Cohort study,Diseases and disorders,Health,Medical specialties,Clinical medicine,Medicine,Health care,Causes of death,Health sciences]
author: Susan L. Calcaterra | Andrea N. Burnett‐Hartman | J. David Powers | Douglas A. Corley | Carmit McMullen | Pamala A. Pawloski | Heather Spencer Feigelson
institutions: Kaiser Permanente
doi: "https://bmccancer.biomedcentral.com/articles/10.1186/s12885-020-06887-1"
---


After adjusting for demographics, medical comorbidities, stage and site of CRC diagnosis, and prescription opioid use, people who used cannabis had significantly lower QoL than people who did not use cannabis (difference of − 6.14, 95% CI − 8.07 to − 4.20). For QoL measures, we calculated functional and symptom scales ranging from 0 to 100 [10]. Overall, 293 (16.4%) patients reported cannabis use following their CRC diagnosis; among these 293, 93 (31.7%) used cannabis ≥100 times, 67 (22.9%) used cannabis 3 to 10 times, and 163 (55.6%) used cannabis during the 30 days prior to survey completion. People who used cannabis reported lower functioning roles (all statistically significant) and higher symptom scores (all statistically significant except for diarrhea) compared to people who did not use cannabis (Table 2). We surveyed over 1700 CRC survivors and found cannabis use was not associated with improved QoL or cancer-related function or symptoms compared to CRC survivors without cannabis use.