---
layout: post
title: "Medical Cannabis Activity Against Inflammation: Active Compounds and Modes of Action"
date: 2022-07-31
categories:
- cannabinoid
author: Anil, Seegehalli M., Institute Of Plant Science, Peeri, Koltai, Francesca Baratta, Ethan B. Russo
tags: [Cannabinoid,Cannabidiol,Inflammation,Tetrahydrocannabinol,Cannabinoid receptor type 2,Medical cannabis,Tumor necrosis factor,NF-B,Interleukin 10,Inflammatory cytokine,Cytokine,Macrophage,Interleukin 6,Endocannabinoid system,Cannabinoid receptor,Interferon,Cannabinoid receptor type 1,Reactive oxygen species,Cell biology,Biology,Biochemistry,Signal transduction,Neurochemistry,Biotechnology,Cell signaling,Immunology,Medical specialties,Cell communication]
---


Cannabigerol  The anti-inflammatory activity of CBG is less studied than that of CBD. Appendix A  Methodology: To reflect on the effect of cannabis and its derived compounds on acute or chronic inflammation and the accumulating knowledge regarding cannabis active compounds and their mode of action, we have conducted a literature review using the following terms: “inflammation “, “acute inflammation”, “chronic inflammation”, “medical use of cannabis”, “therapy” “Cannabis sativa”, “C. Anti-inflammatory Activity of a CB2 Selective Cannabinoid Receptor Agonist: Signaling and Cytokines Release in Blood Mononuclear Cells. Inflammation 44 (2), 466–479. Anti-Inflammatory Activity in Colon Models Is Derived from Δ9-Tetrahydrocannabinolic Acid that Interacts with Additional Compounds in Cannabis Extracts.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2022.908198/full){:target="_blank" rel="noopener"}


