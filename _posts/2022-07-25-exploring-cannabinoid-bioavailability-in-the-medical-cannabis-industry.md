---
layout: post
title: "Exploring cannabinoid bioavailability in the medical cannabis industry"
date: 2022-07-25
categories:
- CBD
- THC
author: Please Enter Your Name Here
tags: [Bioavailability,Dose (biochemistry),Medical cannabis,Absorption (pharmacology),Cannabidiol,Medication,First pass effect,Pharmaceutical formulation,Route of administration,Pharmacokinetics,Tetrahydrocannabinol,Pharmacology,Medicine,Products of chemical industry,Clinical medicine,Health sciences,Health,Chemicals in medicine,Health care,Medical research,Pharmaceutical sciences,Drugs,Pharmacy,Medical treatments,Medicinal chemistry,Pharmaceutics,Therapy,Chemistry,Life sciences]
---


Speaking to Christian Carlsen, Business Director of NNE and Morten Allesoe, Principal Consultant at NNE, Medical Cannabis Network gains an insight into the importance of achieving cannabinoid bioavailability, along with understanding ways new therapeutic medications can be developed and monitored to provide effective medication to the patient. Allesoe explains that the optimal formulation technology addresses the challenges of bioavailability reduction at all stages:  Improving dissolution in the gut;  Increasing the absorption across the gut wall; and  Reducing the extent of first-pass metabolism. For the sake of improved clinical efficacy, the concept of molecular modification into prodrugs may be relevant to the cannabis industry in the future.”  Prodrugs are derivatives of therapeutic agents created to improve the pharmacokinetics profile of a drug. Within a prodrug, the pharmacological activity of the drug is masked and is recovered within the human body upon bioconversion of the prodrug in the liver, a process that is typically mediated by enzymes.2  The prodrug strategy can improve bioavailability after absorption turning the otherwise negative effects of first-pass metabolism into an advantage (Fig. By this I mean if you have a high bioavailability you need a smaller portion of the drug, thereby reducing dosage for the patient and reducing cost of production; in contrast when you have a very low bioavailability, patients or consumers would need a higher dosage of the drug to gain the similar effect.”  Allesoe adds: “We are essentially trying to help businesses in the long term to understand the importance of bioavailability; and the ultimate benefits it will bring to not only businesses but also patients and consumers.”  Did you know that NNE are Partners with Medical Cannabis Network?

<hr>[Visit Link](https://www.healtheuropa.com/exploring-cannabinoid-bioavailability-in-the-medical-cannabis-industry/98987/){:target="_blank" rel="noopener"}


