---
layout: post
title: "Mediators of Placebo Response to Cannabinoid Treatment in Children with Autism Spectrum Disorder"
date: 2024-08-29
tags: [Placebo,Medical cannabis,Autism spectrum,Placebo-controlled study,Clinical global impression,Mental disorder,P-value,Health care,Clinical medicine,Medicine,Health]
author: Adi Aran | Moria Harel | Aminadav Ovadia | Shulamit Shalgy | Dalit Cayam-Rand | Cayam-Rand
---


We hypothesized that both the child’s comprehension of treatment and the parental treatment expectation would contribute to higher placebo response in children with ASD. Placebo Response in the CBA Trial  Overall, 19 participants (22%) had a positive response (as defined in the methods) in at least 2 of the 4 outcome measures following treatment with the placebo ( ). Placebo Response in Previous Studies with Medical Cannabis  A meta-analysis of 47 controlled studies of cannabinoid treatment in chronic noncancer pain demonstrated a very small differences between cannabinoids and placebo (n = 9958 participants) [23]. [Google Scholar] [CrossRef] [Green Version]  McCracken, J.T. Mediators of Placebo Response to Cannabinoid Treatment in Children with Autism Spectrum Disorder Journal of Clinical Medicine 12, no.

[Visit Link](https://www.mdpi.com/2077-0383/12/9/3098){:target="_blank rel="noopener"}

