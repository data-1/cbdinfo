---
layout: post
title: "Antidepressant-Like Effect of Terpineol in an Inflammatory Model of Depression: Involvement of the Cannabinoid System and D2 Dopamine Receptor"
date: 2024-08-20
tags: [Docking (molecular),Ligand (biochemistry),Cannabinoid receptor 2,Cannabinoid,Cannabinoid receptor 1,Receptor (biochemistry),Active site,Tumor necrosis factor,Tail suspension test,Molecular binding,Binding site,Adenosine,Serotonin,Hydrogen bond,Dopamine receptor D2,Dose (biochemistry),Caffeine,Cannabinoid receptor,Biochemistry,Neurochemistry]
author: Graziela Vieira | Juliana Cavalli | Elaine C. D. Gonçalves | Saulo F. P. Braga | Rafaela S. Ferreira | Adair R. S. Santos | Maíra Cola | Nádia R. B. Raposo | Raffaele Capasso | Rafael C. Dutra
---


Moreover, terpineol showed an antidepressant-like effect in the preventive treatment that was blocked by a nonselective dopaminergic receptor antagonist (haloperidol), a selective dopamine D2 receptor antagonist (sulpiride), a selective CB1 cannabinoid receptor antagonist/inverse agonist (AM281), and a potent and selective CB2 cannabinoid receptor inverse agonist (AM630), but it was not blocked by a nonselective adenosine receptor antagonist (caffeine) or a β-adrenoceptor antagonist (propranolol). Terpineol (100 mg/kg, p.o. Antidepressant-Like Effect of Terpineol in an Inflammatory Model of Depression: Involvement of the Cannabinoid System and D2 Dopamine Receptor. Biomolecules 2020, 10, 792. https://doi.org/10.3390/biom10050792  AMA Style  Vieira G, Cavalli J, Gonçalves ECD, Braga SFP, Ferreira RS, Santos ARS, Cola M, Raposo NRB, Capasso R, Dutra RC. 2020; 10(5):792. https://doi.org/10.3390/biom10050792  Chicago/Turabian Style  Vieira, Graziela, Juliana Cavalli, Elaine C. D. Gonçalves, Saulo F. P. Braga, Rafaela S. Ferreira, Adair R. S. Santos, Maíra Cola, Nádia R. B. Raposo, Raffaele Capasso, and Rafael C. Dutra.

[Visit Link](https://www.mdpi.com/2218-273X/10/5/792){:target="_blank rel="noopener"}

