---
layout: post
title: "Endocannabinoid Anandamide Attenuates Acute Respiratory Distress Syndrome through Modulation of Microbiome in the Gut-Lung Axis"
date: 2022-09-01
categories:
- Cannabinoid
- endocannabinoid
author: Sultan, Wilson, Abdulla, Osama A., Busbee, Philip Brandon, Hall, Carter, Singh, Chatterjee
tags: [Cannabinoid,Acute respiratory distress syndrome,Gut microbiota,Microbiota,Human microbiome,COVID-19,Lung microbiota,Immune system,T cell,Inflammation,Endocannabinoid system,Dysbiosis,Lung,Bacteria,Biology,Clinical medicine,Medical specialties]
---


ARDS was induced in mice as described previously [ 9 28 ]. To that end, we measured the levels of SCFAs in Naïve, SEB + VEH and SEB + AEA and we found that butyric acid, valeric acid and isovaleric acid levels were significantly decreased in the SEB + VEH group when compared to the naïve mice, while in the SEB + AEA group, the levels of these SCFAs increased significantly compared to the SEB + VEH group ( Figure 9 ). Of significant note, there was an increase in the cluster numbered 6 (regulatory cell 2) in SEB + AEA when compared to SEB + VEH, consistent with our previous studies that AEA induces Tregs [ 9 ]. In the current study, we noted that AEA treatment increased the expression of mBD2, which was associated with decreased Pseudomonas in the SEB + AEA group when compared to SEB + VEH group. Interestingly, SEB also caused alterations in the lung and gut microbiota, specifically causing an abundance of pathogenic bacteria, such as Pseudomonas, while AEA reversed this effect.

<hr>

[Visit Link](https://www.mdpi.com/2073-4409/10/12/3305/htm){:target="_blank" rel="noopener"}


