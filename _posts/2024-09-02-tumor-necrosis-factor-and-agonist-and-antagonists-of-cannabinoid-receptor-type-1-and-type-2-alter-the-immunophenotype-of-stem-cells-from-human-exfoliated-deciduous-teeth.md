---
layout: post
title: "Tumor necrosis factor α, and agonist and antagonists of cannabinoid receptor type 1 and type 2 alter the immunophenotype of stem cells from human exfoliated deciduous teeth"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Mesenchymal stem cell,Endocannabinoid system,Inflammation,Stromal cell,Interleukin 10,Cytokine,Cannabinoid,PD-L1,Immune system,Immunotherapy,Cannabinoid receptor 1,Tumor necrosis factor,Regulatory T cell,Transforming growth factor beta,Cell biology,Medical specialties,Biochemistry,Biology,Immunology]
author: Marizia Trevizani | Laís Lopardi Leal | João Vitor Paes Rettore | Gilson Costa Macedo | Silvioney Augusto da Silva | Fernando de Sá Silva
---


This study shows the interaction between mesenchymal stromal cells and the immune and endocannabinoid systems. Human mesenchymal stem cells modulate allogeneic immune cell responses. Stem Cells . The cannabinoid receptor type 2 as mediator of mesenchymal stromal cell immunosuppressive properties. Stem Cells .

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10631756/){:target="_blank rel="noopener"}

