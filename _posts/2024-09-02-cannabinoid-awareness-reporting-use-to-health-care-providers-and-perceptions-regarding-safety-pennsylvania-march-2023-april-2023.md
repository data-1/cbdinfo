---
layout: post
title: "Cannabinoid Awareness, Reporting Use to Health Care Providers, and Perceptions Regarding Safety – Pennsylvania, March 2023–April 2023"
date: 2024-09-02
tags: [Cannabidiol,Cannabis (drug),Cannabinoid,Over-the-counter drug,Health,Psychoactive drugs,Pharmacology,Health care]
author: Paul T. Kocis | Daniel J. Mallinson | Timothy J. Servinsky Jr
---


The Lion Poll survey was conducted to better understand patients’ views on cannabinoid awareness, reporting of use to HCPs, and perceptions of cannabinoid safety when compared to other substances. Discussion  The 2019 US National Survey on Drug Use and Health reports that the use of cannabis increased from 11% (2002) to 17.5% (2019) and represents 48.2 million Americans aged 12 years or older [10]. Second, the survey then asked how safe each respondent perceived alcohol, CBD, products containing THC, OTC pain medications, and anxiety/depression medications when taken with other prescription medications. Physicians’ perceptions of medical cannabis. Physicians’ perceptions of medical cannabis.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11324245/){:target="_blank rel="noopener"}

