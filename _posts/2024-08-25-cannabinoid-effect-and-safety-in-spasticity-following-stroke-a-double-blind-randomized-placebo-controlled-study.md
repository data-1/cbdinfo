---
layout: post
title: "Cannabinoid Effect and Safety in Spasticity Following Stroke: A Double-Blind Randomized Placebo-Controlled Study"
date: 2024-08-25
tags: [Spasticity,Cannabinoid,Nabiximols,Hypertonia,Neurotransmitter,Cannabinoid receptor 1,Multiple sclerosis,Tetrahydrocannabinol,Clinical medicine]
author: Gian Marco
---


throughout the study. While statistical analyses were performed only on the 34 patients who completed the study, safety data related to adverse events and concomitant medications included all 41 patients who entered the study. Effect on Outcome Measures  The median number of sprays taken was 12 (11–12) in patients on placebo treatment at the end of the first phase (T1) and 12 (10–12) in patients on placebo treatment at the end of the second phase (T3). Cannabinoid Effect on Outcome Measures  The present study does not suggest an improvement in spasticity from the treatment of patients with post-stroke spasticity with nabiximols. In our post-stroke patients, baseline (T0/T2) pain NRS level was low (2.4) and was not affected by the active treatment or placebo, while in our previous open-label study in patients with multiple sclerosis pain level was higher (3.9) and improved significantly after initiation of treatment with nabiximols (31).

[Visit Link](https://www.frontiersin.org/journals/neurology/articles/10.3389/fneur.2022.892165/full){:target="_blank rel="noopener"}

