---
layout: post
title: "The Enteric Glia and Its Modulation by the Endocannabinoid System, a New Target for Cannabinoid-Based Nutraceuticals?"
date: 2024-08-27
tags: [Cannabinoid receptor 2,Endocannabinoid system,Anandamide,TRPV1,Cannabinoid,Cannabinoid receptor 1,2-Arachidonoylglycerol,Gastrointestinal tract,Irritable bowel syndrome,Fatty-acid amide hydrolase 1,Cell signaling,Biology,Biochemistry,Neurochemistry,Cell biology]
author: Laura López-Gómez | Agata Szymaszkiewicz | Marta Zielińska | Raquel Abalo | López-Gómez
---


Cannabinoids are molecules that act on the endogenous cannabinoid system (ECS), also known as the endocannabinoid system, and are usually divided into three main groups: phytocannabinoids (cannabinoids found in plants), endocannabinoids (endogenous compounds found in animals that modulate cannabinoid receptors); and synthetic cannabinoids (synthetic compounds that may or may not be structurally related that also produce agonistic effects in cannabinoid receptors) [128]. Not much is known about the expression and role of CB1 and CB2 receptors in human EGCs. Since enteric neurons do express CB2 receptors under inflammatory conditions, LPS activation of EGCs could be secondary to CB2-mediated neuronal activation [156]. [Google Scholar] [CrossRef] [PubMed]  Sharkey, K.A. [Google Scholar] [CrossRef]  Bornstein, J.C. Purinergic mechanisms in the control of gastrointestinal motility.

[Visit Link](https://www.mdpi.com/1420-3049/27/19/6773){:target="_blank rel="noopener"}

