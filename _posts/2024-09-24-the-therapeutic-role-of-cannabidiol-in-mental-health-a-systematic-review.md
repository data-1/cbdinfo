---
layout: post
title: "The therapeutic role of Cannabidiol in mental health: a systematic review"
date: 2024-09-24
tags: [Attention deficit hyperactivity disorder,Mental disorder,Cannabidiol,Bipolar disorder,Psychosis,Schizophrenia,Nabiximols,Mania,Medical cannabis,Dose (biochemistry),Tetrahydrocannabinol,Cannabis (drug),Tourette syndrome,Cannabis use disorder,Randomized controlled trial,Major depressive disorder,Clozapine,Evidence-based medicine,Treatment-resistant depression,Olanzapine,Health,Clinical medicine,Mental disorders,Medicine,Diseases and disorders]
author: Rabia Khan | Sadiq Naveed | Nadeem Mian | Ania Fida | Muhammad Abdur Raafey | Kapil Kiran Aedma
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7819291"
---


Results  CBD and CBD-containing compounds such as nabiximols were helpful in alleviating psychotic symptoms and cognitive impairment in patients with a variety of conditions, and several studies provided evidence of effectiveness in the treatment of cannabis withdrawal and moderate to severe cannabis use disorder with Grade B recommendation. Other disorders  The present review included two RCTs (54 patients), one open-label trial (53 patients), one retrospective chart review (72 patients), and four case reports for CBD and nabiximols use in the treatment of other psychiatric disorders. Cannabidiol for the treatment of cannabis withdrawal syndrome: a case report. Cannabidiol for the treatment of cannabis withdrawal syndrome: a case report. [PMC free article] [PubMed] [CrossRef] [Google Scholar] [Ref list]