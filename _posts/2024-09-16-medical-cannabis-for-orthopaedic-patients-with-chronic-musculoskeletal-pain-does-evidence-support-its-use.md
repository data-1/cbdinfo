---
layout: post
title: "Medical cannabis for orthopaedic patients with chronic musculoskeletal pain: does evidence support its use?"
date: 2024-09-16
tags: [Cannabidiol,Tetrahydrocannabinol,Cannabis (drug),Cannabinoid,Tetrahydrocannabinolic acid,Medical cannabis,Nabiximols,Endocannabinoid system,Health care,Medicine,Clinical medicine,Health]
author: Herman Johal | Christopher Vannabouathong | Yaping Chang | Meng Zhu | Mohit Bhandari
institutions: McMaster University
doi: "https://doi.org/10.1177/1759720x20937968"
---


Open access  Review article  Medical cannabis for orthopaedic patients with chronic musculoskeletal pain: does evidence support its use? The medicinal uses of cannabis and cannabinoids. The health effects of cannabis and cannabinoids: the current state of evidence and recommendations for research. Chronic pain patients’ perspectives of medical cannabis. Learn more about the Altmetric Scores  See more details  Mendeley (88)  Articles citing this one  Receive email alerts when this article is cited  Web of Science: 13 view articles Opens in new tab  Crossref: 10  Medical Cannabis in Hand Surgery: A Review of the Current Evidence  Hand Surgery Patient Perspectives on Medical Cannabis: A Survey of Ove...  Self-reported cannabis use is not associated with greater opioid use i...  Cannabis for Rheumatic Disease Pain: a Review of Current Literature  Cannabis and cannabinoid for pain  Pharmacological Treatment in the Management of Glenohumeral Osteoarthr...  Medical Cannabis Use Reduces Opioid Prescriptions in Patients With Ost...  Medical Cannabis Use Reduces Opioid Prescriptions in Patients With Chr...