---
layout: post
title: "The (endo)cannabinoid signaling in female reproduction: What are the latest advances?"
date: 2024-09-24
tags: [Cannabinoid,Cannabidiol,Cannabis (drug),Pregnancy,Tetrahydrocannabinol,Cannabis,Fatty-acid amide hydrolase 1,Endocannabinoid system,Epigenetics,Intrauterine growth restriction,Health sciences,Biology]
author: Sandra Cecconi | Cinzia Rapino | Valentina Di Nisio | Gianna Rossi | Mauro Maccarrone
institutions: European Brain Research Institute | Università Campus Bio-Medico
doi: "https://www.sciencedirect.com/science/article/abs/pii/S0163782719300621?via%3Dihub"
---


Indeed, consumption during pregnancy of plant-derived and synthetic cannabinoids has been associated with gestational disorders such as abnormal embryo development, tubal pregnancy, implantation failure, preterm birth, intrauterine growth restriction, low birth weight and increased risk of miscarriage [[14], [15], [16]]. Section snippets  Cannabinoids versus endocannabinoids  Cannabis (Cannabis sativa or Cannabis indica) extracts contain more than 550 known compounds, of which more than 110 have been identified as cannabinoids, including the potent psychoactive substance THC [24]. Metabolic regulation plays important roles in embryo development and uterine receptivity during early pregnancy, ultimately influencing pregnancy efficiency in mammals. However, the psychotropic effects of Δ9-tetrahydrocannabinol (THC), the main psychoactive compound of Cannabis sativa, are of concern. Moreover, it also highlights the therapeutic potential of CBD and CBDV in several medical conditions and clinical applications.