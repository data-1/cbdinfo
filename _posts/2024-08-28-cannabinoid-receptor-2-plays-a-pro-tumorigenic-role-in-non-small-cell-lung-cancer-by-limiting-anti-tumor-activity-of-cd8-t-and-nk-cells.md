---
layout: post
title: "Cannabinoid receptor 2 plays a pro-tumorigenic role in non-small cell lung cancer by limiting anti-tumor activity of CD8+ T and NK cells"
date: 2024-08-28
tags: [Programmed cell death protein 1,Cannabinoid receptor 2,Cannabinoid receptor,Natural killer cell,Tumor microenvironment,PD-L1,Cancer immunotherapy,Checkpoint inhibitor,Cannabinoid,Cell biology,Clinical medicine,Immunology,Medical specialties,Biology,Immune system]
author: Valadez-Cosmes
---


In line with our mouse data, CB1 and CB2 expression were not only seen in lung cancer cells, but also in infiltrated immune cells, such as CD3+ T and CD8+ T cells, NKp46/NCR1+or CD56+ NK cells, and CD163+ macrophages. According to RT-qPCR, tumors of WT mice showed higher levels of CB2 mRNA than those from CB2-/- mice, because host cells, such as immune cells infiltrating the TME in CB2-/- mice, are devoid of CB2 expression (Figure 4B). Knockout of CB2 in cells of the TME favors an anti-carcinogenic immune cell profile and enhances CD8+ T and NK cell activity  To determine the immune cell profile in tumors of CB2-/- and WT mice, we used flow cytometry and identified changes in infiltration of immune cells and their subtypes, observing a significant shift of lymphoid cell populations in CB2-/- as compared to WT mice (gating strategies shown in Figures S1A–C). Based on these findings, we treated CB2-/- mice with anti-PD-1 to boost immune cell activity (Figure 7A). FIGURE 8  CB receptors are present in tumor cells and immune cells in situ  Using dual ISH-IF analysis of mouse and human lung cancer sections, we revealed that tumor cells as well as tumor-infiltrating immune cells, such as CD8+ T, NK cells, and macrophages express CB2 at much higher levels than CB1.

[Visit Link](https://www.frontiersin.org/journals/immunology/articles/10.3389/fimmu.2022.997115/full){:target="_blank rel="noopener"}

