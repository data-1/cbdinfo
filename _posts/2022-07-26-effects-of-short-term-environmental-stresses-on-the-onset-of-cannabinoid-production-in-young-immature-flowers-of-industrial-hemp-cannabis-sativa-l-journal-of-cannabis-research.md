---
layout: post
title: "Effects of short-term environmental stresses on the onset of cannabinoid production in young immature flowers of industrial hemp (Cannabis sativa L.) - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Hemp
author: Park, Institute Of Cannabis Research, Colorado State University Pueblo, Pueblo, Department Of Biology, Pauli, Christopher S., Gostin, Eric L., Staples
tags: [Cannabis,Cannabidiol,Cannabinoid,Cannabis sativa,Tetrahydrocannabinolic acid,Tetrahydrocannabinol,Plant hormone,High-performance liquid chromatography,Cannabigerol,Plant]
---


Supplementary Table 1 presents the measured floral cannabinoid concentrations of the immature flower tissues from control plants (day 1) and stress-treated plant (day 6 or day 8) that were used to calculate the production values during that timeframe. The values provided represent the difference between the cannabinoid concentration on day 1 and day 6 of flower. After 7 days of excessive heat treatment, the concentration of CBGA and CBG decreased by 182 μg/g (0.0182% w/w) (n = 3, p < 0.001 and p < 0.05) and by 112 μg/g (0.0112% w/w) (n = 3, p < 0.05), respectively (Fig. 3 Differential cannabinoids production in response to 7 days of short-term heat stress during the first 2 weeks of flowering. Cannabinoid productions in response to drought  Figure 4 and Table 1 show the cannabinoid production in response to 7 days of drought stress in the various hemp tissues.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00111-y){:target="_blank" rel="noopener"}


