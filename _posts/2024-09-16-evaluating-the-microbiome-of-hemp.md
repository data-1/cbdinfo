---
layout: post
title: "Evaluating the Microbiome of Hemp"
date: 2024-09-16
tags: [Rhizosphere,Microbiome,Phyllosphere,Polymerase chain reaction,Biology]
author: Samuel E. Barnett | Ali R. Cala | Julie Hansen | Jamie Crawford | D. R. Viands | Lawrence B. Smart | Christine D. Smart | Daniel H. Buckley
institutions: Cornell University
doi: "https://doi.org/10.1094/pbiomes-06-20-0046-r"
---


For each DESeq2 analysis, we used unrarefied OTU tables filtered to include only OTUs represented by at least five reads in at least one sample of the plant compartment-bulk soil set. We defined the core microbiome as bacterial and fungal OTUs that were ubiquitous in hemp plants and enriched significantly in a plant compartment relative to bulk soil. We defined a member of the core microbiome for a given plant compartment as an OTU that was ubiquitous across all plants in all sampling locations and enriched significantly in their plant compartment relative to the bulk soil. A number of the microbes that were highly abundant in the rhizosphere belong to taxa commonly found in plants; however, we only included in the core hemp microbiome those OTUs that were enriched significantly in the hemp rhizosphere relative to bulk soil. A leaf core fungal member, OTU_174, classified to the genus Epicoccum, was also highly abundant and ubiquitous in the flower community, though not significantly enriched in that compartment.