---
layout: post
title: "Bioengineering of the Marine Diatom Phaeodactylum tricornutum with Cannabis Genes Enables the Production of the Cannabinoid Precursor, Olivetolic Acid"
date: 2024-09-02
tags: [Heterologous expression,Cannabinoid,Plasmid,Cannabinoid receptor 2,Plasmid preparation,Molecular biology,Biotechnology,Life sciences,Biochemistry,Biology]
author: Fatima Awwad | Elisa Ines Fantino | Marianne Héneault | Aracely Maribel Diaz-Garza | Natacha Merindol | Alexandre Custeau | Sarah-Eve Gélinas | Fatma Meddeb-Mouelhi | Jessica Li | Jean-François Lemay
---


This study investigates the bioengineering potential of the marine diatom Phaeodactylum tricornutum through the introduction of cannabis genes, specifically, tetraketide synthase (TKS), and olivetolic acid cyclase (OAC), for the production of the cannabinoid precursor, olivetolic acid (OA). P. tricornutum transconjugants expressing these genes showed the production of the recombinant TKS and OAC enzymes, detected via Western blot analysis, and the production of cannabinoids precursor (OA) detected using the HPLC/UV spectrum when compared to the wild-type strain. Heterologous Protein Detection and Localization  The successful expression of cannabis genes and the accumulation of the corresponding enzymes was observed in P. tricornutum transconjugants. Another approach to increase OA titers could be the construction of alternative pathways using enzymes found in other OA-producing plants than cannabis, in order to achieve OA and other CB precursors while using fungal enzymes or mutation-enhanced enzymes. [Google Scholar] [CrossRef] [PubMed]  Tian, B.; Liu, J. Resveratrol: A Review of Plant Sources, Synthesis, Stability, Modification and Food Application.

[Visit Link](https://www.mdpi.com/1422-0067/24/23/16624){:target="_blank rel="noopener"}

