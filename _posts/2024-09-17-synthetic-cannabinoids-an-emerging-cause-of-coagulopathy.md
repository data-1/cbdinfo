---
layout: post
title: "Synthetic Cannabinoids: An Emerging Cause of Coagulopathy"
date: 2024-09-17
tags: [Brodifacoum,Prothrombin time,Anticoagulant,Medical specialties,Medicine,Clinical medicine,Blood]
author: Shakir Ullah | Amna Mohyud Din Chaudhary | Khalid Nawab | Ravi Athwani | Amandeep Singh
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/8383497"
---


We present a case of a patient found to have coagulopathy of unclear cause as patient did not report the use of synthetic cannabinoid initially. Keywords: Coagulopathy, Synthetic cannabinoids, Marijuana  Introduction  Synthetic cannabinoids (SC) are a heterogeneous group of compounds used as a substitute for natural marijuana. An increasing trend of coagulopathies in the USA has been reported after the rise of SC use with even some large outbreaks reported over the years [7]. Over the years, there has been an increasing trend in ER visits due to SC, with a 900% increase reported by a study conducted in Maryland with 40 cases in 2013 to 353 in 2017, with the majority of the patients being females [11]. [PubMed] [CrossRef] [Google Scholar]  Articles from Journal of Medical Cases are provided here courtesy of Elmer Press  1.