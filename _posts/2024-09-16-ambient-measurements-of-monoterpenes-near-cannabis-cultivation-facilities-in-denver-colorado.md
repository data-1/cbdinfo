---
layout: post
title: "Ambient measurements of monoterpenes near Cannabis cultivation facilities in Denver, Colorado"
date: 2024-09-16
tags: [Air pollution,Volatile organic compound,Ozone,Environmental issues,Human impact on the environment,Pollution,Natural environment,Societal collapse,Earth sciences]
author: C.-T. Wang | K. Ashworth | C. Wiedinmyer | J. Ortega | P.C. Harley | Q.Z. Rasool | W. Vizuete
institutions: 
doi: "https://www.sciencedirect.com/science/article/abs/pii/S1352231020302478"
---


By 2018 the industry expanded to 608 medical and recreational Cannabis Cultivation Facilities (CCFs) in Denver County (CDOR, 2018). and its products, and many previous studies have measured these compounds in dried plant material and essential oils (Hood et al., 1973; Turner et al., 1980; Ross and ElSohly, 1996; Rice and Koziel, 2015). Three of these measured mixing ratios of volatiles inside growing and processing rooms in cultivation facilities. In subsequent work, Wang et al. (2019) developed a monoterpene emissions inventory of CCFs across Colorado and estimated that total emissions could be as high as 362 tons year−1 for Denver County alone. When Wang et al. (2019) included monoterpene emissions from CCFs in an air quality model in the configuration used by the State of Colorado and EPA, they demonstrated these emissions were sufficient to increase hourly average ozone levels by as much as 1 ppbv per 1000 tons per year of monoterpenes released from CCFs.