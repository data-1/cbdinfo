---
layout: post
title: "Cannabinoid pharmacology and its therapeutic uses in Alzheimer’s disease"
date: 2024-08-20
tags: [Cannabinoid,Cannabinoid receptor 2,Cannabinoid receptor 1,Endocannabinoid system,Anandamide,Tetrahydrocannabinol,Cannabidiol,2-Arachidonoylglycerol,Cell biology,Neurochemistry,Biochemistry]
author: Kadja Luana Chagas Monteiro | Thiago Mendonça de Aquino
---


During AD, the endocannabinoid system undergoes some changes, where the progress of the disease may be related to the CB1 levels, in addition to the alteration of the CB2 expression, which starts to be expressed only in microglial cells. The neuroprotective function of cannabinoids stands out by preventing this activation. A combination of studies involving CBD and THC demonstrates that CBD could antagonize the psychoactive effects of THC, along with improving therapeutic effects. This remarkable study showed the prevention of Aβ-induced microglial activation, improvement in cognitive impairment, and loss of neuronal markers (Ramírez et al., 2005). Considering the extensive potential of natural, endogenous, and synthetic cannabinoids, few studies have been carried out to explore their role in AD ( ).

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8178768/){:target="_blank rel="noopener"}

