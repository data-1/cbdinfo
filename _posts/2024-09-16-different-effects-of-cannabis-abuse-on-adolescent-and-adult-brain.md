---
layout: post
title: "Different Effects of Cannabis Abuse on Adolescent and Adult Brain"
date: 2024-09-16
tags: [Cannabinoid receptor 2,Cannabinoid,Cannabinoid receptor 1,Endocannabinoid system,Cannabinoid receptor,Cannabis (drug),Anandamide,Tetrahydrocannabinol,Brain-derived neurotrophic factor,Neurochemistry,Neuroscience,Neurophysiology,Branches of neuroscience]
author: Stefan Dhein
institutions: Leipzig University
doi: "https://doi.org/10.1159/000509377"
---


Taken together, there is a complex interplay between endogenous (agonistic) endocannabinoids, NGF, BDNF, and exogenous (partial agonistic) THC [19, 20] and cannabidiol [20]. This is attributable to its pharmacological characteristics as a partial agonist and, therefore, depends on the concomitant activation of the system by other endogenous cannabinoids, the receptor density, and possible limitations of the post-receptor signal pathway [33]. In humans, these authors found gender-related differences in the associations between age of onset of cannabis use and neuropsychological deficits [91]. In further support of these studies, chronic exposure to various cannabinoid agonists such as THC during adolescence, but not during adulthood, in rats of either gender was shown to induce long-term impairments in working memory [95-97]. THC exposure also impaired adolescent learning in male rats [98].