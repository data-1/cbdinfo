---
layout: post
title: "Acute human exposure assessment to tetrahydrocannabinol (Δ9‐THC)"
date: 2024-09-16
tags: [Cannabis,Cannabidiol,Detection limit,Tetrahydrocannabinol,Drink,Federal Institute for Risk Assessment,Cannabis sativa,Cannabinoid]
author: Maria Anastassiadou | Claudia Cascio | Karen Mackay
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7008849"
---


Acute dietary exposure assessment methodology11  Acute exposure to total‐Δ9‐THC (Tot‐Δ9‐THC) for consuming days was estimated by matching food consumption data with occurrence data using 12 scenarios based on proxies of consumption of hemp and hemp‐based products (Table ). Results of acute dietary exposure assessment to Total‐Δ9‐THC  Acute dietary exposure to Total‐Δ9‐THC is presented here for 12 scenarios, each of them related to different hemp and hemp‐based products. Mean and high (P95) exposure estimates based on different percentiles of occurrence are presented below whereas those based on mean occurrence levels are only reported in Annex B.3 and B.4. Exposure to Total‐Δ9‐THC at the UB for the ‘hemp seeds’ is reported in Table while, also in this case, LB can be found in Table B.3. Table 22  Sources of uncertaintyDirectiona Extrapolation of occurrence data to the whole of Europe+/–Very low number of food samples available for exposure assessment for certain food categories+/–Variable selectivity towards ∆9‐THC of different analytical methods used to quantify Total‐∆9‐THC+Effect of food processing considered in the exposure assessment+/–Use of Proxy of Consumption for hemp and hemp‐based products+Consumption of only one hemp‐based product considered within a day–  Considering all the above‐mentioned factors, exposure estimates presented in this report are expected to represent an overestimation of the acute exposure to Δ9‐THC in ‘single food’ scenarios in consuming days.