---
layout: post
title: "Trends in intellectual property rights protection for medical cannabis and related products - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabis
author: Wyse, Dr. Eyal Bressler, Co. Patent Attorneys, Ramat-Gan, Luria, Lazrom House, Tuval St., Joseph Wyse, Gilad Luria, You Can Also Search For This Author In
tags: [Intellectual property,Medical cannabis,Patent,Genetic engineering,Cannabidiol,Cannabis (drug),Cannabinoid,Plant breeders rights,Tetrahydrocannabinol,CRISPR gene editing,Research,Cannabis,Patent Cooperation Treaty,Creative Commons license,Research exemption]
---


As for plant patents, 30 cannabis plant patents and applications have been filed in the USA. For more information on the PatSnap software operations, see the “Materials and methods” section Full size image  Table 5 Patents on medical cannabis midstream technologies Full size table  Downstream technologies  Medical cannabis extractions obtained from midstream technologies are formed into compositions, formulations, and compounds for which patent protection is sought in regard to the treatment of a number of medical conditions. 3 Patent filing trend in the field of downstream medical cannabis technologies. The company patent portfolio should also include technologies that may be contemplated by their competitors so as to exclude them from the market, at least for a time. Table 11 Patent litigation cases in medical cannabis field Full size table  GW Pharmaceuticals v. INSYS Therapeutics  In this case, US patent No.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-020-00057-7){:target="_blank" rel="noopener"}


