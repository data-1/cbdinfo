---
layout: post
title: "Relationship among subjective responses, flavor, and chemical composition across more than 800 commercial cannabis varieties - Journal of Cannabis Research"
date: 2022-07-26
categories:
- cannabinoid
- terpene
author: De La Fuente, Buenos Aires Physics Institute, Ifiba, Physics Department, University Of Buenos Aires, Buenos Aires, Institute Of Cognitive, Translational Neuroscience, Incyt, Ineco Foundation
tags: [Cannabis sativa,Decision tree learning,Cannabinoid,Cannabidiol,Cannabis,Cross-validation (statistics),Cannabis strain,Principal component analysis,Matrix (mathematics),Random forest,Modularity (networks)]
---


Figure 3a shows negative correlations, i.e. inverse relationships between the frequency of the reported effect and flavour tags, while Fig. C. Both positive (Panel A) and negative (Panel B) values grouped by hierarchical clustering of the effects and flavours according to their correlations. The network in the right presents the same analysis for effect tags Full size image  The network on the right is color-coded based on cannabis species tag: the first and largest module contained cultivars belonging to all species tags (similar to chemotype I); another module, situated in the middle, presented a more balanced proportion of species tags, but also contained a smaller proportion of cultivars (similar to chemotype II), and the remaining module was composed mostly by “hybrids” (as in chemotype III). Modularity analysis (Q = 0.194) of the network of effect tags associated by terpene similarity (Fig. The fact that the network of effects associated by terpene content similarity reflected the hierarchical clustering of effects obtained from flavour association (Fig.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-020-00028-y){:target="_blank" rel="noopener"}


