---
layout: post
title: "Cannabinoid receptor gene polymorphisms and cognitive performance in patients with schizophrenia and controls"
date: 2024-08-20
tags: [Cannabinoid receptor 1,Endocannabinoid system,Dominance (genetics),Cannabinoid receptor 2,Schizophrenia,Dorsolateral prefrontal cortex,Working memory,Prefrontal cortex,Nervous system,Memory,Single-nucleotide polymorphism,Cannabinoid,Allele,Biology]
author: Rodrigo Ferretjans | Renan P. de Souza | Bruna Panizzutti | Pâmela Ferrari | Lucas Mantovani | Rafael R. Santos | Fernanda C. Guimarães | Antonio L. Teixeira | Clarissa S. Gama | João V. Salgado
---


Methods:  In this genetic association case-control study, cannabinoid receptor polymorphisms CNR1 rs12720071 and CNR2 rs2229579 were tested for association with neurocognitive performance in 69 patients with schizophrenia and 45 healthy controls. A reduction in P300 wave amplitude was found in alcohol users homozygous for the CNR1 (AAT)n repetition polymorphism.26 Similar results were observed in healthy individuals who were exposed to cannabis, which suggests involvement of CB1R in neural mechanisms related to attention and working memory.27 CNR1 gene polymorphisms ([AAT]n variants and rs2180619) have been associated with lower efficiency of working memory, attentional processing and procedural learning.28-31 Besides, an interaction between CNR1 rs1406977 polymorphism and cannabis use was associated with prefrontal cortex connectivity32 and activity33 during working memory performance in healthy subjects. There was no association between the CNR1 rs12720071 polymorphism and cognitive performance in controls. There was no association between the CNR2 rs2229579 polymorphism and cognitive performance in controls. Cannabinoids, working memory, and schizophrenia.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8827365/){:target="_blank rel="noopener"}

