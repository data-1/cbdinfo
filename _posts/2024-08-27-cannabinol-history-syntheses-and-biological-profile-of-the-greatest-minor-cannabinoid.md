---
layout: post
title: "Cannabinol: History, Syntheses, and Biological Profile of the Greatest “Minor” Cannabinoid"
date: 2024-08-27
tags: [Cannabinoid receptor 2,Cannabinoid,TRPM8,Transient receptor potential channel,Organic chemistry]
author: Chiara Maioli | Daiana Mattoteia | Hawraz Ibrahim M. Amin | Alberto Minassi | Diego Caprioglio | Hawraz Ibrahim M
---


Despite its historical significance, the “red oil” brought, in addition to the tragedies mentioned above, a great deal of confusion to the cannabis research community during its early years, mainly due to two major factors: first, the issue of the name “cannabinol” itself, which has been transferred from the distillate (narcotic) to the natural product (inactive) [31]; the second reason is related to the fact that the plant material from which the oil was distilled often exhibited a fluctuating phytochemical profile resulting from different methods of storage under ambiguous conditions, a profile further influenced by the dramatic conditions of the resin distillation [27]. A “full stop” to the confusion that hovered over CBN (5a) was posed in the 1930s by Cahn (famous for nomenclature and stereochemistry with the Cahn–Ingold–Prelog rules [36]): by defining red oil as “raw cannabinol” and the pure compound as “cannabinol”, he was able to elucidate almost completely the structure of the natural product through degradation studies, identifying a dibenzopyranic structure with the presence of a phenol and of a n-pentyl on the resorcinyl portion, indicating their relative positions [37]. [Google Scholar] [CrossRef] [PubMed]  Pain, S. A Potted History. Synthesis of Cannabinol, 1-Hydroxy-3-n-Amyl-6,6,9-Trimethyl-6-Dibenzopyran 1. Cannabis Cannabinoid Res.

[Visit Link](https://www.mdpi.com/2223-7747/11/21/2896){:target="_blank rel="noopener"}

