---
layout: post
title: "Can prescribed medical cannabis use reduce the use of other more harmful drugs?"
date: 2024-09-16
tags: [Substance abuse,Harm reduction,Substance dependence,Medical cannabis,Cannabis (drug),Cannabidiol,Mental disorder,Health,Psychoactive drugs,Clinical medicine,Drugs acting on the nervous system,Medicine,Health care,Social aspects of psychoactive drugs]
author: Rosalind Gittins | Ben Sessa
institutions: 
doi: "https://doi.org/10.1177/2050324519900067"
---


Cannabis has been proposed as a useful treatment option in a variety of different physical and mental health conditions, including pain management, multiple sclerosis, epilepsy, depression and anxiety, and it is acknowledged internationally that there is a need for increased research into the use of medical cannabis (Amar, 2006; Black et al., 2019; Department of Health and Social Care, 2019; National Academies of Sciences, Engineering, and Medicine, 2017; National Institute for Health and Care Excellence, 2019). As the interest in the role of medical cannabis is growing, there is also an increase in the consideration of medical cannabis as a harm reduction intervention to reduce the use of illicitly obtained cannabis, since there are currently no medications licensed for managing cannabis dependency (Allsop et al., 2014). Authors’ note  This letter summarises the findings of our recent survey, conducted in a UK substance misuse treatment service to scope the potential for prescribing medical cannabis as a harm reduction strategy. BS is a Medical Lead working for Addaction, a provider of specialist substance misuse services in the UK. Harm Reduction Journal 14; article 58.