---
layout: post
title: "Effects of short-term cannabidiol treatment on response to social stress in subjects at clinical high risk of developing psychosis"
date: 2024-09-24
tags: [Trier social stress test,Stress (biology),Fatty-acid amide hydrolase 1,Psychosis,Missing data,Institute of Psychiatry, Psychology and Neuroscience,Health]
author: Elizabeth Appiah‐Kusi | Natalia Petros | Rebecca Wilson | Marco Colizzi | Matthijs G. Bossong | Lucia Valmaggia | Valeria Mondelli | Philip McGuire | Sagnik Bhattacharyya
institutions: King's College London
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7113209"
---


Cannabidiol (CBD) has antipsychotic and anxiolytic effects. Across the three participant groups, there was a significant F(1,54) = 9.46, p = .003 linear decrease in cortisol reactivity, such that the change in cortisol (cortisol at time 0 min relative to baseline; − 60 min) associated with experimental stress exposure was greatest in HC controls and least in CHR-P patients, with CHR-CBD patients exhibiting an intermediate response (Fig. Collectively, these findings suggest that CHR participants under placebo displayed abnormal neuroendocrine and psychological responses to experimental stress compared with HC participants, and that 7-day treatment with CBD may potentially help partially attenuate these altered responses to experimental stress in CHR participants. [PubMed] [CrossRef] [Google Scholar] [Ref list]  Campos AC, Guimarães FS. [PubMed] [CrossRef] [Google Scholar] [Ref list]