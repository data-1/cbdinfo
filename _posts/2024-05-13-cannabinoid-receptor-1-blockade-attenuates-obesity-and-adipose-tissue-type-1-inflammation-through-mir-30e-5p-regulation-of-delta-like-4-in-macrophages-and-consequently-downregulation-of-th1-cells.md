---
layout: post
title: "Cannabinoid Receptor 1 Blockade Attenuates Obesity and Adipose Tissue Type 1 Inflammation Through miR-30e-5p Regulation of Delta-Like-4 in Macrophages and Consequently Downregulation of Th1 Cells"
date: 2024-05-13
categories:
- 
author: Miranda, Department Of Pathology, Microbiology, Immunology, School Of Medicine, University Of South Carolina, United States, Mehrpouya-Bahrami, Nagarkatti, Prakash S.
tags: [Adipose tissue,Macrophage,Inflammation,MicroRNA,Adipocyte,Interferon gamma,Tumor necrosis factor,T helper cell,CCL2,Diet-induced obesity model,Notch signaling pathway,Insulin resistance,Endocannabinoid system,Cytokine,Appetite,Adipose tissue macrophages,Immune system,Cannabinoid receptor 1,Downregulation and upregulation,Toll-like receptor 4,Inflammatory cytokine,Obesity,Leptin,Cell signaling,Macrophage polarization,Gene expression,Metabolic syndrome,Cell biology,Biology,Biochemistry]
---


Interestingly, work done by our group has shown that treatment of DIO mice with the CB1 antagonist SR141716A reverses obesity and is associated with a decrease in ATM-dependent inflammation in epididymal fat of high-fat diet (HFD)-induced obese mice (18). AM251 Intervention Modulates miRNA Expression in ATMs  Next we determined if AM251-intervention alters miRNA expression in ATMs as we have previously shown that miRNA dysregulation is observed in ATMs from obese mice and contributes to their inflammatory state (21). We found that treatment of DIO mice with AM251 for 4-weeks led to various alterations of miRNA expression in ATMs from epididymal fat. We have also previously demonstrated that the miR-30 family is downregulated in ATMs from obese vs. lean mice, while DLL4 expression is increased in ATMs from obese vs. lean mice, which indicates that miR-30e-5p attenuates inflammation in ATMs through regulation of the Notch signaling pathway (21). Consistent with increased miR-30e-5p expression, we found that DLL4 expression was reduced in adipose tissue from HFD + AM251 mice.

[Visit Link](https://www.frontiersin.org/journals/immunology/articles/10.3389/fimmu.2019.01049/full){:target="_blank" rel="noopener"}


