---
layout: post
title: "Changing the Tone of Clinical Study Design in the Cannabis Industry"
date: 2024-09-16
tags: [Endocannabinoid system,Cannabinoid,Cannabinoid receptor 1,Circadian rhythm,Suprachiasmatic nucleus,Anandamide,CLOCK,Tetrahydrocannabinol]
author: Joseph M. Antony | Alison C. McDonald | Farshid Noorbakhsh | Najla Guthrie | Malkanthi Evans
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7029654"
---


Effect of exogenous cannabinoids on the circadian rhythm  Exogenous cannabinoid consumption can distort perception of time due to modulation of the brain’s circadian clock, SCN, which expresses CB1 receptors. Sleep. [PubMed] [Google Scholar] [Ref list]  [2] Zou S, Kumar U. Cannabinoid Receptors and the Endocannabinoid System: Signaling and Function in the Central Nervous System. et al. [PMC free article] [PubMed] [Google Scholar] [Ref list]  [23] Lucas CJ, Galettis P, Schneider J. [PubMed] [Google Scholar] [Ref list]