---
layout: post
title: "Efficacy of cannabinoids compared to the current standard treatments on symptom relief in persons with multiple sclerosis (CANSEP trial): study protocol for a randomized clinical trial"
date: 2024-09-03
tags: [Cannabidiol,Medical cannabis,Multiple sclerosis,Tetrahydrocannabinol,Cannabinoid,Missing data,Spasticity,Analysis of variance,Randomized controlled trial,Evidence-based medicine,Health,Clinical medicine,Medicine,Health care,Student's t-test]
author: Alami Marrouni | Jutras-Aswad | Marie-Pascale | Marie-Pierre | Jean-Sylvain
---


1.2 Study objectives  The main aims of this study are: (1) to compare the efficacy of THC and CBD, alone and in combination, as add-on therapies to the current standard treatments for relief of spasticity in PwMS, (2) to assess the tolerability profile of THC and CBD, alone and in combination, (3) to identify the mechanisms underlying such therapeutic and adverse effects, considering sex, age, pharmacology, and immune profile. 2 Methods/design  2.1 Study overview  CANSEP is the first Canadian randomized clinical trial focusing on interventions with THC, CBD, or their combination, for the treatment of spasticity in PwMS. Our study is part of the Integrated Cannabis Research Strategy funded by the Canadian Institutes of Health Research (21), and MS Canada. Participants will be allocated to four arms and initially receive THC (4 mg/day), CBD (40 mg/day), THC/CBD combination (THC 4 mg/day and CBD 40 mg/day), or placebo, on the first day. It was developed in response to the lack of evidence about the safety and the efficacy of specific cannabinoids in MS (20).

[Visit Link](https://www.frontiersin.org/journals/neurology/articles/10.3389/fneur.2024.1440678/full){:target="_blank rel="noopener"}

