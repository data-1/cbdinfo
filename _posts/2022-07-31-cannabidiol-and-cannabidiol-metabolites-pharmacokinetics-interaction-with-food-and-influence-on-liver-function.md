---
layout: post
title: "Cannabidiol and Cannabidiol Metabolites: Pharmacokinetics, Interaction with Food, and Influence on Liver Function"
date: 2022-07-31
categories:
- CBD
author: Abbotts, Kieran Shay Struebin, Ewell, Taylor Russell, Butterklee, Hannah Michelle, Bomar, Matthew Charles, Akagi, Dooley
tags: [Cannabidiol,Liquid chromatographymass spectrometry,Dose (biochemistry),Cannabinoid,Pharmacokinetics,Obesity,Basal metabolic rate,Endocannabinoid system,Insulin,Health]
---


The thermic effect of food was examined using two-way ANOVA with repeated measures comparing energy expenditure across time for the two conditions (i.e., CBD and placebo); respiratory exchange ratio and circulating concentrations of glucose, insulin, and triglycerides were compared in the same way. Venous blood (~9 mL) was collected for subsequent analysis of circulating concentrations of CBD and CBD metabolites prior to, and 10, 20, 30, 45, 60, 120, 180, and 240 min after CBD ingestion. Venous blood (~3 mL) was also sampled for analysis of circulating concentrations of markers of liver function prior to, and 60 and 240 min after CBD ingestion. Consistent with previous studies of CBD formulations prepared for use in beverages [ 5 26 ], the water-soluble preparations in the current study proved superior to the hydrophobic, lipid-soluble preparations in that they typically demonstrated faster t, and greater Cand AUC. The endocannabinoid system is known to be involved with the development of multiple liver diseases, including cirrhosis, hepatitis and steatosis [ 55 ], and several studies have reported on adverse liver outcomes following CBD administration [ 25 ].

<hr>[Visit Link](https://www.mdpi.com/2072-6643/14/10/2152/htm){:target="_blank" rel="noopener"}


