---
layout: post
title: "A Scoping Review on Clinical Trials of Pain Reduction With Cannabis Administration in Adults"
date: 2024-09-16
tags: [Cannabinoid receptor,Medical cannabis,Cannabinoid receptor 2,Cannabidiol,Tetrahydrocannabinol,Peripheral neuropathy,Nabiximols,Clinical medicine,Medicine,Pharmacology,Drugs,Health care,Medical specialties,Health]
author: Reham Haleem | Robert F. Wright
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7295551"
---


The cannabis products considered for cancer pain were herbal cannabis - inhalation and/or smoked - and cannabis extract and/or oral. J Pain. J Pain. J Pain. Pain.