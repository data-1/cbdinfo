---
layout: post
title: "Targeting Cannabinoid Receptors: Current Status and Prospects of Natural Products"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Cannabinoid,Dronabinol,Cannabinoid receptor 1,Tetrahydrocannabinol,Cannabinol,Receptor antagonist,Cannabidiol,RVD-Hpα,Allosteric regulation,Cannabinoid receptor antagonist,Medical cannabis,Anandamide,Ligand (biochemistry),Cannabinoid receptor,Allosteric modulator,Synthetic cannabinoids,Receptor (biochemistry),Neurochemistry,Biochemistry,Cell biology]
author: Dongchen An | Steve Peigneur | Louise Antonia Hendrickx | Jan Tytgat | Louise Antonia
---


CB2-selective agonists”. Mixed CB1/CB2 Agonists  Most known synthetic agonists of cannabinoid receptors show little selectivity between CB1 and CB2 [21], but exhibit stronger affinity for cannabinoid receptors compared to endo- and phytocannabinoids [65]. HU-210 ( a), an example of a classical synthetic cannabinoid, is a highly potent cannabinoid receptor agonist, and its potency and affinity at cannabinoid receptors exceed those of many other cannabinoids [66]. Both compounds bind with much higher affinity to CB2 than to CB1, exhibit marked potency as CB2-selective antagonists and behave as inverse agonists that can produce inverse cannabimimetic effects at CB2 by themselves [66]. These peptides can be regarded as novel cannabinoids because they exhibit activity at cannabinoid receptors and have distinct structures from phytocannabinoids and synthetic cannabinoids.

[Visit Link](https://www.mdpi.com/1422-0067/21/14/5064){:target="_blank rel="noopener"}

