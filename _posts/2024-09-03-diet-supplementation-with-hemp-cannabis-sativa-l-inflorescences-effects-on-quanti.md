---
layout: post
title: "Diet supplementation with hemp (Cannabis sativa L.) inflorescences: effects on quanti"
date: 2024-09-03
tags: [Gas chromatography,Fat,Omega-3 fatty acid,Cannabis,Polyunsaturated fat,Hemp]
author: Ruggero Amato | Marianna Oteri | Biagina Chiofalo | Fabio Zicarelli | Nadia Musco | Fiorella Sarubbi | Severina Pacifico | Marialuisa Formato | Pietro Lombardi | Federica Di Bennardo
---


Conjugated linoleic acids alter milk fatty acid composition and inhibit milk fat secretion in dairy cows. [CrossRef] [Google Scholar]  Mierlita D. 2016. [CrossRef] [Google Scholar]  Mierlita D. 2018. Conjugated linoleic acids alter milk fatty acid composition and inhibit milk fat secretion in dairy cows. [PubMed] [CrossRef] [Google Scholar] [Ref list]

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11318486/){:target="_blank rel="noopener"}

