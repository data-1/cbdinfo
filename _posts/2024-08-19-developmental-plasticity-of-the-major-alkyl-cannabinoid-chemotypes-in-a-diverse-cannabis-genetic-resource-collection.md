---
layout: post
title: "Developmental Plasticity of the Major Alkyl Cannabinoid Chemotypes in a Diverse Cannabis Genetic Resource Collection"
date: 2024-08-19
tags: [Cannabidiol,Cannabinoid,Gene,Cannabis,Tetrahydrocannabinolic acid,Biology]
author: Matthew T | Carolyn A | Graham J
---


Cluster analysis of alkyl cannabinoid fractions was performed to provide insight into the categorization and genetic regulation of alkyl cannabinoid chemotypes in Cannabis. FIGURE 2  Distribution of the Major Cyclic and Alkyl Cannabinoid Chemotypes  Chemotypes of 99 individual Cannabis plants from 20 seed accessions were characterized across three developmental stages using LC-MS analysis. Fresh leaf tissue samples were taken at the vegetative and flowering stages and cannabinoid composition was compared with dried floral tissue cannabinoid composition at maturation. To determine the total cannabinoid fraction and to compare the FC3, FC5, Fdicyclic, and Ftricyclic values between juvenile and mature plants, neutral cannabinoids CBDV, CBD, THCV, and THC were expressed as acidic cannabinoids using formulae which accounted for differences in molecular weight:  At maturation, variation in chemotype appeared to segregate within the accessions and so chemotype was reported at the plant level (Figure 3A), although within-accession chemotypic variation was more evident from the Fdicyclic values than from the FC3 values (Figure 3A). The optimal number of clusters based on criterion values as a function of clusters was the predicted three for the di-/tri-cyclic as well as three for the C3-/C5-alkyl cannabinoid fractions (Figure 5A).

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2018.01510/full){:target="_blank rel="noopener"}

