---
layout: post
title: "Impact of Growth Stage and Biomass Fractions on Cannabinoid Content and Yield of Different Hemp (Cannabis sativa L.) Genotypes"
date: 2024-08-20
tags: [Cannabidiol,Cannabis,Tetrahydrocannabinolic acid,High-performance liquid chromatography,Cannabinoid,Hemp,Tetrahydrocannabinol,Cannabis sativa]
author: Lisa Burgel | Jens Hartung | Annegret Pflugfelder | Simone Graeff-Hönninger | Graeff-Hönninger
---


Although these industrial hemp genotypes are mainly cultivated for fibre and seed production, however, cannabinoids offer an additional value. At seed maturity (S4), genotype Futura75 (1.759%), Félina32 (1.639%) and Fédora17 (1.363%), as well as Finola (1.613%) indicated the highest CBDA values in their inflorescence ( ). Hemp 2004, 9, 79–86. Hemp 2004, 9, 97–103. Hemp 2002, 7, 7–23.

[Visit Link](https://www.mdpi.com/2073-4395/10/3/372){:target="_blank rel="noopener"}

