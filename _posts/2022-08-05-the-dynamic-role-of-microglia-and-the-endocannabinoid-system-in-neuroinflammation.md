---
layout: post
title: "The Dynamic Role of Microglia and the Endocannabinoid System in Neuroinflammation"
date: 2022-08-05
categories:
- Cannabinoid
- Endocannabinoid
author: Young, Alexander P., Department Of Pharmacology, Denovan-Wright, Eileen M., Cecilia Hillard, Natasha L. Grimsey, Julian Romero
tags: [Microglia,Cannabinoid receptor type 2,Neuroinflammation,Cannabinoid receptor,Endocannabinoid system,Cannabinoid,Cannabinoid receptor type 1,Inflammation,MAPKERK pathway,Mitogen-activated protein kinase,Cell signaling,Amyloid beta,Neurodegenerative disease,Receptor (biochemistry),Macrophage polarization,Interleukin 6,Cytokine,2-Arachidonoylglycerol,Tumor necrosis factor,Substantia nigra,Receptor antagonist,Interleukin 10,Neurofibrillary tangle,Putamen,Tetrahydrocannabinol,Adenosine A2A receptor,Huntingtin,MPTP,Signal transduction,Inflammatory cytokine,Biotechnology,Neuroscience,Biochemistry,Neurophysiology,Cell biology,Neurochemistry,Cell communication]
---


Many of the CB 2 receptors were expressed in activated M1 microglia, although astrocytes were also identified as CB 2 receptor positive. As aged brains exhibit a higher proportion of pro-inflammatory microglia compared to young brains, it would be expected for aged brains to contain elevated levels of CB 2 receptors as well. This indicated that the enhanced effect on IL-10 release was mediated by the activation of CB 2 but not CB 1 receptors. Front Cel Neurosci 13, 424. doi:10.3389/fncel.2019.00424 PubMed Abstract | CrossRef Full Text | Google Scholar  Aso, E., Juvés, S., Maldonado, R., and Ferrer, I. Increased Interleukin-6 Expression by Microglia from Brain of Aged Mice.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2021.806417/full){:target="_blank" rel="noopener"}


