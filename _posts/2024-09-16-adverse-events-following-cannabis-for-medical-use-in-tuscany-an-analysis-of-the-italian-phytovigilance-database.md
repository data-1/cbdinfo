---
layout: post
title: "Adverse events following cannabis for medical use in Tuscany: An analysis of the Italian Phytovigilance database"
date: 2024-09-16
tags: [Medical cannabis,Cannabidiol,Cannabis (drug),Psychosis,Dose (biochemistry),Tetrahydrocannabinol,Cannabinoid,Opioid,Mental disorder,Pregabalin,Pharmacology,Diseases and disorders,Health care,Clinical medicine,Medical specialties,Medicine,Health]
author: Giada Crescioli | Niccolò Lombardi | Alessandra Bettiol | Francesca Menniti‐Ippolito | Roberto Da | M Parrilli | Martina Del Lungo | Eugenia Gallo | Alessandro Mugelli | Valentina Maggini | Fabio Firenzuoli | Alfredo Vannacci
institutions: University of Florence
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/6983517"
---


DISCUSSION  Since 2006 (date of cannabis for medical use approval) until December 2018, the Italian National Institute of Health collected within its Phytovigilance database a total of 103 AE reports following cannabis for medical use administration, mainly coming from Tuscany (61 reports).17 The high number of AE reports collected in this Italian region could be related to the well‐established use of medical cannabis18 and to the clinicians' knowledge of AE reporting procedures.19 To the best of our knowledge this is the first case series study describing all medical cannabis‐related AEs observed in the general population (all age groups). Drug‐drug interactions  In the majority of cases reported in our sample, patients are administered with at least another concomitant prescribed medication over their cannabis prescription. Eur Rev Med Pharmacol Sci. Cannabis. Medicinal cannabis‐potential drug interactions.