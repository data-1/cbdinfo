---
layout: post
title: "Cholesterol as a modulator of cannabinoid receptor CB2 signaling"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Ligand (biochemistry),Cell membrane,Receptor (biochemistry),Receptor antagonist,G protein,Cell signaling,Cholesterol,Cannabinoid,Agonist,Protein,Lipid bilayer,Lipid,Thin-layer chromatography,Affinity chromatography,Ligand binding assay,Inverse agonist,Nutrients,Molecular biology,Cell biology,Biochemistry,Biology]
author: Malliga R | Thomas T | Nathan J
---


However, these same ligands acted as partial inverse agonists on CB2 expressed in CHO cells (Fig. We quantified the relative content of lipids and cholesterol in several preparations of membranes of mammalian, insect, and E. coli cells (Supporting Fig. 5b); and in membranes of Sf9 cells expressing CB2, it acted as a partial agonist (Fig. The levels of protein, and the density of ligand binding sites for receptor preparations reconstituted in liposomes with different content of cholesterol varied only slightly (Supporting Fig. If a residue in the 40% cholesterol structure deviated more than the same residue in the 0% cholesterol structure, the resulting quantity would be positive, and if less, negative.

[Visit Link](https://www.nature.com/articles/s41598-021-83245-6){:target="_blank rel="noopener"}

