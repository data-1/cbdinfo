---
layout: post
title: "Cannabinoid extract in microdoses ameliorates mnemonic and nonmnemonic Alzheimer’s disease symptoms: a case report - Journal of Medical Case Reports"
date: 2022-07-31
categories:
- CBD
- THC
author: Ruver-Martins, Ana Carolina, Laboratório De Cannabis Medicinal E Ciência Psicodélica, Department Of Medicine, Universidade Federal Da Integração Latino-Americana, Unila, Foz Do Iguaçu, Bicca, Maíra Assunção, Department Of Neurosurgery
tags: [Cannabinoid,Endocannabinoid system,Cannabidiol,Dementia,Tetrahydrocannabinol,Dose (biochemistry),Medicine,Health,Clinical medicine,Health sciences,Neuroscience,Medical specialties,Health care]
---


Table 1 Summary of laboratory tests throughout the experimental treatment and follow-up Full size table  AD history  The patient was diagnosed with AD 2 years prior to the start of this experimental treatment, according to brain magnetic resonance imaging, anamnesis, and clinical assessment, which includes and is not limited to the use of National Institute of Neurologic and Communicative Disorders and Stroke, and the Alzheimer Disease and Related Disorders Association (NINCDS-ADRDA) criteria. Of note, the patient continues to use the extract at this dose without any additional drug of continuous use, after the official evaluation/follow-up for this case report ended. Here we report evidence that the cannabinoid extract improved MMSE (Fig. 1D) scores in the subject evaluated. We tried to titrate the dose up to 1 mg THC, but the most frequent dose was 500 µg THC.

<hr>[Visit Link](https://jmedicalcasereports.biomedcentral.com/articles/10.1186/s13256-022-03457-w){:target="_blank" rel="noopener"}


