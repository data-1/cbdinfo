---
layout: post
title: "MicroRNA-29a Attenuates Diabetic Glomerular Injury through Modulating Cannabinoid Receptor 1 Signaling"
date: 2024-08-19
tags: [Cannabinoid receptor 1,Transcription factor Jun,Cannabinoid receptor 2,MicroRNA,Tumor necrosis factor,Gene expression,Glomerulus (kidney),Diabetic nephropathy,Downregulation and upregulation,Interleukin 6,Kidney disease,Kidney,Mesangial cell,Real-time polymerase chain reaction,Extracellular matrix,Life sciences,Cell biology,Biochemistry,Biotechnology,Molecular biology,Biology]
author: Chun-Wu Tung | Cheng Ho | Yung-Chien Hsu | Shun-Chen Huang | Ya-Hsueh Shih | Chun-Liang Lin | Chun-Wu | Yung-Chien | Shun-Chen | Ya-Hsueh
---


Emerging evidence has shown the protective effects of PPAR-γ against diabetic kidney disease [26,27]; our quantitative RT-PCR and immunohistochemical studies ( and ) showed that overexpression of miR-29a significantly reversed the attenuated PPAR-γ expression in diabetic mice. Induction of proteinuria by cannabinoid receptors 1 signaling activation in CB1 transgenic mice. Diabetes 2014, 63, 2120–2131. Share and Cite  MDPI and ACS Style  Tung, C.-W.; Ho, C.; Hsu, Y.-C.; Huang, S.-C.; Shih, Y.-H.; Lin, C.-L. MicroRNA-29a Attenuates Diabetic Glomerular Injury through Modulating Cannabinoid Receptor 1 Signaling. Molecules 2019, 24, 264. https://doi.org/10.3390/molecules24020264  AMA Style  Tung C-W, Ho C, Hsu Y-C, Huang S-C, Shih Y-H, Lin C-L. MicroRNA-29a Attenuates Diabetic Glomerular Injury through Modulating Cannabinoid Receptor 1 Signaling.

[Visit Link](https://www.mdpi.com/1420-3049/24/2/264){:target="_blank rel="noopener"}

