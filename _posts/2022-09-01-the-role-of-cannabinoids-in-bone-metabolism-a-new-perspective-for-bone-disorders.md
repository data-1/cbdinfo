---
layout: post
title: "The Role of Cannabinoids in Bone Metabolism: A New Perspective for Bone Disorders"
date: 2022-09-01
categories:
- Cannabinoid
author: Saponaro, Ferrisi, Gado, Polini, Saba, Manera, Chiellini, Federica Saponaro, Rebecca Ferrisi, Francesca Gado
tags: [RANKL,Osteoporosis,Cannabinoid receptor type 2,Bone,Single-nucleotide polymorphism,Osteoclast,Leptin,NF-B,Bone resorption,Vitamin D,Sample preparation in mass spectrometry,Osteoprotegerin,2-Arachidonoylglycerol,Anandamide,Cannabinoid,Osteoblast,Cell biology,Biotechnology,Biochemistry]
---


Concentration values in healthy, normal-weight females (F,= 76) and males (M,= 45) ranged in the following intervals: AEA, 0.40–1.65 pmol/mL (F) and 0.41–1.88 pmol/mL (M); 2-AG, 0.69–3.44 pmol/mL (F) and 0.71–5.40 pmol/mL (M); 1-AG, 0.21–1.09 pmol/mL (F) and 0.26–1.78 pmol/mL (M); PEA, 7.28–28.8 pmol/mL (F) and 8.01–21.0 pmol/mL (M); and OEA, 2.41–10.20 pmol/mL (F) and 2.41–9.06 pmol/mL (M) [ 34 ]. Thus, the sample extraction was based on C18 Sep-Pak SPE cartridges from Waters (Milford, MA, USA), and the following tandem mass spectrometry quantification was carried out in both the positive and the negative MRM, depending on the chemical structures of the analytes. The method was less sensitive compared to that of Fanelli et al. as the LLOQ were 0.288, 8.465, 10.702, and 0.712 pmol/mL for AEA, 2-AG, PEA, and OEA, respectively (1-AG was not taken into consideration). Using liquid-liquid extraction combined with UHPLC coupled to tandem mass spectrometry, Ney et al. quantified AEA, 2-AG, and OEA, along with cortisol and progesterone, in plasma samples from 121 healthy subjects, 48 females and 73 males [ 48 ]. Ney et al. assayed the same analytes in saliva samples from 90 healthy subjects, 55 females and 35 males, by using a method strictly derived from that used for plasma, except for the LLE, which made use of a freezer-cold 50:50 ethyl methanol:acetone mixture [ 48 ].

<hr>

[Visit Link](https://www.mdpi.com/1422-0067/22/22/12374/htm){:target="_blank" rel="noopener"}


