---
layout: post
title: "The Endocannabinoid/Cannabinoid Receptor 2 System Protects Against Cisplatin-Induced Hearing Loss"
date: 2024-08-18
tags: [Cannabinoid receptor 2,Synapse,Neurotransmitter,Medical specialties,Biology,Cell biology,Biochemistry]
author: Leonard P
---


ABR threshold shifts induced by cisplatin in rats pretreated with AM630 + JWH015 were 5.6 ± 1.2, 15.6 ± 2.4, and 24.3 ± 3.1 dB at 8, 16, and 32 kHz, respectively, which were similar to those obtained with cisplatin alone. This finding suggests that CB2R protects both the sensory hair cells and ribbon synapses, at least in the basal turn of the cochlea, which could account for its overall efficacy in reducing cisplatin ototoxicity. Rats which were treated with CB2R siRNA, followed by cisplatin, showed significantly increased threshold shifts at 8 and 16 kHz over those observed in the scrambled siRNA plus cisplatin group. Administration of 10 μM cisplatin to cell cultures produced significant reductions in cell viability which averaged 28.0 ± 5.1%, 52.1 ± 8.6%, and 62.6 ± 7.8% of control cells, respectively, in UMSCC10B, HeyA8, and HCT116 WT cells (Figure 8). Trans-tympanic administration of CB2R agonist provides effective protection against cisplatin-induced hearing loss and should produce limited side effects of this drug.

[Visit Link](https://www.frontiersin.org/journals/cellular-neuroscience/articles/10.3389/fncel.2018.00271/full){:target="_blank rel="noopener"}

