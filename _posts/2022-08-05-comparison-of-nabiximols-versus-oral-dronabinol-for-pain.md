---
layout: post
title: "Comparison of nabiximols versus oral dronabinol for pain"
date: 2022-08-05
categories:
- Cannabinoid
- THC
author: Dove Press, Michael A Ueberall, Ute Essner, Carlos Vila Silván, Gerhard Hh Mueller-Schwefe, Author Guidelines
tags: [Neuropathic pain,Cannabinoid,Analgesic,Tetrahydrocannabinol,Cannabidiol,Pain,Peripheral neuropathy,Clinical medicine,Health,Medicine,Health sciences,Health care,Medical specialties]
---


Superior analgesic effects compared with placebo were demonstrated in randomized clinical trials (RCTs)15–19 and in a large observational study.20 A recent meta-analysis of nine RCTs involving 1289 participants concluded that NBX was superior to placebo in reducing chronic neuropathic pain, with a small but significant effect size.21  In the United States, dronabinol (DRO), a synthetic THC, is approved in adults for the treatment of anorexia-associated weight loss in AIDS patients and for cancer chemotherapy-associated nausea and vomiting.22,23 In Europe, DRO is approved in Austria, Denmark and Ireland for nausea and vomiting refractory to conventional treatment in cancer and palliative care; and additionally for cancer pain in Denmark and appetite stimulation in HIV in Ireland.24 Limited data are available for the efficacy of DRO in neuropathic pain and results are equivocal.25,26  Although neither NBX nor DRO is currently approved by the European Medicines Agency (EMA) for treatment of neuropathic pain, since March 2017, German physicians have been permitted to prescribe cannabis-based medicines for on-/off-label use in patients with severe disease resistant to available therapeutic options.27,28 The aim of the current study was to compare the effectiveness and tolerability of NBX with DRO for neuropathic pain by analyzing real-world data from the German Pain e-Registry (GPeR), a national web-based pain treatment registry that collects and stores data from around 230 pain centers and more than 800 pain specialists across the country. At the end of week 24, mean relative change (improvement) versus baseline in the ASR-9 composite score (primary effectiveness outcome) was significantly greater with NBX (Figure 3A) than DRO (Figure 3B): 60.8% vs 46.4% (LSM difference 14.4%; p<0.001; Cohen’s d effect size, 1.268). Table 3 Change in Pain Intensity from Baseline to Week 24 in Nabiximols and Dronabinol Groups  Figure 4 shows response rates of patients treated with NBX (Figure 4A) or DRO (Figure 4B) who reported improvement from baseline in the ASR-9 composite score and individual components over the 24-week evaluation period, according to level of achievement (≥10%, ≥20%, etc.). In comparison to DRO, a significantly higher proportion of NBX-treated patients reported clinically relevant improvement compared with baseline over the entire 24-week evaluation period, as well as significantly fewer TRAEs and TRAE-related treatment discontinuations. Importantly, both cannabinoid treatments were able to reduce patients’ requirement for long-acting opioid analgesics as background medication and especially immediate-release/rapid-onset opioid analgesics as rescue medication, which are known to carry a significant risk in patients with chronic (neuropathic) pain.

<hr>

[Visit Link](https://www.dovepress.com/comparison-of-the-effectiveness-and-tolerability-of-nabiximols-thccbd--peer-reviewed-fulltext-article-JPR){:target="_blank" rel="noopener"}


