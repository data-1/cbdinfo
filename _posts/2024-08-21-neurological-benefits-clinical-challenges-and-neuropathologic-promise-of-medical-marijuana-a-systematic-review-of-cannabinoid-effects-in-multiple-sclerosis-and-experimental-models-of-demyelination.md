---
layout: post
title: "Neurological Benefits, Clinical Challenges, and Neuropathologic Promise of Medical Marijuana: A Systematic Review of Cannabinoid Effects in Multiple Sclerosis and Experimental Models of Demyelination"
date: 2024-08-21
tags: [Cannabinoid receptor 1,Experimental autoimmune encephalomyelitis,Multiple sclerosis,Remyelination,Clinical medicine,Medical specialties]
author: Victor Longoria | Hannah Parcel | Bameelia Toma | Annu Minhas | Rana Zeine
---


Because animal studies offer more detailed mechanistic information about the pathobiology of neuroinflammation and remyelination than is possible with human studies, the second branch of this paper reviews the effects of cannabinoid use in several experimental animal models of MS. Studies assessed measures of clinical disease severity, CNS inflammatory infiltration, microglial activation, neuroprotection, demyelination, and remyelination in actively induced and adoptively transferred EAE, Theiler’s murine encephalomyelitis virus-induced demyelinating disease (TMEV-IDD), and toxin-induced demyelination models. Effects of cannabinoids on spasticity in multiple sclerosis. [Google Scholar] [CrossRef]  Miller, S.D. Cannabinoids and multiple sclerosis. study).

[Visit Link](https://www.mdpi.com/2227-9059/10/3/539){:target="_blank rel="noopener"}

