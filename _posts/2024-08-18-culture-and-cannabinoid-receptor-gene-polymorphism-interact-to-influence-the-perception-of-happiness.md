---
layout: post
title: "Culture and cannabinoid receptor gene polymorphism interact to influence the perception of happiness"
date: 2024-08-18
tags: [Happiness,Fatty-acid amide hydrolase 1,5-HTTLPR,Endocannabinoid system,Genotype,Behavioural sciences,Psychology,Social psychology,Psychological concepts]
author: Keiko Ishii | Yohsuke Ohtsubo | Yasuki Noguchi | Misaki Ochi | Hidenori Yamasue | Masahiro Matsunaga | Takahiko Masuda
---


Furthermore, CNR1 polymorphism has been associated with happiness [7]. A previous study indicated that both the subjective happiness level and situation-specific happiness were higher in C allele carriers than in individuals with the TT genotype [7]. Japanese people may be less likely to have a positive life event that impacts the subjective happiness level compared to Canadians. First, we only compared individuals from Japan and Canada, making any generalizations on differences in the effects of CNR1 polymorphism between different cultures highly speculative. Although we only investigated the association between CNR1 polymorphism and happiness in the present study, it is possible that FAAH and cannabinoid receptor gene polymorphisms interact to influence the perception of happiness.

[Visit Link](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0209552){:target="_blank rel="noopener"}

