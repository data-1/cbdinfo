---
layout: post
title: "Anti-staphylococcal activity of soilless cultivated cannabis across the whole vegetation cycle under various nutritional treatments in relation to cannabinoid content"
date: 2024-09-02
tags: [Minimum inhibitory concentration,Tetrahydrocannabinolic acid,Staphylococcus aureus,Cannabinoid,Cannabis,Antimicrobial resistance,Cannabis sativa,Coefficient of determination,Antibiotic,Methicillin-resistant Staphylococcus aureus,Filtration,Cannabis cultivation,Biology]
author: 
---


Statistical evaluation of the dependence of growth inhibition on individual phytocannabinoids  Statistical analyses based on stepwise regression models of the dependence of the MIC on S. aureus, ATCC 29213 and 43300 strains and on the concentrations of individual cannabinoids in cannabis extracts showed that THCVA was the most significant of all eight models and alone contributed to adjusted R-squared values of 0.743 and above. In the context of our research, all the ethanolic extracts from cannabis plants inhibited the growth of both S. aureus strains in vitro (MIC 32–256 µg/mL), including antibiotic-resistant and antibiotic-sensitive forms, across all vegetation stages (weeks 1–13) under the various nutritional treatments. The HPLC–DAD results showed that the most concentrated cannabinoid in the plants and consequently in the extracts was THCA. Cultivation of cannabis plants  The soilless cultivation of cannabis plants in expanded clay was conducted in a room with controlled lighting, temperature, and humidity. Each file contained a table with 19 columns, 2 columns for the dependent variables (MICs of cannabis extracts against S. aureus strains, ATCC 29213 and 43300) and 17 columns for the independent variables representing the contents of the examined phytocannabinoids in the extracts.

[Visit Link](https://www.nature.com/articles/s41598-024-54805-3){:target="_blank rel="noopener"}

