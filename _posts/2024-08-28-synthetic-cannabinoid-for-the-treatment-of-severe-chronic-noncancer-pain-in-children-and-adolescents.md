---
layout: post
title: "Synthetic cannabinoid for the treatment of severe chronic noncancer pain in children and adolescents"
date: 2024-08-28
tags: [Cannabinoid,Medical cannabis,Nabilone,Peripheral neuropathy,Cannabis (drug),Pain,Chronic pain,Health care,Clinical medicine,Health,Medicine,Diseases and disorders,Medical specialties]
author: Naiyi Sun | Natasha Cunha | Shawnee Amar | Stephen Brown | Naiyi  Sun | Natasha  Cunha | Shawnee  Amar | Stephen  Brown
---


Stimulation of the CB2 receptor results in anti-inflammatory effects. The pediatric chronic pain clinic at The Hospital for Sick Children provides interdisciplinary pain management to children and adolescents. None of the patients started nabilone treatment less than 3 months from the time of chart review. Side effects and effectiveness were evaluated at follow-up visits, which varied between 1 and 6 months after initiation of therapy. The median daily nabilone dose prescribed was 1.5 mg. Due to limited documentation of follow-up pain scores, only 13 patients had documented pain scores both at the start of treatment and at follow-up visits.

[Visit Link](https://www.tandfonline.com/doi/10.1080/24740527.2022.2132138){:target="_blank rel="noopener"}

