---
layout: post
title: "Cannabis and autism, explained"
date: 2024-09-24
tags: [Cannabidiol,Medical cannabis,Cannabis (drug),Cannabinoid,Cannabis,Drugs,Pharmacology,Health,Psychoactive drugs,Clinical medicine]
author: Peter Hess
institutions: 
doi: "https://doi.org/10.53053/xdrx4830"
---


What is medical marijuana? Is medical marijuana legal? The Rett syndrome trial is not focused on alleviating seizures, but on improving cognitive and behavioral problems. How might cannabis help autistic people? Cannabis may have effects that go beyond the cannabinoid receptors, too.