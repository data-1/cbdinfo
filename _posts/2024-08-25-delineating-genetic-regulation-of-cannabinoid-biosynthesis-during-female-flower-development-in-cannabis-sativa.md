---
layout: post
title: "Delineating genetic regulation of cannabinoid biosynthesis during female flower development in Cannabis sativa"
date: 2024-08-25
tags: [High-performance liquid chromatography,Gene expression,Tetrahydrocannabinolic acid,Biochemistry,Biology,Biotechnology]
author: J. K | M. S | B. L | N. J | H. J | N. C | J. E | A. L | S. J | T. D
---


The extracts were stored at −20°C until further analysis. The enzyme prenyltransferase (PT) catalyzes the alkylation of GPP and OA to create the precursor cannabinoid, cannabigerolic acid (CBGA). In GG, peak PT1 expression is much higher than PT4 (Figure 1b,d), and expression of both genes abolished as the flowers further matured. In CW, THC biosynthesis showed a very similar trend to that of CBD production (Figure 2a,c). In GG, GPPS expression was highly upregulated starting in week 4 (Figure 3b).

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1002/pld3.412){:target="_blank rel="noopener"}

