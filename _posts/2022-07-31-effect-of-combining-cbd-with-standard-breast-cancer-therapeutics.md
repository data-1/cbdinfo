---
layout: post
title: "Effect of combining CBD with standard breast cancer therapeutics"
date: 2022-07-31
categories:
- CBD
author: 
tags: [Palbociclib,Breast cancer,Estrogen receptor,Tamoxifen,Estrogen,Cancer,Cannabidiol,Medical treatments,Clinical medicine,Endocrine system,Medical specialties,Drugs,Neoplasms,Causes of death,Diseases and disorders,Biochemistry,Health]
---


Breast cancer is the most common malignancy in women worldwide. The aim of this study was to investigate the effect of cannabidiol on estrogen receptor-positive and estrogen receptor-negative representative breast cancer cell lines in combination with standard therapeutic agents used in clinical practice. The effects of cannabidiol in combination with the endocrine therapeutics tamoxifen, fulvestrant, and the cyclin-dependent kinase inhibitor palbociclib on breast cancer cell viability were examined. The addition of cannabidiol to tamoxifen had an additive negative effect on cell viability in ER+ ​in estrogen receptor positive T-47D line. Cannabidiol did not attenuate the effect of standard treatment of hormone receptor-positive breast cancer with fulvestrant and palbociclib.

<hr>[Visit Link](https://www.sciencedirect.com/science/article/pii/S2667394022000120){:target="_blank" rel="noopener"}


