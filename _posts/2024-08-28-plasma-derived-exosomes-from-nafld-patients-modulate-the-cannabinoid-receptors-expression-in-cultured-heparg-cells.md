---
layout: post
title: "Plasma-Derived Exosomes from NAFLD Patients Modulate the Cannabinoid Receptors’ Expression in Cultured HepaRG Cells"
date: 2024-08-28
tags: [Cannabinoid receptor 2,Exosome (vesicle),Inflammation,Cannabinoid receptor 1,Stearoyl-CoA 9-desaturase,Metabolic dysfunction–associated steatotic liver disease,Complementary DNA,Biochemistry,Cell biology,Biology]
author: Valentina De Nunzio | Livianna Carrieri | Maria Principia Scavo | Tamara Lippolis | Miriam Cofano | Giusy Rita Caponio | Valeria Tutino | Federica Rizzi | Nicoletta Depalo | Alberto Ruben Osella
---


Cell. Cell. Cells 2021, 10, 586. Cell. Cells 2020, 9, 817.

[Visit Link](https://www.mdpi.com/1422-0067/24/2/1739){:target="_blank rel="noopener"}

