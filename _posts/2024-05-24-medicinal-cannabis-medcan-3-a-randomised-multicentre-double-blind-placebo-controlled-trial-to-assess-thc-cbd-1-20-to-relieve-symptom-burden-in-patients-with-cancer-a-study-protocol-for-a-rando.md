---
layout: post
title: "Medicinal Cannabis (MedCan 3): a randomised, multicentre, double-blind, placebo-controlled trial to assess THC/CBD (1:20) to relieve symptom burden in patients with cancer—a study protocol for a rando"
date: 2024-05-24
categories:
- 
author: 
tags: [Cannabidiol,HTTP cookie,Medical cannabis,Palliative care,Tetrahydrocannabinol,Clinical trial,Randomized controlled trial,Dose (biochemistry),Cannabinoid,Placebo,Regression analysis,Informed consent,Placebo-controlled study,Actigraphy,Privacy,Clinical medicine,Health care,Medicine,Health]
---


Secondary objectives include (a) ascertaining the median dose of 1:20 THC/CBD formulation achieved when patients are asked to titrate to effect or tolerance within a pre-specified range; (b) describing MC-induced change in individual symptom scores at days 7, 21 and 28; and (c) assessing the change in total physical and emotional scores, global impression of change, anxiety and depression and sleep quality. b)  The investigator will complete a trial prescription with the participant’s study ID number and provide this to the trial pharmacy. Assessments  The research staff will contact all participants twice weekly for the first 2 weeks in addition to outpatient clinic reviews on days 0, 7, 14, 21 and 28, with outcome measures recorded at each of these time points. Participants will be contacted at day 56 (+ 4 weeks post-last dose) to assess for adverse events as well as record post-trial cannabinoid use. The DASS-21 will be used for assessment at baseline (day 0) and at days 14 and 28.

[Visit Link](https://trialsjournal.biomedcentral.com/articles/10.1186/s13063-024-08091-z){:target="_blank" rel="noopener"}


