---
layout: post
title: "Bridging Disciplines: Applications of Forensic Science and Industrial Hemp"
date: 2022-07-31
categories:
- Hemp
author: Finley, Sheree J., Department Of Physical Sciences, Forensic Science Programs, Alabama State University, United States, Javan, Gulnaz T., Green, Robert L.
tags: [Cannabidiol,Hemp,Cannabis,Tetrahydrocannabinol,Cannabis sativa,Cannabinoid,Cannabis (drug),Drug test,Tetrahydrocannabinolic acid,Medical cannabis,Plant]
---


Hemp Diseases  Hemp diseases are primarily caused by fungi and rarely by bacteria and viruses (Table 1; McPartland, 1997; Kusari et al., 2013; Giladi et al., 2020; Jerushalmi et al., 2020; Chiginsky et al., 2021; Punja, 2021). As it relates to industrial hemp, analytical methods have primarily been used for forensic differentiation between illegal drug-type Cannabis and legal products (i.e., industrial fibers and CBD-rich/THC-poor materials; Hayley et al., 2018). Hemp 9, 15–36. Flower and foliage-infecting pathogens of marijuana (Cannabis sativa L.) plants. Google Scholar  Small, E. (2015).

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fmicb.2022.760374/full){:target="_blank" rel="noopener"}


