---
layout: post
title: "Anti-cancer properties of cannflavin A and potential synergistic effects with gemcitabine, cisplatin, and cannabinoids in bladder cancer - Journal of Cannabis Research"
date: 2022-07-26
categories:
- flavonoids
author: Tomko, Andrea M., Faculty Of Medicine, Department Of Pharmacology, Dalhousie University, Halifax, Whynot, Erin G., Dupré, Denis J.
tags: [Bladder cancer,Tetrahydrocannabinol,Cannabidiol,Cannabis sativa,Chemotherapy,Cannabinoid,Bafilomycin,Cancer,Clinical medicine]
---


The results show that cannflavin A concentrations ranging from 2.5 to 50 μM do not induce significant cytotoxicity compared to their vehicle control, while known chemotherapeutic treatments (gemcitabine and cisplatin) significantly reduce cell viability in the non-tumorigenic cell line (Fig. 2 Effects of cannflavin A on cell viability. The results from Fig. In these cells, the combination of cannflavin A at concentrations of 12.5 and 50 μM significantly increased the cytotoxic effects between the concentrations of 0.156–3.13 μM of cisplatin (Fig. Significant effects were also observed in T24 cells for the combinations of 12.5 μM cannflavin A and lower concentrations of the chemotherapeutic agent combination (0.159–1.56 μM cisplatin) (Fig.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00151-y){:target="_blank" rel="noopener"}


