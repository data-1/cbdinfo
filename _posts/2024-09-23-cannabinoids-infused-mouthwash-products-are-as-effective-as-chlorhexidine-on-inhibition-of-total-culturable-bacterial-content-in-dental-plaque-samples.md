---
layout: post
title: "Cannabinoids infused mouthwash products are as effective as chlorhexidine on inhibition of total-culturable bacterial content in dental plaque samples"
date: 2024-09-23
tags: [Mouthwash,Dental plaque,Disk diffusion test,Minimum inhibitory concentration,Oral hygiene,Chlorhexidine,Periodontology,Human tooth,Gingivitis,Mouth,Medical specialties,Dentistry]
author: Kumar Vasudevan | Veronica Stahl
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7819473/"
---


Both chlorhexidine 0.2% and cannabinoids infused mouthwash products were effective against all the samples tested. Results  Cannabinoid infused mouthwash products perform equal or better than that of chlorhexidine 0.2%  Chlorhexidine 0.2% (CHX 0.2%) showed consistent bacterial growth inhibition with clear zone of inhibition on all the samples tested in this study (Fig. VS performed the dental plaque sampling. [PMC free article] [PubMed] [CrossRef] [Google Scholar]  Richards D. Chlorhexidine mouthwash more effective than dentifrice or gel. [PMC free article] [PubMed] [CrossRef] [Google Scholar] [Ref list]  Stahl V, Vasudevan K. Comparison of efficacy of cannabinoids versus commercial Oral care products in reducing bacterial content from dental plaque: a preliminary observation.