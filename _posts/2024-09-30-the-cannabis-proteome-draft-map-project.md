---
layout: post
title: "The Cannabis Proteome Draft Map Project"
date: 2024-09-30
tags: [UniProt,Proteomics,Protein,DNA annotation,Post-translational modification,Tandem mass spectrometry,Genomics,DNA sequencing,FASTA format,Metabolomics,Biochemistry,Molecular biology,Biology,Life sciences,Biotechnology]
author: Conor Jenkins | Benjamin C. Orsburn
pubdate: "2020-01-16"
institutions: University of Virginia
doi: "https://www.mdpi.com/1422-0067/21/3/965"
---


Correlation Analysis between Small Molecules and Proteins  Correlation analysis in proteomics has been shown to be a powerful tool in the identification of cooperating proteins in biological processes [32]. Protein analysis by shotgun/bottom-up proteomics. Proteomics 2014, 13, 339–347. Proteomics 2018, 17, 1850–1863. Proteomics 2018, 18.