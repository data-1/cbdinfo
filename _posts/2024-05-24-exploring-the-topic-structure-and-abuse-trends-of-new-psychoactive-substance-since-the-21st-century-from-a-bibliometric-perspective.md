---
layout: post
title: "Exploring the topic structure and abuse trends of new psychoactive Substance since the 21st century from a bibliometric perspective"
date: 2024-05-24
categories:
- 
author: 
tags: [Synthetic cannabinoids,Designer drug,Bibliometrics,Substance abuse,Research,Pharmacology,Fentanyl,Psychoactive drugs,Drugs acting on the nervous system]
---


The clustering network is calculated by extracting keywords from the articles using the endogenous clustering algorithm in CiteSpace on the reference co-citation network. The top 10 highly cited articles listed in Table 2 were mainly reviews (7 of 10) summarizing information on synthetic cannabinoid and synthetic cathinone, which revealed the extensive abuse of these two substances and continuing concern from researchers. Specifically, the research hotspots were mainly synthetic cathinone and synthetic cannabinoid in early 2010 s. Since 2018, more and more research had gradually shifted to synthetic opioids, which was basically consistent with the prevalence and abuse of fentanyl, fentanyl analogs and other new synthetic opioids in recent years (UNODC, 2022). 8 were generally consistent with the results of keywords burstiness analysis. The first 6 articles focused on the study of synthetic cannabinoid and synthetic cathinone in early 2010 s. Since 2016, the latest four articles largely focused on summarizing information on the epidemiology, pharmacology and legal status of various NPS as well as proposing new monitoring strategies to address NPS abuse.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S1319016424000410){:target="_blank" rel="noopener"}


