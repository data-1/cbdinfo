---
layout: post
title: "Understanding the role of the CB1 toggle switch in interaction networks using molecular dynamics simulation"
date: 2022-09-05
categories:
- Cannabinoid
author: Ji, Department Of Brain, Cognitive Sciences, Dgist, Daegu, Republic Of Korea, Yang, Yu, Core Protein Resources Center, Author Notes
tags: [G protein-coupled receptor,Alpha helix,Cannabinoid receptor,Biotechnology,Chemistry,Cell biology,Biochemistry]
---


Figure 2 Structural and dynamical differences between wildtype and mutant CB1 for each activation state. (A) For each activation state, all residues are colored as blue according to their residual interaction energies. In both CB1 receptor activation states, the D163 residue interacts with two neighboring residues in TM7, D390 (D7.46), and N393 (N7.49). The residual interaction energy analysis shows that there are more strong interactions in the N-terminus region of inactive CB1 receptor (Fig. (B) The interaction network among ECL2, TM5, TM6, and ECL3 of active and inactive CB1.

<hr>

[Visit Link](https://www.nature.com/articles/s41598-021-01767-5){:target="_blank" rel="noopener"}


