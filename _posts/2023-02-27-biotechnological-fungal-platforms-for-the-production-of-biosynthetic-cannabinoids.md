---
layout: post
title: "Biotechnological Fungal Platforms for the Production of Biosynthetic Cannabinoids"
date: 2023-02-27
categories:
- Cannabinoid
author: Kosalková, Barreiro, Sánchez-Orejas, Cueto, García-Estrada, Katarina Kosalková, Carlos Barreiro, Isabel-Clara Sánchez-Orejas, Laura Cueto, Carlos García-Estrada
tags: [Dronabinol,Tetrahydrocannabinol,Biochemistry,Biology,Biotechnology]
---


The expression of THCAS in these hosts was very low, although it opened the door to the possibility of biotechnological Δ-THCA production by means of biotechnological platforms.was initially used as a potential host microorganism, but it was quickly discarded, since no functional expression of THCAS was achieved in this microorganism [ 74 75 ], thereby suggesting that a eukaryotic expression system was necessary for appropriate enzyme folding and maturation. The introduction of the OA biosynthetic genes CsTKS and CsOAC, a gene encoding a previously undiscovered enzyme with geranylpyrophosphate:olivetolate geranyltransferase activity (CsPT4) involved in CBGA biosynthesis, together with THCAS and CBDAS (with a vacuolar localization tag), allowed S. cerevisiae to produce 1.1 mg/L Δ9-THCA or 4.3 μg/L CBDA from hexanoic acid, or 2.3 mg/L Δ9-THCA or 4.2 μg/L CBDA from galactose. In S. cerevisiae , CBDA was produced upon the addition of hexanoic acid. In another patent, S. cerevisiae was engineered to incorporate the cannabinoid biosynthetic pathway, including mutant variants of Erg20. The inclusion of CsPT4 led to the production of 215.6 mg/L CBGA after feeding 1 mM OA [ S. cerevisiae have also been reported in other patents [ In addition to the scientific papers indicated above, which represent some of the research carried out in this field, several patents have been published dealing with the biosynthesis of cannabinoids in yeast.

<hr>

[Visit Link](https://www.mdpi.com/2309-608X/9/2/234){:target="_blank" rel="noopener"}


