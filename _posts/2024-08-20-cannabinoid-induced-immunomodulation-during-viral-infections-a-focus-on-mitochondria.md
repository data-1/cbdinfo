---
layout: post
title: "Cannabinoid-Induced Immunomodulation during Viral Infections: A Focus on Mitochondria"
date: 2024-08-20
tags: [Mitophagy,Mitochondrion,Cannabinoid,Cannabinoid receptor 1,Cannabinoid receptor 2,Reactive oxygen species,T cell,HIV,Apoptosis,Inflammation,Virus,Immune system,Immunology,Biology,Biochemistry,Medical specialties,Cell biology]
author: Cherifa Beji | Hamza Loucif | Roman Telittchenko | David Olagnier | Xavier Dagenais-Lussier | Julien van Grevenynghe | Dagenais-Lussier | van Grevenynghe
---


Cell Rep. 2018, 22, 1509–1521. Cell 2016, 167, 829–842.e13. Cell Rep. 2019, 28, 3011–3021.e4. Cell. Cells 2019, 8, 1293.

[Visit Link](https://www.mdpi.com/1999-4915/12/8/875){:target="_blank rel="noopener"}

