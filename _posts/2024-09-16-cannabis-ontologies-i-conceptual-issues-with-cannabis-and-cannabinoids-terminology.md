---
layout: post
title: "‘Cannabis’ ontologies I: Conceptual issues with Cannabis and cannabinoids terminology"
date: 2024-09-16
tags: [Herbal medicine,Cannabinoid,Cannabis,Cannabis sativa,Parthenocarpy,Hash oil,Tetrahydrocannabinol,Resin,Herb,Hashish,Fruit,Flower,Dronabinol,Natural product,Medication,Synthetic cannabinoids,Drink,Pharmacology]
author: Kenzi Riboulet-Zemouli
institutions: 
doi: "https://doi.org/10.1177/2050324520945797"
---


d.  ‘Extracts and tinctures of cannabis’, for which no definition is provided. Only ‘cannabis’, ‘cannabis resin’, and ‘extracts and tinctures of cannabis’ are listed in these Schedules (UN, 1961: 239). Resin is often described as the product of a simple and often traditional extraction process. Defining resin  Because ‘cannabis’ is defined in C61 as tops of C. sativa ‘from which the resin has not been extracted’, it is suggested that extraction is the method of obtention of ‘resin’ from ‘cannabis’. This allows for cannabinoid APIs to be arranged in four meta-categories:  • In vivo phytocannabinoids: naturally occurring compounds, derived:  - from C. sativa plant material (e.g. dronabinol present in EGTs),  - from other plant genera (e.g. (−)-cis-perrottetinene present in some plants of the genus Radula; see Gertsch, 2018),  • In vitro phytocannabinoids: the same naturally occurring compounds as above, obtained by full ‘chemical synthesis’,  • Synthetic cannabinoid analogues (e.g. nabilone, HU-210, dexanabinol): non-naturally occurring compounds, obtained by full chemical synthesis,  • Semisynthetic cannabinoids:  -Semisynthetic phytocannabinoids, i.e. naturally occurring compounds, derived from C. sativa plant material, obtained by partial chemical synthesis different than those of the plant’s natural phytocannabinoid biosynthetic pathways (e.g. CBD transformed into dronabinol),  -Semisynthetic cannabinoid analogues, i.e. non-naturally occurring compounds, derived from C. sativa plant material, obtained by partial chemical synthesis (e.g. the (+)-enantiomer of CBD, not found in natural environments).