---
layout: post
title: "Comparison of decarboxylation rates of acidic cannabinoids between secretory cavity contents and air-dried inflorescence extracts in Cannabis sativa cv. ‘Cherry Wine’"
date: 2024-09-03
tags: [Cannabinoid,Tetrahydrocannabinolic acid,Cannabidiol,Tetrahydrocannabinol,Decarboxylation,High-performance liquid chromatography,Cannabis,Secretion,Gland (botany)]
author: Eun-Soo | Sang-Hyuck | Chad A | Kenneth J | Corredor-Perilla | Ingrid Carolina
---


The major cannabinoid content was determined as follows: CBDA (189.0 ± 68.8 mg/L), CBD (1.90 ± 0.7 mg/L), THCA (14.2 ± 5.4 mg/L), CBCA (9.3 ± 3.1 mg/L), CBGA (3.0 ± 1.5 mg/L), and CBDVA (1.2 ± 0.6 mg/L) were observed in the secretory cavity contents, respectively (Table 1). However, the content of CBD in the dried inflorescences was much higher (4.79 ± 1.70%) compared to that of the secretory cavity samples (1.16 ± 0.97%) (Fig. Thus, processing and storage temperature of cannabis products may be crucial factors in causing decarboxylation of acidic cannabinoids and degradation of neutral cannabinoids. In addition, no Δ9-THC, CBC, and CBG were detected in the secretory cavity contents and the inflorescence samples except for Δ9-THCV and Δ9-THCVA in the former sample, and CBDVA in the latter sample. In this research, CBDV was not observed in either the secretory cavity or inflorescence samples, but a small amount of CBDVA was detected in the secretory cavity contents.

[Visit Link](https://www.nature.com/articles/s41598-024-66420-3){:target="_blank rel="noopener"}

