---
layout: post
title: "Differences in prescribed medicinal cannabis use by cannabinoid product composition: Findings from the cannabis as medicine survey 2020 (CAMS-20) Australia-wide study"
date: 2024-09-02
tags: [Cannabidiol,PLOS One,Institutional review board,Medical cannabis,Consent,P-value,Research]
author: Benjamin T. Trevitt | Sasha Bailey | Llewellyn Mills | Thomas R. Arkell | Anastasia Suraev | Iain S. McGregor | Nicholas Lintzeris
---


The PLOS Data policy requires authors to make all data underlying the findings described in their manuscript fully available without restriction, with rare exception (please refer to the Data Availability Statement in the manuscript PDF file). Compared to participants using CBD products, participants spent more on THC products and these same individuals self-reported improvements in the conditions/symptoms for which they were prescribed medical cannabis. Comment (Page 5, Lines 86-91): To distinguish the current work from existing findings, the authors should consider specifying that a “…better understanding of MC product composition is critical…’ among prescribed medical cannabis users in Australia. Comment (Page 8, Lines 166-167): How did participants complete the survey? It is noted by the authors in the Methods section however, that 350 of these participants reported using both prescribed as well as illicit MC.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10866492/){:target="_blank rel="noopener"}

