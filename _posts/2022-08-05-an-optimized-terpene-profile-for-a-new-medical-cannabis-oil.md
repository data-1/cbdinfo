---
layout: post
title: "An Optimized Terpene Profile for a New Medical Cannabis Oil"
date: 2022-08-05
categories:
- Cannabis
- Terpene
author: Maggini, Calvi, Pelagatti, Gallo, Eugenia Rosaria, Civati, Privitera, Squillante, Maniglia, Di Candia
tags: [Cannabis sativa,Cannabinoid,Medical cannabis,High-performance liquid chromatography,Tetrahydrocannabinol,Cannabidiol,Gas chromatography,Chemistry]
---


A total of 2 different extraction procedures were performed according to previous published studies [ 30 32 ] to obtain medical Cannabis oil 1 (MCO-1) and MCO-2 (SIFAP and Calvi methods, respectively). Sesquiterpenes were about 6% of the total identified terpenes in each MCO; MCO-3 had a significantly increased concentration compared to MCO-2 and MCO-1 (value < 0.001; Figure 4 ). Starting from 5 g of cannabis flos in 50 mL of MCT oil, a final product of 50 mL of medical cannabis oil was obtained. In the first method, the plant material was extracted by maceration at 100 °C for 40 min [ 32 ] whereas in the second method, the extraction was carried out for 30 min by sonication at controlled temperature [ 30 ]. MCT oil was shown to extract and preserve a significant concentration of terpenes compared to olive oil [ 29 ].

<hr>

[Visit Link](https://www.mdpi.com/1999-4923/14/2/298/htm){:target="_blank" rel="noopener"}


