---
layout: post
title: "Cannabidiol for Viral Diseases: Hype or Hope?"
date: 2024-09-27
tags: [Medical cannabis,Cannabidiol,Antiviral drug,Kaposi's sarcoma-associated herpesvirus,Herpes,Virus,Shingles,Infection,Hepatitis C,Neuroinflammation,Interferon,Tetrahydrocannabinol,Hepatitis,Cannabinoid,Health,Medicine,Medical specialties,Clinical medicine,Health sciences,Immunology,Diseases and disorders]
author: Alex Mabou Tagne | Barbara Pacchetti | Mikael H. Sodergren | Marco Cosentino | Franca Marino
pubdate: "2020-01-15"
institutions: University of Insubria
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7347053"
---


Results: PubMed search referenced two original articles supporting the use of CBD for the treatment of hepatitis C and Kaposi sarcoma and one article reporting the ability of CBD to reduce neuroinflammation in a virus-induced animal model of multiple sclerosis. Cannabinoids and viral infections. Effects of cannabinoids and their receptors on viral infections. Cannabinoids and viral infections. Effects of cannabinoids and their receptors on viral infections.