---
layout: post
title: "Cannabigerol Effect on Streptococcus mutans Biofilms—A Computational Approach to Confocal Image Analysis"
date: 2022-07-31
categories:
- CBG
author: Aqawi, The Biofilm Research Laboratory, The Faculty Of Dental Medicine, Institute Of Biomedical, Oral Research, The Hebrew University Of Jerusalem, The Institute Of Drug Research, School Of Pharmacy, Steinberg, Feuerstein
tags: [Biofilm,Streptococcus mutans,Porosity,Bacteria,Confocal microscopy,Tooth decay,Statistics,Microscopy,Correlation,Oral microbiology,Streptococcus]
---


In order to better characterize the effect of cannabigerol on biofilms of S. mutans , this paper provides a series of computational assays for biofilm analysis, applied on confocal images of S. mutans biofilms treated with cannabigerol. Figure 4 illustrates differences in the characteristics of the central layer of biofilms—CBG-treated biofilms are characterized by a significant reduction in the intensity of the central layer, in addition to reduced normalized standard deviation. In terms of intensity, the amount of SYTO 9 signal, representative of live bacterial cells, in the central layer of CBG-treated biofilms is significantly reduced when compared to EtOH/untreated biofilms: when compared to control samples, EtOH, CBG 2.5 and 5 μg/mL samples exhibited reductions of 91.2, 28.9, and 32.5%, respectively. When assessing the live (SYTO 9) signal variation at the central layer, CBG-treated biofilms exhibited significantly lower normalized standard deviation values compared to EtOH-treated/control biofilms: when compared to control samples, EtOH, CBG 2.5, and 5 μg/mL samples exhibit values that are 102.5, 58.6, and 80.8%, respectively. The process of biofilm formation can be numerically quantified from confocal laser microscopy images using objective parameters for biofilm image analysis, used to compare and monitor variations in biofilm structure.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fmicb.2022.880993/full){:target="_blank" rel="noopener"}


