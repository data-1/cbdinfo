---
layout: post
title: "Commercial Cannabidiol Caution: A New Gold Rush"
date: 2024-09-16
tags: [Cannabidiol,Medicine,Health,Health care,Clinical medicine,Medical specialties,Drugs,Medical treatments,Health sciences,Pharmacology]
author: Eugene Scharf | Alexandra M. Ward | Jon O. Ebbert
institutions: Mayo Clinic | WinnMed
doi: "https://www.mayoclinicproceedings.org/article/S0025-6196(19)30921-8/fulltext"
---


Letter to the editorVolume 95, Issue 1p200  Download started  Ok  Commercial Cannabidiol Caution: A New Gold Rush  1Department of Neurology, Mayo Clinic, Rochester, MN  2Primary Care Internal Medicine Research, Mayo Clinic, Rochester, MN  Copyright: © 2019 Mayo Foundation for Medical Education and Research  Review  Clinicians’ Guide to Cannabidiol and Hemp Oils  VanDolah et al.  Mayo Clinic ProceedingsVol. 94Issue 9  Letter to the editor  In reply—Commercial Cannabidiol Caution: A New Gold Rush  VanDolah et al.  Mayo Clinic ProceedingsVol. We would add a word of caution about the use of products and would also suggest readers of Mayo Clinic Proceedings consider additional factors when discussing commercial CBD use with patients. First, although hemp farming is now legal under federal law, and for profit CBD commercial establishments are widespread, the actual sale of hemp-derived CBD food or supplement formulations remains illegal because plant-derived CBD (Epidiolex) is a Food and Drug Administration–approved drug.2 Second, many commercial sellers of CBD imply medical claims for these products, which are both unsupported by clinical evidence and in violation of Food and Drug Administration labeling laws. Pathology of vaping-associated lung injury  N Engl J Med.