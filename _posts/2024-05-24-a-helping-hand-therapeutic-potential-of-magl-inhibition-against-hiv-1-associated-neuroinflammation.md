---
layout: post
title: "A helping HAND: therapeutic potential of MAGL inhibition against HIV-1-associated neuroinflammation"
date: 2024-05-24
categories:
- 
author: Alexis F, Yadav-Samudrala, Barkha J, Calista A, Ian R, Micah J, Benjamin F, Aron H, Ignatowska-Jankowska, Bogna M
tags: [Cyclooxygenase,Neuroinflammation,HIV,High-performance liquid chromatography,2-Arachidonoylglycerol,Microglia,Dopamine,Correlation,Inflammation,Astrogliosis]
---


For both brain regions (i.e., nucleus accumbens and infralimbic cortex), images were sampled from 3-4 sagittal sections, spaced 300 μm apart, per animal. Figure 2  3.2 No behavioral alterations were observed following Tat induction or MJN110 treatment  No baseline performance differences were observed between Tat(−) and Tat(+) subjects across drug groups during FR 1 shaping trials, either in the number of sessions required to learn the task (Figure 3A) or the number of reinforcers earned during sessions wherein criteria for advancement to the PFR breakpoint test were met (Figure 3A’). Additionally, no significant differences were observed between Tat(−) and Tat(+) subjects across drug groups during the PFR breakpoint test in measures of breakpoint (Figure 3B), total session time (Figure 3B’), number of nose pokes (Figure 3C), or nose pokes per minute (Figure 3C’). Further, Tat expression increased 11-HETE [F(1, 30) = 4.4, p = 0.046] that was altered by drug [genotype x drug interaction, F(1, 30) = 4.1, p = 0.051]. Immune tolerance may also contribute to the lack of Tat-induced effects on lipid metabolite expression observed presently; while Tat initially induces inflammation, potentially contributing to dendritic injury seen in vehicle-exposed Tat(+) mice, the immune system becomes desensitized to its effects after about three months of exposure (63).

[Visit Link](https://www.frontiersin.org/journals/immunology/articles/10.3389/fimmu.2024.1374301/full#h6){:target="_blank" rel="noopener"}


