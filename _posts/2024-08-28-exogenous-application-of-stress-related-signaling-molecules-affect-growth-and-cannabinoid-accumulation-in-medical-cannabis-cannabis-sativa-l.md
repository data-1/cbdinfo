---
layout: post
title: "Exogenous application of stress-related signaling molecules affect growth and cannabinoid accumulation in medical cannabis (Cannabis sativa L.)"
date: 2024-08-28
tags: [Plant hormone,Cannabinoid,Cannabis,Cannabidiol,Trichome,Real-time polymerase chain reaction,Tetrahydrocannabinolic acid,Biotic stress,Tetrahydrocannabinol,Polymerase chain reaction]
author: Martínez-Quesada | Juan José | Ferreiro-Vera
---


Our results can be applied to improve the cultivation of medical cannabis varieties, enhancing plant growth and cannabinoid yields. Some treatments affected the production of cannabinoids in medical cannabis inflorescences (Figure 3A). Relative expression profiles of biosynthetic genes  Changes in the relative expression of the genes involved in the final steps of the cannabinoid biosynthesis pathway (CsAOC-1, CsAOC-2, CsPT4, CsTHCAS, and CsCBDAS) were studied to assess the influence of the treatments on the gene expression. However, the changes measured in the relative gene expression were not significant regarding control plants (Figure 4). Nevertheless, both the 0.1 mM SA and MeJA treatments produced a sharp increase in the cannabinoids yield in medical cannabis leaves, improving the profitability of this plant material as a source of cannabinoids.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2022.1082554/full){:target="_blank rel="noopener"}

