---
layout: post
title: "Evaluation of thermo-chemical conversion temperatures of cannabinoid acids in hemp (Cannabis sativa L.) biomass by pressurized liquid extraction"
date: 2024-08-20
tags: [Cannabinoid,Decarboxylation,Tetrahydrocannabinolic acid,Cannabidiol,Cannabinol,Cannabis,Chemistry]
author: Kenneth J | Chad A
---


However, cannabis plants only produce carboxylated cannabinoids. Optimum temperatures were established for the conversion of 6 cannabinoid acids to their neutral cannabinoid forms. Results  The use of PLE for thermo-chemical decarboxylation has resulted in a rapid decarboxylation process taking merely 6 min. CBC, CBD, CBDV, and CBG have an optimum temperature of conversion of 140 °C, while THC was 120 °C for 6 min. Consequently, the temperatures necessary to decarboxylate the common cannabinoids in hemp biomass using the PLE system were examined.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00098-6){:target="_blank rel="noopener"}

