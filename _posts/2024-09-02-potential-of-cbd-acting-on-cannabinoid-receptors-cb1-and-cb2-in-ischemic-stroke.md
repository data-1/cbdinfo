---
layout: post
title: "Potential of CBD Acting on Cannabinoid Receptors CB1 and CB2 in Ischemic Stroke"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Cannabinoid receptor 1,Cannabinoid,Adenosine A2A receptor,Microglia,Stroke,Endocannabinoid system,Fatty-acid amide hydrolase 1,Cannabidiol,Cell biology,Biochemistry,Neurochemistry]
author: Iu Raïch | Jaume Lillo | Rafael Rivas-Santisteban | Joan Biel Rebassa | Toni Capó | Montserrat Santandreu | Erik Cubeles-Juberias | Irene Reyes-Resina | Gemma Navarro | Rivas-Santisteban
---


On the one hand, these compounds could selectively bind the CB2 receptor that does not show psychoactive effects and, in glia, has opened new avenues in this field of research, shedding new light on the use of cannabinoid receptors as therapeutic targets to combat neurodegenerative diseases such as Alzheimer’s, Parkinson’s disease, or stroke. Besides the cannabinoid receptors, it is known that CBD activates serotonin 5HT1A receptors [67]. Stroke 2022, 53, 1460–1472. Stroke 2019, 50, 2640–2645. [Google Scholar] [CrossRef] [PubMed]  Hillard, C. Role of Cannabinoids and Endocannabinoids in Cerebral Ischemia.

[Visit Link](https://www.mdpi.com/1422-0067/25/12/6708){:target="_blank rel="noopener"}

