---
layout: post
title: "Minor Cannabinoids: Biosynthesis, Molecular Pharmacology and Potential Therapeutic Uses"
date: 2022-09-01
categories:
- Cannabinoid
author: Walsh, Kenneth B., Department Of Pharmacology, Physiology, Neuroscience, School Of Medicine, University Of South Carolina, United States, Mckinney, Amanda E.
tags: [Cannabinoid,Tetrahydrocannabinol,Cannabidiol,Tetrahydrocannabinolic acid,Cannabinoid receptor,Cannabinoid receptor type 2,Medical cannabis,Tetrahydrocannabivarin,Cannabinoid receptor type 1,Cannabis sativa,Cannabigerol,Anti-inflammatory,Receptor (biochemistry),2-Arachidonoylglycerol,Type 2 diabetes,Cannabis (drug),Delta-8-Tetrahydrocannabinol,TRPV1,Receptor antagonist,Transient receptor potential channel,G protein-coupled receptor,Endocannabinoid system,Nonsteroidal anti-inflammatory drug,Cannabinol,Ligand (biochemistry),TRPA1,Pharmacology,Neurochemistry,Clinical medicine,Neuroscience,Biochemistry,Cell biology]
---


As described for the endocannabinoids, the overall pharmacological action of the minor cannabinoids often results from binding at both cannabinoid and “off target” receptors. Although CBN is not as potent an appetite stimulant as Δ9-THC, CBN administration is not associated with the psychotropic effects of Δ9-THC. While clinical trials have not been reported, CBDA may be more effective than CBD in reducing seizures in humans. Potential therapeutic uses of phytocannabinoids. Evidence that the Plant Cannabinoid delta9-tetrahydrocannabivarin Is a Cannabinoid CB1 and CB2 Receptor Antagonist.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2021.777804/full){:target="_blank" rel="noopener"}


