---
layout: post
title: "Cannabidiol oxidation product HU-331 is a potential anticancer cannabinoid-quinone: a narrative review - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
- HU-331
author: Trac, Department Of Pharmaceutical Sciences, Lipscomb University College Of Pharmacy, Health Sciences, Nashville, Keck, J. Myles, Deweese, Joseph E., Department Of Biochemistry
tags: [HU-331,Topoisomerase,Cannabidiol,Angiogenesis,Anthracycline,Peroxisome proliferator-activated receptor gamma,Type II topoisomerase,Cancer,Doxorubicin,Cytochrome P450,Chemotherapy,Cannabinoid,Angiogenesis inhibitor,Medical specialties,Biology,Life sciences,Medicine,Clinical medicine,Biochemistry]
---


A follow-up study demonstrated that this mechanism of HU-331 also works against the other human isoform of topoisomerase II, TOP2B (Wilson et al. 2018). These results indicate that HU-331 has an effect on proliferation of vascular cells and may help decrease the ability of tumors to recruit new blood vessels. Results indicated that a combination of cisplatin and HU-331 was effective at inhibiting cancer cell growth. Table 2 Results from animal model-based experiments with HU-331 Full size table  HU-331 was also tested for inhibition of tumor angiogenesis in nude mice that were injected with HT-29 cancer cells (Kogan et al. 2006). Further, HU-331-treated nude mice with HT-29 colon cancer xenografts demonstrated a reduction in tumor area and weight compared to control.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00067-z){:target="_blank" rel="noopener"}


