---
layout: post
title: "Cannabidiol Induces Cell Death in Human Lung Cancer Cells and Cancer Stem Cells"
date: 2022-09-05
categories:
- CBD
author: Hamad, Olsen, Birgitte Brinkmann, Hussein Hamad, Birgitte Brinkmann Olsen
tags: [Apoptosis,Bcl-2,SOX2,Reactive oxygen species,Cannabidiol,Oct-4,Bcl-2-associated X protein,P53,Mitochondrion,Homeobox protein NANOG,Cancer stem cell,Cancer,DNA repair,CD44,Lung cancer,Stem cell,Bcl-2 homologous antagonist killer,P21,Oxidative stress,Cellular processes,Biological processes,Life sciences,Molecular biology,Biology,Biotechnology,Biochemistry,Cell biology,Clinical medicine,Diseases and disorders]
---


Although it has been shown that CBD has anti-cancer effects on cancer cells, not much is known about the effect of CBD on cancer stem cells. Here, we show that CBD reduced viability in both lung cancer stem cells and adherent lung cancer cells. Next, we tested the effect of CBD on viability in spheres and adherent cells in the presence and absence of serum ( Figure 2 ). Overall, the results showed that CBD reduces the viability of both adherent lung cancer cells and lung cancer stem cells and that the effect is affected by serum in the medium. In A549 spheres, the expression levels of SOX2 , POU5F1 , and PROM1 decreased significantly, in particular PROM1 ( p < 0.01), whereas NANOG and CD44 increased, although not significant ( SOX2 ( p < 0.05) and PROM1 ( p < 0.01; SOX2 , CD44 and PROM1 ( p < 0.05) and POU5F1 ( p < 0.01; Based on the viability results, where we saw an effect also on the spheres, we tested the effect of CBD on sphere formation.

<hr>

[Visit Link](https://www.mdpi.com/1424-8247/14/11/1169/htm){:target="_blank" rel="noopener"}


