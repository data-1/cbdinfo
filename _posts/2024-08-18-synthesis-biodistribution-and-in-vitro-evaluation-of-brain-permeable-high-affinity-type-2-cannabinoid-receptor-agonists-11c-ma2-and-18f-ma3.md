---
layout: post
title: "Synthesis, Biodistribution and In vitro Evaluation of Brain Permeable High Affinity Type 2 Cannabinoid Receptor Agonists [11C]MA2 and [18F]MA3"
date: 2024-08-18
tags: [Cannabinoid receptor 2,Ligand (biochemistry),Ester,Endocannabinoid system,Cannabinoid,Thin-layer chromatography,High-performance liquid chromatography,Cannabinoid receptor 1,Microglia,Chemistry]
author: van Veghel | Van Laere | Guy M
---


Compound 12 was synthesized using a literature procedure and NMR and MS data matched reported data (Cheng et al., 2008). In accordance, brain uptake of [11C]MA2 (1.6% ID at 2 min post injection) was higher than brain uptake of [18F]MA3 (1.2% ID at 2 min post injection) although the difference was not statistically significant (p = 0.7), but was followed by a rapid wash-out from brain (% ID 2 min/60 min ratio = 18.4 and 9.2, respectively). Among the tested compounds MA3 exhibited the highest binding affinity (Ki) for hCB2 (Ki = 0.8 nM) and had about 4 times higher affinity compared to 12, about 100 times higher affinity compared to MA2 and 5 times higher affinity compared to NE40. The brain uptake of [11C]MA2 was found to be similar to that of [11C]NE40 (% ID = 1.7 in mouse brain at 2 min post injection) (Evens et al., 2012) and in comparision to that of [11C]A-836339 (SUV ~ 0.5 in rat brain at 2 min post injection) (Horti et al., 2010). Functional and Binding Experiments  The competition binding experiments and functional assays were performed by Roche Healthcare.

[Visit Link](https://www.frontiersin.org/journals/neuroscience/articles/10.3389/fnins.2016.00431/full){:target="_blank rel="noopener"}

