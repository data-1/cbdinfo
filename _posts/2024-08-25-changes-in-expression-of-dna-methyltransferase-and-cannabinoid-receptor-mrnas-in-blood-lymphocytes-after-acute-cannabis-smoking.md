---
layout: post
title: "Changes in Expression of DNA-Methyltransferase and Cannabinoid Receptor mRNAs in Blood Lymphocytes After Acute Cannabis Smoking"
date: 2024-08-25
tags: [Cannabinoid receptor 1,Schizophrenia,Cannabis (drug),Cannabinoid,Cannabinoid receptor 2,Tetrahydrocannabinol,Γ-Aminobutyric acid,DNA methyltransferase,Psychosis,Effects of cannabis,Complementary DNA,Gene expression,Cannabis,Real-time polymerase chain reaction]
author: Robert C | David S | Jon A | Wynnona S | Thomas D | John M
---


To further investigate the effects of smoked marihuana (cannabis) on these biological effects in human peripleural blood cells, we analyzed its effects on DNA methylation, CB receptor and immunological mRNAs after two concentrations of cannabis (THC) vs. placebo in lymphocytes of a subset of subjects participating in a double-blind study of the effects of cannabis on driving performance. Assays for mRNA in Lymphocytes  Samples available for analysis for mRNA values were collected at baseline, and at 55 min, 1 h 20 min, and 4 h post smoking the experimental cigarette. Lymphocyte mRNA Levels  There were no significant differences between groups in baseline mRNA levels, except for the cannabinoid receptor 2 levels, where the 5.9% THC group had significantly higher levels compared to placebo and 13.4% THC (Table 3). Cannabinoid receptor 2 levels increased at the 13.4% THC concentration but decreased at the 5.9% THC concentration with significant differences from placebo at some time points. The degree participants experienced feeling associated with a “high” after smoking placebo at 2 different cannabis concentrations showed that there was a significant difference in scores between the cannabis and placebo groups but not between the two cannabis concentration groups (Supplementary Table 2).

[Visit Link](https://www.frontiersin.org/journals/psychiatry/articles/10.3389/fpsyt.2022.887700/full){:target="_blank rel="noopener"}

