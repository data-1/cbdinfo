---
layout: post
title: "Identification of histone acetyltransferase genes responsible for cannabinoid synthesis in hemp"
date: 2024-08-29
tags: [Histone acetylation and deacetylation,Cannabidiol,Cannabinoid,Cannabis,Histone deacetylase,Real-time polymerase chain reaction,Sequence motif,Gene expression,Tetrahydrocannabinol,Biochemistry,Biotechnology,Life sciences,Molecular biology,Biology,Genetics]
author: 
---


Some genes, such as PT6, GPP4, AAE5, OAC1, and OAC2, were expressed only in S1 and S4. In addition, we found that the expression of CsHATs and CsHDACs genes were significantly associated with cannabinoid synthesis genes. Effect of histone acetylation inhibitor treatment on cannabinoid biosynthesis gene expression and contents of CBD and CBG  We treated hemp inflorescences with a histone acetylation inhibitor (PU139) to further investigate whether histone acetylation could affect cannabinoid biosynthesis. Genes in a gene family may or may not have the same motifs. In addition, the expression of CsHATs and the contents of CBD and CBG declined in the samples treated by inhibitor.

[Visit Link](https://cmjournal.biomedcentral.com/articles/10.1186/s13020-023-00720-0){:target="_blank rel="noopener"}

