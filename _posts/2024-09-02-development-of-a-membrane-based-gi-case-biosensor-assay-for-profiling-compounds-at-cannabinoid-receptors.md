---
layout: post
title: "Development of a membrane-based Gi-CASE biosensor assay for profiling compounds at cannabinoid receptors"
date: 2024-09-02
tags: [Receptor antagonist,Ligand (biochemistry),EC50,Receptor (biochemistry),Cannabinoid receptor 2,G protein-coupled receptor,Cannabinoid receptor 1,Agonist,Cyclic adenosine monophosphate,Cell signaling,Intrinsic activity,G protein,Biosensor,Biology,Signal transduction,Cell communication,Cell biology,Biochemistry,Neurochemistry]
author: Scott-Dennis | Fikri A | Clare R | Arne C | Dmitry B | David A
---


Methods  Cell culture  The HEK293TR cell line was used for the generation of stable cell lines expressing either the CB1R and CB2R and the Gi-CASE biosensor. The response kinetics of the reference agonist (HU-210 and HU-308) and inverse agonist (rimonabant and SR-144,528) ligands at EC50 concentrations at both receptors, measured for up to 30 min at 37°C, are shown in Figures 2A, B. Ligands were added after obtaining three basal readings with activation of CB1Rs and CB2Rs by agonists, resulting in a decrease in the BRET ratio, whilst inverse agonist activity increased the BRET ratio by reducing G protein activation to levels below basal activity. The response kinetics of the reference agonist and inverse agonist ligands at EC50 concentrations at both receptors, measured for up to 60 min at 28°C, are shown in Figures 3A, B. pEC50 values of the four reference ligands, measured at 28°C, are summarized in Table 2 and associated response curves are presented in Figures 3C, D.  FIGURE 3  TABLE 2  To directly compare the intact cell and membrane-based responses to the reference ligands under study, we monitored their CB1R and CB2R Gi-CASE BRET signals at different ligand concentrations at 37°C, as opposed to the 28°C measurements described above. The response kinetics of reference agonist and inverse agonist ligands at EC50 concentrations were measured for up to 60 min at 37°C, are shown in Figures 4A, B.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2023.1158091/full){:target="_blank rel="noopener"}

