---
layout: post
title: "Nightmares and the Cannabinoids"
date: 2024-09-26
tags: [Hippocampus,Hippocampus proper,Sharp waves and ripples,Cannabinoid receptor 1,Memory,Anandamide,Memory consolidation,Neuroscience,Brain]
author: Mortimer Mamelak
pubdate: "2020-01-14"
institutions: Baycrest Hospital | University of Toronto
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7536831"
---


The cannabinoids impair neuronal synchronization within the hippocampus but also impair the tight neuronal integration between the hippocampus and other brain regions such as the prefrontal cortex and amygdala. Nightmares, Cannabinoids and the Post-traumatic Stress Disorder  The ability of cannabinoids to interfere with the retrieval of episodic memories and to promote the extinction of memories should make them ideal drugs for treating replicative post-traumatic nightmares as these nightmares reflect the nightly cycle of episodic memory retrieval and reconsolidation of the fearful event as well as the failure to extinguish this fearful memory. System consolidation of memory during sleep. Sleep on it! [PubMed] [CrossRef] [Google Scholar]  Articles from Current Neuropharmacology are provided here courtesy of Bentham Science Publishers  1.