---
layout: post
title: "Studies on Hemp and Recycled Aggregate Concrete"
date: 2024-09-17
tags: [Fracture,Concrete,Composite material,Fiber-reinforced concrete,Ductility,Strength of materials,Compressive strength,Mechanical engineering,Building materials,Construction,Artificial materials,Physical sciences,Applied and interdisciplinary physics,Materials science,Manufacturing,Goods (economics),Building engineering,Materials]
author: Samer Ghosn | Bilal S. Hamad
institutions: American University of Beirut
doi: "https://ijcsm.springeropen.com/articles/10.1186/s40069-020-00429-6#Sec28"
---


Besides, the peak loads of the hemp fibers beams were comparable to the control beams without fibers for each of the three investigated modes of failure while a 20% reduction in the coarse aggregates was possible. Overall the reductions in flexural strength for the HRAC mixes due to the incorporation of hemp fibers, reduction in coarse aggregate content, and replacement of 50% of NCA with RCA were less significant than the reductions reported above for the compressive strength and the modulus of elasticity. Mixes with hemp fibers in both groups of different MSA had similar tensile strength regardless of fiber length or fiber treatment or whether a 50% replacement of NCA with RCA was used. In each group, normal and recycled aggregate mixes (N and R mixes) with or without hemp fibers and with different fiber lengths and fiber treatments were tested and compared. For Group 2 mixes with MSA of 20 mm, the resistance to freeze–thaw cycles was much lower than that of Group 1 mixes, as Pc decreased intensively to reach values ranging between 31% and 56.5% after 144 cycles.