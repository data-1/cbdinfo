---
layout: post
title: "The Mediterranean Diet as a Source of Bioactive Molecules with Cannabinomimetic Activity in Prevention and Therapy Strategy"
date: 2022-08-07
categories:
- endocannabinoid
author: Vago, Fiorio, Trevisani, Salonia, Montorsi, Bettiga, Riccardo Vago, Francesco Fiorio, Francesco Trevisani, Andrea Salonia
tags: [Cannabinoid receptor type 2,Gut microbiota,TRPV1,Cannabinoid,Anandamide,Fat,Cannabinoid receptor type 1,Endocannabinoid system,Biochemistry]
---


Recent research on the use of Cannabis in the medical field has highlighted the possible presence of an entourage effect between terpenes and cannabinoids, which could explain the observation that the administration of whole plant extracts is much more effective than the administration of the single cannabinoid [ 18 ]. In this study, the effects of oral BCP administration were shown to be CB2 dependent following CB2 blockage by the antagonist SR 144528 and were demonstrated to be effectively higher than the ones obtained when using the known CB2 agonist JWH-133. The authors suggested that the mechanism through which this effect is achieved is through the inhibition of pro-inflammatory cytokine expression and phospho-ERK1/2 (both of which are elevated in models of neuropathic pain), confirming in an in vivo model the mechanisms earlier proposed by Gertsch et al. Given this discovery, Bahi et al. [ 29 ] set out to elucidate the role of CB2 in this BCP-mediated anxiolytic and antidepressant effects. Once again, they were able to prove CB2 involvement by administration of a CB2 selective antagonist (AM630).

<hr>

[Visit Link](https://www.mdpi.com/2072-6643/14/3/468/htm){:target="_blank" rel="noopener"}


