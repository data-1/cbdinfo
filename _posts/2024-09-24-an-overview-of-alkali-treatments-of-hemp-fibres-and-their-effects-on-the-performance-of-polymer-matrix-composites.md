---
layout: post
title: "An Overview of Alkali Treatments of Hemp Fibres and Their Effects on the Performance of Polymer Matrix Composites"
date: 2024-09-24
tags: [Thermoplastic,Thermosetting polymer,Polyethylene,Polymer,Polypropylene,Sodium hydroxide,Cellulose,Polymer chemistry,Materials,Artificial materials,Manufacturing,Goods (economics),Polymers,Chemical substances,Materials science,Building engineering,Organic polymers,Physical sciences,Building materials]
author: Tom Sunny | Kim L. Pickering | Shen Hin Lim
institutions: University of Waikato
doi: "https://www.intechopen.com/chapters/78892"
---


It has separate male plants and female plants. 3.1.1 Alkali treatment  Among different chemical treatments, the alkali treatment with sodium hydroxide (NaOH) is one of the most widely used treatments. However, alkali treatment is commonly carried out to remove the cementing materials. As it can be seen, for different high temperature treatments significant improvement in average tensile strength was reported for hemp fibres treated with 5 wt% NaOH and 2 wt% Na2SO3 (sodium sulphate) at 120°C with a holding time of 60 minutes compared to 10 wt% NaOH and untreated fibres [18]. Methods of applicationsFibres or composites producedObservations on properties of fibres or compositesSoaked hemp mats in 0.16 wt% NaOH for 48 hoursNon-woven hemp mats in euphorbia resinIncrease in tensile strength of composites produced with treated fibre mats [37]Immersed pre-dried hemp fibres in 5 wt% NaOH solution for 30 minutes, *FSR- 1:20Hemp fibreAverage tensile strength of the fibres increased [17]Hemp fibres were soaked in 0%, 4%, 6%, 8% and 10% for 3 hours at room temperature.