---
layout: post
title: "Changes in Alcohol and Cigarette Consumption in Response to Medical and Recreational Cannabis Legalization: Evidence from U.S. State Tax Receipt Data"
date: 2024-09-16
tags: [Cannabis (drug),Dependent and independent variables,Linear regression,Legalization of non-medical cannabis in the United States,Cigarette,Tobacco smoking,Medical cannabis,Tax,Alcohol (drug),Cannabis in Canada,Fixed effects model,Statistics,Recreational drug use,PubMed Central,Preventive healthcare,Health]
author: Sirish Veligati | Seth Howdeshell | Sara Beeler‐Stinn | Deepak Lingam | Phylicia C. Allen | Li-Shiun Chen | Richard A. Grucza
institutions: Washington University in St. Louis
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6957726/"
---


Methods:  Dependent variables included per capita consumption of alcohol and cigarettes from all 50 U.S. states, estimated from state tax receipts and maintained by the Centers for Disease Control and National Institute for Alcohol Abuse and Alcoholism, respectively. Results:  Primary models found no statistically significant associations between medical or recreational cannabis legalization policies and either alcohol or cigarette sales per capita. We used linear regression to model state per capita alcohol and cigarette consumption as a function of state medical and recreational cannabis policy. In the Series I model, adjusting only for state and year fixed effects, there was a trend toward a negative association of medical cannabis policies and per capita alcohol sales (β= −0.031; 95% CI: −0.064, 0.003, p=0.075)—suggesting a possible reduction in per capita alcohol sales associated with medical cannabis legalization. [PMC free article] [PubMed] [Google Scholar] [Ref list]