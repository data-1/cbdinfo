---
layout: post
title: "Patient-Related Barriers to the Prescription of Cannabinoid-Based Medicines in Palliative Care: A Qualitative Approach"
date: 2024-08-27
tags: [Cannabis (drug),Palliative care,Medical cannabis,Tetrahydrocannabinol,End-of-life care,Health,Medicine,Health care,Clinical medicine]
author: Pauline Kalonji | Aurélie Revol | Barbara Broers | Michael Ljuslin | Sophie Pautex
---


No patients had a prescription for CBMs at the time of the interviews. Patients reported a lack of medical information on cannabinoids, both theoretical and clinical. (2)  What do you know about medical cannabis? Medical use of cannabis in 2019. Medical use of cannabis in 2019.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9531874/){:target="_blank rel="noopener"}

