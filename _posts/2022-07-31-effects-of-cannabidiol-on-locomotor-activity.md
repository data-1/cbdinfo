---
layout: post
title: "Effects of Cannabidiol on Locomotor Activity"
date: 2022-07-31
categories:
- CBD
author: Calapai, Cardia, Di Mauro, Trimarchi, Ammendolia, Mannucci, Fabrizio Calapai, Luigi Cardia, Gioacchino Calapai, Debora Di Mauro
tags: [Cannabidiol,Ketamine,Cannabinoid,Tetrahydrocannabinol,NMDA receptor,Cannabinoid receptor type 1,Neuroscience,Neurochemistry]
---


The results showed that CBD administration did not influence motor activity evaluated by accelerating rotarod tests, while it reduced locomotor activity in the OF [ 42 ]. In summary, these findings show that long-term CBD treatment did not produce side effects regarding motor activity or memory and did not cause anxiety behavior [ 42 ]. However, the results showed that acute CBD reduced anxiety induced by EPM, but it had no impact on locomotion or anxiety-related parameters of Fmr1mice in the OF experiments [ Fmr1 -deficient and WT mice, without any effect on locomotor activity, social, or cognitive performance. In this study, CBD administration resulted in a further reduction in anxiety-like behavior in both-deficient and WT mice, without any effect on locomotor activity, social, or cognitive performance. CBD (5 mg/kg) or a vehicle were investigated in 12-month-old APPSwe/PS1ΔE9 (APP/PS1) transgenic female mice and their control, commencing the treatment three weeks prior to the assessment of behavior consisting of sectors including anxiety, exploration, locomotion, motor functions, cognition, and sensorimotor gating.

<hr>[Visit Link](https://www.mdpi.com/2075-1729/12/5/652/htm){:target="_blank" rel="noopener"}


