---
layout: post
title: "Beyond Pain Relief: A Review on Cannabidiol Potential in Medical Therapies"
date: 2023-02-27
categories:
- CBD
author: Luz-Veiga, Azevedo-Silva, Fernandes, João C., Mariana Luz-Veiga, João Azevedo-Silva, João C. Fernandes
tags: [Cannabidiol,Inflammatory bowel disease,Cannabinoid,Lupus,Cannabinoid receptor type 2,Tetrahydrocannabinol,Psoriasis,Inflammation,TRPV1,Apoptosis,Endocannabinoid system,Cannabinoid receptor type 1,Cancer,Cannabinoid receptor,Reactive oxygen species,Dose (biochemistry),Biochemistry,Diseases and disorders,Neurochemistry,Cell biology,Health,Medicine,Medical specialties,Clinical medicine]
---


Although there is a lack of strong evidence on the efficacy of cannabinoids for the treatment of RA and other rheumatic diseases, cannabinoids have been seen as a potential therapy due to their anti-inflammatory effects. Indeed, RA is included in the list of conditions eligible for receiving medical cannabis in several countries and most patients with arthritis taking cannabinoids on a regularly basis report beneficial effects, such as less pain and an opioid-sparing effect [ 189 ]. In vitro and animal experimentation results have been the main source of data used so far to support cannabinoids potential therapeutic effect on RA. Gui and co-workers demonstrated the expression of CB2 receptor in synovial tissue from patients with RA, and its specific activation revealed inhibitory effects on fibroblast-like synoviocytes [ 190 ]. The same team demonstrated that the activation of CB2 in collagen-induced arthritis in mice, using HU-308 (a CBD-derivative drug that acts as a potent cannabinoid agonist) has therapeutic potential for RA to suppress synovitis and alleviate joint destruction by inhibiting the production of autoantibodies and proinflammatory cytokines [ 191 ].

<hr>

[Visit Link](https://www.mdpi.com/1424-8247/16/2/155){:target="_blank" rel="noopener"}


