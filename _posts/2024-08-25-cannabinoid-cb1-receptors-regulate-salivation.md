---
layout: post
title: "Cannabinoid CB1 receptors regulate salivation"
date: 2024-08-25
tags: [Fatty-acid amide hydrolase 1,Cannabinoid receptor 1,Cannabinoid,Tetrahydrocannabinol,Acetylcholine,Anandamide,Saliva,N-Acylethanolamine,Cannabinoid receptor 2,Cannabidiol,Immunohistochemistry]
author: Naimi Shirazi | Wager-Miller
---


We found that in both males and females the baseline saliva levels were lower in FAAH knockout mice (Fig. Our working hypothesis is that FAAH deletion/inhibition increases NAE levels and that these NAEs then act on CB1 receptors to reduce signaling that reduces salivation. FAAH protein is expressed in acinar cells of the SMG  To determine where the FAAH protein is expressed we used immunohistochemistry. Our results are consistent with the following model of cannabinoid regulation of salivation: CB1 receptors are expressed on the axons of cholinergic neurons that innervate the submandibular gland; these CB1 receptors are activated by endogenous lipids, reducing acetylcholine release and, as a result, basal salivation. In contrast, CB1 receptor activation reduces salivation in mice in both sexes, a major difference between these two exocrine glands.

[Visit Link](https://www.nature.com/articles/s41598-022-17987-2){:target="_blank rel="noopener"}

