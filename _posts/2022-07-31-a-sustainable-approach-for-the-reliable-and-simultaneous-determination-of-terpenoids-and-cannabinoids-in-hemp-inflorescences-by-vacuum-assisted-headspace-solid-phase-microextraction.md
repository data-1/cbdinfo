---
layout: post
title: "A sustainable approach for the reliable and simultaneous determination of terpenoids and cannabinoids in hemp inflorescences by vacuum assisted headspace solid-phase microextraction"
date: 2022-07-31
categories:
- Cannabis
- hemp
author: 
tags: [Cannabis,Cannabis sativa,Cannabaceae,Physical sciences,Chemistry]
---


Cannabis sativa L. is an intriguing plant that has been exploited since ancient times for recreational, medical, textile and food purposes. The plant's most promising bioactive constituents discovered so far belong to the terpenoid and cannabinoid classes. These specialised metabolites are highly concentrated in the plant aerial parts and their chemical characterisation is crucial to guarantee the safe and efficient use of the plant material irrespective of which use it is. This study investigates for the first time the use of vacuum assisted HS-SPME as a sample preparation process in an analytical protocol based on HS-SPME combined to fast GC-MS analysis that aims at comprehensively characterising both the terpenoid and cannabinoid profiles of Cannabis inflorescences in a single step. The results proved that vacuum in the HS should be preferred over atmospheric pressure conditions as it ensures the fast recovery of cannabinoid markers at relatively lower sampling temperatures (i.e., 90°C) that do not discriminate the most volatile fraction nor cause the formation of artefacts when the sampling time is minimised.

<hr>[Visit Link](https://www.sciencedirect.com/science/article/pii/S2772582022000110){:target="_blank" rel="noopener"}


