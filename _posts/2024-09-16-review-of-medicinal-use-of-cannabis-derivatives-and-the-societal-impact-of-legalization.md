---
layout: post
title: "Review of Medicinal Use of Cannabis Derivatives and the Societal Impact of Legalization"
date: 2024-09-16
tags: [Cannabis (drug),Cannabis sativa,Medical cannabis,Causes of death,Medicine,Health,Clinical medicine,Medical specialties,Diseases and disorders]
author: Akshat Malik | Khuzema Fatehi | Nandini Menon | Pankaj Chaturvedi
institutions: Tata Memorial Hospital
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7725166"
---


[23] Few other reviews evaluated studies in which Cannabis has been compared to a placebo for the treatment of chronic pain and have shown promising results [ ]. [77] Overall, about one in 11 Cannabis users suffered from Cannabis dependence, which could be counted as being in the category of problem Cannabis users. Medical Marijuana for Cancer. Clearing the Smoke on Cannabis: Medical use of Cannabis and Cannabinoids. Clearing the Smoke on Cannabis: Medical use of Cannabis and Cannabinoids.