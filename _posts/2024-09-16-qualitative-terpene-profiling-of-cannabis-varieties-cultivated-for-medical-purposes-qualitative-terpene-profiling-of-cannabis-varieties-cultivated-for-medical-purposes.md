---
layout: post
title: "Qualitative terpene profiling of Cannabis varieties cultivated for medical purposes"
date: 2024-09-16
tags: [Cannabis,Cannabidiol,Cannabinoid,Tetrahydrocannabinolic acid,Terpene,Cannabis sativa,Gas chromatography–mass spectrometry,Tetrahydrocannabinol]
author: Ernesto Díaz Rocha | Vitória EA Silva | Fernanda CS Pereira | Valery M Jean | Fabio L Costa Souza | Leopoldo Clemente Baratto | Ana CM Vieira | Virgínia Martins Carvalho
institutions: Universidade Federal do Rio de Janeiro
doi: "https://doi.org/10.1590/2175-7860202071040"
---


For most purposes, it will suffice to apply the name Cannabis sativa to all cannabis plants encountered (UNODC 2009UNODC - United Nations Office on Drugs and Crimes (2009) Recommended methods for the identification and analysis of cannabis and cannabis products. The total THC (the sum of THCA, THC and CBN) and total CBD (the sum of CBDA and CBD) were used to calculate the THC/CBD ratios and classified the specimen as hemp or marijuana according with the guidelines used by National Drug Analysis Laboratories of United Nations Office on Drugs and Crimes (UNODC, 2009UNODC - United Nations Office on Drugs and Crimes (2009) Recommended methods for the identification and analysis of cannabis and cannabis products. The Figure 1 shows the separation of 30 compounds and the Table 2 shows the identification of terpenes with abundance above 1 × 105 TIC in the chromatogram. The Harle-Tsu samples showed two different profiles, two samples showed caryophyllene as main component and the others two presented β-myrcene as major component. The level of this compound should increase significantly in relation to other terpenes and terpenoids with drying.
