---
layout: post
title: "Reasons for cannabidiol use: a cross-sectional study of CBD users, focusing on self-perceived stress, anxiety, and sleep problems - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Moltke, Clinic Horsted, Chronic Pain Clinic, Copenhagen, Hindocha, Clinical Psychopharmacology Unit, Department Of Clinical, Educational, Health Psychology, University College London
tags: [Cannabidiol,Dose (biochemistry),Survey methodology,Medical specialties,Clinical medicine,Medicine,Health,Health sciences]
---


This study found that the patients had an increase in overall quality of life, including improved sleep and decreased self-perceived anxiety levels and reduced pain scores. Most people were using less than 100 mg (72.9%) per day. CBD and self-perceived stress  37.5% of respondents reported using CBD for perceived stress, with 92.2% reporting reduced stress levels, making it the third-highest ranking reason for CBD use amongst our sample. CBD and self-perceived anxiety  Self-perceived anxiety was the top-ranked reason for the use of CBD with 42.6% reporting they take CBD for this reason. A recent controlled study of 300 mg CBD found no effect on any sleep indices (Linares et al. 2018), whilst observational and cross-sectional studies showed improvement in sleep outcomes (Corroon and Phillips 2018; Gulbransen et al. 2020).

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00061-5){:target="_blank" rel="noopener"}


