---
layout: post
title: "A Brief History of Cannabis and the Drug Conventions"
date: 2024-09-16
tags: [Cannabis,Cannabaceae,Entheogens,Individual psychoactive drugs,Social aspects of psychoactive drugs,Religion and drugs,Psychoactive drugs]
author: John Collins
institutions: International Drug Development
doi: "https://doi.org/10.1017/aju.2020.55"
---


“Drugs”Footnote 1 have been regulated at the international level since 1912, while cannabis has been specifically regulated since 1925. Contemporary local, national, and international cannabis regulations are now diverging, with some jurisdictions legalising its recreational production, sale and consumption. This essay explores the legal and historical complexity and contingencies around the development of international cannabis regulations and prohibitions. It highlights that the global drug control system was not solely focused on prohibition and instead was a complex mix of regulations underpinned by frequently ill-defined and unclear prohibitions. It argues that the international drug control system should not serve as a bar to national-level reforms and that the two can continue to coexist.