---
layout: post
title: "Going Green for CBG"
date: 2022-07-25
categories:
- Cannabinoid
- CBG
author: Jeff Ubersax, Ceo Of Demetrix, Berkeley, Ca
tags: [Cannabis sativa,Cannabidiol,Cannabinoid]
---


Getting to this point has meant developing a new manufacturing process that overcomes many of the challenges associated with plant-based extraction, a cornerstone of large-scale cannabinoid production. To counteract this, we decided to develop our own (more sustainable) biomanufacturing production method based on fermentation. Our process uses 340 times less water and 3.9 times less land than outdoor C. sativa cultivation, producing 45 times less CO 2 . Compared with indoor growth operations, Demetrix biomanufacturing uses 59 times less water and emits 538 times less CO 2 than warehouse cultivation (and 158 times less CO 2 than greenhouse cultivation). Our CBG is also of a high quality, eliminating contaminants that can often find their way into plant-based cannabinoid preparations.

<hr>[Visit Link](https://thecannabisscientist.com/testing-processing/going-green-for-cbg){:target="_blank" rel="noopener"}


