---
layout: post
title: "What are the informational pathways that shape people’s use of cannabidiol for medical purposes? - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Zenone, Marco A., Faculty Of Health Sciences, Simon Fraser University, Burnaby, Snyder, Crooks, Valorie A., Department Of Geography, University Dr
tags: [Cannabidiol,Health sciences,Health,Clinical medicine,Medicine,Health care]
---


In the most prevalent proposed use, cancer, CBD was proposed for curative or primary treatment (n=57), pain/symptom relief from cancer and cancer treatment (n=24), enhancing conventional treatment (n=11), and unspecified uses in 4 campaigns. Campaigns describing this informational pathway were additionally characterized by those who came to CBD from a hope or desire to treat their disease or condition using natural or alternative options. For example: ‘A new neurologist along with another one of her doctors has recommended that we start [recipient] on CBD oil. For example: ‘[Rachel]’s family research alternative methods to try to control her seizures and asked the doctor about CBD. CBD recommendations came from a person in the immediate social network of the person or their caregiver, including family, friends, colleagues, and acquaintances.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00069-x){:target="_blank" rel="noopener"}


