---
layout: post
title: "The cannabinoid receptor-1 gene interacts with stressful life events to increase the risk for problematic alcohol use"
date: 2024-08-21
tags: [Mental disorder,Alcoholism,Reward system,Cannabinoid receptor 1,Alcohol (drug),Major depressive disorder,Health]
author: Philippe A
---


In analyses where the main exposure was adult SLEs, we also adjusted for SLEs in childhood, to estimate the effects for adult SLEs irrespective of having experienced child stress. Participant and genotype characteristics  The socio-demographic characteristics of the study participants with information on alcohol use and psychiatric diagnoses (N = 2857) are displayed in Table 1 and show that being male, having experienced child or adult SLEs, and having anxiety or depression, were significantly associated with problematic alcohol use (AUDIT ≥ 8) at a level of p ≤ 0.01. Evidence for interaction between rs2023239 and SLEs on risk for problematic alcohol use  We evaluated the interaction of rs2023239 with (i) child SLEs and (ii) adult SLEs on problematic alcohol use, i.e., AUDIT ≥ 8. Results from the two-way ANOVA using three-level categorical variables for both genotype and adult SLEs suggested a tendency toward effect heterogeneity for SLE exposure, with risk-allele carriers experiencing a sharper increase in AUDIT scores across SLE categories, although the interaction term had a p-value of > 0.05 (Fig. Collectively, our analyses provide the first evidence, to our knowledge, for a synergistic interaction between the risk allele of rs2023239 and SLEs, which increases the risk for developing problematic alcohol use.

[Visit Link](https://www.nature.com/articles/s41598-022-08980-w){:target="_blank rel="noopener"}

