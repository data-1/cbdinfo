---
layout: post
title: "Chemical Characteristics, Therapeutic Uses, and Legal Aspects of the Cannabinoids of Cannabis sativa: A Review"
date: 2024-09-16
tags: [Cannabinoid,Tetrahydrocannabinol,Medical cannabis,G protein,Cannabinoid receptor,Cannabidiol,Cannabis (drug),Cannabinoid receptor 2,Cannabinol,Endocannabinoid system,Dronabinol,Cannabinoid receptor 1,Neurochemistry]
author: Hebert Jair Barrales-Cureño | Luis Germán López-Valdez | César Reyes | Víctor Manuel Cetina Alcalá | Irma Vásquez-García | Oscar Francisco Diaz-Lira | Braulio Edgar Herrera-Cabrera
institutions: Colegio de Postgraduados
doi: "https://doi.org/10.1590/1678-4324-2020190222"
---


Cannabinoid receptors and pain. Pharmacological actions and therapeutic uses of cannabis and cannabinoids. Cannabinoid receptors and pain. Pharmacological actions and therapeutic uses of cannabis and cannabinoids. Cannabinoids.