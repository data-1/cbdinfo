---
layout: post
title: "Cannabinoids and triple-negative breast cancer treatment"
date: 2024-09-03
tags: [Cannabinoid receptor 2,Triple-negative breast cancer,Cannabinoid,Tumor microenvironment,2-Arachidonoylglycerol,Anandamide,Cannabinoid receptor 1,Endocannabinoid system,Lymphocyte,PD-L1,Metastasis,Macrophage,Immunotherapy,Medical cannabis,Programmed cell death protein 1,Cancer,Tetrahydrocannabinol,Pembrolizumab,Cannabinoid receptor,Immune system,Chemotherapy,Cancer immunotherapy,B cell,Cancer treatment,Breast cancer,Sacituzumab govitecan,Cannabidiol,Cell signaling,MAPK/ERK pathway,Apoptosis,T cell,Tetrahydrocannabivarin,Biochemistry,Medical specialties,Biology,Cell biology,Clinical medicine]
author: Kranjc Brezar
---


Immune cells express both CB1R and CB2R (44, 45), although the expression of CB1R is significantly lower compared to CB2R (41, 46). 4 Cells of the TNBC microenvironment and the endocannabinoid system  It appears that most of the cell types present in the TNBC TME express CBRs and may be affected in some way by the exogenous cannabinoids and/or the endocannabinoid system (4, 72). 6 Effect of different cannabinoids on TNBC  The antitumor effect of THC on breast cancer cell lines was documented. CB2R expression is increased in the MDA‐MB‐231 cell line when treated with chemotherapy. 7.2 Cannabinoids and Immunotherapy  Exposure to THC can promote growth and metastasis of the 4T1 cell line.

[Visit Link](https://www.frontiersin.org/journals/immunology/articles/10.3389/fimmu.2024.1386548/full){:target="_blank rel="noopener"}

