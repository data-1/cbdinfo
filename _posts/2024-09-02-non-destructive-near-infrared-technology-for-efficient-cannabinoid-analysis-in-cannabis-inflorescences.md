---
layout: post
title: "Non-Destructive Near-Infrared Technology for Efficient Cannabinoid Analysis in Cannabis Inflorescences"
date: 2024-09-02
tags: [Training, validation, and test data sets,Linear regression,Cannabidiol,Cross-validation (statistics),Coefficient of determination,Standard score,Deviation (statistics),Mean squared error,Partial least squares regression,Near-infrared spectroscopy,Cannabinoid,Regression analysis,Cannabis]
author: Hamza Rafiq | Jens Hartung | Torsten Schober | Maximilian M. Vogt | Dániel Árpád Carrera | Michael Ruckle | Simone Graeff-Hönninger | Maximilian M | Dániel Árpád | Graeff-Hönninger
---


Pre-Processing Techniques for NIR Spectra  In the context of cannabinoid prediction using near-infrared (NIR) spectroscopy, pre-processing techniques, such as standard normal variate (SNV) and Savitzky–Golay (SG) smoothing, are commonly employed together to enhance the efficacy of predictive models [13,20]. Nevertheless, to the best of our knowledge, whole inflorescences have not been investigated for the individual comparison of pre-processing techniques and raw data for the prediction of cannabinoid concentration using NIR spectroscopy ( ). In , the performance of different pre-processing techniques in predicting total CBD concentration is detailed. Figure S1: Observed versus predicted plot of the total delta-9-tetrahydrocannabinol (total THC) concentration for (a) raw data, (b) standard normal variate (SNV), and (c) Savitzky–Golay (SG) smoothing and total cannabigerol (total CBG) concentration for (d) raw data, (e) standard normal variate (SNV), and (f) Savitzky–Golay (SG) smoothing. Developing Prediction Models Using Near-Infrared Spectroscopy to Quantify Cannabinoid Content in Cannabis sativa.

[Visit Link](https://www.mdpi.com/2223-7747/13/6/833){:target="_blank rel="noopener"}

