---
layout: post
title: "Targeting the Endocannabinoidome in Pancreatic Cancer"
date: 2022-08-05
categories:
- Cannabinoid
author: Falasca, Valerio Falasca, Marco Falasca
tags: [Cannabinoid receptor,Cannabinoid receptor type 2,Pancreatic cancer,Chemotherapy,Cannabinoid receptor type 1,Cannabinoid,Tumor microenvironment,Tetrahydrocannabinol,Endocannabinoid system,TRPV1,Cancer,Cell signaling,2-Arachidonoylglycerol,Clinical medicine,Biochemistry,Cell biology,Neurochemistry,Biology,Biotechnology,Medical specialties]
---


The ligands AEA and 2-AG, belonging to the N-acylethanolamine (NAE) and monoacylglycerol (MAG) families, respectively, were classified as cannabinoids and gained a significant amount of research attention ( Figure 1 ). The GPR119 receptor, expressed in the pancreas and GI tract, has gained importance in the fight against type 2 diabetes and obesity as an insulin and glucose regulator [ 34 ]. In turn, these orphan receptors were found to have other ligands other than AEA and 2-AG modulating their activity, also belonging to the two main families of-acylamides and monoacylglycerols ( Figure 1 ). Thus, the evidence suggests there are more factors involved in the ECS signalling than was initially thought, due to the promiscuity of ligands and redundancy in their biological pathways. Altogether, the eCBome system is greatly expressed across the body, including the CNS, adipose and inflammatory tissue, the GI tract and the pancreas [ 45 ].

<hr>

[Visit Link](https://www.mdpi.com/2218-273X/12/2/320/htm){:target="_blank" rel="noopener"}


