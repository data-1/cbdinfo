---
layout: post
title: "Theoretical Study of the Structural Stability, Chemical Reactivity, and Protein Interaction for NMP Compounds as Modulators of the Endocannabinoid System"
date: 2022-08-27
categories:
- Endocannabinoid
author: Rangel-Galván, Castro, María Eugenia, Perez-Aguilar, Jose Manuel, Caballero, Norma A., Rangel-Huerta, Melendez, Francisco J.
tags: [Molecular orbital,Chemical bond,Chemical compound,Molecule,Frontier molecular orbital theory,Electronegativity,Electron density,Docking (molecular),Hydrogen bond,Antibonding molecular orbital,Chemical reaction,Natural bond orbital,Active site,Molecular geometry,Amino acid,Proteinprotein interaction,Protein domain,Cannabinoid receptor type 1,Implicit solvation,Nuclear magnetic resonance,Amide,Chemistry,Physical chemistry,Applied and interdisciplinary physics,Physical sciences]
---


The structural stability and chemical reactivity properties of the semi-rigid NMP compounds are calculated by using Density Functional Theory (DFT). Finally, molecular docking calculations were made to establish the possible binding sites of NMP compounds in the Ca3.2 channel. In addition, the amine group of the ligand formed a hydrogen bond interaction with residue T568. In NMP-181, the electronic delocalization is observed on the ester group (O1 and O3) and the carbazole group (N17). The contribution of the LUMO orbital is located at >75% on the NMP carbazole group, it can be speculated that this molecular region possesses the major participation during the ligand-receptor interaction for both CB1/CB2 receptors and T-type calcium channel.

<hr>

[Visit Link](https://www.mdpi.com/1420-3049/27/2/414/htm){:target="_blank" rel="noopener"}


