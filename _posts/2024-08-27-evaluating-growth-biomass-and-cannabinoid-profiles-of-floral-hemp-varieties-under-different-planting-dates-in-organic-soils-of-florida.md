---
layout: post
title: "Evaluating Growth, Biomass and Cannabinoid Profiles of Floral Hemp Varieties under Different Planting Dates in Organic Soils of Florida"
date: 2024-08-27
tags: [Cannabis,Cannabidiol,Hemp]
author: Saroop S. Sandhu | Anuj Chiluwal | Zachary T. Brym | Mike Irey | James Mabry McCray | Dennis Calvin Odero | Samira H. Daroub | Hardev S. Sandhu | Saroop S | Zachary T
---


Furthermore, plant growth data including plant height, SPAD values and green canopy cover were measured every 2 to 3 weeks during crop growth from five randomly selected plants in each plot at both locations. Plant height (A) and Soil Plant Analysis Development (SPAD) values (B) during the growing season for different varieties at different planting dates at United States Sugar Corporation (USSC) location. Plant biomass (Kg/ha) at harvesting for different varieties at different planting dates at the USSC location. Furthermore, the CBD and THC content and their ratios (CBD:THC) at harvest at the USSC location were significantly different in various varieties, but the planting date had a significant effect on the CBD and THC content of the Cherry abacus variety only. Supplementary Materials  The following supporting information can be downloaded at: https://www.mdpi.com/article/10.3390/agronomy12112845/s1, Table S1: Different macro and micronutrient contents (pre-planting) at USSC and EREC locations; Figure S1: Monthly average air temperature during the growing season at USSC and EREC locations; Figure S2: Monthly cumulative rainfall during the growing season at USSC and EREC locations.

[Visit Link](https://www.mdpi.com/2073-4395/12/11/2845){:target="_blank rel="noopener"}

