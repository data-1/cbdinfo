---
layout: post
title: "Gene Expression Data Mining Reveals the Involvement of GPR55 and Its Endogenous Ligands in Immune Response, Cancer, and Differentiation"
date: 2022-09-01
categories:
- endocannabinoid
author: Wnorowski, Wójcik, Maj, Artur Wnorowski, Jakub Wójcik, Maciej Maj
tags: [T helper cell,Downregulation and upregulation,Chimeric antigen receptor T cell,Immune system,T cell,BCL6,Inflammation,Antigen-presenting cell,Immunotherapy,Cellular differentiation,EpsteinBarr virus,Receptor (biochemistry),Gene expression,Monocyte,Cell signaling,GPR55,Proteolysis,Lymphocyte,CD19,Clinical medicine,Biochemistry,Cell biology,Molecular biology,Biology,Medical specialties,Biotechnology,Life sciences,Immunology]
---


Several endogenous peptides were discovered to activate GPR55 [ 10 ]. As depicted in Figure 3 B, the identified experiments focused on immune activation (55%), response to drug treatment (29%), immune-mediated inflammatory diseases (IMIDs; 10%), cancer (4%), and differentiation (2%).upregulation was observed predominantly in the immune cells (67%) or in whole blood (10%). The experimental conditions that led to the downregulation of GPR55 were identified based on gene expression data ( Figure 2 A). Here, we identified UC as a condition characterized by upregulation of GPR55 and increased PACAP27/38 production. PACAP upregulation was described previously in UC patients [ 56 ], and conditions favoring PACAP27/38 production are reported here.

<hr>

[Visit Link](https://www.mdpi.com/1422-0067/22/24/13328/htm){:target="_blank" rel="noopener"}


