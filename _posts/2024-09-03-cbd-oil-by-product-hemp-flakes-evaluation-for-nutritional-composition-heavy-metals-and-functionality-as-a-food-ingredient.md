---
layout: post
title: "CBD oil by-product (Hemp flakes): Evaluation for nutritional composition, heavy metals and functionality as a food ingredient"
date: 2024-09-03
tags: [Cannabidiol,Cannabis,Cannabinoid,Medical cannabis,Hemp,Ferric reducing ability of plasma,Polyphenol,High-performance liquid chromatography,Cannabis edible,Copper,Cannabinol]
author: Elvis A. Baidoo | Ernst Cebert | Regine Mankolo | Martha Verghese | Joshua L. Herring
---


Thus, this research aimed to assess the nutritional, safety (heavy metals), and functional (antioxidant properties and cannabinoid content) values of hemp flakes by comparing it with existing commercial hemp protein products. These results are similar to reported data when using similar extraction methods reported by Zago et al., [23]. Except for the aqueous extract, all hemp flake (H) extracts were among the highest (p ≤ 0.05) when compared with the commercial hemp samples. The results further indicate that all the levels of heavy metals found in all samples were within the permissible levels according to the Environmental Protection Agency (EPA) and the US Food and Drug Administration (FDA). The by-product tested in this work was shown to contain similar kinds of cannabinoids and even higher amounts of specific cannabinoids compared to related commercial hemp food ingredients on the market and could, therefore, be recommended for use as a food ingredient to benefit consumers' health.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11334841/){:target="_blank rel="noopener"}

