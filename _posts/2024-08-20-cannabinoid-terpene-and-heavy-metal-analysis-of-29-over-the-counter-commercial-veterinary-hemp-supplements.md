---
layout: post
title: "Cannabinoid, Terpene, and Heavy Metal Analysis of 29 Over-the-Counter Commercial Veterinary Hemp Supplements"
date: 2024-08-20
tags: [Cannabidiol,Cannabinoid,Cannabis,Tetrahydrocannabinolic acid,Tetrahydrocannabinol,Dietary supplement,Charlotte's Web (cannabis),Cannabaceae]
author: Joseph J Wakshlag | Stephen Cital | Scott J Eaton | Reece Prussin | Christopher Hudalla
---


Two recent publications examining selected cannabinoid concentrations in human over-the-counter products showed a tremendous disparity between labeling claims and analysis of the products, with over 40% having less than the labeled amount and over 40% having more than the labeled amount.12,13 The THC concentrations in a Canadian study were less than 0.01% for all products showing compliance with the Canadian standard for Cannabis CBD products.12 In a study examining 14 European products, all with THC concentrations being below the 0.2% allowable limit, CBD concentrations varied from either total cannabinoids or specific CBD concentrations as labeled.14  This disparity in products can be from a range of issues including batch to batch variation, intentional improper labeling, degradation over time, poor extraction techniques and lack of certification of the laboratories being used to measure cannabinoids. Discussion  This pet product examination is the first of its kind to utilize a certified laboratory in the analysis of cannabinoids, terpenes and heavy metals in commercially available low-THC Cannabis sativa pet supplements. Further examination of the other THC and CBD forms including THCA and CBDV show that there is less than 1 mg/mL or gram on average and that these are not major cannabinoids found in products. Results from this testing are collected with statistical comparison to other laboratories and/or to the established values for the testing performed. As with any plant material, accumulation of minerals from soil is part of the nutritional benefits of plant consumption; however, chronic consumption of any plant material with accumulation of heavy metals is an important health consideration.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7169471/){:target="_blank rel="noopener"}

