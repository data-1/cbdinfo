---
layout: post
title: "Sex-dependent effects of in utero cannabinoid exposure on cortical function"
date: 2024-08-19
tags: [Cannabis (drug),Synapse,Gap junction,Cannabinoid,Prefrontal cortex,Chemical synapse,Hippocampus,Pregnancy,Neuroscience,Neurophysiology,Basic neuroscience research,Branches of neuroscience]
author: Anissa Bara | Antonia Manduca | Axel Bernabeu | Milene Borsoi | Michela Serviado | Olivier Lassalle | Michelle Murphy | Jim Wager-Miller | Ken Mackie | Anne-Laure Pelissier-Alicot
---


A major caveat is that most studies investigated effects using male progeny only. Specifically, we found that prenatal cannabinoid exposure (PCE) reduces social interactions in males but not females, while other behaviors were spared. We also identify a pharmacological strategy to normalize synaptic functions and socialization in adult prenatally exposed males. While our work clearly shows previously undisclosed sexual divergence in the consequences of fetal cannabinoids, they also reveal sex differences in the main elements involved in eCB signaling: CB1R was similar in both sexes while lower mRNA levels of the 2-arachidonoylglycerol (2-AG) synthesizing (DAGLα) enzyme were found in naive females compared to naive males. In the PFC, CB1R density is lower in males than in females (Castelli et al., 2014).

[Visit Link](https://elifesciences.org/articles/36234){:target="_blank rel="noopener"}

