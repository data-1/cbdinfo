---
layout: post
title: "Interplay between the Cannabinoid System and microRNAs in Cancer"
date: 2024-08-21
tags: [Cannabinoid,Endocannabinoid system,MicroRNA,Cannabinoid receptor 2,Cancer,Enzalutamide,Carcinogenesis,Metastasis,Epigenetics,Anandamide,Downregulation and upregulation,Cannabinoid receptor 1,Colorectal cancer,Inflammation,Cannabidiol,Apoptosis,Biochemistry,Clinical medicine,Biology,Cell biology]
author: Julia M. Salamat | Kodye L. Abbott | Patrick C. Flannery | Elizabeth L. Ledbetter | Satyanarayana R. Pondugula
---


Oncogenic and Tumor Suppressor Effects of miRNAs via Cannabinoid System  Aside from the cannabinoid system modulating miRNA expression, miRNAs can also regulate the cannabinoid system contributing to either promotion or inhibition of cancer growth and progression. Cancer 2015, 14, 73. Cancer 2018, 18 (1), 5–18. Cancer 2015, 14, 73. Cancer 2018, 18 (1), 5–18.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8973111/){:target="_blank rel="noopener"}

