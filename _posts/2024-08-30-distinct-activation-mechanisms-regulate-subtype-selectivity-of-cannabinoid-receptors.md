---
layout: post
title: "Distinct activation mechanisms regulate subtype selectivity of Cannabinoid receptors"
date: 2024-08-30
tags: [Cannabinoid receptor 2,G protein-coupled receptor,Ligand (biochemistry),Docking (molecular),Kinetic Monte Carlo,Receptor (biochemistry),Receptor antagonist,Markov chain,Biochemistry,Cell biology]
author: 
---


As agonist binding leads to the activation of CB1, active and inactive structures have distinct N-terminus feature values. For CB1, I1 and I2 are structurally similar to the inactive state of the receptor (Supplementary Table 4 and Supplementary Fig. For CB2, intermediate states I1, I2, and I3 are structurally and kinetically similar to the inactive state of the receptor, whereas I4 is structurally similar to the active state (Supplementary Table 5; Fig. Most of the metastable states contains extracellular to intracellular communications (Supplementary Fig. Featurization of protein conformational ensemble  To perform adaptive sampling and markov state model (MSM) building (Discussed in the following sections), protein conformations in MD system need to be represented by the descriptors that can capture protein conformational changes.

[Visit Link](https://www.nature.com/articles/s42003-023-04868-1){:target="_blank rel="noopener"}

