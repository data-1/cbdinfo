---
layout: post
title: "One-stop Microfluidic Assembly of Human Brain Organoids to Model Prenatal Cannabis Exposure"
date: 2024-09-30
tags: [Cerebral organoid,Organoid,Subventricular zone,Cannabinoid,Brain,Development of the nervous system,Cannabinoid receptor 1,Tetrahydrocannabinol,Endocannabinoid system,Cannabinoid receptor,Hypoxia (medicine),Cannabinoid receptor 2,Astrocyte,Biology,Cell biology,Life sciences]
author: Zheng Ao | Hongwei Cai | Daniel Havert | Zhuhao Wu | Zhiyi Gong | John M. Beggs | Ken Mackie | Feng Guo
pubdate: "2020-01-16"
institutions: Indiana University Bloomington
doi: "https://www.biorxiv.org/content/10.1101/2020.01.15.908483v1.full"
---


By using this device, we were able to fabricate 169 human brain organoids each batch in a standard 6-well plate format (Fig. Live/Dead staining  To visualize brain organoid viability, EB and mature organoids were stained using a live/dead cell imaging kit (Invitrogen). Briefly, mature brain organoids were allowed to attach to the MEA electrodes for 4 - 6 days before we recorded its spontaneous firing (Fig. Our microfluidic device is good at performing medium changes and controlling THC perfusion of organoids. Brain organoids were treated for 27 days under prolonged THC dosing with the medium change from the bottom chamber of the microfluidic device every other day.