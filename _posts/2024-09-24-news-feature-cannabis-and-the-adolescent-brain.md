---
layout: post
title: "News Feature: Cannabis and the adolescent brain"
date: 2024-09-24
tags: [Cannabis (drug),Adolescence,Cannabinoid,Schizophrenia,Brain,Intelligence quotient,Substance abuse,Tetrahydrocannabinol,Mental disorder,Neurotransmitter,Health,Behavioural sciences,Neuroscience]
author: Helen Shen
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/6955325"
---


Many observational studies have suggested that adolescent cannabis use may be linked to long-term harms, including cognitive impairment and increased risk of schizophrenia (2). And some of THC’s effects on brain development may be limited to specific windows of vulnerability, Tseng has found. “I don’t think there’s any compelling evidence that moderate levels of use are going to produce long-lasting cognitive deficits.”  In 2012, one high-profile study used the Dunedin Study data to compare people’s intelligence quotient (IQ) between ages 7 and 13 (before cannabis use) and at age 38, and assess their drug use at various ages. Besides schizophrenia and cognition, the Adolescent Brain Cognitive Development (ABCD) study could potentially clarify the role of adolescent cannabis use in other conditions, such as addiction or mood disorders. Although few researchers dismiss the potential for cannabis to harm the developing teenage brain, much remains unknown.