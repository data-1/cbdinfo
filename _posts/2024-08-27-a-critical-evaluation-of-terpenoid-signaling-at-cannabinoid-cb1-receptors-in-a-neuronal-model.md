---
layout: post
title: "A Critical Evaluation of Terpenoid Signaling at Cannabinoid CB1 Receptors in a Neuronal Model"
date: 2024-08-27
tags: [Cannabinoid,Synapse,2-Arachidonoylglycerol,Cannabinoid receptor 1,Endocannabinoid system,TRPV1,Transient receptor potential channel,Chemical synapse,Excitatory postsynaptic potential]
author: Michaela Dvorakova | Sierra Wilson | Wesley Corey | Jenna Billingsley | Anaëlle Zimmowitch | Joye Tracey | Alex Straiker | Ken Mackie
---


We tested a panel of five common cannabis terpenoids, myrcene, linalool, limonene, α-pinene and nerolidol, in two neuronal models, autaptic hippocampal neurons and dorsal root ganglion (DRG) neurons. Most terpenoids had little or no effect on neuronal cannabinoid signaling. (B) Myrcene also did not have an effect on DSE responses. We tested a panel of terpenoids frequently present in cannabis-- myrcene, linalool, limonene, α-pinene, and nerolidol—based on their range of claimed effects, using two neuronal models. [Google Scholar] [CrossRef] [PubMed]  Straiker, A.; Mackie, K. Cannabinoid signaling in inhibitory autaptic hippocampal neurons.

[Visit Link](https://www.mdpi.com/1420-3049/27/17/5655){:target="_blank rel="noopener"}

