---
layout: post
title: "Sussing That “Skunky” Smell"
date: 2022-07-25
categories:
- Cannabis
author: Lauren Robertson, The Time I Finished My Degree In Microbiology I Had Come To One Conclusion, I Did Not Want To Work In A Lab. Instead, I Decided To Move To The South Of Spain To Teach English. After Two Brilliant Years, I Realized That I Missed Science, What I Really Enjoyed Was Communicating Scientific Ideas, Whether That Be To Four-Year-Olds Or Mature Professionals. On Returning To England I Landed A Role In Science Writing, Found It Combined My Passions Perfectly. Now At Texere, I Get To Hone These Skills Every Day Writing About The Latest Research In An Exciting, Creative Way.
tags: [Odor,Plant,Physical sciences,Chemistry]
---


At the same time, SCD allows us to detect compounds that contain sulfur very easily. Indeed, as we analyzed many datasets, we found that the compounds present in the SCD data and then identified with TOF-MS were the exact ones that contribute the skunky, gassy notes in cannabis. We had our suspected structures synthesized and collected data using our 2DGC-TOFMS, and the similar elution times and mass spectral data confirmed we were correct for each one. I think this data is important not only for cultivation but also for odor control. We’re now working out the structures of other VSCs we detected but did not identify; we will hopefully be publishing a follow-up paper to report these details sometime in 2022.

<hr>[Visit Link](https://thecannabisscientist.com/testing-processing/sussing-that-skunky-smell){:target="_blank" rel="noopener"}


