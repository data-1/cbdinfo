---
layout: post
title: "A bibliometric analysis of the cannabis and cannabinoid research literature"
date: 2024-08-25
tags: [Cannabis,Cannabis (drug),Cannabidiol,Clinical medicine,Health]
author: Jeremy Y
---


The term “marijuana” refers to the parts of or the products from the cannabis plant that contain THC, while industrial hemp refers to plants that have minimal THC (National Center for Complementary and Integrative Health (NCCIH) 2019). The last few decades have seen a large increase in the volume of literature on the topic of cannabis and cannabinoids (Liu et al. 2020; Matielo et al. 2018), and the application of a bibliometric analysis can facilitate a stronger understanding of the field. The following bibliometric data were collected: number of publications (in total and per year), authors, and journals; open access status; publications per journal; journals publishing the highest volume of literature and their impact factors, language of publication; document type; publication country; author affiliations; funding sponsors; most highly cited publications; and most highly published authors. A citation analysis of journals that published the largest volume of cannabis and cannabinoid research was performed to identify possible trends between a journal’s volume of publication and its relative impact through the number of citations. The most common publication countries included the USA (n = 12,420, 41.68%), the UK (n = 2236, 7.50%), and Canada (n = 2062, 6.92%) (Fig.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00133-0){:target="_blank rel="noopener"}

