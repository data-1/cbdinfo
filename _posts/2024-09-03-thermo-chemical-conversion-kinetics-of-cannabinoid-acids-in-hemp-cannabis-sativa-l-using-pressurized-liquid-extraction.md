---
layout: post
title: "Thermo-chemical conversion kinetics of cannabinoid acids in hemp (Cannabis sativa L.) using pressurized liquid extraction"
date: 2024-09-03
tags: [Tetrahydrocannabinolic acid,Cannabis,Cannabinoid,Rate equation,Cannabidiol,High-performance liquid chromatography,Chemistry,Concepts in chemistry]
author: Tae-Hyung | Sang-Hyuck | Chad A | Kenneth J | Joon-Hee
---


From the kinetics studies the reaction rate constants and activation energies required for the conversion process, as well as those necessary for side reactions and degradation can be determined. For instance, the CBGA to CBG and CBDA to CBD decarboxylation at 140 °C temperature, continued to show maximum concentration after 20 min. The reaction order was found to be a pseudo-first order reaction and the rate constant, k, was found to be equal to the slope of this line. For instance, in the case of CBDA at 140 °C, the maximum concentration can be achieved in 6.3 min through thermo-chemical conversion, while thermal decarboxylation required 27 min at same temperature, based on the mass balance model of Moreno et al. (2020). The CBD model predicts the CBD values for the control, which the model was based on, and the model conditions.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-024-00243-x){:target="_blank rel="noopener"}

