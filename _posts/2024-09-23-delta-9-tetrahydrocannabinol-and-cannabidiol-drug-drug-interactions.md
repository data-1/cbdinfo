---
layout: post
title: "Delta-9-Tetrahydrocannabinol and Cannabidiol Drug-Drug Interactions"
date: 2024-09-23
tags: [Dronabinol,Cannabidiol,Drug interaction,Chemicals in medicine,Medical treatments,Pharmacy,Pharmaceutical sciences,Medicinal chemistry,Drugs,Pharmacology]
author: Paul T. Kocis | Kent E. Vrana
institutions: 
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8489344/"
---


The use of medical marijuana (whether smoked or in extracts), CBD oil, or recreational marijuana can deliver a highly variable cannabinoid concentration. Medical use of cannabinoids. Drugs. Medical use of cannabinoids. Drugs.