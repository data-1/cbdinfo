---
layout: post
title: "The systems medicine of cannabinoids in pediatrics: the case for more pediatric studies"
date: 2022-08-27
categories:
- cannabinoid
author: Chloe P. O Dell, Dawn S. Tuell, Darshan S. Shah, William L. Stone
tags: [Cannabidiol,Neurotransmitter,Chemical synapse,Cannabis (drug),Single-nucleotide polymorphism,Tetrahydrocannabinol,Medical cannabis,Cannabis edible,Metabolomics,Cannabis,Neuron,Synapse,Cannabinoid receptor type 2,Reward system,Retrograde signaling,Cannabinoid,Dopamine,Synthetic cannabinoids,Cell signaling,Cannabinoid receptor,Pregnancy,Pediatrics,Cannabinoid receptor type 1,Breastfeeding,Cannabis sativa,Medicine,Health sciences,Neuroscience,Health]
---


What are cannabinoids? 3.1.4 Medical marijuana and pregnancy Marijuana is the most widely used drug during pregnancy [43]. 5.1 Retrograde cannabinoid signaling, behavior, and neuronal plasticity We will briefly review the role of retrograde eCB signaling in the ECS with the limited goal of identifying key ECS protein/gene players (see Fig. The genomics of cannabis use and cannabis use disorder (CUD) Most users of cannabis do not develop CUD [117, 118]. The epigenomics of cannabis use/CUD The clinical epigenetics of cannabis use has not been well studied in populations relevant to pediatrics.

<hr>

[Visit Link](https://www.imrpress.com/journal/FBL/27/1/10.31083/j.fbl2701014/htm){:target="_blank" rel="noopener"}


