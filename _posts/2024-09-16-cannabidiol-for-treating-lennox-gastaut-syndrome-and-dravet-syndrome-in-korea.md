---
layout: post
title: "Cannabidiol for Treating Lennox-Gastaut Syndrome and Dravet Syndrome in Korea"
date: 2024-09-16
tags: [Lennox–Gastaut syndrome,Cannabidiol,Epilepsy,Diseases and disorders,Health care,Medical specialties,Health,Medicine,Clinical medicine]
author: Chung Mo Koo | Se Hee Kim | Joon Soo Lee | Byung‐Joo Park | Hae Kook Lee | Heung Dong Kim | Hoon‐Chul Kang
institutions: Severance Hospital | Yonsei University
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7769699"
---


Find articles by Heung Dong Kim  Hoon-Chul Kang  2Division of Pediatric Neurology, Department of Pediatrics, Epilepsy Research Institute, Severance Children's Hospital, Yonsei University College of Medicine, Seoul, Korea. This study was performed to evaluate the tolerability and efficacy of CBD in children with LGS or DS for the first time in Korea. On the contrary, more individuals experienced a reduction in seizure frequency of more than 75% in the CBD 10 group than in the CBD 20 group.19 In a study related to LGS treatment with CBD published in 2018, a reduction in the frequency of seizures by more than 50% during the research period was observed for 36% of patients in the CBD 10 group and 39% of patients in the CBD 20 group. The case for assessing cannabidiol in epilepsy. The case for assessing cannabidiol in epilepsy.