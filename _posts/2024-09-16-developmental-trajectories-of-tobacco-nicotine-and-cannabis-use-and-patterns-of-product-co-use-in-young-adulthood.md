---
layout: post
title: "Developmental Trajectories of Tobacco/Nicotine and Cannabis Use and Patterns of Product Co-use in Young Adulthood"
date: 2024-09-16
tags: [Vaping,Electronic cigarette,Vaporizer (inhalation device),Cannabis (drug),Likelihood function,Cigarette,Construction of electronic cigarettes,Habits,Smoking,Health,Individual psychoactive drugs]
author: Michael S. Dunbar | Jordan P. Davis | Joan S. Tucker | Rachana Seelam | Regina A. Shih | Elizabeth J. D’Amico
institutions: RAND Corporation
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7446261"
---


Over 1-in-4 young adults ages 18 to 24 report vaping nicotine in the past month.20 -22 Similarly, data from the nationally representative Monitoring the Future (MTF) study showed a more than 50% increase in cannabis vaping among 12th graders between 2017 and 2018 (4.9% to 7.5%),22 and other studies suggest increasing rates of cannabis vaping among young people who live in states where cannabis is legal.23 Most studies have focused on co-use of combustible tobacco cigarettes and combustible cannabis.24 -27 Despite the increasing popularity of vaping products, recent longitudinal studies have not considered their use when characterizing longitudinal patterns of T/C co-use in young people, which may lead to mischaracterizations with respect to longitudinal T/C co-use patterns.28,29 However, some recent cross-sectional work has begun to assess co-use with respect to vaping and other types of T/C products. Behavioral Health Trends in the United States: Results from the 2014 National Survey on Drug Use and Health. Progression from marijuana use to daily smoking and nicotine dependence in a national sample of US adolescents. Behavioral Health Trends in the United States: Results from the 2014 National Survey on Drug Use and Health. Progression from marijuana use to daily smoking and nicotine dependence in a national sample of US adolescents.