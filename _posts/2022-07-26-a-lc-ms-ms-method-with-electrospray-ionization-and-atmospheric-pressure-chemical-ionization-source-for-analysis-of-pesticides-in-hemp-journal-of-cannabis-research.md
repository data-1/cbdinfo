---
layout: post
title: "A LC-MS/MS method with electrospray ionization and atmospheric pressure chemical ionization source for analysis of pesticides in hemp - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Hemp
author: Dalmia, Perkin Elmer, Inc., Shelton, Cudjoe, Woodbridge, Jalali, Qin, Bridgeport Ave., Ct
tags: [Mass spectrometry,Atmospheric-pressure chemical ionization,Liquid chromatographymass spectrometry,Electrospray ionization,Physical sciences,Analytical chemistry,Chemistry,Laboratory techniques,Instrumental analysis,Analysis]
---


The response RSD for each pesticide at its LOQ level in the hemp matrix was less than 20%. 1 a Overlay of the response of blank hemp matrix (red) and propiconazole (green) at a level of 0.1 μg/g in the hemp matrix showing the matrix interference with a MRM transition of 342.1 to 69. b Overlay of the response of blank hemp matrix (red) and propiconazole (green) at a level of 0.1 μg/g in the hemp matrix demonstrating no matrix interference with a MRM transition of 344.1 to 69 Full size image  Analysis of challenging analytes using the LC-MS/MS method with an ESI source  A number of pesticides in cannabis and hemp, regulated by California and other states, are analyzed traditionally using GC-MS/MS with an EI source. Figure 2 shows a good signal to noise of 10 for captan at LOQ level of 0.1 μg/g in hemp using the LC-MS/MS method with an ESI source and it can easily meet California state action limits of 0.7 μg/g for captan in hemp. This shows that the LC-MS/MS method with an APCI source for the analysis of PCNB in hemp is extremely sensitive and can easily meet the California state action limits. Similarly, the matrix blank signal for the other three pesticides (chlorfenapyr, methyl parathion, and chlordane) with an APCI source showed no matrix interference peaks at the retention time of these analytes, and the LC-MS/MS method with an APCI source showed a good signal to noise for the hemp matrix spiked with these pesticides at the level of California action limits of 0.1 μg/g or lower.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00106-9){:target="_blank" rel="noopener"}


