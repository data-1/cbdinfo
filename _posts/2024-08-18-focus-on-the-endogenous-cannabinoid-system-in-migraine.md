---
layout: post
title: "Focus on the Endogenous Cannabinoid System in Migraine"
date: 2024-08-18
tags: [Anandamide,Migraine,Calcitonin gene-related peptide,Endocannabinoid system,Clinical medicine]
author: staceywhite | Domenico Chirchiglia | Attilio Della Torre | Pasquale Chirchiglia
---


In a case study by Prof Ethan Russo, a leading authority on the subject, 80% of migraineurs benefit from the ingestion of cannabis, often with complete resolution of symptoms.4  Returning to the subject in review, the authors argue that:  Cannabis has a long history of safe and effective use in the treatment and prophylaxis of migraine  Cannabis appears to modulate nociceptive processes in the brain and can function via serotonergic and other pathways involved in migraine  Cannabis has also recognised anti-emetic properties, which are useful in treating migraine  Cannabis, when inhaled, is rapidly activated, skipping intestinal absorption, which is markedly reduced in migraine. The reason for this can be explained through effects on the trigeminal- vascular system, a regulating and modulating pain system, as supposed in migraine without aura or in the process of cortical spreading depression, as hypothesised in migraine with aura.5,6  Anandamide, also known as N-arachidonoylethanolamide (AEA), a fatty acid neurotransmitter, has an effect on both cannabinoid receptor Type 1 (CB1) and Type 2 (CB2) in the central and peripheral nervous system. The formulation comprises tablets and microgranule sachets, the latter representing the ultramicronised form. It is hoped that the knowledge acquired from this theory on how CGRP and PACAP-38 might be involved in migraine pathophysiology will contribute to the development of novel and better migraine treatments in the future.9 PACAP plays several important roles in vasodilation, neurotransmission, neuromodulation, and neurotrophy, as well as activation of the trigeminal vascular system. Thus, modulating the ECS, which is considered the origin of the neuroinflammation processes, could be a promising therapeutic option.

[Visit Link](https://www.emjreviews.com/neurology/article/focus-on-the-endogenous-cannabinoid-system-in-migraine/){:target="_blank rel="noopener"}

