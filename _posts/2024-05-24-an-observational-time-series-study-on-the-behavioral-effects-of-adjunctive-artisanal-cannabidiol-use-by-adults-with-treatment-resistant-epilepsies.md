---
layout: post
title: "An observational time-series study on the behavioral effects of adjunctive artisanal cannabidiol use by adults with treatment resistant epilepsies"
date: 2024-05-24
categories:
- 
author: Barbara A, Matthew X, Kalcheff-Korn
tags: [Cannabidiol,Imputation (statistics),Missing data,Epilepsy,Major depressive disorder,Anticonvulsant,Power of a test,Creative Commons license,Tetrahydrocannabinol,Analysis of variance,Data analysis,Randomized controlled trial,Health]
---


TIME 2 was also not significantly different from TIME 3 (p = 0.124). HADS data was initially analyzed using a two-way repeated measures ANOVA, with factors of TIME (3 levels = TIME 1, 2, 3) and SUBSCALE (2 levels = anxiety, depression). Time 2 was not significantly different from TIME 3 (p = 0.407). No main effect of TIME was found for medication effects (2df, F = 0.992, p = 0.396). Time 2 was not significantly different from TIME 3 (p = 0.513).

[Visit Link](https://bmcneurol.biomedcentral.com/articles/10.1186/s12883-024-03646-8#Sec27){:target="_blank" rel="noopener"}


