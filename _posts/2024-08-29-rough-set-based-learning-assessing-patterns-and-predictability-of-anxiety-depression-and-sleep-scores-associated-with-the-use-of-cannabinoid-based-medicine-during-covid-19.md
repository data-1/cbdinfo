---
layout: post
title: "Rough-set based learning: Assessing patterns and predictability of anxiety, depression, and sleep scores associated with the use of cannabinoid-based medicine during COVID-19"
date: 2024-08-29
tags: [Rough set,Statistical classification,PHQ-9]
author: 
---


In this research, we seek i) to evaluate the relationship of the clinical delivery of cannabinoid-based medicine for anxiety, depression and sleep scores by utilizing machine learning specifically rough set methods; ii) to discover patterns based on patient features such as specific cannabinoid recommendations [includes medical cannabis products contain varying amounts of cannabidiol (CBD) and tetrahydrocannabinol (THC)], diagnosis information, decreasing/increasing levels of clinical assessment tools: GAD-7 (General Anxiety Disorder-7), PHQ-9 (Patient Health Questionnaire-9), and PSQI (Pittsburgh Sleep Quality Index) (Buysse et al., 1988) scores over a period of time including during the COVID timeline; and iii) to predict whether new patients could potentially experience either an increase or decrease in clinical assessment tool scores (pl. Rough sets  In classical set theory, we can classify whether elements either belong to a set or not. Rough fuzzy sets and fuzzy rough sets*. Fuzzy rough sets. Rough sets.

[Visit Link](https://www.frontiersin.org/journals/artificial-intelligence/articles/10.3389/frai.2023.981953/full){:target="_blank rel="noopener"}

