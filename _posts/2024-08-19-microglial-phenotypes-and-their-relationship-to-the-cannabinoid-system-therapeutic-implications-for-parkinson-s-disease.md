---
layout: post
title: "Microglial Phenotypes and Their Relationship to the Cannabinoid System: Therapeutic Implications for Parkinson’s Disease"
date: 2024-08-19
tags: [Microglia,Cannabinoid receptor 2,Neuroinflammation,Cannabinoid,Anandamide,Inflammation,Cannabinoid receptor 1,Endocannabinoid system,Neurodegenerative disease,2-Arachidonoylglycerol,T helper cell,Alpha-synuclein,Immune system,Cannabinoid receptor,MPTP,Cell biology,Biology,Medical specialties,Biochemistry,Immunology,Neurochemistry]
author: Rachel Kelly | Valerie Joers | Malú G. Tansey | Declan P. McKernan | Eilís Dowd | Malú G | Declan P
---


Microglia  2.1. Cell. Cell 2008, 132, 631–644. Cell 2010, 7, 483–495. Cell 2013, 155, 1596–1609.

[Visit Link](https://www.mdpi.com/1420-3049/25/3/453){:target="_blank rel="noopener"}

