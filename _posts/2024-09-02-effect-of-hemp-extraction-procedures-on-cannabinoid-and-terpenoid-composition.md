---
layout: post
title: "Effect of Hemp Extraction Procedures on Cannabinoid and Terpenoid Composition"
date: 2024-09-02
tags: [Cannabinoid,Gas chromatography–mass spectrometry,Principal component analysis,Cannabis,Cannabis sativa,Terpene,Ethanol,Fragrance extraction,Steam distillation,Carbon dioxide,Solvent,Chemistry]
author: Francisco T. Chacon | Wesley M. Raup-Konsavage | Kent E. Vrana | Joshua J. Kellogg | Francisco T | Raup-Konsavage | Wesley M | Kent E | Joshua J
---


There are a number of extraction techniques that have been developed for the extraction of phytochemicals from hemp inflorescence; the most prominent in the industry include supercritical fluid CO2 extraction (SFE), solvent-based extractions, hydrodistillation, ultrasonication-assisted, and microwave-assisted extraction [28,29,30,31,32]. (A) F1 samples, (B) F2 samples, and (C) F3 samples. (A) F1 samples, (B) F2 samples, and (C) F3 samples. Conclusions  Overall, this study provides evidence of the differences in cannabinoid and terpene composition obtained from hemp inflorescence after three predominant extraction techniques: supercritical CO2, solvent extraction (ethanol), and hydrodistillation. Cannabis Cannabinoids 2020, 3, 25–60.

[Visit Link](https://www.mdpi.com/2223-7747/13/16/2222){:target="_blank rel="noopener"}

