---
layout: post
title: "CB2 Receptor Involvement in the Treatment of Substance Use Disorders"
date: 2022-09-05
categories:
- Cannabinoid
author: Navarrete, García-Gutiérrez, María S., Gasparyan, Navarro, Manzanares, Francisco Navarrete, María S. García-Gutiérrez, Ani Gasparyan, Daniela Navarro
tags: [Cannabinoid receptor type 2,Nucleus accumbens,Dopamine,Self-administration,Substance abuse,Alcohol (drug),Pharmacology,Nicotine,Reward system,Substance dependence,Addiction,Relapse,Neuroscience,Clinical medicine,Biochemistry,Neurochemistry]
---


In addition, after administration of different doses of ethanol, a marked increase of tyrosine hydroxylase (TH) gene expression in the VTA and MOr in the NAcc was observed. In contrast, in the intermittent drinking model, a significant increase in alcohol consumption was observed in group-stabled CB2-/- mice, with no differences found in isolated mice. Finally, contrary to the previously discussed results [ 50 ], in the voluntary consumption paradigm, a significant reduction in the level of consumption and preference (10–12 weeks) was found in CB2-/- mice. Considering that they employ the same mouse strain and genotype, possible differences in the performance of the experimental procedures or the mice’s housing conditions could be responsible for these discrepancies. This finding suggested that neurodevelopmental modifications in CB2-/- mice were likely behind the changes in vulnerability to the reinforcing effects of alcohol.

<hr>

[Visit Link](https://www.mdpi.com/2218-273X/11/11/1556/htm){:target="_blank" rel="noopener"}


