---
layout: post
title: "Minnesota wild hemp: a crucial botanical source in early cannabinoid discovery"
date: 2024-08-20
tags: [Cannabis,Cannabidiol,Hemp,Cannabinoid,Cannabinol,Tetrahydrocannabinol,Natural product,Raphael Mechoulam,Chemistry]
author: Crist N
---


Natural product isolation and characterization is an especially challenging process involving human creativity along with available resources. Further complicating progress in Cannabis science by the mid-twentieth century was that the attitude in the United States toward Cannabis was quickly changing. In contrast to the earlier results of Cahn, Adams could not obtain a crystalline cannabinoid acetate or p-nitrobenzoate derivative from the Minnesota wild hemp red oil. However, Adams’ exploration of the Minnesota wild hemp red oil continued and proved to be even more rewarding. Despite the increasingly prohibitive climate toward Cannabis in 1940, an interesting collaboration was forged then between the Treasury Department Narcotics Laboratory and Roger Adams at the University of Illinois.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-020-00031-3){:target="_blank rel="noopener"}

