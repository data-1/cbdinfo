---
layout: post
title: "Cannabinoids and monoaminergic system: implications for learning and memory"
date: 2024-09-03
tags: [Endocannabinoid system,Neurotransmitter,Cannabinoid receptor 1,Ventral tegmental area,Cannabinoid,Dopaminergic pathways,Dopamine,Neurotransmission,Cannabinoid receptor 2,Norepinephrine,Serotonin,Long-term potentiation,Hippocampus,Basic neuroscience research,Branches of neuroscience,Neuroscience,Neurochemistry,Neurophysiology]
author: Sha Zhao | Zhao-Liang Gu | Ya-Nan Yue | Xia Zhang | Yuan Dong
---


10.1016/j.bbr.2008.03.020 [PubMed] [CrossRef] [Google Scholar]  Hansen N. (2017). 10.1017/S0033291704002260 [PubMed] [CrossRef] [Google Scholar]  Marsicano G., Lutz B. 10.1016/j.lfs.2017.11.029 [PubMed] [CrossRef] [Google Scholar]  Mendiguren A., Pineda J. 10.1016/j.ejphar.2006.01.002 [PubMed] [CrossRef] [Google Scholar]  Mendiguren A., Pineda J. 10.1016/j.neuroscience.2007.10.026 [PubMed] [CrossRef] [Google Scholar]  Sara S. J.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11349573/){:target="_blank rel="noopener"}

