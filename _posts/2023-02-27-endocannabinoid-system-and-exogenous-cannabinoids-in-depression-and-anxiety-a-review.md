---
layout: post
title: "Endocannabinoid System and Exogenous Cannabinoids in Depression and Anxiety: A Review"
date: 2023-02-27
categories:
- Endocannabinoid
author: Hasbi, Madras, Bertha K., George, Susan R., Ahmed Hasbi, Bertha K. Madras, Susan R. George
tags: [Cannabis use disorder,Cannabinoid,Anandamide,Endocannabinoid system,Cannabis (drug),Tetrahydrocannabinol,Chemical synapse,Cannabidiol,Cannabinoid receptor type 2,Inhibitory postsynaptic potential,2-Arachidonoylglycerol,Neurotransmission,Gliotransmitter,Nucleus accumbens,Cannabinoid receptor type 1,Cannabinoid receptor,Major depressive disorder,Neurotransmitter,Neuron,Neuroscience,Biochemistry,Health,Neurophysiology,Neurochemistry]
---


There is a definite need for more RCT studies, especially in severe mood disorders such as MDD and BD, where there is a lack of such studies. CBD-enriched preparations appear to show a more consistent, beneficial effect in ameliorating mood under different conditions, which could present some promise for future use in mood-related disorders. Most studies from cannabis abusers have shown that high concentrations of THC and high ratios of THC:CBD were associated with more vigorous euphoria, but also with higher anxiety, depression and psychotic symptoms. It has also been shown that CBD attenuates some effects of THC such as anxiety, cognitive deficits, and psychosis in heavy cannabis users, and high CBD:THC ratios produce an effect opposite to the high THC:CBD concentrations [ 11 226 ]. This is particularly important with the view of identifying novel therapeutic targets for multiple illnesses, including anxiety and depression, using cannabinoid preparations devoid of side effects apparent during chronic treatment, including after the use of very low dosages [ 227 ], as well as to possibly reverse the adverse consequences of long-term cannabis use and abuse or withdrawal signs in CUD individuals [ 11 145 ].

<hr>

[Visit Link](https://www.mdpi.com/2076-3425/13/2/325){:target="_blank" rel="noopener"}


