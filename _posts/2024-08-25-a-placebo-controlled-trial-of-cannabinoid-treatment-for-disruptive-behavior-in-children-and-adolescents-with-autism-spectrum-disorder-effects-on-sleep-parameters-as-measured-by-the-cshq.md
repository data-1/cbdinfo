---
layout: post
title: "A Placebo-Controlled Trial of Cannabinoid Treatment for Disruptive Behavior in Children and Adolescents with Autism Spectrum Disorder: Effects on Sleep Parameters as Measured by the CSHQ"
date: 2024-08-25
tags: [Cannabinoid,Cannabidiol,Anandamide,Cannabinoid receptor 1,Clinical global impression,Tetrahydrocannabinol,Cannabis (drug),Health]
author: Aviad Schnapp | Moria Harel | Dalit Cayam-Rand | Hanoch Cassuto | Lola Polyansky | Adi Aran | Cayam-Rand
---


Impact of Cannabinoid Treatment on Sleep  The impact of the cannabinoid treatment on sleep disturbances was assessed using the CSHQ. [Google Scholar] [CrossRef] [PubMed]  Wang, L.; Hong, P.J. A Placebo-Controlled Trial of Cannabinoid Treatment for Disruptive Behavior in Children and Adolescents with Autism Spectrum Disorder: Effects on Sleep Parameters as Measured by the CSHQ. A Placebo-Controlled Trial of Cannabinoid Treatment for Disruptive Behavior in Children and Adolescents with Autism Spectrum Disorder: Effects on Sleep Parameters as Measured by the CSHQ. A Placebo-Controlled Trial of Cannabinoid Treatment for Disruptive Behavior in Children and Adolescents with Autism Spectrum Disorder: Effects on Sleep Parameters as Measured by the CSHQ Biomedicines 10, no.

[Visit Link](https://www.mdpi.com/2227-9059/10/7/1685){:target="_blank rel="noopener"}

