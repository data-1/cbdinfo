---
layout: post
title: "A critical review of cannabis in medicine and dentistry: A look back and the path forward"
date: 2022-07-31
categories:
- cannabinoid
- Cannabis
author: Ammaar H. Abidi, College Of Dentistry, Department Of Bioscience Research, The University Of Tennessee Health Science Center, Memphis Tennessee, Department Of General Dentistry, Sahar S. Alghamdi, Department Of Phamaceutical Sciences, College Of Pharmacy, King Saud Bin Abdulaziz University For Health Sciences
tags: [Cannabidiol,Cannabinoid receptor type 2,Cannabinoid,Tetrahydrocannabinol,Cannabinoid receptor,Cannabis (drug),Cannabis,Phospholipase C,Cell signaling,Signal transduction,Opioid,Pain management,Health sciences,Health care,Pharmacology,Cell biology,Medicine,Drugs,Clinical medicine,Health,Neurochemistry,Biochemistry]
---


Results The review highlights dental concerns of cannabis usage, the need to understand the endocannabinoid system (ECS), cannabinoid receptor system, its endogenous ligands, pharmacology, metabolism, current oral health, and medical dilemma to ascertain the detrimental or beneficial effects of using cannabis–hemp products. These studies demonstrate antinociception through G‐protein‐coupled mechanisms by enhancing the opioid system leading to either improvement in opioid effects or by the local release of β‐endorphin by CB2R. Interestingly, it has been reported that the psychotropic effects of THC are enhanced by CBD, which is due to the inhibition of CYP2C9 and CYP2C19 by CBD, which consequently decreases the clearance of THC (Yamaori et al., 2012). A clinical study reported that among patients using opioids for pain management, CBD hemp extract significantly provided chronic pain relief along with the reduction of opioid use and improved sleep quality among CBD‐treated patients (Capano et al., 2020). CHALLENGES AND CONSIDERATION FOR UTILIZING CANNABINOID PRODUCTS The Clinical Guide of CBD and Hemp oil (VanDolah et al., 2019) states that physicians who want to examine CBD and hemp oils need to find the highest‐quality product as variations occur in commercial products from the declared amount (Pavlovic et al., 2018).

<hr>[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9209799/){:target="_blank" rel="noopener"}


