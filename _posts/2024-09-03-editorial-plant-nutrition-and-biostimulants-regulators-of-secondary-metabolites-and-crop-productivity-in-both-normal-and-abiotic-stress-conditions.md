---
layout: post
title: "Editorial: Plant nutrition and biostimulants: regulators of secondary metabolites and crop productivity in both normal and abiotic stress conditions"
date: 2024-09-03
tags: [Plant,Urtica dioica,Hydroponics,Nutrient,Artemisia annua,Plant nutrition,Antioxidant,Tetrahydrocannabinol,Abiotic stress,Natural environment,Nutrition,Drought tolerance,Biology]
author: Probir Kumar Pal | Jana Šic Žlabur | Hong Wu
---


Therefore, within this Research Topic, efforts have been made to improve our understanding of the mechanisms related to the role of plant nutrients and biostimulants on plant productivity and plant secondary metabolism under both normal and abiotic stress conditions. There was no significant effect of COS on improving the growth of A. annua compared to the control under well-watered conditions. Humic acid (HA) is a proven biostimulant that increases the tolerance of plants to salt stress. This research provides important insights into the physiological, biochemical and molecular mechanisms of salt stress tolerance of ryegrass by applying HA. In summary, this Research Topic provides insights into hitherto poorly explained mechanisms related to the role of plant nutrition and biostimulants in the production of plant secondary metabolites production under normal and abiotic stress conditions.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11342393/){:target="_blank rel="noopener"}

