---
layout: post
title: "Subjective effects of combustible, vaporized, and edible cannabis: Results from a survey of adolescent cannabis users"
date: 2024-09-16
tags: [Cannabis (drug),Cannabis edible,Vaporizer (inhalation device),Vaping]
author: Esthelle Ewusi Boisvert | Dayoung Bae | Raina D. Pang | Jordan P. Davis | Lorraine I. Kelley‐Quon | Jessica L. Barrington‐Trimis | Matthew G. Kirkpatrick | Stephanie H. Chai | Adam M. Leventhal
institutions: University of Southern California
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10408718/"
---


While recent estimates suggest appreciable portions of youth use vaporized or edible cannabis products (e.g.; nationally, past 30-day vaping of cannabis rose from 4.9% in 2017 to 7.5% in 2018 among 12th grade students; survey data from California in 2017 indicates that more than 80% of youth who used cannabis in the past 30 days used edibles) (Borodovsky et al., 2017; Friese et al., 2017; Johnston et al., 2019; Morean et al., 2015; Peters et al., 2018) little is known about whether different methods of administration of cannabis affect key outcomes of relevance to basic addiction science and public health, such as differences in abuse liability and dependence on cannabis. Differences in Cannabis subjective effects, by method of administration  3.2.1. Discussion  This survey of 584 adolescent cannabis product users provides new evidence that adolescents’ report of their subjective responses to cannabis in the natural ecology varied across combustible, edible, and vaporized administration methods. The vaporized cannabis findings reported here do not align with a previous laboratory drug administration experiment in adults, which found that both positive and negative subjective effects of vaporized cannabis were either greater than or equal to those from combustible cannabis (Spindle et al., 2018), but are similar to results from an internet survey of adults conducted in 2013–2014, which found that vaping cannabis produced fewer positive effects than smoking cannabis (Etter, 2015). [PMC free article] [PubMed] [Google Scholar] [Ref list]