---
layout: post
title: "Improvement of the antioxidant activity, phytochemicals, and cannabinoid compounds of Cannabis sativa by salicylic acid elicitor"
date: 2022-08-27
categories:
- cannabinoid
- cannabis
author: Elham Mirzamohammad, Department Of Biology, Faculty Of Sciences, Urmia University, Urmia Iran, Abolfazl Alirezalu, Department Of Horticultural Sciences, Faculty Of Agriculture, Kazem Alirezalu, Department Of Food Science
tags: [Cannabinoid,Cannabidiol,Cannabis sativa,Polyphenol,Tetrahydrocannabinol,Cannabis,Gas chromatography,Ferric reducing ability of plasma,Chemistry]
---


The sprayed aerial parts were evaluated based on phenolic (TPC) and flavonoids (TFC) contents, antioxidant capacity (by FRAP and DPPH assay), photosynthetic pigments including chlorophyll a, b (Chl a and Chl b), total carotenoids (TCC), and cannabinoid compounds. The results indicated that phytochemical compounds and antioxidant capacity in C. sativa were influenced by various concentrations of salicylic acid (SA). Results of present study suggest that the elicitor SA (especially 1 M) was able to improve antioxidant activity, phytochemicals, and cannabinoid compounds of Cannabis sativa. The salicylic acid (SA) is classified as a phenolic growth regulator or natural plant phenolics having key role in regulation plant growth and development functions (Szepesi et al., 2008). The present study was designed to evaluate the potential effects of SA on enhanced production of pharmaceutically important phytochemicals.

<hr>

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8645707/){:target="_blank" rel="noopener"}


