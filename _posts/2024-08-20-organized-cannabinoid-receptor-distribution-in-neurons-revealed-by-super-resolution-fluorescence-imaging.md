---
layout: post
title: "Organized cannabinoid receptor distribution in neurons revealed by super-resolution fluorescence imaging"
date: 2024-08-20
tags: [Cannabinoid receptor 1,STED microscopy,Confocal microscopy,Deconvolution,Axon,Point spread function,Tandem mass spectrometry,Western blot,Fluorescence recovery after photobleaching,Immunoprecipitation,Dendrite,Neuron]
author: Chao-Po | Deska-Gauthier | Garth J | Zhi-Jie
---


PLA signals of CB1 and ankB were observed in cultured primary neurons (Fig. Our results clearly showed that the PLA signal located in axons but not in dendrites (Supplementary Fig. Our results show that select AISs express CB1 with periodic pattern with an interval around 190 nm (Supplementary Fig. 5j), but most were not periodic (Supplementary Fig. Fluorescence labeling of neurons  Cultured neurons were fixed with 4% (w/v) paraformaldehyde (PFA) in phosphate-buffered saline (PBS) for 15 min at 14 day in vitro (DIV 14).

[Visit Link](https://www.nature.com/articles/s41467-020-19510-5){:target="_blank rel="noopener"}

