---
layout: post
title: "Activity-based detection of synthetic cannabinoid receptor agonists in plant materials"
date: 2024-09-02
tags: [Tetrahydrocannabinolic acid,Cannabinoid,Synthetic cannabinoids,Psychoactive drug,Tetrahydrocannabinol]
author: Christophe P
---


Starting from a previously described in vitro activity-based CB1 assay, this study reports on a novel application of this assay to screen for cannabinoid activity in herbal products. This extract was further diluted 1:1 in OptiMEM®. Optimization of the methodology for screening of herbal products  Screening of plant material for the presence of SCRAs using the CB1 activity-based assay requires the samples to be in the format of an extract. Initially, a 1:1 dilution of methanolic extract in OptiMEM® was directly applied in the assay. Suitability of the proposed sample preparation was evaluated by assessing herbal materials spiked with CP55,940, typically used as a reference SCRA in SCRA pharmacology research.

[Visit Link](https://harmreductionjournal.biomedcentral.com/articles/10.1186/s12954-024-01044-4){:target="_blank rel="noopener"}

