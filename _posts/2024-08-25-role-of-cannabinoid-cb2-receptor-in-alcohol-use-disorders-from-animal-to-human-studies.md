---
layout: post
title: "Role of Cannabinoid CB2 Receptor in Alcohol Use Disorders: From Animal to Human Studies"
date: 2024-08-25
tags: [Nucleus accumbens,Cannabinoid receptor 2,Ventral tegmental area,Chemical synapse,Reward system,Γ-Aminobutyric acid,Inhibitory postsynaptic potential,Dopamine,Neurotransmitter,Mesolimbic pathway,Alcohol (drug),Cannabinoid receptor 1,Addiction,NF-κB,Neurophysiology,Neurochemistry,Basic neuroscience research]
author: María Salud García-Gutiérrez | Francisco Navarrete | Gasparyan | Daniela Navarro | Álvaro Morcuende | Teresa Femenía | Jorge Manzanares | García-Gutiérrez | María Salud
---


Mainly, the rewarding effects of alcohol occurred by the interaction between the opioidergic and dopaminergic mesolimbic systems, involving the increased firing of dopamine neurons in the VTA [74] and a subsequent increase of dopamine release into the NAcc [80]. Alcohol. ; Gardner, E.; et al. Functional expression of brain neuronal CB2 cannabinoid receptors are involved in the effects of drugs of abuse and in depression. Involvement of cannabinoid CB2 receptor in alcohol preference in mice and alcoholism in humans. Cannabinoid 2 receptors regulate dopamine 2 receptor expression by a beta-arrestin 2 and GRK5-dependent mechanism in neuronal cells.

[Visit Link](https://www.mdpi.com/1422-0067/23/11/5908){:target="_blank rel="noopener"}

