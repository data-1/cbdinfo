---
layout: post
title: "Alterations in Brain Cannabinoid Receptor Levels Are Associated with HIV-Associated Neurocognitive Disorders in the ART Era: Implications for Therapeutic Strategies Targeting the Endocannabinoid System"
date: 2024-08-20
tags: [Endocannabinoid system,Cannabinoid receptor 2,HIV-associated neurocognitive disorder,Cannabinoid receptor 1,Cannabinoid,Cannabinoid receptor,Astrocyte,Biology]
author: Mary K. Swinton | Erin E. Sundermann | Lauren Pedersen | Jacques D. Nguyen | David J. Grelotti | Michael A. Taffe | Jennifer E. Iudicello | Jerel Adam Fields | Mary K | Erin E
---


by  Mary K. Swinton  ,  Erin E. Sundermann  ,  Lauren Pedersen  ,  Jacques D. Nguyen  ,  David J. Grelotti  ,  Michael A. Taffe  ,  Jennifer E. Iudicello  and  Jerel Adam Fields  *  Department of Psychiatry, University of California, San Diego, CA 92093, USA  *  Author to whom correspondence should be addressed. CB1 and CB2 Expression Are Increased in HAND Brains on ART  To determine the expression levels of CB1 and CB2 in brains of HIV+ donors, we analyzed the frontal lobe lysates generated from WM and GM from HAND cases as well as NUI cases ( ). CB1 Expression and Localization Are Altered in HAND Brains on ART Compared to NUI  To better understand the alterations in CB1 levels in the WM and GM of brains from HAND decedents, we performed immunolabelling for CB1 in vibratome sections from the frontal cortex. This study is the first to analyze CB1 and CB2 receptor expression in HAND brains on ART. Brain 2016, 9, 75.

[Visit Link](https://www.mdpi.com/1999-4915/13/9/1742){:target="_blank rel="noopener"}

