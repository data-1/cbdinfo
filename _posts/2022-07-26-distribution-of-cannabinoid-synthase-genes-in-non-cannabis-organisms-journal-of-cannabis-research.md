---
layout: post
title: "Distribution of cannabinoid synthase genes in non-Cannabis organisms - Journal of Cannabis Research"
date: 2022-07-26
categories:
- cannabinoid
author: Aryal, Socal Cannabis Science Research Group, Sccsrg, Los Angeles, Orellana, Debbie Figueroa, Bouie, Ca, Niranjan Aryal, Debbie Figueroa Orellana
tags: [Tetrahydrocannabinol,Tetrahydrocannabinolic acid synthase,Cannabidiol,Cannabinoid,Cannabis,Tetrahydrocannabinolic acid,Biology,Biochemistry,Biotechnology]
---


THC and CBD are the main cannabinoids that have gained most of the attention. THCA synthase is the enzyme responsible for the production of THCA; CBDA synthase is the enzyme responsible for the production of CBDA. A recent study on heterogenicity of THCAs and CBDAs in different strains of Cannabis sativa found the SNPs in these transcripts which could have caused the difference in chemical phenotype. The study also proposed CBDAs as the ancestral enzyme of both enzymes (Onofri et al. 2015). In this paper, we searched for THCAs and CBDAs in organisms other than Cannabis sativa.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-019-0008-7){:target="_blank" rel="noopener"}


