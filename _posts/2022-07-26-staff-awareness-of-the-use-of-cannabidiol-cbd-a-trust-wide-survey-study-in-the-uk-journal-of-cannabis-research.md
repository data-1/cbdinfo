---
layout: post
title: "Staff awareness of the use of cannabidiol (CBD): a trust-wide survey study in the UK - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Ukaegbu, East Wandsworth Community Mental Health Team, Springfield Hospital, South West London, St George S Mental Health Nhs Trust, London, Smith, Population Health Research Institute, St George S, University Of London
tags: [Cannabidiol,Medical cannabis,Psychosis,Tetrahydrocannabinol,General practitioner,Health care,Medicine,Clinical medicine,Health sciences,Health]
---


Most participants did not believe it had dangerous side effects, were not aware that it is available on the UK high street and were keen for it to be prescription only. And free text response by a nurse highlighted the need for more CBD research in mental health settings. Two examples would be statements to the effect: (1) CBD and cannabis have the same effects and (2) CBD can interact with other medications. So, it is reasonable to consider that concerns about interactions with other drugs are relevant with regard to CBD. Just like our study, these studies also noted a likely lack of knowledge on the part of the healthcare professionals that would benefit from education.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00104-x){:target="_blank" rel="noopener"}


