---
layout: post
title: "Effect of the Cannabinoid Receptor-1 antagonist SR141716A on human adipocyte inflammatory profile and differentiation"
date: 2024-05-13
categories:
- 
author: Ravi Murumalla, Geico, Groupe D'Etude Sur L'Inflammation Et L'Obésité Chronique, Université De La Réunion, Plateforme Cyroi, Avenue René Cassin, Saint-Denis Messag Cedex, Karima Bencharif, Lydie Gence, Amritendu Bhattacharya
tags: [Adiponectin,Adipose tissue,Endocannabinoid system,Adipocyte,Interleukin 6,Tumor necrosis factor,Toll-like receptor 4,CCL2,Biochemistry,Cell biology,Biology]
---


A total of 21 samples were obtained from 24 patients. In order to measure adiponectin secretion in LPS-stimulated mature adipocytes, as well as the effect of SR141716A on these cells, we treated adipocyte cells with 1 μg/mL LPS alone or with SR141716A (200 nM). No effect on the other endocannabinoid, anandamide, was observed (Figure ). SR141716A has thus its own effect. Our results show that SR141716A has no effect upon human adipocyte differentiation of SVF cells since we did not observe any change in lipid accumulation (Figure ), nor any variation in the expression of a key gene of adipocyte differentiation: A-FABP (Figure ).

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3253048/){:target="_blank" rel="noopener"}


