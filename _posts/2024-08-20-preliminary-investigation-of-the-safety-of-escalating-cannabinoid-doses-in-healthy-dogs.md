---
layout: post
title: "Preliminary Investigation of the Safety of Escalating Cannabinoid Doses in Healthy Dogs"
date: 2024-08-20
tags: [Cannabidiol,Tetrahydrocannabinol,Medical cannabis,Cannabinoid,Nabilone,Internal standard,Liquid chromatography–mass spectrometry,Dose (biochemistry)]
author: 
---


The dosing schedule of the placebo oils was such that two placebo doses were administered prior to the start of cannabinoid oil dosing to ascertain tolerability to increasing volumes. These blood collections also occurred 7 days following the final dose of the cannabinoid or placebo oils. Plasma levels of CBD, THC, and their metabolites were highly variable between dogs in the same treatment group receiving either the ninth dose of CBD oil or THC oil. Both CBD and THC were detected in plasma 1 week following administration of the final dose of either CBD oil (~62 mg/kg; n = 4) or THC oil (~49 mg/kg; n = 3), at levels ranging from 3.6 to 31.7 ng/mL CBD (CBD oil) and 2.8 to 4.6 ng/mL THC (THC oil). Of the three cannabinoid oil formulations tested, dose escalation of the CBD-predominant oil formulation was the most tolerated by dogs up to a maximum dose of 640.5 mg CBD (~62 mg CBD/kg) and only mild AEs were experienced.

[Visit Link](https://www.frontiersin.org/journals/veterinary-science/articles/10.3389/fvets.2020.00051/full){:target="_blank rel="noopener"}

