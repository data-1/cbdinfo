---
layout: post
title: "Acute objective and subjective intoxication effects of legal-market high potency THC-dominant versus CBD-dominant cannabis concentrates"
date: 2024-05-13
categories:
- 
author: Drennan, M. L., Department Of Psychology, Colorado State University, Fort Collins, Karoly, H. C., Institute Of Cognitive Science, University Of Colorado Boulder, Boulder
tags: [Cannabidiol,Cannabis (drug),Cannabinoid,Cannabis,Tetrahydrocannabinol,Cannabis sativa,Effects of cannabis,Tetrahydrocannabinolic acid,Psychoactive drugs,Health,Individual psychoactive drugs]
---


The results in the THC-dominant group in the present study are similar to findings from our prior study18, in which individuals using 70% or 90% THC concentrate products demonstrated somewhat higher THC blood levels immediately post-use (320 ng/mL) compared to immediate-post use plasma-THC in the THC-dominant concentrate group in the present analysis (238.58 ng/mL). Future studies should also explore whether adding specific levels of CBD to THC-based concentrates results in a reduction of negative effects in users of such high potency products, as seen in prior studies using different products or methods of administration28. The current study suggests that while paranoia is an undesirable effect, negative effects of THC concentrates such as this are relatively short-lived. This pattern may be interpreted as a harm reduction effect of a CBD-dominant cannabis, which was associated with some level of intoxication, but overall decreased negative affect and without the accompanying negative effects of higher levels of THC in our study of regular cannabis concentrate users. These results point to a further need to explore the harm-reducing potential of CBD, which could confer beneficial effects (e.g., on mood and drug reward) to individuals who use high amounts of THC, and perhaps especially for those who use highly potent THC concentrate products.

[Visit Link](https://www.nature.com/articles/s41598-021-01128-2#Sec21){:target="_blank" rel="noopener"}


