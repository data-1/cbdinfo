---
layout: post
title: "Cannabinoid and terpene data for 10.6084/m9.figshare.11778936"
date: 2024-09-23
tags: [Statistics,Data,Information science,Applied mathematics,Statistical methods]
author: Philippe Henry
institutions: 
doi: "https://doi.org/10.6084/m9.figshare.11780103"
---


dataset  posted on 2020-01-31, 18:35 authored by Philippe HenryPhilippe Henry  Terpene and cannabinoiud data for a subset of 120 samples from Las Vegas, Nevada.