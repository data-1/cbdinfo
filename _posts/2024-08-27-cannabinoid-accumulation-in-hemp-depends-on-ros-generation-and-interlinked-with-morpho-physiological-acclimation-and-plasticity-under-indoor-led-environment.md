---
layout: post
title: "Cannabinoid accumulation in hemp depends on ROS generation and interlinked with morpho-physiological acclimation and plasticity under indoor LED environment"
date: 2024-08-27
tags: [High-performance liquid chromatography,Photosynthesis,Reactive oxygen species,Tetrahydrocannabinolic acid,Cannabidiol]
author: Md Jahirul | Byeong Ryeol | Md Hafizur | Md Soyel | Eun Ju | Myeong-Hyeon | Jung-Dae | Mohammad Anwar | Young-Seok
---


2.2 Light treatment  After adjustment, the plants were subjected to treatment with 4 LED lights (Bisol LED light Co., Seoul, Korea). 2.4 Photosynthetic pigments analysis  For the determination of photosynthetic pigments, the freeze-dried (25 mg) leaves were extracted (10 mL of 80% acetone) and placed at room temperature for 15 min, then centrifuged at 4000 rpm for 10 min. Although, green light is considered less effective for plant growth since plant photosynthetic pigments have limited absorbance for these wavelengths. Plant J. Wavelengths of LED light affect the growth and cannabidiol content in Cannabis sativa l. Ind.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2022.984410/full){:target="_blank rel="noopener"}

