---
layout: post
title: "Motifs in Natural Products as Useful Scaffolds to Obtain Novel Benzo[d]imidazole-Based Cannabinoid Type 2 (CB2) Receptor Agonists"
date: 2024-09-02
tags: [Anandamide,2-Arachidonoylglycerol,Cannabinoid,Ligand binding assay,Thin-layer chromatography,Ligand (biochemistry),Cannabinoid receptor 2,Endocannabinoid system,Fatty-acid amide hydrolase 1,Biochemistry,Chemistry]
author: Analia Young Hwa Cho | Hery Chung | Javier Romero-Parra | Poulami Kumar | Marco Allarà | Alessia Ligresti | Carlos Gallardo-Garrido | Hernán Pessoa-Mahana | Mario Faúndez | Carlos David Pessoa-Mahana
---


The synthesis of 2-thioxybenzo[d]imidazole derivatives is described in and . (A) Superposition of compounds 5, 6, 16 with WIN55212-2; (B) binding interactions established by compound 5; (C) compound 6; and (D) compound 16 within the orthosteric pocket of CB2 receptor. The presence of an ethyl group at R1 yielded compounds with moderate to excellent affinity (compounds 3, 6, 9, 15, 17, 20, 23, and 26). CB1/CB2 Receptor Binding Assay  CB1 and CB2 receptor binding assays were performed exactly as previously described [35]. [Google Scholar] [CrossRef]  Mackie, K. Cannabinoid receptors: Where they are and what they do.

[Visit Link](https://www.mdpi.com/1422-0067/24/13/10918){:target="_blank rel="noopener"}

