---
layout: post
title: "Cannabinoid CB1 Receptors Are Localized in Striated Muscle Mitochondria and Regulate Mitochondrial Respiration"
date: 2024-08-18
tags: [Mitochondrion,Endocannabinoid system,Immunoprecipitation,Anandamide,Cell biology,Biology,Biochemistry]
author: Mendizabal-Zubiaga | Rodríguez De Fonseca
---


Mitochondrial Isolation from Heart and Mitochondrial Respiration  Mitochondria were isolated from the heart of the mice by differential centrifugation. Mitochondrial respiration was measured on heart mitochondria isolated from both wild-type and CB1-KO mice. In contrast, no significant difference was observed in the muscle expression of Sdha and Cox4i1 between CB1-WT and CB1-KO mice (Figure 4). Interestingly, most of the CB1 receptors in the gastrocnemius, the rectus abdominis and the myocardium are distributed in the mitochondria, in contrast to the brain where they are preferentially localized in neuronal membranes (Freund et al., 2003; Kano, 2014; Di Marzo et al., 2015). Mitochondrial CB1 receptors regulate neuronal energy metabolism.

[Visit Link](https://www.frontiersin.org/journals/physiology/articles/10.3389/fphys.2016.00476/full){:target="_blank rel="noopener"}

