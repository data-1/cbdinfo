---
layout: post
title: "Cannabidiol, among Other Cannabinoid Drugs, Modulates Prepulse Inhibition of Startle in the SHR Animal Model: Implications for Schizophrenia Pharmacotherapy"
date: 2024-08-18
tags: [Prepulse inhibition,Anandamide,Schizophrenia,Cannabinoid,Psychosis,Endocannabinoid system,Antipsychotic,Cannabinoid receptor 1,Clinical medicine,Mental disorders]
author: Fernanda F | Antonio W | Jaime E | José A | Vanessa C
---


The absence of AM 404 effects on the PPI of SHRs suggest that increasing anandamide levels is not sufficient to restore SHRs' PPI impairment. The results, thus, point to an antipsychotic profile of cannabidiol and to the use of this compound on the treatment of sensorimotor gating impairments seen in schizophrenia. In animals without PPI impairments, some authors describe that cannabidiol does not modify the PPI levels when administered acutely or chronically (Long et al., 2006; Gomes et al., 2015; Pedrazzi et al., 2015), while one study shows that it is able to disrupt PPI (Gururajan et al., 2011). Therefore, the effects of cannabidiol on PPI of WRs are in accordance with the antipsychotic profile suggested for this drug. Among the drugs that act on the endocannabinoid system, pre-clinical and the subsequent clinical data point to cannabidiol as the most promising compound for treating schizophrenia symptoms without inducing significant side effects.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2016.00303/full){:target="_blank rel="noopener"}

