---
layout: post
title: "Pharmacological insights emerging from the characterization of a large collection of synthetic cannabinoid receptor agonists designer drugs"
date: 2024-09-02
tags: [Pharmacology,Designer drug,Cannabinoid,Cannabinoid receptor 2,Neurochemistry,Medicinal chemistry,Drugs acting on the nervous system,Biochemistry,Psychoactive drugs,Drugs]
author: 
---


Synthetic cannabinoid receptor agonists (SCRAs) constitute the largest and most defiant group of abuse designer drugs. These new psychoactive substances (NPS), developed as unregulated alternatives to cannabis, have potent cannabimimetic effects and their use is usually associated with episodes of psychosis, seizures, dependence, organ toxicity and death. Due to their ever-changing structure, very limited or nil structural, pharmacological, and toxicological information is available to the scientific community and the law enforcement offices. Here we report the synthesis and pharmacological evaluation (binding and functional) of the largest and most diverse collection of enantiopure SCRAs published to date. Our results revealed novel SCRAs that could be (or may currently be) used as illegal psychoactive substances.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S0753332223007242){:target="_blank rel="noopener"}

