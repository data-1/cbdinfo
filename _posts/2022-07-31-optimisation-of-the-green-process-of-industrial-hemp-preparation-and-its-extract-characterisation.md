---
layout: post
title: "Optimisation of the Green Process of Industrial Hemp—Preparation and Its Extract Characterisation"
date: 2022-07-31
categories:
- Cannabinoid
- cannabis
- hemp
author: Žitek, Kotnik, Makoter, Postružnik, Knez, Knez Marevci, Taja Žitek, Petra Kotnik, Teo Makoter, Vesna Postružnik
tags: [Cannabis,Cannabidiol,Cannabinoid,Tetrahydrocannabinol,Hemp,Chemistry]
---


The hemp plant consists of the woody part (44% of the plant weight), fibres (24%), seeds (11%) and other components such as flowers, leaves and dust (21%) [ 1 3 ]. Since each part of the hemp has its own potential for a certain product, the main thing in planning is the pretreatment of the material (for example, the screening method), which separates plant parts. Certain known forms of leaves also constitute a part of the plant and are located above the cola from which the flower emerges [ 4 ]. Nonacidic forms of cannabinoids (CBD, THC, CBG, etc.) The extraction of raw material results in the acidic form of cannabinoid compounds and decarboxylation is required to obtain nonacidic forms [ 22 ].

<hr>[Visit Link](https://www.mdpi.com/2223-7747/11/13/1749/htm){:target="_blank" rel="noopener"}


