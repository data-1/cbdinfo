---
layout: post
title: "Too Dense or Not Too Dense: Higher Planting Density Reduces Cannabinoid Uniformity but Increases Yield/Area in Drug-Type Medical Cannabis"
date: 2024-08-27
tags: [Cannabis,Cannabinoid,Leaf,Tetrahydrocannabinolic acid,Agriculture,Stomatal conductance,Stoma,Analysis of variance,Δ-8-Tetrahydrocannabinol,Photosynthesis,Medical cannabis,Plant]
author: 
---


Inflorescence yield production per cultivation area (gDW/m2) was higher (by 28–78%) in the higher density treatment than in the lower density treatments in the control, “BBLR” and “defoliation” treatments, but was not significantly affected by plant density in the “Pruning” treatment (Table 1). Under all architecture altering treatments, membrane leakage was higher under higher density (Figure 7A), and under the lower density all plant architecture treatments showed a lower stress response than the “Control.” The plants WUE (Figure 7B) was calculated using the CO2 assimilation rate, and it presents three different responses according to the plant architecture treatments: higher density increased WUE in the “BBLR” treatments, reduced WUE in the “Pruning” treatment but had no effect in both the “Control” and “Defoliation” treatments. In all plant architecture treatments evaluated in this study, inflorescence biomass yield production per m2 was higher in the higher density treatment compared with the lower density treatment, except for the “Pruning” treatment that was not significantly affected by plant density (Table 1). In the present study, cannabinoid concentrations in the plant, calculated as the plant average concentration (Supplementary Figure 2) were mostly reduced (by up to 24%) or not affected by the increase in density (excluding “BBLR” CBDA and CBDVA, which were increased by up to 18%). The results indicated that an increase in plant density decreased inflorescence yield per plant but increased yield per area (except in the pruning treatment that was not affected significantly) thus supporting the hypotheses.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2022.713481/full){:target="_blank rel="noopener"}

