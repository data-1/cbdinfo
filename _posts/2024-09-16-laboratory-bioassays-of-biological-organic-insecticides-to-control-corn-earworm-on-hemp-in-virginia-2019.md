---
layout: post
title: "Laboratory Bioassays of Biological/Organic Insecticides to Control Corn Earworm on Hemp in Virginia, 2019"
date: 2024-09-16
tags: [Bacillus thuringiensis,Helicoverpa zea]
author: Kadie E Britt | Thomas P. Kuhar
institutions: Virginia Tech
doi: "https://doi.org/10.1093/amt/tsaa102"
---


Two separate bioassays were conducted in fall 2019 to evaluate the effects of biological/organic insecticide products on CEW in hemp. On 16 Sep 2019, hemp seed heads (‘Felina-32’) were collected from field plots at Kentland, brought to the laboratory, and cut into ~9 cm3 sections. aizawai + kurstaki), Pyganic (pyrethrins), Entrust (spinosad), and an untreated check (Table 2). Javelin and DiPel resulted in a significantly higher mortality (32.5% and 35%, respectively) than Gemstar (10%) and the untreated check (15%). It should be noted that resistance to pyrethroid/pyrethrin insecticides is also observed widely in Virginia CEW populations, and thus, although not tested in Bioassay 1, Pyganic would not be expected to result in 100% mortality of field-collected CEW larvae.1  © The Author(s) 2020.