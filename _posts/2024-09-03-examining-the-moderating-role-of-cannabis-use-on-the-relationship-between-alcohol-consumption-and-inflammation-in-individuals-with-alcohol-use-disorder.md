---
layout: post
title: "Examining the moderating role of cannabis use on the relationship between alcohol consumption and inflammation in individuals with alcohol use disorder"
date: 2024-09-03
tags: [Interleukin 6,Toll-like receptor 4,Inflammation,Toll-like receptor,Pattern recognition receptor,Cannabinoid,Alcohol (drug),Medical specialties,Clinical medicine]
author: Erica N. Grodin | Kaitlin R. McManus | Lara A. Ray
---


Keywords: alcohol and cannabis co‐use, IL‐6, inflammation  The relationship between circulating IL‐6 levels, alcohol consumption, and cannabis use was assessed in individuals with an alcohol use disorder. Cannabis use moderated the relationship between alcohol use and log IL‐6 levels, such that cannabis use mitigated the association between alcohol use and increases in peripheral inflammation. Examining the moderating role of cannabis use on the relationship between alcohol consumption and inflammation in individuals with alcohol use disorder. Alcohol. Alcohol.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11294675/){:target="_blank rel="noopener"}

