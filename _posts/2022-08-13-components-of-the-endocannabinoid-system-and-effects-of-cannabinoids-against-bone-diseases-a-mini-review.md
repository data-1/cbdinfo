---
layout: post
title: "Components of the Endocannabinoid System and Effects of Cannabinoids Against Bone Diseases: A Mini-Review"
date: 2022-08-13
categories:
- Cannabinoid
- Endocannabinoid
author: Xin, Department Of Oral, Maxillofacial Surgery, Tang, Pan, Zhang, Cristoforo Pomara, Paolo Tucci, Chawon Yun
tags: [Cannabinoid receptor type 2,Cannabinoid receptor,Cannabinoid,Inflammation,Bone,2-Arachidonoylglycerol,TRPV1,Tumor necrosis factor,Cannabinoid receptor type 1,Arthritis,Medical cannabis,Cannabidiol,Inflammatory cytokine,Osteoporosis,Tetrahydrocannabinol,Anandamide,Bone fracture,Cancer,GPR55,Medical specialties,Clinical medicine,Cell biology,Biochemistry]
---


Results: Many studies have demonstrated that endocannabinoids (eCBs) and cannabinoid receptors (CBRs) are expressed in bone and synovial tissues, playing important roles in bone metabolism. The role of cannabinoid receptor ligands in regulating tumor cells and bone diseases in vitro and in vivo are shown in Table 2. The role of cannabinoid receptor ligands in regulating tumor cells and bone diseases in vitro and in vivo. Bone 46 (4), 1089–1099. Bone 44 (3), 476–484.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2021.793750/full){:target="_blank" rel="noopener"}


