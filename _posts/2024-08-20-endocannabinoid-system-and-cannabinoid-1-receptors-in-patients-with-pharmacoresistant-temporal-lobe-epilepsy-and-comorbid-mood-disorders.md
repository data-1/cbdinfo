---
layout: post
title: "Endocannabinoid System and Cannabinoid 1 Receptors in Patients With Pharmacoresistant Temporal Lobe Epilepsy and Comorbid Mood Disorders"
date: 2024-08-20
tags: [Cannabinoid receptor 1,Neurotransmission,Epilepsy,Postictal state,Electroencephalography,Hippocampus,Cannabinoid,Mass spectrometry,Neuroscience]
author: Guevara-Guzmán | Alonso-Vanegas | San-Juan | Martínez-Juárez | Castañeda-Cabral | José Luis | Carmona-Cruz
---


Endocannabinoids in the Hippocampus and the Temporal Neocortex of Patients With Pharmacoresistant Mesial Temporal Lobe Epilepsy  The tissue levels of endocannabinoids and OEA in the hippocampus and the temporal neocortex of control subjects showed the following values: AEA, 26.8 ± 2.1 and 17.8 ± 1.8 fmol/mg, respectively; 2-AG, 4378 ± 941 and 2620 ± 423 pmol/mg, respectively; and OEA, 208.9 ± 29 and 127 ± 18 fmol/mg, respectively. TABLE 1  Gi/o Protein Activation by CB1Rs in Patients With Mesial Temporal Lobe Epilepsy  In autopsy samples, the binding assay in the presence of WIN 55212-2 revealed a maximal incorporation of [35S]GTPγS (Emax) of 25.5 and 29.6% (hippocampus and neocortex, respectively), with EC50 values of 704 ± 127 and 378 ± 64 nM (hippocampus and temporal neocortex, respectively). Discussion  The present study revealed a higher CB1R-induced Gi/o protein activation and significant changes in the tissue content of AEA, OEA, and 2-AG in the epileptic hippocampus and the temporal neocortex of patients with pharmacoresistant MTLE. Concerning endocannabinoid tissue levels, the results of the present study revealed that patients with MTLE present opposite changes, i.e., high levels of AEA and OEA and decreased levels of 2-AG. An important limitation of the present study is the lack of correlation between the results obtained and the conditions that may modify the endocannabinoid system in both autopsies and patients with MTLE.

[Visit Link](https://www.frontiersin.org/journals/behavioral-neuroscience/articles/10.3389/fnbeh.2020.00052/full){:target="_blank rel="noopener"}

