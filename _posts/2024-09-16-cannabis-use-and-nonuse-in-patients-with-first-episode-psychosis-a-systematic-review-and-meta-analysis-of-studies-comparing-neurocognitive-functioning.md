---
layout: post
title: "Cannabis use and nonuse in patients with first-episode psychosis: A systematic review and meta-analysis of studies comparing neurocognitive functioning"
date: 2024-09-16
tags: [Variance,Cannabis (drug),Antipsychotic,Psychosis,Mental disorder,Working memory,Wechsler Adult Intelligence Scale,Effect size]
author: Teresa Sánchez‐Gutiérrez | Belén Fernández‐Castilla | Sara Barbeito | Ana González‐Pinto | Juan Antonio Becerra‐García | Ana Calvo
institutions: Universidad Internacional De La Rioja
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/8057396"
---


However, few have examined the effects of cannabis use on the cognitive symptoms of psychosis (i.e., neurocognitive functioning) in patients with first-episode psychosis (FEP). The lack of significant differences in neurocognitive functioning between CU and NU with FEP in this study could be explained by the fact that vulnerable patients with better cognitive function can develop psychosis owing to cannabis use; this subsample can mask the deleterious effect of cannabis on cognition in other patient groups. [PMC free article] [PubMed] [Google Scholar] [Ref list]  Hedges LV. [Google Scholar] [Ref list]  Grace AA. [PubMed] [Google Scholar] [Ref list]