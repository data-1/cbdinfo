---
layout: post
title: "A Humanized CB1R Yeast Biosensor Enables Facile Screening of Cannabinoid Compounds"
date: 2024-09-02
tags: [Cannabinoid receptor 1,Cannabinoid,G protein-coupled receptor,EC50,Cannabinoid receptor 2,Signal transduction,Biochemistry,Biology,Cell biology,Neurochemistry]
author: Colleen J. Mulvihill | Joshua D. Lutgens | Jimmy D. Gollihar | Petra Bachanová | Caitlin Tramont | Edward M. Marcotte | Andrew D. Ellington | Elizabeth C. Gardner | Colleen J | Joshua D
---


Synthetic cannabinoids have been developed to elicit responses by cannabinoid receptors. A compound library of over 300 synthetic cannabinoids was screened against the strain. (a) Screen of CB1R with synthetic cannabinoid compounds at 1 µM and 10 nM concentrations. Yeast Transformations  The yeast background strain was BY4741 (See yeast strain list). Yeast 2000, 16, 11–22.

[Visit Link](https://www.mdpi.com/1422-0067/25/11/6060){:target="_blank rel="noopener"}

