---
layout: post
title: "Synthetic cannabinoid WIN 55,212–2 inhibits growth and induces cell death of oral and pancreatic stem-like/poorly differentiated tumor cells"
date: 2024-08-27
tags: [Natural killer cell,PD-L1,Cancer stem cell,Biochemistry,Immunology,Cell biology,Mammal genes,Immune system,Human biology,Biology,Medical specialties,Human genes,Cell signaling,Signal transduction,Molecular biology,Neurochemistry,Cell communication]
author: 
---


We report here that synthetic cannabinoid WIN 55,212–2 inhibits tumor cell proliferation and induces cell death of oral and pancreatic tumor cells, and the effect is much more pronounced on stem-like/poorly differentiated OSCSCs and MP2 cells when compared to well-differentiated OSCCs, and PL-12 tumor cells. In addition, WIN 55,212-2 decreases cell surface expression of CD44, CD54, MHC class I and PD-L1 on oral and pancreatic tumor cells with the exception of PD-L1 expression on well-differentiated PL-12 pancreatic tumor cells which exhibits an increase in the expression rather than a decrease. Overall, we demonstrate that WIN 55,212-2 has an increased targeting activity against cancer stem cells/poorly differentiated oral and pancreatic tumor cells when compared to well-differentiated tumor cells, and furthermore, such differences in function do not correlate with the levels of CB1 and CB2 receptor expression on tumor cells, suggesting it's function either through post-receptor mediated activation and/or yet-to-be identified novel receptors. Intraperitoneal (IP) delivery of WIN 55-212-2 in humanized BLT mice is found to impart an activating potential for NK cells demonstrating increased NK cell mediated cytotoxicity and secretion of IFN-γ in our preliminary experiments. These results not only suggest a direct targeting of CSCs/poorly differentiated tumors by WIN 55-212-2 but also by indirect targeting of such tumors through the activation and increased functions of NK cells.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S266739402200017X){:target="_blank rel="noopener"}

