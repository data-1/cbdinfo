---
layout: post
title: "Interaction between Cannabinoid System and Toll-Like Receptors Controls Inflammation"
date: 2024-08-12
tags: [Cannabinoid receptor 2,Toll-like receptor,Cannabinoid receptor,Cannabinoid,NF-κB,Endocannabinoid system,Monocyte,Interleukin 10,Anandamide,Cell signaling,Interleukin-1 family,2-Arachidonoylglycerol,Receptor (biochemistry),Macrophage,Toll-like receptor 4,Biochemistry,Biology,Cell biology,Neurochemistry,Signal transduction,Cell communication,Medical specialties]
author: Fischer-Stenger K | Pettit D. A. D | Cabral G. A | Choi D.-K | More S. V | Park J.-Y | Kim B.-W | Kumar H | Lim H.-W | Kang S.-M
---


Cannabinoid receptors and their ligands: Beyond CB1 and CB2, Pharmacological Reviews. 40 Howlett A. C., Cannabinoid receptor signaling, Handbook of Experimental Pharmacology. 98 Nong L., Newton C., Cheng Q., Friedman H., Roth M. D., and Klein T. W., Altered cannabinoid receptor mRNA expression in peripheral blood mononuclear cells from marijuana smokers, Journal of Neuroimmunology. 114 Smith S. R., Terminelli C., and Denhardt G., Effects of cannabinoid receptor agonist and antagonist ligands on production of inflammatory cytokines and anti-inflammatory interleukin-10 in endotoxemic mice, Journal of Pharmacology and Experimental Therapeutics. 124 Waksman Y., Olson J. M., Carlisle S. J., and Cabral G. A., The central cannabinoid receptor CB1 mediates inhibition of nitric oxide production by rat microglial cells, Journal of Pharmacology and Experimental Therapeutics.

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1155/2016/5831315){:target="_blank rel="noopener"}

