---
layout: post
title: "Targeting Cannabinoid Signaling in the Immune System: “High”-ly Exciting Questions, Possibilities, and Challenges"
date: 2024-08-18
tags: [Cannabinoid receptor 2,Endocannabinoid system,Cannabinoid,Inflammation,Experimental autoimmune encephalomyelitis,T helper cell,Cannabis (drug),2-Arachidonoylglycerol,Cannabinoid receptor 1,Adenosine A2A receptor,Fatty-acid amide hydrolase 1,Cannabinoid receptor,Anandamide,Tetrahydrocannabinol,Immune system,Rimonabant,Receptor (biochemistry),Infection,Transplant rejection,Regulatory T cell,Cell signaling,Autoimmune disease,Graft-versus-host disease,Medical specialties,Immunology,Clinical medicine,Cell biology,Biology]
author: 
---


On the other hand, FAAH−/− mice (having elevated eCB levels) showed reduced allergic responses (29), further arguing for the concept that elevation of the eCB tone (e.g., by abrogating degradation of the eCBs or by directly activating cannabinoid receptors) usually leads to potent anti inflammatory/antiallergic actions [extensively reviewed in, e.g., Ref. Indeed, in an acute murine model of GVHD, THC (20 mg/kg i.p.) Indeed, AEA levels of the peripheral lymphocytes of MS patients was found to be elevated compared to healthy individuals, suggesting the development of a complex dysregulation in the ECS of MS patients (82, 83). According to another EAE study, in which CBD was applied after the development of the disease, CBD (10 mg/kg mouse, i.p.) reduced host immune response, and prevented cognitive impairments (152).

[Visit Link](https://www.frontiersin.org/journals/immunology/articles/10.3389/fimmu.2017.01487/full){:target="_blank rel="noopener"}

