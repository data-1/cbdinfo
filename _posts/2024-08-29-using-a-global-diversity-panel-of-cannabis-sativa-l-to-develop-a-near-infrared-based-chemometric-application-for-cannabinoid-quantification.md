---
layout: post
title: "Using a global diversity panel of Cannabis sativa L. to develop a near InfraRed-based chemometric application for cannabinoid quantification"
date: 2024-08-29
tags: [Detection limit,Cannabinoid,Near-infrared spectroscopy,Cannabis,High-performance liquid chromatography,Cannabis sativa,Tetrahydrocannabinol,Chromatography,Tetrahydrocannabinolic acid,Absorption spectroscopy]
author: Gloerfelt-Tarp | Amitha K | William M
---


The C5-alkyl side chain cannabinoids Δ9-THC and Δ9-THCA were detected in every sample, CBDA and CBGA were reported in 98% of samples, CBD in 96% of samples, followed by CBN in 95%, CBG in 88% and CBC in 87% of all samples tested. For example, for accession SC1900022, eight individual samples were distributed across three clusters (3, 5 and 7; Supplementary Information Table S3). While it was reasonable to assume that models for CBDVA and CBDV failed due to low concentrations and lack of range of training data, other minor cannabinoids with smaller ranges and lower average concentrations, such as CBC and CBG, produced models of relatively good predictive power (r2 above 0.7), but had some of the highest RMSE % of range values for HV with CBG at 7.54 RMSE % of range. This particular sample set was limited to low concentration ranges for the majority of the target cannabinoids. Data analysis and calculations  HPLC data analysis was performed using Agilent OpenLab Chemstation CDS processing software (version 2.5).

[Visit Link](https://www.nature.com/articles/s41598-023-29148-0){:target="_blank rel="noopener"}

