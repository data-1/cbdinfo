---
layout: post
title: "Profiling Cannabinoid Contents and Expression Levels of Corresponding Biosynthetic Genes in Commercial Cannabis (Cannabis sativa L.) Cultivars"
date: 2024-08-27
tags: [Cannabinoid,Real-time polymerase chain reaction,Tetrahydrocannabinolic acid synthase,Cannabis,Tetrahydrocannabinol,High-performance liquid chromatography,Reverse transcription polymerase chain reaction,Cannabidiol,Cannabinol]
author: Ae Lim Kim | Young Jae Yun | Hyong Woo Choi | Chang-Hee Hong | Hyun Joo Shim | Jeong Hwan Lee | Young-Cheon Kim | Ae Lim | Young Jae | Hyong Woo
---


To quantify the content of neutral cannabinoids in Cannabis plants, new heating decarboxylation conditions were adopted using reference mixtures of acidic and neutral cannabinoids, and the decarboxylation of acidic cannabinoids and conversion of their corresponding neutral cannabinoids were estimated. Evaluation of New Decarboxylation Conditions Using Cannabis Plant Extracts  To elucidate the effects of decarboxylation treatment on Cannabis plant extracts, the extracts from Cannabis plants were heated under the same conditions as those used for the decarboxylation of standard acidic cannabinoids ( b). ; Kim, Y.-C. Profiling Cannabinoid Contents and Expression Levels of Corresponding Biosynthetic Genes in Commercial Cannabis (Cannabis sativa L.) Cultivars. Plants 2022, 11, 3088. https://doi.org/10.3390/plants11223088  AMA Style  Kim AL, Yun YJ, Choi HW, Hong C-H, Shim HJ, Lee JH, Kim Y-C. Profiling Cannabinoid Contents and Expression Levels of Corresponding Biosynthetic Genes in Commercial Cannabis (Cannabis sativa L.) Cultivars. Profiling Cannabinoid Contents and Expression Levels of Corresponding Biosynthetic Genes in Commercial Cannabis (Cannabis sativa L.) Cultivars Plants 11, no.

[Visit Link](https://www.mdpi.com/2223-7747/11/22/3088){:target="_blank rel="noopener"}

