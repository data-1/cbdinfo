---
layout: post
title: "Cannabinoids in Medicine: Cancer, Immunity, and Microbial Diseases"
date: 2022-08-27
categories:
- Cannabinoid
author: Śledziński, Nowak-Terpiłowska, Zeyland, Paweł Śledziński, Agnieszka Nowak-Terpiłowska, Joanna Zeyland
tags: [Cannabis,Tetrahydrocannabinol,Cannabinoid receptor,Cannabinoid,Inflammation,Immune system,HER2neu,Cytokine,DNA damage-inducible transcript 3,Cannabis (drug),Virus,Apoptosis,T helper cell,Cannabinoid receptor type 2,Inflammatory bowel disease,Interleukin 10,Chemical synapse,Cannabidiol,Medicine,Neurochemistry,Cell biology,Biochemistry,Clinical medicine,Health,Diseases and disorders,Medical specialties]
---


The CBR2 is mainly found on the surfaces of immune cell types, though its expression has also been observed in the CNS [ 31 ]. THC, CBD, and R(+)-methanandamide have been shown to stimulate ICAM-1 expression in lung cancer cell lines A549, H460, and metastatic lung cancer cells from a patient. Secondary metabolites may affect THC affinity for the CB1 receptor and interact with neurotransmitter receptors, which suggests an impact on the psychoactive effects of cannabinoids. The THC concentration used in that study (100–300 nM) was like those found in the blood after cannabis smoking or oral THC administration [ 48 ]. On the other hand, it may lead to a decrease in the host immune response, promoting infection and disease progression.

<hr>

[Visit Link](https://www.mdpi.com/1422-0067/22/1/263/htm){:target="_blank" rel="noopener"}


