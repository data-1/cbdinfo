---
layout: post
title: "Comparing CB1 receptor GIRK channel responses to receptor internalization using a kinetic imaging assay"
date: 2024-09-03
tags: [Cannabinoid receptor 1,Cannabinoid receptor,G protein-coupled receptor kinase,Cell signaling,Cannabinoid,Endocannabinoid system,Receptor antagonist,Receptor (biochemistry),Neurochemistry,Cell biology,Biochemistry]
author: Haley K. Andersen | Duncan G. Vardakas | Julie A. Lamothe | Tannis E. A. Perault | Kenneth B. Walsh | Robert B. Laprairie
---


Keywords: Cannabinoid receptor, Cannabinoid, G protein-coupled receptor, Receptor trafficking, Receptor signaling, Real-time assay  Subject terms: Pharmacology, Molecular neuroscience  Introduction  Cannabinoid receptors have gained interest due to their potential in a range of therapeutic applications, such as anxiety, depression, obesity, pain, and neurodegenerative disorders1,2. CB1R imaging  CB1R internalization was recorded in 96-well plates by imaging AtT20-SEPCB1R expression on the cell surface. Andersen, H. K. & Walsh, K. B. Molecular signaling of synthetic cannabinoids: Comparison of CB1 receptor and TRPV1 channel activation. Hsieh, C. et al. Internalization and recycling of the CB1 cannabinoid receptor. Hsieh, C. et al. Internalization and recycling of the CB1 cannabinoid receptor.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11306342/){:target="_blank rel="noopener"}

