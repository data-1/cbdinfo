---
layout: post
title: "Different Fertility Approaches in Organic Hemp (Cannabis sativa L.) Production Alter Floral Biomass Yield but Not CBD:THC Ratio"
date: 2022-07-31
categories:
- cannabis
- CBD
- hemp
- THC
author: Bruce, Connelly, Ellison, Dylan Bruce, Grace Connelly
tags: [Cannabidiol,Cannabis,Hemp,Cannabinoid,Cannabis sativa,Soil fertility,Tetrahydrocannabinol,Fertilizer,Analysis of variance,Errors and residuals]
---


Based on prior research, we hypothesize that fertility treatments would not significantly change cannabinoid ratio, and that higher N fertility regimes would increase biomass yield. There were five plants per plot, and the two edge plants were treated as guard plants to provide an additional buffer between fertility treatments. Accordingly, there was no clear effect on CBD:THC ratio from fertilizer treatment (> 0.1, F = 2.0), or interactions between fertilizer with year (> 0.2, F = 1.49) or sampling date (> 0.9, F = 0.47). Overall, studies informing best practices for fertilizer application in hemp production for cannabinoids have continued to be sparse. Our results show that additional organic fertilizer with higher nitrogen levels will not cause lower CBD to THC ratios.

<hr>[Visit Link](https://www.mdpi.com/2071-1050/14/10/6222/htm){:target="_blank" rel="noopener"}


