---
layout: post
title: "Cannabinoid Drugs-Related Neuroprotection as a Potential Therapeutic Tool Against Chemotherapy-Induced Cognitive Impairment"
date: 2024-08-20
tags: [Endocannabinoid system,Post-chemotherapy cognitive impairment,Reactive oxygen species,Cannabinoid receptor 2,Cannabinoid,Neuroinflammation,Chemotherapy,Fatty-acid amide hydrolase 1,Cannabinoid receptor,Cannabinoid receptor 1,Cancer,Angiogenesis,Neurodegenerative disease,Clinical medicine,Medical specialties]
author: Llorente-Berzal
---


Several studies have observed that the activation of CB2 receptors decrease neuroinflammation in animal models of AD (Uddin et al., 2020). However, since PF3845 not only induced an increase on AEA levels but also 2-AG levels (Tchantchou et al., 2014), both endocannabinoid ligands could be involved in this neuroprotective activity observed in the ipsilateral brain. This effect was attenuated in CB1−/− mice, suggesting the implication of CB1 receptors in the oxidative stress induced by doxorubicin (Mukhopadhyay et al., 2010a). In the neurotoxic animal model of AD induced by the intracerebroventricular administration of β-amyloid peptide in mice, chronic administration of CBD reduced the hippocampal expression of iNOS and the subsequent NO release (Esposito et al., 2007) and prevented the spatial memory deficits usually observed in this animal model (Martín-Moreno et al., 2011). However, to the best of our knowledge, there is no clinical evidence of improvement in the cognitive alterations associated with these neurodegenerative disorders yet.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2021.734613/full){:target="_blank rel="noopener"}

