---
layout: post
title: "Anti-Inflammatory and Pro-Autophagy Effects of the Cannabinoid Receptor CB2R: Possibility of Modulation in Type 1 Diabetes"
date: 2024-08-21
tags: [Autophagy,Cannabinoid receptor 2,Macrophage,T cell,T helper cell,Type 1 diabetes,Inflammation,Beta cell,MHC class I,T helper 17 cell,Cannabinoid receptor 1,MHC class II,Antigen presentation,TFEB,Inflammasome,Human leukocyte antigen,Pancreatic islets,Tumor necrosis factor,Reactive oxygen species,Interleukin 1 beta,Antigen,Antigen-presenting cell,Inflammatory cytokine,Adipose tissue,Major histocompatibility complex,Secretion,Antigen processing,Lysosome,Vesicle (biology and chemistry),Insulin,Immune system,Adipocyte,Immune tolerance,Autoimmunity,Immunology,Molecular biology,Biochemistry,Medical specialties,Biology,Cell biology]
author: Qing-Rong | Kanikkai Raja | O’Connell | Jennifer F | Josephine M
---


Although CB2R is enriched in the immune system, we observed CB2R expression in microglia, as might be expected, and neurons in different mouse brain regions (Liu et al., 2020a). We found that the CB2Ra transcript levels are about 8-fold higher than that of CB2Rb in human islets (Figure 2B), indicating that the upstream promoter is more active in cell types outside of immune system (Zhang et al., 2017b). Selective activation of CB2R with HU308 had a cardio-protective effect against diabetic cardiomyopathy and protected the cardiomyocytes by promoting autophagy via the AMPK-mTOR-p70S6K signaling pathway when maintained under the stress of high glucose (Wu et al., 2018). HU-308 (a CB2R agonist) promotes autophagy, inhibits the NLRP3 inflammasome, and protects mice from autoimmune encephalomyelitis (Shao et al., 2014). CB1R might regulate MHC-I in β-cells and CB2R regulate MHC-II in immune cells since CB1R and not CB2R is found in β-cells (Benner et al., 2014).

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2021.809965/full){:target="_blank rel="noopener"}

