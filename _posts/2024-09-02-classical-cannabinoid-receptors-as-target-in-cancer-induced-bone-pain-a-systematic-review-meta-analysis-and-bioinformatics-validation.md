---
layout: post
title: "Classical cannabinoid receptors as target in cancer-induced bone pain: a systematic review, meta-analysis and bioinformatics validation"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Cannabinoid receptor 1,Cannabinoid,Anandamide,Neurochemistry]
author: Joshua S | Cornelia K | Aymen I
---


Inclusion and exclusion criteria  Studies that used animal models to assess the effects of pharmacological and/or genetic manipulation of the classical cannabinoid CB1 and/or CB2 receptors using standard rodent cancer models of mechanical allodynia, thermal hyperalgesia, and spontaneous and ambulatory behaviour assays6,73 (Table 1) and human studies that measured the effect of CB1/2 natural and synthetic ligands on intensity of cancer-induced pain2,3 were included (Table S1). Effect parameters  We included studies assessing the effects of natural and synthetic verified ligands of CB1/2 on a panel of standard experimental outcomes in animal (in vivo, Tables S2) models of cancer, and cancer patients (human, Tables S3). All of the included articles (29 animal and 35 human) were published from 1974 to 2022, and reported cancer-induced bone pain in rodents, and pain intensity in humans. Meta-analysis of outcomes  Bone pain in animal models of cancer  The present meta-analysis and systematic review identified 29 articles that examined the pharmacological effects of endocannabinoids (2 articles), synthetic agonist (22 articles) and antagonist/inverse agonists (5 articles) of the classical CB1/2 receptors on mechanical allodynia (25 articles), thermal hyperalgesia (9 articles) and spontaneous pain (6 articles) in adult males (23 articles) and females (6 articles) rodents (mice, 16 and rats, 13 articles) (Tables 1, 2 and S2). Effectiveness of cannabinoid ligands in rodents  To evaluate the effectiveness of cannabinoids assessed in the meta-analysis, we compared the (std) MD of different interventions:  When different interventions applied to male mice and paw withdraw frequency (%) was measured as an outcome of pain, the effectiveness order is: CP 55,940 (− 52.38 [− 62.03, − 42.73]) > 2-AG (− 32.09 [− 47.38, − 16.80]) > ACPA (− 26.5 [− 32.53, − 20.47]) > AM1241 (− 24.93 [− 40.59, − 9.27]) > AEA (− 19.26 [− 32.64, − 5.88]) > WIN 55,212-2 (− 10.2 [− 21.44, 1.04]).

[Visit Link](https://www.nature.com/articles/s41598-024-56220-0){:target="_blank rel="noopener"}

