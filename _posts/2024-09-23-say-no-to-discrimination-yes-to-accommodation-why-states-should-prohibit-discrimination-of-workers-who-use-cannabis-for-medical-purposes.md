---
layout: post
title: "Say 'No' to Discrimination, 'Yes' to Accommodation: Why States Should Prohibit Discrimination of Workers Who Use Cannabis for Medical Purposes"
date: 2024-09-23
tags: [Employment,Cannabis (drug),Medical cannabis,Law,At-will employment]
author: Anne Marie Lofaso | Lakyn D. Cecil
institutions: 
doi: "https://doi.org/10.2139/ssrn.3697455"
---


Part I briefly traces the legal regulation of cannabis from an unregulated medicine known as cannabis to a highly regulated illicit substance known as marijuana under the Controlled Substances Act. Our travail through this history reveals, unsurprisingly, an increasing demonization of cannabis throughout the twentieth century. American society’s negative perception of cannabis began to yield, however, as scientific evidence of cannabis’s healing capacity gained popularity. Part II surveys the role of employment law in protecting employees who use cannabis for medical purposes. Applying the knowledge gained from Part II, we collated what we believed to be the best language from the statutes of those two states and rewrote Colorado’s constitution in a manner that would account for employees’ interests and employer’s legitimate concerns.