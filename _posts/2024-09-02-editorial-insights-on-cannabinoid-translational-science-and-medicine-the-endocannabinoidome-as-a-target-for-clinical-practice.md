---
layout: post
title: "Editorial: Insights on cannabinoid translational science and medicine: the endocannabinoidome as a target for clinical practice"
date: 2024-09-02
tags: [Cannabinoid,Anandamide,Endocannabinoid system,Cannabinoid receptor,Tetrahydrocannabinol,Cannabinol,Neurochemistry,Biochemistry,Cannabinoids,Neurophysiology,Cannabis research]
author: Raquel Maria Pereira | Fabio Arturo
---


In mammals and other vertebrates, the endocannabinoid system (ECS) is in a simplicistic manner formed by endocannabinoids, two cannabinoid receptors and large set of metabolic enzymes deputed to the synthesis and degradation of endocannabinoids. The identification of structure of the main phytocannabinoid present in Cannabis sativa, Δ9-tetrahydrocannabinol (THC) and cannabidiol (CBD), led to the discovery of the cannabinoid receptors first, and then also using synthetic cannabinoids, to the deciphering of the entire system (Cristino et al., 2020; Maccarrone et al., 2023). In the following years, other endocannabinoids and congeners were isolated, however the biological activity and pharmacological proprieties of AEA and 2-AG remain the most studied (Fezza et al., 2014). Despite its medicinal potential the use of Cannabis was prohibited in several countries in the 20th century. Funding  The author(s) declare that no financial support was received for the research, authorship, and/or publication of this article.

[Visit Link](https://www.frontiersin.org/journals/neuroscience/articles/10.3389/fnins.2024.1432892/full){:target="_blank rel="noopener"}

