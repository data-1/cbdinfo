---
layout: post
title: "Cornflower Extract and Its Active Components Alleviate Dexamethasone-Induced Muscle Wasting by Targeting Cannabinoid Receptors and Modulating Gut Microbiota"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Real-time polymerase chain reaction,Muscle atrophy,Reverse transcription polymerase chain reaction,Sarcopenia,Protein kinase B,Reactive oxygen species,Mitochondrion,Biochemistry,Biology,Cell biology]
author: Ngoc Bao Nguyen | Tam Thi Le | Suk Woo Kang | Kwang Hyun Cha | Sowoon Choi | Hye-Young Youn | Sang Hoon Jung | Myungsuk Kim | Ngoc Bao | Tam Thi
---


Cell cytotoxicity was assessed for eight of the isolated compounds ( B). As shown in , among the eight compounds isolated from CC, 3-O-caffeoylquinic acid (compound 2, −12.3 kcal/mol for CB1, −14.2 kcal/mol for CB2), graveobioside A (compound 5, −17.7 kcal/mol for CB1, −19.1 kcal/mol for CB2) and apiin (compound 6, −14.9 kcal/mol for CB1, −18.3 kcal/mol for CB2) commonly showed high affinity to CB1 and CB2. Cell cytotoxic effects of CC (A) and its isolated compounds (B) with various concentrations. Nutrients 2024, 16, 1130. https://doi.org/10.3390/nu16081130  AMA Style  Nguyen NB, Le TT, Kang SW, Cha KH, Choi S, Youn H-Y, Jung SH, Kim M. Cornflower Extract and Its Active Components Alleviate Dexamethasone-Induced Muscle Wasting by Targeting Cannabinoid Receptors and Modulating Gut Microbiota. 2024; 16(8):1130. https://doi.org/10.3390/nu16081130  Chicago/Turabian Style  Nguyen, Ngoc Bao, Tam Thi Le, Suk Woo Kang, Kwang Hyun Cha, Sowoon Choi, Hye-Young Youn, Sang Hoon Jung, and Myungsuk Kim.

[Visit Link](https://www.mdpi.com/2072-6643/16/8/1130){:target="_blank rel="noopener"}

