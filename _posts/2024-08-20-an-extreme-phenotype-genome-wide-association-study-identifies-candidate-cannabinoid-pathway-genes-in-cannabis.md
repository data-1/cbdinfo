---
layout: post
title: "An extreme-phenotype genome‐wide association study identifies candidate cannabinoid pathway genes in Cannabis"
date: 2024-08-20
tags: [Single-nucleotide polymorphism,Gene,Fatty acid synthesis,National Center for Biotechnology Information,Cannabidiol,Allele,Genome-wide association study,DNA sequencing,BLAST (biotechnology),Cannabis,Genetics,Cannabinoid,Zygosity,Sequence alignment,DNA annotation,Biotechnology,Molecular biology,Biochemistry,Life sciences,Biology,Branches of genetics]
author: Matthew T | Graham J
---


DNA sequencing and mapping  Bulked DNA from each chemotypically extreme pool was subject to whole genome sequencing with Illumina sequencing-by-synthesis technology, generating 250,996,133 paired end (PE) reads for the CBDA pool and 238,918,478 PE reads for the THCVA pool (Table 1). For the FN reference sequence, mean sequencing depths were 53.8 × for the CBDA pool and 52.7 × for the THCVA pool (Table 1). Chemotypic pools are able to detect the known CBDAS locus  Bulked DNA from individual plants with divergent cannabinoid values allowed genome-wide variant comparison for alkyl and cyclic cannabinoid chemotypes (Fig. Reads from the THCVA pool mapped to the THCAS locus, while those from the CBDA pool failed to align to this region (Fig. Depth of coverage for the BKR homolog variant sites averaged 67 × for the CBDA pool and 74 × for the THCVA pool, with nonsynonymous SNPs supported by 62/61 reads for the CBDA pool and 65/64 reads for the THCVA pool (Supplementary Table S5).

[Visit Link](https://www.nature.com/articles/s41598-020-75271-7){:target="_blank rel="noopener"}

