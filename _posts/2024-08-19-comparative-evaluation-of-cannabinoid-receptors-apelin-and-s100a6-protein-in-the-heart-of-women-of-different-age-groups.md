---
layout: post
title: "Comparative evaluation of cannabinoid receptors, apelin and S100A6 protein in the heart of women of different age groups"
date: 2024-08-19
tags: [Cardiac muscle,Ventricular hypertrophy,Endothelium,Heart,Endocannabinoid system,Apelin,Cannabinoid receptor 2,Calcium in biology,Heart failure,Myocardial infarction,Sarcoplasmic reticulum,Biology,Cell biology,Medical specialties,Physiology]
author: 
---


Clinical and experimental data have demonstrated that aging is accompanied by histopathological changes in the heart such as cardiomyocyte apoptosis, cardiac muscle cell hypertrophy and heart fibrosis [26]. Considering the above-mentioned unfavourable impact of aging on the cardiovascular system and the involvement of the cannabinoid system, apelin and S100A6 protein in control of cardiac function, we decided to perform immunohistochemical detection and comparative evaluation of the distribution of cannabinoid receptors (CB1 and CB2), apelin and S100A6 in the heart of healthy women in different age groups. The results of the latest research demonstrate that the endocannabinoid system, apelin and S100A6 participate in the complex process controlling the cardiovascular system function since they exert a significant influence on blood pressure, heart rate and myocardial contractility [1, 9, 21]. Our study demonstrated decreased immunoreactivity for both cannabinoid receptors, while increased immunohistochemical reaction for apelin and S100A6 in the cardiomyocyte cytoplasm in the hearts of older women. Enlarging literature data indicate on significant effect of cannabinoids, apelin and S100A6 on endothelial cell functioning.

[Visit Link](https://link.springer.com/article/10.1186/s12872-018-0923-0){:target="_blank rel="noopener"}

