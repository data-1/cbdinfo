---
layout: post
title: "Effects of Cannabidiol on Innate Immunity: Experimental Evidence and Clinical Relevance"
date: 2023-02-27
categories:
- CBD
author: Martini, Gemma, Ferrari, Cosentino, Marino, Stefano Martini, Alessandra Gemma, Marco Ferrari, Marco Cosentino, Franca Marino
tags: [Inflammation,Neuroinflammation,Macrophage,Single-nucleotide polymorphism,Reactive oxygen species,Microglia,Inflammatory cytokine,Cannabidiol,Immune system,Monocyte,Cytokine,Neurodegenerative disease,Tumor necrosis factor,Natural killer cell,Cannabinoid,Granulocyte,Neutrophil,Autoimmune disease,Cannabinoid receptor type 2,Anti-inflammatory,Tetrahydrocannabinol,Mast cell,Oligodendrocyte,Autophagy,Interleukin 6,Astrocyte,Innate immune system,TRPV2,Eosinophil,Biochemistry,Medical specialties,Cell biology,Immunology,Biology,Medicine,Clinical medicine]
---


After tissue infiltration, macrophages contribute to innate immune responses through antigen presentation, the production of several cytokines, such as IL-1, IL-6, and TNF-α, and the phagocytosis process [ 77 80 ]. Indeed, on the one hand, CBD’s effects on resting oligodendrocytes include an increase in ROS generation, which suggests a proinflammatory effect for CBD; on the other hand, the activation of apoptotic processes in these cells supports an anti-inflammatory role for CBD [ 114 115 ]. CBD was found to be involved in the decrease in microglial activation [ 102 104 ] and inhibition of ROS release by these cells [ 105 ]. In particular, in ex vivo models, PMN activation was shown to induce an increase in proinflammatory cytokine production [ 165 ], while treatment with CBD reduced TNF-α production only in activated cells [ 156 ]. Moreover, CBD seems to exert inhibitory effects on ROS and proinflammatory cytokine production, particularly in models in which PMNs were previously activated.

<hr>

[Visit Link](https://www.mdpi.com/1422-0067/24/4/3125){:target="_blank" rel="noopener"}


