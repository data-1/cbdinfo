---
layout: post
title: "MOESM4 of Therapeutic use of cannabis and cannabinoids: an evidence mapping and appraisal of systematic reviews"
date: 2024-09-23
tags: [Drugs,Health,Pharmacognosy,Herbalism,Medicine,Religion and drugs,Biologically based therapies,Cannabis,Medical research,Cannabis and health,Shamanism,Medicinal herbs and fungi,Individual psychoactive drugs,Revelation,Entheogens,Medical treatments,Psychoactive drugs,Natural environment based therapies,Naturopathy,Pharmacology,Ethnobotany,Medicinal plants,Cannabaceae,Medicinal use of cannabis,Social aspects of psychoactive drugs,Religious practices]
author: Nadia Montero‐Oleas | Ingrid Arévalo-Rodríguez | Solange Núñez-González | Andrés Viteri-García | Daniel Simancas‐Racines
institutions: 
doi: "https://doi.org/10.6084/m9.figshare.11626914"
---


MOESM4 of Therapeutic use of cannabis and cannabinoids: an evidence mapping and appraisal of systematic reviews