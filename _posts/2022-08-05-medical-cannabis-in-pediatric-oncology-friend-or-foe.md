---
layout: post
title: "Medical Cannabis in Pediatric Oncology: Friend or Foe?"
date: 2022-08-05
categories:
- Cannabis
author: Malach, Kovalchuk, Megan Malach, Igor Kovalchuk, Olga Kovalchuk
tags: [Medical cannabis,Cannabidiol,Apoptosis,Cancer,Cannabinoid,Tetrahydrocannabinol,Radiation therapy,Cannabinoid receptor type 2,Treatment of cancer,Chemotherapy,Reactive oxygen species,Dose (biochemistry),Cannabinoid receptor type 1,Nabilone,Childhood cancer,Clinical trial,Addiction,Somatic mutation,Diseases and disorders,Medicine,Medical specialties,Health sciences,Health,Causes of death,Clinical medicine]
---


Studies confirming these or other molecular anti-cancer effects of cannabinoids in pediatric cancer cells are scarce, and given the differences between pediatric and adult tumors and the role of the ECS in brain development in children, the anti-cancer effects of cannabis need to be investigated in pediatric cancer specifically. Additionally, the pharmacokinetics of most anti-cancer drugs have been well studied in adult cancer patients, but the same cannot be said for pediatric cancer patients [ 13 ]. There has been some evidence of cannabis’ anti-cancer activity in patients. Most investigation into the use of medical marijuana in children is done in the context of epileptic disorders, and currently available evidence examining the use of cannabis in pediatric oncology patients is limited to case studies and descriptive studies, rather than clinical studies or trials. A common concern in reviews concerning pediatric patients and cannabis is the increased levels of side effects that occur with cannabis-based treatments, regardless of whether the cannabis-based treatment is effective [ 59 83 ].

<hr>

[Visit Link](https://www.mdpi.com/1424-8247/15/3/359/htm){:target="_blank" rel="noopener"}


