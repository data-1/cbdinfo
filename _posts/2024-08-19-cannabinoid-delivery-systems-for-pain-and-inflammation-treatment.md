---
layout: post
title: "Cannabinoid Delivery Systems for Pain and Inflammation Treatment"
date: 2024-08-19
tags: [Cannabinoid receptor 2,Cannabinoid,Nabiximols,Cannabinoid receptor 1,Tetrahydrocannabinol,Medical cannabis,Nabilone,Cannabidiol,Nanocarrier,Endocannabinoid system,Liposome,Inflammation,Cannabinoid receptor,Peripheral neuropathy,Pharmaceutical formulation,Topical medication,Nasal administration,Anandamide,Dronabinol,Pharmacology,Clinical medicine]
author: Natascia Bruni | Carlo Della Pepa | Simonetta Oliaro-Bosso | Enrica Pessione | Daniela Gastaldi | Franco Dosio | Della Pepa | Oliaro-Bosso
---


The release of cannabinoids from a microneedle formulation that is administered transdermally has been reported by Brooke [101], while a patent by Weimann has more recently focused on CBD delivery [102]. Polymeric drug delivery systems are able to protect drugs from degradation and control drug release. Cannabis and Cannabinoids for Chronic Pain. Drugs 2007, 16, 951–965. The endocannabinoid system and pain.

[Visit Link](https://www.mdpi.com/1420-3049/23/10/2478){:target="_blank rel="noopener"}

