---
layout: post
title: "A cell-free platform for the prenylation of natural products and application to cannabinoid production"
date: 2024-08-19
tags: [Gas chromatography,High-performance liquid chromatography,Glycolysis,Enzyme,Chromatography,Molecular cloning,Cofactor (biochemistry),Concepts in chemistry,Biochemistry,Analytical chemistry,Chemistry]
author: Meaghan A | Tyler P | Nicholas B | Gregory A | Robert E | James U
---


Improved production of cannabinoids  With our designed CBGA synthase in hand (M23), we tested the ability to produce CBGA directly from glucose and OA using the full synthetic biochemistry system, including the PDH bypass (Fig. So we added CBGVA, extracted from the cell-free system, to a reaction containing CBDA synthase. The plate was incubated at room temperature for 10 min, and the reactions were initiated with 10 µL of 100 mM pyruvate. The olivetolate concentration was set at 5 mM. 4a was achieved with 0.5 mg/mL of WT NphB and M23 and M31 (for divarinic acid), and reactions were quenched at ~6, 9, 12, 24, 48, 72, and 96 h.  The conditions were identical to the method above with the following exceptions, the final concentration of the aromatic substrates was 1 mM and the initial glucose concentration was 150 mM.

[Visit Link](https://www.nature.com/articles/s41467-019-08448-y){:target="_blank rel="noopener"}

