---
layout: post
title: "Cannabinoid Receptor 1 Agonist ACEA and Cannabinoid Receptor 2 Agonist GW833972A Attenuates Cell-Mediated Immunity by Different Biological Mechanisms"
date: 2024-08-29
tags: [Cannabinoid receptor 2,Anandamide,Cannabinoid receptor,Cell biology,Cell communication,Signal transduction,Cell signaling,Medical specialties,Neurochemistry,Biology,Biochemistry,Immune system,Immunology]
author: Nuchjira Takheaw | Kanyaruck Jindaphun | Supansa Pata | Witida Laopajon | Watchara Kasinrerk
---


Cannabinoid receptors, CB1 and CB2, are expressed on both CD4+ and CD8+ T cells and are upregulated upon T cell activation by anti-CD3/CD28 mAb [13,36,37]. [Google Scholar] [CrossRef]  Shah, S.A.; Gupta, A.S.; Kumar, P. Emerging role of cannabinoids and synthetic cannabinoid receptor 1/cannabinoid receptor 2 receptor agonists in cancer treatment and chemotherapy-associated cancer management. Cell. Cell. Cells.

[Visit Link](https://www.mdpi.com/2073-4409/12/6/848){:target="_blank rel="noopener"}

