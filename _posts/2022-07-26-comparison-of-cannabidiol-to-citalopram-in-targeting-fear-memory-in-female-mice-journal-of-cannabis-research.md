---
layout: post
title: "Comparison of cannabidiol to citalopram in targeting fear memory in female mice - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Montoya, Zackary T., Colorado State University-Pueblo, Pueblo, Uhernik, Amy L., Smith, Jeffrey P., Bonforte Blvd, Co
tags: [Cannabidiol,Post-traumatic stress disorder,Classical conditioning,Selective serotonin reuptake inhibitor,Memory,Extinction (psychology),Fear conditioning,Dose (biochemistry),Mental disorder,Serotonin,Major depressive disorder,Citalopram,Amygdala,Learning,Experiment,Cognitive science,Neuroscience]
---


CBD causes sex-dependent effects on contextual fear memory and extinction but citalopram does not affect either memory  We previously found that CBD enhances contextual memory and its extinction when assessed 24 h following trace fear conditioning in male mice (Uhernik et al. 2018). Therefore, our results suggest a sexual dimorphism in the effect of CBD on contextual memory in mice. While a majority of previous studies have shown increased fear and anxiety responses with acute dosing of SSRIs (Burghardt et al. 2004; Burghardt et al. 2007; Ravinder et al. 2011; Mir and Taylor 1997; Spigset 1999; Salchner and Singewald 2002; Sánchez and Meier 1997; Dekeyne et al. 2000), at least one study showed an anxiolytic effect of an acute dose on contextual memory recall, therefore, our result is not inconsistent with the published literature (Inoue et al. 1996). Pre-clinical relevance of the divergent effects of CBD and citalopram on fear memory  Both drugs in our study inhibited particular types of fear memory when given prior to conditioning, suggesting the mechanisms of action involved either the acquisition or early consolidation phases of fear memory. Since PTSD involves both fear generalization and misrepresentation of contextual fear associations, our study suggests that CBD might provide a spectrum of effects that would be more comprehensive than citalopram for targeting processes involved in acquisition of memories that lead to PTSD.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-020-00055-9){:target="_blank" rel="noopener"}


