---
layout: post
title: "Determination of the Cannabinoid CB1 Receptor’s Positive Allosteric Modulator Binding Site through Mutagenesis Studies"
date: 2024-09-02
tags: [Allosteric regulation,Ligand (biochemistry),Binding site,Cannabinoid receptor 1,Agonist,Allosteric modulator,Receptor (biochemistry),Docking (molecular),Protein,Inverse agonist,Protein–protein interaction,G protein,Molecular biology,Biotechnology,Biology,Cell biology,Biochemistry]
author: Hayley M. Green | Daniel M. J. Fellner | David B. Finlay | Daniel P. Furkert | Michelle Glass | Hayley M | Daniel M. J | David B | Daniel P
---


To understand the effect of each receptor mutant on allosteric ligand G protein dissociation, we first evaluated the effect of mutations within putative allosteric binding sites on orthosteric agonist (CP55940)-induced G protein dissociation. ; Denovan-Wright, E.M.; et al. Enantiospecific Allosteric Modulation of Cannabinoid 1 Receptor. [Google Scholar] [CrossRef] [PubMed]  Yang, X.; Wang, X.; Xu, Z.; Wu, C.; Zhou, Y.; Wang, Y.; Lin, G.; Li, K.; Wu, M.; Xia, A.; et al. Molecular mechanism of allosteric modulation for the cannabinoid receptor CB1. ; Glass, M. Determination of the Cannabinoid CB1 Receptor’s Positive Allosteric Modulator Binding Site through Mutagenesis Studies. Determination of the Cannabinoid CB1 Receptor’s Positive Allosteric Modulator Binding Site through Mutagenesis Studies Pharmaceuticals 17, no.

[Visit Link](https://www.mdpi.com/1424-8247/17/2/154){:target="_blank rel="noopener"}

