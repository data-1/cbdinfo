---
layout: post
title: "Global brain network dynamics predict therapeutic responsiveness to cannabidiol treatment for refractory epilepsy"
date: 2024-09-16
tags: [Electroencephalography,Cannabidiol,Neural oscillation,Epilepsy,Management of drug-resistant epilepsy,Modularity (networks),Theta wave,Seizure,Shortest path problem,Cannabinoid,Dose (biochemistry)]
author: David E. Anderson | Deepak Madhavan | Arun Swaminathan
institutions: Boys Town | University of Nebraska Medical Center
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7751013"
---


Graph theoretical analysis of brain network dynamics was extracted from phase coherence to evaluate measures of network integration (i.e. characteristic path length, global efficiency and degree) and segregation (i.e. modularity and transitivity). Effects of age on global brain network dynamics  We evaluated effects of age on graph theoretical measures of global brain network dynamics derived from topographic patterns of EEG phase coherence across each frequency band. [PMC free article] [PubMed] [Google Scholar] [Ref list]  Devinsky O, Cilio MR, Cross H, Fernandez-Ruiz J, French J, Hill C, et al. Cannabidiol: pharmacology and potential therapeutic role in epilepsy and other neuropsychiatric disorders. [PubMed] [Google Scholar] [Ref list]  Rubinov M, Sporns O. [PMC free article] [PubMed] [Google Scholar] [Ref list]