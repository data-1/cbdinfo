---
layout: post
title: "Amino Acid Supplementation as a Biostimulant in Medical Cannabis (Cannabis sativa L.) Plant Nutrition"
date: 2022-07-31
categories:
- Cannabis
author: Malík, Department Of Agroenvironmental Chemistry, Plant Nutrition, Faculty Of Agrobiology, Food, Natural Resources, Czech University Of Life Sciences Prague, Velechovský, Praus, Janatová
tags: [Cannabis sativa,Soil,Amino acid,Tetrahydrocannabinolic acid,Dietary supplement,Gas chromatography,Nutrient,Plant,Hydroponics,Metabolism,Medical cannabis,Tetrahydrocannabinolic acid synthase,Cannabinoid,Plant nutrition,Cannabis,Calcium,Terpene,Iron,Root,Nutrition,Carbon dioxide,PH,Tetrahydrocannabinol,Fertilizer,Biosynthesis,Physical sciences,Chemistry]
---


Materials and Methods  Basic Parameters of the Growing Space  Cannabis plants were grown on tables in a room with controlled conditions. Plant density was 27.5 plants per m2 (55 plants/table/treatment). Plant sampling method. The effect of amino acid supplementation (AAs) and growing nutritional cycle on medical cannabis plant biomass. Plants 17, 291–295.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fpls.2022.868350/full){:target="_blank" rel="noopener"}


