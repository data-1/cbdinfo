---
layout: post
title: "Neuroprotective constituents from the aerial parts of Cannabis sativa L. subsp. sativa †"
date: 2024-09-16
tags: [Two-dimensional nuclear magnetic resonance spectroscopy,High-performance liquid chromatography,Scientific techniques,Physical sciences,Physical chemistry,Laboratory techniques,Analytical chemistry,Chemistry,Concepts in chemistry]
author: Jia Li | Guan Wang | Yu Qin | Xue Zhang | Hai-Feng Wang | Hong-Wei Liu | Ling-Juan Zhu | Xin-Sheng Yao
---


These signals were positioned at C-3 based on the 1H–1H COSY (H-5′/H-4′; H-2′/H-3′, H-1′) and HMBC (H-5′/C-3′, C-4′; H-1′/C-3, C-4) correlations ( ). Finally, the structure of compound 4 was identified and named as (7Z,9Z)-cannabiphenolic acid A.  No.45 δ H (J in Hz) δ C δ H (J in Hz) δ C 1153.7153.92123.1123.737.11, d (7.7)131.07.04, d (7.6)130.846.76, br d (7.7)121.36.85, br d (7.6)119.25137.0138.566.71, br s115.56.87, br s113.87140.5150.486.58, d (11.5)122.45.36, overlap69.995.95, dd (11.5, 11.2)122.65.31, overlap127.7105.28, dd (11.2, 10.3)141.65.29, overlap140.6112.88, m27.02.62, br s27.2122.25, s15.52.21, s15.6134.40, s67.95.34, overlap112.95.26, br s141.00, d (6.6)23.10.97, d (6.6)23.2150.99, d (6.6)23.10.79, d (6.6)22.6  Compound 5, obtained as yellowish oil, displayed an HRESIMS peak at m/z 231.1387 [M–H]− corresponding to the molecular formula C15H20O2, suggesting 6 degrees of unsaturation. The EtOAc soluble extract (121.4 g) was subjected to CC over silica gel eluted with cyclohexane/EtOAc (from 100 : 0 to 0 : 100) to afford Fr. Compound 7 (57.2 mg) was afforded from Fr. EH3 (506.6 mg) was purified by ODS CC, then Fr.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9056577/){:target="_blank rel="noopener"}

