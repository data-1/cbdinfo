---
layout: post
title: "The Expanded Endocannabinoid System Contributes to Metabolic and Body Mass Shifts in First-Episode Schizophrenia: A 5-Year Follow-Up Study"
date: 2022-08-07
categories:
- Endocannabinoid
author: Parksepp, Haring, Kilk, Koch, Uppin, Kangro, Zilmer, Vasar, Madis Parksepp, Liina Haring
tags: [Cannabinoid receptor type 1,Cannabinoid receptor type 2,N-Acylethanolamine,Peroxisome proliferator-activated receptor alpha,Psychosis,2-Arachidonoylglycerol,Dose (biochemistry),Biochemistry]
---


Having collected a set of multivariate data, the first step was to test the effects of the AP treatment and disease status by LME models based on 2-AG, AEA, LEA, OEA, PEA, and their metabolically dominating specific lipids: 38 PCs (PC aa C24:0—PC aa C42:6), and BMI. According to our results, the level of 2-AG tended to be decreased in AP-naïve FEP patients, and 5 years of AP treatment resulted in a significant increase in 2-AG. In addition, cannabimimetic actions over CB 1 receptor activation are associated with analgesia, neuroprotection, hypotension, and appetite stimulation [ 2 receptors are very abundant in immune tissues and cells, and they are primarily expressed only in active inflammation [ α agonists, which include fibrates, are used in the treatment of cholesterol disorders and for their effects on several cardiovascular risk markers associated with the metabolic syndrome and diabetes [62, The cannabinoid receptors, eCBs, and their synthesizing and metabolizing enzymes are presented in the central and peripheral nervous systems and many other peripheral tissues [ 48 49 ]. Our study suggests the existence of alterations in the peripheral serum levels of eCBs and eCB-like NAEs in AP-naïve FEP patients. According to the results of our study, the progression of the disease and concomitant 5-year AP treatment reflect the development of chronic stress in the body, which was indirectly confirmed by a significant increase in 2-AG and a decrease in NAEs levels.

<hr>

[Visit Link](https://www.mdpi.com/2227-9059/10/2/243/htm){:target="_blank" rel="noopener"}


