---
layout: post
title: "Cannabinoid Ligands Targeting TRP Channels"
date: 2024-08-19
tags: [TRPV1,TRPM8,Cannabinoid,Transient receptor potential channel,TRPA1,Cannabinol,Neurochemistry,Molecular neuroscience,Physiology,Cell biology,Biophysics,Transmembrane proteins,Neurophysiology,Biochemistry]
author: Patricia H
---


FIGURE 1  FIGURE 2  FIGURE 3  Many endogenous and exogeneous compounds activate receptors found in the TRP superfamily. In addition to these pungent compounds, the six TRP channels that make up the ionotropic cannabinoid receptors can also be modulated by endogenous, phytogenic, and synthetic cannabinoids. FIGURE 4  FIGURE 5  FIGURE 6  There are many more cannabinoid ligands that target the ionotropic cannabinoid receptors. While TRPV1 is activated by endogenous, phytogenic, and synthetic cannabinoids, TRPV2 is mainly activated by phytocannabinoids (De Petrocellis et al., 2017; Soethoudt et al., 2017). Interestingly, this compound does not modulate any other TRP channel tested.

[Visit Link](https://www.frontiersin.org/journals/molecular-neuroscience/articles/10.3389/fnmol.2018.00487/full){:target="_blank rel="noopener"}

