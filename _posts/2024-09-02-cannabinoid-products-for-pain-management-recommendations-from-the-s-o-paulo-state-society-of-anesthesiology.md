---
layout: post
title: "Cannabinoid products for pain management: recommendations from the São Paulo State Society of Anesthesiology"
date: 2024-09-02
tags: [Pain management,Pain,Prescription drug,Medical specialties,Medicine,Health care,Health,Clinical medicine,Medical treatments,Pharmacology,Health sciences,Therapy,Drugs,Pharmaceutical sciences,Pharmacy]
author: 
---


There is growing interest in using cannabinoids across various clinical scenarios, including pain medicine, leading to the disregard of regulatory protocols in some countries. Legislation has been implemented in Brazil, specifically in the state of São Paulo, permitting the distribution of cannabinoid products by health authorities for clinical purposes, free of charge for patients, upon professional prescription. Thus, it is imperative to assess the existing evidence regarding the efficacy and safety of these products in pain management. In light of this, the São Paulo State Society of Anesthesiology (SAESP) established a task force to conduct a narrative review on the topic using the Delphi method, requiring a minimum agreement of 60% among panelists. The study concluded that cannabinoid products could potentially serve as adjuncts in pain management but stressed the importance of judicious prescription.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S0104001424000356){:target="_blank rel="noopener"}

