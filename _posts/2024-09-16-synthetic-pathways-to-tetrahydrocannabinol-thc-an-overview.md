---
layout: post
title: "Synthetic pathways to tetrahydrocannabinol (THC): an overview"
date: 2024-09-16
tags: [Diels–Alder reaction,Cannabinoid,Grignard reagent,Organic synthesis,Ether,Cannabinol,Enantioselective synthesis,Chemical reaction,Epoxide,Claisen rearrangement,Alkene,Tetrahydrocannabinol,Ester,Lewis acid catalysis,Chemical reactions,Concepts in chemistry,Organic chemistry,Unit processes,Chemistry]
author: Victor R. L. J. Bloemendal | Jan C. M. van Hest | Floris P. J. T. Rutjes
institutions: Radboud University Nijmegen
doi: "https://pubs.rsc.org/en/content/articlehtml/2020/ob/d0ob00464b"
---


Additionally, She and co-workers aimed to prepare (−)-trans-Δ8-THC to demonstrate the versatility of silyl enol ether 43 (Scheme 11).46 By using MOM-protected olivetolic cuprate 44 and (−)-43, the authors were successful in obtaining C-ring oxidised cannabinoid 50 after acidic hydrolysis. Two years later, the key-step Diels Alder cyclisation catalysed by C1 was further investigated in a mechanistic study with a significant larger substrate scope.59  Subsequent seminal work of Avery and co-workers represented the first entry into a direct synthesis of the B- and C-rings of hexahydrocannabinol derivatives through a Diels–Alder cyclisation reaction (Scheme 14).60 Though this approach utilises (S)-citronellal, a chiral pool terpinoid, a concerted double cyclisation approach was required to provide the tricyclic cannabinoid scaffold. A recent effective approach to obtain synthetic cannabinoids was published by Westphal and co-workers, who demonstrated the late stage derivatisation of a functionalised (−)-trans-Δ9-THC scaffold.77 Currently, this late-stage approach is attractive to efficiently prepare synthetic cannabinoid derivatives and is a hot topic in the scientific field.78–80 Alternatively, the groups of Carreira and Studer have disclosed stereodivergent syntheses of cannabinoids, affording a variety of cannabinoids by varying catalyst ligands.81–83 This latter approach may provide fast access into more stereodiverse cannabinoids. She, Synthesis, 2010, 1766–1770 CAS. Wang and Q.-L. Zhou, Org.