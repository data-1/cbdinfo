---
layout: post
title: "How high: differences in the developments of cannabis markets in two legalized states"
date: 2024-09-16
tags: [Cannabis edible,Cannabis (drug),Cannabidiol,Entheogens,Individual psychoactive drugs,Psychoactive drugs,Cannabis,Social aspects of psychoactive drugs,Cannabaceae]
author: Caislin L. Firth | Steven Davenport | Rosanna Smart | Julia A. Dilley
institutions: Multnomah County Health Department
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7041961/"
---


Retail market opening and sales  The market opening process varied between the states. There is currently no limit on the number of retail licenses allowed in Oregon. Sales data include both quantity sold and product prices, and the price of cannabis products varies by potency and product type. The cost of flower and edibles were similar between the two states ($11 per gram of high THC (26%) flower, $2-3 per 10 milligram THC edible mint). Oregon’s relatively faster initial growth, quicker stabilization and current lower prices for concentrates could be related to more established prelegalization medical markets, the state’s “soft opening” approach, and lower tax rate.