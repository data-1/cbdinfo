---
layout: post
title: "Sex and Gender Interactions on the Use and Impact of Recreational Cannabis"
date: 2024-09-26
tags: [Cannabis (drug),Gender,Cannabinoid,Substance dependence,Substance abuse,Driving under the influence,Cannabis smoking,Cannabis use disorder,Tetrahydrocannabinol,Harm reduction,Cannabis in Canada,Vaporizer (inhalation device),Health,Bong,Endocannabinoid system]
author: Lorraine Greaves | Natalie Hemsing
pubdate: "2020-01-14"
institutions: British Columbia Centre of Excellence for Women's Health
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7014129"
---


According to data from the 2019 National Cannabis Survey in Canada, more men than women reported cannabis use in the past three months (20.3% vs. 14%). Sex differences, gender and addiction. Sex and gender differences in substance use disorders. Sex differences, gender and addiction. Sex and gender differences in substance use disorders.