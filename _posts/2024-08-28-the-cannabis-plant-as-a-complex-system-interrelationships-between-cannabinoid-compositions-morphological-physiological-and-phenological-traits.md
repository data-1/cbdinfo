---
layout: post
title: "The Cannabis Plant as a Complex System: Interrelationships between Cannabinoid Compositions, Morphological, Physiological and Phenological Traits"
date: 2024-08-28
tags: [Cannabinoid,Cannabidiol,Cannabis,Cannabinol]
author: Erez Naim-Feil | Aaron C. Elkins | M. Michelle Malmberg | Doris Ram | Jonathan Tran | German C. Spangenberg | Simone J. Rochfort | Noel O. I. Cogan | Naim-Feil | Aaron C
---


Cannabis sativa and Cannabis indica versus “Sativa” and “Indica.” In Cannabis sativa L.—Botany and Biotechnology; Chandra, S., Lata, H., ElSohly, M.A., Eds. Cannabis Cannabinoids 2018, 1, 19–27. Cannabinoids 2014, 9, 9–15. Plants 2022, 11, 140. Plants.

[Visit Link](https://www.mdpi.com/2223-7747/12/3/493){:target="_blank rel="noopener"}

