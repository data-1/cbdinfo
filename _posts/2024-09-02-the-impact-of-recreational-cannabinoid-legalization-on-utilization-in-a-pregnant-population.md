---
layout: post
title: "The impact of recreational cannabinoid legalization on utilization in a pregnant population"
date: 2024-09-02
tags: [Cannabis (drug),Pregnancy,Legalization of non-medical cannabis in the United States,Prenatal development,Tetrahydrocannabinol,Drug prohibition,Cannabinoid,Health,Health care]
author: Jessie R
---


Through this biphasic effect, the increased potency and concentration of THC products may lead to increased levels of maternal stress and anxiety, which may also impact fetal well-being (23). The two epoch groups were further divided into prenatal cannabinoid exposure (case group, hereafter referred to as PCE) and no known prenatal cannabinoid exposure (control group). This could explain why we did not observe an increase in cannabinoid use in pregnancy during this period, as individuals may not have had the ease of access to cannabinoid products. As described in other studies, the pregnant population in the PCE group were significantly more likely to use other substances during pregnancy such as tobacco, alcohol, and opioids compared to the control group (see Table 2) (20–22). Conclusion  Cannabinoid use in pregnancy had a statistically significant decrease during the post-legalization period compared to the pre-legalization period in this study.

[Visit Link](https://www.frontiersin.org/journals/public-health/articles/10.3389/fpubh.2024.1278834/full){:target="_blank rel="noopener"}

