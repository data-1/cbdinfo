---
layout: post
title: "Cannabinoid Receptors: An Update on Cell Signaling, Pathophysiological Roles and Therapeutic Opportunities in Neurological, Cardiovascular, and Inflammatory Diseases"
date: 2024-08-20
tags: [Cannabinoid receptor 1,Cannabinoid receptor 2,G protein-coupled receptor,Cannabinoid,Synapse,Synaptic plasticity,Long-term potentiation,Endocannabinoid system,Cell signaling,Multiple sclerosis,Astrocyte,Microglia,GLUT4,Neurogenesis,Cell biology,Neurophysiology,Neurochemistry,Biochemistry,Signal transduction,Physiology,Cell communication]
author: Dhanush Haspula | Michelle A. Clark | Michelle A
---


Cannabinoid Receptors  2.1. Interestingly, under pathological conditions, various peripheral cell types have also been shown to express detectable levels of the CB2R. Signaling  2.4.1. Additionally, the CB1R activation was also shown to activate the PI3K/AKT pathway resulting in the regulation of neuronal survival [124]. Regulation of synaptic activity and neuroinflammatory states by cannabinoid receptors.

[Visit Link](https://www.mdpi.com/1422-0067/21/20/7693){:target="_blank rel="noopener"}

