---
layout: post
title: "Kahweol, a natural diterpene from coffee, induces peripheral antinociception by endocannabinoid system activation"
date: 2022-09-05
categories:
- Cannabinoid
- endocannabinoid
author: L.S. Guzzo, Departamento De Farmacologia, Instituto De Ciências Biológicas, Universidade Federal De Minas Gerais, Belo Horizonte, Mg, C.C. Oliveira, R.C.M. Ferreira, D.P.D. Machado, M.G.M. Castor
tags: [Cannabinoid,Cannabinoid receptor type 1,Cannabinoid receptor,Cannabinoid receptor type 2,2-Arachidonoylglycerol,Endocannabinoid system,Anandamide,Isotopic labeling,Neurochemistry,Biochemistry,Neurophysiology,Neuroscience]
---


On the other hand, the intraplantar injection of the CB 2 cannabinoid receptor antagonist AM630 (100 μg/paw) did not significantly reduce the antinociceptive effect of kahweol (80 μg/paw) [F (2,13)=130.8, P=0.0001] ( ). A dose-dependent antinociceptive effect was observed with injection of kahweol (20, 40, and 80 µg/paw) into the right hind paw against PGE 2 -induced hyperalgesia (2 µg/paw). A previous report from our research group showed that the peripheral antinociceptive effects of AEA, an endogenous CB 1 cannabinoid agonist, and PEA, an AEA-related lipid mediator, which might indirectly activate CB 2 receptors, were blocked by the CB 1 and CB 2 receptor antagonists, AM251 and AM630, respectively, in a dose-dependent manner (21). On the other hand, the involvement of the CB 2 cannabinoid receptor in the peripheral antinociception induced by kahweol was ruled out here since the intraplantar administration of the CB 2 cannabinoid receptor antagonist AM630 did not antagonize the antinociceptive effect of kahweol. Furthermore, it has been shown that these inhibitors, when administered peripherally, may enhance the peripheral antinociceptive effects that are induced by low doses of endocannabinoid agonists (34).

<hr>

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8555452/){:target="_blank" rel="noopener"}


