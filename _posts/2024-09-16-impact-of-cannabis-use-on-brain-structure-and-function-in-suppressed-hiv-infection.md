---
layout: post
title: "Impact of Cannabis Use on Brain Structure and Function in Suppressed HIV Infection"
date: 2024-09-16
tags: [Cannabis (drug),HIV-associated neurocognitive disorder,Neuroscience,Clinical medicine,Medical specialties]
author: Kalpana J. Kallianpur | Rasmus M. Birn | Lishomwa C. Ndhlovu | Scott A. Souza | Brooks I. Mitchell | Robert Paul | Dominic C. Chow | Lindsay B. Kohorn | Cecilia M. Shikuma
institutions: University of Hawaiʻi at Mānoa
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7508465"
---


Brain regionPredictor variables β pR2Adjusted R2Caudate nucleusIntracranial volume0.390.002Years of cannabis use−0.47 <0.001 Recent cannabis use0.36 0.006 0.440.40PallidumIntracranial volume0.240.064Years of cannabis use−0.48 0.001 Recent cannabis use0.350.0120.330.29AmygdalaIntracranial volume0.280.033Years of cannabis use−0.46 0.001 Recent cannabis use0.310.0270.330.28PutamenIntracranial volume0.300.028Years of cannabis use−0.38 0.011 Recent cannabis use0.250.0830.270.22Nucleus accumbensIntracranial volume0.250.044Years of cannabis use−0.57 <0.0001 Recent cannabis use0.230.0840.390.35ThalamusIntracranial volume0.47<0.001Years of cannabis use−0.34 0.012 Recent cannabis use0.220.0860.390.35HippocampusIntracranial volume0.330.012Years of cannabis use−0.400.006Recent cannabis use0.220.1110.310.26Cortical GMIntracranial volume0.67<0.0001Years of cannabis use−0.300.007Recent cannabis use0.040.6910.600.58Cerebral WMIntracranial volume0.67<0.0001Years of cannabis use−0.37<0.001Recent cannabis use0.320.0010.700.68  Tobacco smoking status (current smoking, or never vs. ever having smoked) and alcohol use did not affect regional brain volumetric associations with cannabis use in the HIV+ or HIV− groups. Brain, 129, 1096–1112. Brain, 141, 1678–1690. 10.1172/JCI18533 [PMC free article] [PubMed] [CrossRef] [Google Scholar] [Ref list]  [41] Chang L and Chronicle EP (2007) Functional Imaging Studies in Cannabis Users. 10.3389/fphar.2016.00355 [PMC free