---
layout: post
title: "Cannabinoid Receptor-1 suppresses M2 macrophage polarization in colorectal cancer by downregulating EGFR"
date: 2024-08-25
tags: [Macrophage,Cannabinoid receptor 2,Tumor microenvironment,Macrophage polarization,Epithelial–mesenchymal transition,Epidermal growth factor receptor,Cannabinoid receptor 1,Interleukin 10,Cancer,Colorectal cancer,Cannabinoid receptor,Cell biology,Biochemistry,Biology,Cell signaling,Health sciences]
author: You-Ming | Xin-Yu
---


Our results demonstrated that CB1 activation suppressed M2 macrophage differentiation and tumor growth by downregulating EGFR. After treated with ACEA, the expression of CB1 was increased significant, and AM251 inhibited the expression of CB1 (Fig. What’s more, EGFR knockdown reversed the effects of CB1 inhibition on the expression of macrophage polarization markers in cells treated with media from colorectal cells (Figs. Moreover, our results confirmed the link between EGFR and macrophage differentiation, since EGFR overexpression significantly promoted M2 macrophage activation [34, 35]. In conclusion, our study showed that CB1 activation suppressed tumor growth and M2 macrophage activation in colorectal cancer by downregulating EGFR.

[Visit Link](https://www.nature.com/articles/s41420-022-01064-8){:target="_blank rel="noopener"}

