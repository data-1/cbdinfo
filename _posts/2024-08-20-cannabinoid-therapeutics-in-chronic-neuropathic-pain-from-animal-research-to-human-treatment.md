---
layout: post
title: "Cannabinoid Therapeutics in Chronic Neuropathic Pain: From Animal Research to Human Treatment"
date: 2024-08-20
tags: [Cannabinoid,Endocannabinoid system,Medical cannabis,Synapse,Neuropathic pain,Pain,Cannabinoid receptor 2,Cannabidiol,Neuron,TRPV1,Cannabinoid receptor 1,Tetrahydrocannabinol,Chemical synapse,Allodynia,Astrocyte,TRPM8,Group C nerve fiber,Fatty-acid amide hydrolase 1,Transient receptor potential channel,Opioid,Purinergic receptor,Peripheral neuropathy,Microglia,Pain management,Glutamate receptor,Analgesic,Neurochemistry]
author: Raquel Maria P | Andrey F. L | Paes-Colli | Priscila Martins Pinheiro | Bruna K | de Melo Reis | Ricardo A | Luzia S
---


The synapses between Aδ, Aβ, and C fibers and spinal cord neurons are excitatory, having glutamate as neurotransmitter (West et al., 2015; Alles and Smith, 2018). In addition to the fact that THC is a CB1R and CB2R agonist, reducing neurotransmitters release by neurons, especially glutamate, it also acts as TRPM8 antagonist and TRPA1 agonist (De Petrocellis et al., 2008; Storozhuk and Zholos, 2018). CBD also contributes to decreased chronic neuropathic pain symptoms. extracts, the latter more commonly used by patients. to treat pain is remarkable, as described in this review.

[Visit Link](https://www.frontiersin.org/journals/physiology/articles/10.3389/fphys.2021.785176/full){:target="_blank rel="noopener"}

