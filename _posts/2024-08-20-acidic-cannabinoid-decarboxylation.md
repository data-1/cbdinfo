---
layout: post
title: "Acidic Cannabinoid Decarboxylation"
date: 2024-08-20
tags: [Cannabinoid,Cannabinol,Decarboxylation,Cannabis,Tetrahydrocannabinolic acid,Tetrahydrocannabinol,Cannabidiol,Carbon,Natural product,Ultraviolet,Chemistry]
author: Crist N. Filer
---


This review addresses acidic and neutral cannabinoid structural pairs, when and where acidic cannabinoid decarboxylation occurs, the kinetics and mechanism of the decarboxylation reaction as well as possible future directions for this topic. When and Where Does Acidic Cannabinoid Decarboxylation Occur? Trichomes of Cannabis sativa L. (Cannabaceae). Quantitative determination of cannabinoids in individual glandular trichomes of Cannabis sativa L. (Cannabaceae). Trichomes of Cannabis sativa L. (Cannabaceae).

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9225410/){:target="_blank rel="noopener"}

