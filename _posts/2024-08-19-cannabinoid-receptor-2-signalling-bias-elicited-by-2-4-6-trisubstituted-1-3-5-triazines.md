---
layout: post
title: "Cannabinoid Receptor 2 Signalling Bias Elicited by 2,4,6-Trisubstituted 1,3,5-Triazines"
date: 2024-08-19
tags: [Ligand (biochemistry),Cannabinoid receptor 2,Receptor (biochemistry),Receptor antagonist,Intrinsic activity,Agonist,EC50,Cell signaling,High-performance liquid chromatography,Biochemistry,Cell biology]
author: Caitlin R. M | de la Harpe | Sara M | Andrea J | Natasha Lillia
---


1 and 2 were characterised by NMR spectroscopy and HRMS (data not shown), and sample purity of test compounds was 97.3 and 100%, respectively. 7, 15, and 16 were characterised by NMR spectroscopy and HRMS (data not shown), and sample purity of test compounds was 97.2, 99.5, and 98.5%, respectively. Comparison of Ligand Efficacy and Potency, and Bias Analysis  Having measured hCB2 binding affinities and responses in three functional assays, we were interested to compare the patterns of efficacy and potency between the 2,4,6-trisubstituted 1,3,5-triazine ligands and a prototypic CB2 ligand (CP 55,940), and determine whether any ligands exhibited evidence of bias between activation of these signalling pathways. CP 55,940 was measured to have the highest binding affinity of the compounds tested, acted as a full agonist in all three signalling pathways, and exhibited the smallest range of activation potencies between the pathways, all of which support selection of CP 55,940 as a reference ligand for comparison with previously uncharacterised compounds (van der Westhuizen et al., 2014). However, it was a weak partial agonist in the pERK pathway, producing only ∼36% of the maximal measured response across all ligands and exhibiting significant bias away from pERK relative to CP 55,940 and the other two signalling pathways measured.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2018.01202/full){:target="_blank rel="noopener"}

