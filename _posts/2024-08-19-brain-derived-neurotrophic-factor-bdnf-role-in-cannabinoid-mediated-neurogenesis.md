---
layout: post
title: "Brain-Derived Neurotrophic Factor (BDNF) Role in Cannabinoid-Mediated Neurogenesis"
date: 2024-08-19
tags: [Cannabinoid receptor 2,Subventricular zone,Cannabinoid receptor 1,Subgranular zone,Brain-derived neurotrophic factor,Neurogenesis,Cell signaling,Biology,Biochemistry,Signal transduction,Cell communication,Neurochemistry,Cell biology,Neurophysiology]
author: Filipa Fiel | Filipa F | Rui S | Ana Maria
---


BDNF Crosstalk With Cannabinoid Receptors Modulates Neuronal Differentiation at SVZ  To evaluate the effects on SVZ neuronal differentiation, SVZ cells were treated with the test drugs in serum-free medium devoid of growth factors for 7 days (Figure 3A). These data indicate that endogenous BDNF is necessary for the actions of CB1R and CB2R upon SVZ neuronal differentiation. BDNF-CB2R Interaction Regulates Cell Proliferation in DG Cell Cultures  To assess the effects on DG cell proliferation, as we did before for SVZ cell proliferation, DG cells were treated with selective ligands for 48 h, incorporated BrdU was immunolabeled and positive nuclei percentage was determined (Figure 5A). Finally, we have observed that the effect promoted by BDNF on DG neuronal differentiation was blocked when cells were co-incubated with the CB2R selective antagonist AM630 (BDNF 30 ng/mL+AM630 1 μM: 84.3 ± 14.7% [95% CI: 20.8–147.8%]; n = 3, p < 0.05 vs. BDNF alone) (Figure 6H) but not with the CB1R selective antagonist AM251 (Figure 6H), indicating that CB2R is preponderant to modulate the action of BDNF on DG neuronal differentiation. BDNF was shown to be an important modulator of SVZ and DG postnatal neurogenesis, its actions being under control of cannabinoid receptors.

[Visit Link](https://www.frontiersin.org/journals/cellular-neuroscience/articles/10.3389/fncel.2018.00441/full){:target="_blank rel="noopener"}

