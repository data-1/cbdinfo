---
layout: post
title: "Recent advances in electrochemical sensor technologies for THC detection—a narrative review - Journal of Cannabis Research"
date: 2022-07-26
categories:
- THC
author: Amini, Selective Lab Inc., Richmond Hill, Sepehrifard, Valinasabpouri, Safruk, Angelone, De Campos Lourenco, On, Kaveh Amini
tags: [Biosensor,Voltammetry,Molecularly imprinted polymer,Sensor,Physical sciences,Chemistry]
---


(b) Sensors developed for THC detection in oral fluids samples  Fig. The detection limits have been indicted to be 0.5 μM THC and 0.2 mM alcohol. Taken from (Mishra et al. 2020) with permission from Elsevier (Copyright 2020) Full size image  In another work, Nissim and Compton have introduced an optimized carbon paste electrode, made from graphite powder and mineral oil, for sensitive detection of THC in both aqueous solutions of pH 10.0 and in synthetic oral fluids. Wanklyn et al. have also reported development of a screen-printed carbon electrode for N-(4-amino-3-methoxyphenyl)-methane sulfonamide mediated detection of THC in oral fluids. The developed sensor has been used to detect THC spiked in undiluted oral fluids at 25–50 ng/mL with a response time of 30 s. A trial of these sensors on the oral fluids samples from four cannabis smokers has shown a sensitivity of 28 %, specificity of 99% and accuracy of 52%.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00122-3){:target="_blank" rel="noopener"}


