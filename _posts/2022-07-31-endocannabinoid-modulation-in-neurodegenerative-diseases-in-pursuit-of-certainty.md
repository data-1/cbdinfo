---
layout: post
title: "Endocannabinoid Modulation in Neurodegenerative Diseases: In Pursuit of Certainty"
date: 2022-07-31
categories:
- Endocannabinoid
author: Vasincu, Rusu, Ababei, Larion, Bild, Stanciu, Gabriela Dumitrița, Solcan, Alexandru Vasincu, Răzvan-Nicolae Rusu
tags: [Cannabinoid receptor type 2,Neurodegenerative disease,Hypokinesia,Neurotransmitter,Chemical synapse,Microglia,Cannabinoid receptor,Substantia nigra,Demyelinating disease,Huntingtin,TRPV1,Experimental autoimmune encephalomyelitis,Neuroinflammation,Neuroprotection,Tetrahydrocannabinol,Synapse,Amyloid beta,Medical specialties,Neurochemistry,Clinical medicine,Cell biology,Biochemistry,Neuroscience,Physiology,Medicine,Neurophysiology]
---


Neuroprotective properties have been demonstrated in numerous studies of mice and rats whose Aβ-induced memory deficits were prevented by exogenous cannabinoids, while CBD administration determined/promoted neurogenesis, highlighting the potential beneficial effects in AD. Other studies in animal models and in PD patients have shown that the disease is linked to an overactivation of the ECB signalling system, with increased levels of AEA and 2-AG, as well as an increase in the CB1R density, an aspect which has been shown post-mortem in the striatum of PD patients. Chronic administration of WIN 55,212-2 (another CB1R agonist) in a R6/1 transgenic mouse model of the disease demonstrated a protective effect against motor impairment. Positive effects have also been shown for symptoms such as spasticity and tremor, although patients that presented these symptoms had other neurodegenerative diseases and not HD (e.g., spasticity improvement was demonstrated in patients with autoimmune demyelinative disease and MS, while tremor was improved in patients with PD following administration of medical marijuana). It was shown that the treatment demonstrated beneficial effects on dystonia, thus improving motor symptoms.

<hr>[Visit Link](https://www.mdpi.com/2079-7737/11/3/440/htm){:target="_blank" rel="noopener"}


