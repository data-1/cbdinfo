---
layout: post
title: "Blockchain-Enabled Internet-of-Things Platform for End-to-End Industrial Hemp Supply Chain"
date: 2024-09-17
tags: []
author: Keqi Wang | Wencen Wu | Wei Xie | Jinxiang Pei | Qi Zhou
institutions: 
doi: "https://doi.org/10.48550/arxiv.2008.04998"
---


arXivLabs: experimental projects with community collaborators  arXivLabs is a framework that allows collaborators to develop and share new arXiv features directly on our website. Both individuals and organizations that work with arXivLabs have embraced and accepted our values of openness, community, excellence, and user data privacy. arXiv is committed to these values and only works with partners that adhere to them.
