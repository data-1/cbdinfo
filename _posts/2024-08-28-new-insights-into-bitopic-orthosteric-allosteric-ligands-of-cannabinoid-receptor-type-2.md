---
layout: post
title: "New Insights into Bitopic Orthosteric/Allosteric Ligands of Cannabinoid Receptor Type 2"
date: 2024-08-28
tags: [Allosteric regulation,Ligand (biochemistry),Cannabinoid receptor 2,G protein-coupled receptor,Docking (molecular),Functional selectivity,EC50,High-performance liquid chromatography,Receptor antagonist,Biochemistry,Cell biology]
author: Rebecca Ferrisi | Beatrice Polini | Caterina Ricardi | Francesca Gado | Kawthar A. Mohamed | Giovanna Baron | Salvatore Faiella | Giulio Poli | Simona Rapposelli | Giuseppe Saccomanni
---


Experiments in cAMP assays highlighted that only JR22a behaves as a CB2R bitopic (dualsteric) ligand. Further, the results support that JR22a interacts with both orthosteric and allosteric sites (i.e., bitopic), thus reflecting the same bitopic mode of orthosteric/allosteric interaction previously established for the CB2R heterobivalent bitopic ligand FD22a [10]. Computational Studies  Starting from our previous definition of the potential allosteric binding site of CB2R and from docking results obtained for the compounds of the FD series [10], we conducted a computational study with the aim of validating the binding pose of our bitopic compounds in CB2R and adding new details on the structure-activity relationship for the bitopic behavior of our CB2R ligands. [Google Scholar] [CrossRef] [PubMed] [Green Version]  Valant, C.; Sexton, P.M.; Christopoulos, A. Orthosteric/allosteric bitopic ligands: Going hybrid at GPCRs. Bitopic orthosteric/allosteric ligands of g protein-coupled receptors.

[Visit Link](https://www.mdpi.com/1422-0067/24/3/2135){:target="_blank rel="noopener"}

