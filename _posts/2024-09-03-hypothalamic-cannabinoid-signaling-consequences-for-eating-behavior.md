---
layout: post
title: "Hypothalamic cannabinoid signaling: Consequences for eating behavior"
date: 2024-09-03
tags: [Endocannabinoid system,Leptin,Cannabinoid receptor 1,Lateral hypothalamus,Cannabinoid,Hypothalamus,Neuropeptide Y,Melanin-concentrating hormone,Agouti-related peptide,Adipose tissue,Serotonin,Orexin,Synapse,Eating,Ghrelin,Γ-Aminobutyric acid,Neurochemistry,Neurophysiology,Biochemistry]
author: Magen N. Lord | Emily E. Noble
---


Neuron. Neuron. Neuron. Neuron. Neuron.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11331011/){:target="_blank rel="noopener"}

