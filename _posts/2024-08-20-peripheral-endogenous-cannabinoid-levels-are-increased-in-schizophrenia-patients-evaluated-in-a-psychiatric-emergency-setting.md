---
layout: post
title: "Peripheral Endogenous Cannabinoid Levels Are Increased in Schizophrenia Patients Evaluated in a Psychiatric Emergency Setting"
date: 2024-08-20
tags: [Schizophrenia,2-Arachidonoylglycerol,Anandamide,Psychosis,Mental disorder,Antipsychotic,Liquid chromatography–mass spectrometry,Psychiatry,Schizoaffective disorder,Cannabinoid receptor 1,Metabolic syndrome,Fatty-acid amide hydrolase 1,Stress (biology),Substance abuse,Major depressive disorder,Health,Clinical medicine]
author: Charles-Édouard
---


In a study of 20 schizophrenia patients, the blood levels of anandamide were shown to be higher in patients with acute schizophrenia compared to healthy controls (26). A first study showed significantly higher levels of anandamide in patients compared to healthy controls (26), although it only included a small sample of 12 patients. In schizophrenia, most studies on CSF and blood levels of OEA have shown no significant alterations (22, 23, 25). Freshly thawed plasma aliquots and calibration curve standards (450 μL) were diluted with 900 μL of cold acetonitrile containing 10 ng/ml of the internal standard, AEA-d8 (Cayman Chemical, Ann Arbour, MI). The results of the current study show that plasma anandamide and OEA levels are significantly increased in schizophrenia patients evaluated in the psychiatric emergency setting.

[Visit Link](https://www.frontiersin.org/journals/psychiatry/articles/10.3389/fpsyt.2020.00628/full){:target="_blank rel="noopener"}

