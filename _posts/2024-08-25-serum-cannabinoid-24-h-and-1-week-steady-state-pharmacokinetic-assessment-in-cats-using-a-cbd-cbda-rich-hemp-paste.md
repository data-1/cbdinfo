---
layout: post
title: "Serum Cannabinoid 24 h and 1 Week Steady State Pharmacokinetic Assessment in Cats Using a CBD/CBDA Rich Hemp Paste"
date: 2024-08-25
tags: [Cannabidiol,Tetrahydrocannabinolic acid,Cannabinoid,Tetrahydrocannabinol,Cannabis,Dose (biochemistry)]
author: Nathalie L | Wayne S | Joseph J
---


Of these cannabinoids the cannabis plant produces predominantly cannabidiolic acid (CBDA) and tetrahydrocannabinolic acid (THCA), which during heat extraction becomes decarboxylated to cannabidiol (CBD) and Δ9-tetrahydrocannabinol (THC). Considering the absorption and retention kinetics of CBDA and THCA are unknown and may have potential value medicinally, the objective of this study was to perform 24-h pharmacokinetics of a CBD/CBDA-rich hemp extract containing minor cannabinoids including THC, THCA, cannabigerol (CBG) and cannabigerolic acid (CBGA) in cats. Therefore, based on physical examination data, our dosage of 1.37 mg/kg of CBD and 1.13 mg/kg of CBDA compared to that of Deabold et al. (1 mg/kg of CBD and 1 mg/kg of CBDA) and Kulpa et al. (2.8 mg/kg of CBD) appears to be a range of safe tolerable dosage for cats with superior absorption (15, 16). The serum CBD concentration at the 2-h time in our study was similar to that of Kulpa et al. (280 vs. 256 ng/mL, respectively), despite that CBD dosage in Kulpa et al. was 18 times greater than our study (15). Greater plasma cannabinoid and metabolite levels after administration of the CBD/THC mixture were found compare to CBD, or THC alone, suggesting a potential pharmacokinetic interaction between CBD and THC known as the “entourage effect” (15).

[Visit Link](https://www.frontiersin.org/journals/veterinary-science/articles/10.3389/fvets.2022.895368/full){:target="_blank rel="noopener"}

