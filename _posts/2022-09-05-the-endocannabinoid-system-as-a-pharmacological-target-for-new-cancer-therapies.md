---
layout: post
title: "The Endocannabinoid System as a Pharmacological Target for New Cancer Therapies"
date: 2022-09-05
categories:
- Endocannabinoid
author: Ramer, Wittig, Hinz, Robert Ramer, Felix Wittig, Burkhard Hinz
tags: [Cannabinoid,Angiopoietin,Cannabinoid receptor type 2,Metastasis,Angiogenesis,2-Arachidonoylglycerol,Tetrahydrocannabinol,Cancer,Cannabinoid receptor,Epithelialmesenchymal transition,Toll-like receptor 4,Tumor microenvironment,Enzyme inhibitor,Cancer staging,MMP2,HER2neu,Angiogenesis inhibitor,Vascular endothelial growth factor,Endocannabinoid system,MTORC1,Cannabinoid receptor type 1,Protein kinase B,Cell biology,Biotechnology,Diseases and disorders,Neurochemistry,Clinical medicine,Neoplasms,Biochemistry,Medical specialties]
---


In addition, recent studies have shown that the induction of autophagy is associated with these apoptotic effects and is involved in the toxicity of cannabinoid compounds to cancer cells. Further evidence for autophagy inductions was found for THC in melanoma cells [ 112 ] and for CBD in various cancer entities, such as glioma tumour cells [ 113 ], glioma stem-like cells [ 114 ], breast cancer [ 115 ], lymphoblastic leukaemia [ 116 ] and head and neck squamous cell carcinoma cells [ 117 ]. The inhibition of tumour cell growth by FAAH inhibition was later also shown in melanoma cells. Similarly, using a lung tumour xenograft model, it was shown that both URB597 and AA-5HT did not significantly affect tumour growth when the compounds were administered alone at a dose of 10 mg/kg every 72 h for 28 days [ 123 ]. However, not all studies could support these positive effects of MAGL knockdown on cancer progression.

<hr>

[Visit Link](https://www.mdpi.com/2072-6694/13/22/5701/htm){:target="_blank" rel="noopener"}


