---
layout: post
title: "Cannabinoid Receptor 2 Alters Social Memory and Microglial Activity in an Age-Dependent Manner"
date: 2024-08-20
tags: [Endocannabinoid system,Microglia,Cannabinoid receptor 2,Inflammaging,Immune system,Aging brain,Cell biology,Biology]
author: Joanna Agnieszka Komorowska-Müller | Tanushka Rana | Bolanle Fatimat Olabiyi | Andreas Zimmer | Anne-Caroline Schmöle | Komorowska-Müller | Joanna Agnieszka | Bolanle Fatimat | Anne-Caroline
---


Microglia from CB2R−/− mice show enhanced lipofuscin accumulation in comparison to WT mice, which resulted in an interaction effect. Expression of arg1 (D) did not differ between age groups and genotypes, whereas nos2 expression (E) decreased with age. Microglia morphology differs between CB2R−/− mice and WT mice. Aging 2008, 29, 1894–1901. Aging 2015, 36, 710–719.

[Visit Link](https://www.mdpi.com/1420-3049/26/19/5984){:target="_blank rel="noopener"}

