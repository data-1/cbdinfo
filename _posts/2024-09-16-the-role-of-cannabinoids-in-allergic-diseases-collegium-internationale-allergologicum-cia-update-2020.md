---
layout: post
title: "The Role of Cannabinoids in Allergic Diseases: Collegium Internationale Allergologicum (CIA) Update 2020"
date: 2024-09-16
tags: [Cannabinoid receptor 2,Cannabinoid,Anandamide,2-Arachidonoylglycerol,Endocannabinoid system,Neurochemistry,Biology,Biochemistry,Cell biology]
author: Alba Angelina | Mario Pérez‐Diego | Jacobo López‐Abente | Óscar Palomares
institutions: 
doi: "https://karger.com/iaa/article/181/8/565/168447/The-Role-of-Cannabinoids-in-Allergic-Diseases"
---


AEA, 2-AG, THC, and HU210 have been described as GPR55 ligands [51]. The cannabinoid receptor-2 is involved in allergic inflammation  . Cell. Cell. Cells  .