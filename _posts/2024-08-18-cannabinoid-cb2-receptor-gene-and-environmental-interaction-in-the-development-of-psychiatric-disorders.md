---
layout: post
title: "Cannabinoid CB2 Receptor Gene and Environmental Interaction in the Development of Psychiatric Disorders"
date: 2024-08-18
tags: [Cannabinoid receptor 2,Hypothalamic–pituitary–adrenal axis,Stress (biology),Endocannabinoid system,Major depressive disorder]
author: Hiroki Ishiguro | Yasue Horiuchi | Koichi Tabata | Qing-Rong Liu | Tadao Arinami | Emmanuel S. Onaivi | Qing-Rong | Emmanuel S
---


Reduced Fkbp5 and Nr3c1 gene expressions in the brain of the Cnr2 heterozygote knockout mice in comparison to wild type mice indicated the developed anxiety-like behavior by Poly I:C stress shown in rotated pulley test ( , and ), which are also useful as biomarkers, although the anxiety behavior shown in Zero maze test seemed not to be very distinct parallel to the alteration of Fkbp5 expression in mice brain ( , and ). Gene Expression Analysis  Fkbp5 gene expression, as biomarker of depression and stress-related disorders ( ), was analyzed in the hippocampus from the mice subjected to CMS and immunologically stressed (Poly I:C). [Google Scholar] [CrossRef] [PubMed]  Chen, L.; Li, S.; Cai, J.; Wei, T.J.; Liu, L.Y. [Google Scholar] [CrossRef] [PubMed]  Chen, J.; Wang, Z.Z. [Google Scholar] [CrossRef] [PubMed]  Wang, Q.; Dong, X.; Wang, Y.; Liu, M.; Sun, A.; Li, N.; Lin, Y.; Geng, Z.; Jin, Y.; Li, X. Adolescent escitalopram prevents the effects of maternal separation on depression- and anxiety-like behaviours and regulates the levels of inflammatory cytokines in adult male mice.

[Visit Link](https://www.mdpi.com/1420-3049/23/8/1836){:target="_blank rel="noopener"}

