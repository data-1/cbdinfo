---
layout: post
title: "Synaptic changes induced by cannabinoid drugs and cannabis use disorder"
date: 2024-08-25
tags: [Cannabinoid,Cannabis,Drug,Neuroscience,Neurophysiology,Psychoactive drugs]
author: 
---


Under a Creative Commons license  open access  Highlights  •  Summarizes research on the effects of cannabinoid drugs on synaptic transmission spanning animal and human research  •  Organizes these research findings into acute, subacute and chronic exposure effects on synaptic transmission and plasticity  •  Advocates for more pre-clinical and clinical research given the increase in cannabis and cannabinoid drug use  Abstract  The legalization of cannabis in many countries, as well as the decrease in perceived risks of cannabis, have contributed to the increase in cannabis use medicinally and recreationally. Like many drugs of abuse, cannabis and cannabis-derived drugs are prone to misuse, and long-term usage can lead to drug tolerance and the development of Cannabis Use Disorder (CUD). These drugs signal through cannabinoid receptors, which are expressed in brain regions involved in the neural processing of reward, habit formation, and cognition. This article also highlights the need for more research elucidating the neurobiological mechanisms associated with CUD and cannabinoid drug use. Previous article in issue  Next article in issue  Keywords  Cannabis sativa  Long-term potentiation  Long-term depression  Synaptic Modulation  Delta-9 tetrahydrocannabinol  Cannabinoid 1 receptor  Endocannabinoid  References  Cited by (0)  Published by Elsevier Inc.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S0969996122000614){:target="_blank rel="noopener"}

