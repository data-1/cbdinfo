---
layout: post
title: "Case Series of Synthetic Cannabinoid Intoxication from One Toxicology Center"
date: 2024-08-18
tags: [Synthetic cannabinoids,Cannabinoid,Psychoactive drugs,Health,Medical specialties,Clinical medicine,Medicine]
author: Kenneth D. Katz | Adam L. Leonetti | Blake C. Bailey | Ryan M. Surmaitis | Eric R. Eustice | Sherri Kacinko | Scott M. Wheatley
---


Although he developed aspiration pneumonia, he was treated and discharged uneventfully on hospital day (HD) six. The patient was found unresponsive; she was endotracheally intubated and transferred to a TCICU. He was transferred to the TCICU where clinical examination was consistent with anoxic brain injury. These 11 patients were all exposed to the SC, MAB-CHMINACA, and presented to the ED with significant, life-threatening toxicity, ranging from obtundation to severe agitation, seizures and death. Nine of 11 patients required endotracheal intubation for either control of severe agitation or decreased responsiveness.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4899060/){:target="_blank rel="noopener"}

