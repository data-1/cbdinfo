---
layout: post
title: "Effects of Different N, P, and K Rates on the Growth and Cannabinoid Content of Industrial Hemp"
date: 2024-08-29
tags: [Cannabidiol,Cannabis,Cannabinoid,Hemp,Hydroponics,Cannabis sativa]
author: Xiuye Wei | Wei Zhou | Songhua Long | Yuan Guo | Caisheng Qiu | Xinlin Zhao | Yufu Wang | Xinlin  Zhao | Yufu  Wang | Xiuye  Wei
---


As N input is one of the key factors for plant development and function, extensive studies have been focused on the impact of N supply on plant yield (Bouchet et al. Citation2016; Leghari et al. Citation2016). Effects of different N, P, and K rates on industrial hemp growth and biomass. Effects of different N, P, and K rates on the CBD yield of industrial hemp. Differing from N, increasing P nutrition could well improve the plant height, but not significantly enhance the biomass (Aubin et al. Citation2015), excessive P may reduce the biomass of the crop and cause a potential source of environmental pollution (Bevan, Jones, and Zheng Citation2021; Chen et al. Citation2017), a possible explanation in our result is that the P absorption could be selected since the P nutrition was significantly negatively correlated with almost all parameters (). Our data presented that A7 (N8P1K5) had positive effects on the synthesis and production of cannabinoids THC and CBD in the inflorescence and leaves of hemp (), thus, we speculate that high N promotes the accumulation of CBD, which stimulates CBD yield formation under various nutrition conditions.

[Visit Link](https://www.tandfonline.com/doi/full/10.1080/15440478.2022.2159605){:target="_blank rel="noopener"}

