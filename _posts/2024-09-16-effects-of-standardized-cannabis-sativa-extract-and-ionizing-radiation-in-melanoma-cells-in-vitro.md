---
layout: post
title: "Effects of standardized Cannabis sativa extract and ionizing radiation in melanoma cells in vitro"
date: 2024-09-16
tags: [Radiation therapy,Tetrahydrocannabinolic acid,Cannabinoid,Necrosis,Melanoma,Tetrahydrocannabinol,Cannabis,Medical specialties,Neoplasms,Cancer,Diseases and disorders,Clinical medicine,Biology]
author: Jamal Naderi | Nasim Dana | Nasrin Zare | Alireza Amooheidari | Maryam Yahay | Shaghayegh Haghjooy Javanmard
institutions: Isfahan University of Medical Sciences
doi: "https://doi.org/10.4103/jcrt.jcrt_1394_16"
---


[16] This study was performed to find whether C. sativa extract treatment alone or in combination with single radiation dose (6 Gy) may reduce the cell viability paralleled by increased cell death in mouse melanoma cells in vitro. We did not examine the effect of radiation alongside the C. sativa extract in melanoma cells since the combination treatment of the C. sativa extract + radiation did not produce a synergistic reduction in the cell viability, and radiation alone also did not have any significant effects on the cell viability of melanoma cells. Flow cytometry data show that C. sativa extract at 6.25 μg/mL for 72 h significantly induces necrosis. This result supports the data from MTT assay, which C. sativa extract reduces the cell viability paralleled by an increase in the cell death. Further study is required to find whether the active components in C. sativa or lower doses of C. sativa extract may induce apoptosis in melanoma cells in vitro.
