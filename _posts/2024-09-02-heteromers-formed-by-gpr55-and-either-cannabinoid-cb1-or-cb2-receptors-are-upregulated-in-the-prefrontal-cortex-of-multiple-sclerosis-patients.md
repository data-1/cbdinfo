---
layout: post
title: "Heteromers Formed by GPR55 and Either Cannabinoid CB1 or CB2 Receptors Are Upregulated in the Prefrontal Cortex of Multiple Sclerosis Patients"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Cannabinoid,Multiple sclerosis,Cannabinoid receptor 1]
author: Carlota Menéndez-Pérez | Rafael Rivas-Santisteban | Eva del Valle | Jorge Tolivia | Ana Navarro | Rafael Franco | Eva Martínez-Pinilla | Menéndez-Pérez | Rivas-Santisteban | del Valle
---


Expression of CB1R-GPR55 and CB2R-GPR55 Heteromers in Neurons and Different Types of Glial Cells  The final experimental approach was designed to identify those cells in the human prefrontal cortex that expressed CB1R-GPR55 or CB2R-GPR55 complexes. Considering the ability of cannabinoid receptors to form heteromers that may constitute therapeutic targets, as already postulated for neurodegenerative diseases such as Alzheimer’s disease and Parkinson’s disease [30,58], this work aimed to adequately characterize the formation of complexes between CB1 or CB2 and GPR55 receptors in the CNS and to evaluate whether the expression of these receptor complexes is affected in MS. The results presented herein constitute the first description of CB1R-GPR55 and CB2R-GPR55 heteromers in the human prefrontal cortex of control individuals and patients with MS. By taking advantage of the PLA technique and immunohistochemistry using antibodies against neuronal and glial markers, the expression of these receptor complexes was confirmed in neurons of the cerebral cortex, both in GM and WM, but not in glial cells labeled with antibodies against αβ-crystallin, GFAP or Iba-1. participated in the data analysis and preparing the final figures; C.M.-P., E.M.-P. and R.R.-S. performed many of the imaging assays using the confocal microscope, took photographs, and participated in the data analysis; E.M.-P. and R.F. [Google Scholar] [CrossRef]  Ford, H. Clinical presentation and diagnosis of multiple sclerosis.

[Visit Link](https://www.mdpi.com/1422-0067/25/8/4176){:target="_blank rel="noopener"}

