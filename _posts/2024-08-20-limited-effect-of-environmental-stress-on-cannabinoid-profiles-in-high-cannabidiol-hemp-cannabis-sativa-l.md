---
layout: post
title: "Limited effect of environmental stress on cannabinoid profiles in high‐cannabidiol hemp (Cannabis sativa L.)"
date: 2024-08-20
tags: [Cannabis,Tetrahydrocannabinolic acid,Cannabinoid,Plant hormone,Cannabidiol,Zygosity,Herbicide,Tetrahydrocannabinol,Plant breeding,Plants,Botany]
author: K. N | L. J | S. J | R. M | S. O | S. B | J. E | van Bakel | K. U | J. M
---


It is conceivable that allelic variation among CBDA synthases or expression of other cannabinoid synthases could lead to altered CBD:THC ratios. Most studies to date on the effect of stresses on cannabinoid production have focused on cannabinoid chemotype I and II plants grown under controlled environment conditions. The effect of stress on field-grown high-cannabinoid chemotype III hemp plants is not well understood and of great potential importance for production systems. Ethephon has also been used to induce genetically male plants to produce female flowers (Ram & Jaiswal, 1970). Shoot tips were sampled for cannabinoid extraction and analysis immediately prior to application of the stress treatments and again in 1-week intervals for 3 weeks (September 14, 22, 29, October 6, 2019) for a total of four sampling times.

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1111/gcbb.12880){:target="_blank rel="noopener"}

