---
layout: post
title: "Beta-Caryophyllene Exhibits Anti-Proliferative Effects through Apoptosis Induction and Cell Cycle Modulation in Multiple Myeloma Cells"
date: 2022-11-27
categories:
- Cannabinoid
author: Mannino, Pallio, Corsaro, Minutoli, Altavilla, Vermiglio, Allegra, Eid, Ali H., Bitto
tags: [Cannabinoid receptor type 2,Cannabinoid receptor type 1,Inflammatory bowel disease,NF-B,Nociceptin,Cell signaling,Signal transduction,Cannabinoid receptor,Opioid receptor,Nonsteroidal anti-inflammatory drug,Dronabinol,Cannabinoid,Receptor (biochemistry),Opioid,Extracellular signal-regulated kinases,Neuroscience,Cell communication,Physiology,Neurophysiology,Neurochemistry,Biochemistry,Cell biology]
---


The RPMI 1788 cell line, used as normal cells, was treated with BCP at concentrations of 50 and 100 μM for 24 h, thus demonstrating that BCP did not affect the proliferation of normal cells ( Figure 3 ) and confirming the MTT results and BCP-selective effect in the MM cell lines. The results of the MTT assay showed that MM.1R cell viability was reduced when cells were treated with BCP at concentrations of 25 μM to 200 μM; in particular, cell viability was reduced to about 80% when BCP was used at a concentration of 50 μM and to 50% with 100 μM BCP following 24 h of treatment ( Figure 1 A). BCP also proved effective in MM.1S cells and the more resistant MM.1R cell line at concentrations of 50 μM and 100 μM, thus reducing the cell viability of cancer cells and not affecting normal cells, as demonstrated by the MTT assay and trypan blue staining; in particular, the fluorescent marking with FDA confirmed the MTT results and demonstrated that BCP significantly reduced the number of viable cells. Cannabinoids were able to induce apoptosis in melanoma, glioma, breast cancer, and MM cells through a molecular mechanism that provides for Akt modulation, which is one of the most strongly involved pathways in response to cannabinoid receptor stimulation [ 15 36 ]. BCP’s anti-proliferative effect was established by the results obtained from Wnt1 and β-catenin expression: a significant decrease was observed in the treated MM.1R and MM.1S cell lines compared with untreated cancer cells.

<hr>

[Visit Link](https://www.mdpi.com/2077-0383/11/22/6675){:target="_blank" rel="noopener"}


