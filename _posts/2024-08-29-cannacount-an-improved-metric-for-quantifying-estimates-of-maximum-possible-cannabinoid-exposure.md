---
layout: post
title: "CannaCount: an improved metric for quantifying estimates of maximum possible cannabinoid exposure"
date: 2024-08-29
tags: [Cannabidiol,Tetrahydrocannabinol,Cannabinoid,Cannabis (drug),Medical cannabis,Cannabis,Health,Entheogens,Drugs,Cannabaceae,Individual psychoactive drugs,Pharmacology,Psychoactive drugs]
author: Ashley M | Kelly A | M. Kathryn | El-Abboud | Rosemary T | Staci A
---


To demonstrate feasibility and applicability, we utilized CannaCount to generate estimated maximum THC and CBD exposure in a sample of medical cannabis patients enrolled in a longitudinal, observational study who reported using a variety of real-world cannabinoid-based products. Calculating THC and CBD exposure  After using the CannaCount guide (Steps 1–2) to gather information for each of the variables noted above (i.e., product type, cannabinoid concentrations, duration of time product is used, frequency, and amount of product used), THC and CBD exposure can be calculated using either the discrete use approach or the total use approach (see CannaCount guide Step 3). CannaCount was developed to provide a standardized tool for calculating and quantifying estimated maximum possible exposure to individual cannabinoids, which helps address challenges regarding accurate estimation of cannabinoid exposure. As noted, information required for calculating THC and CBD exposure was available for 150 of the 190 (78.95%) patient visits completed at the time of analyses. While information about actual cannabinoid concentrations is the most important for accurately estimating cannabinoid exposure, it is also the most challenging data to collect.

[Visit Link](https://www.nature.com/articles/s41598-023-32671-9){:target="_blank rel="noopener"}

