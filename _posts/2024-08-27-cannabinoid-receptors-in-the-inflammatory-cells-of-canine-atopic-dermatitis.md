---
layout: post
title: "Cannabinoid receptors in the inflammatory cells of canine atopic dermatitis"
date: 2024-08-27
tags: [Macrophage,Dendritic cell,Cannabinoid receptor 2,TRPV1,Cannabinoid,Immune system,Endocannabinoid system,Cannabidiol,Inflammation,Itch,TRPA1,T cell,Lymphocyte,T helper cell,Cytokine,Interleukin 13,Neutrophil,Antibody,Melanocortin 1 receptor,Monocyte,Nerve growth factor,Thymic stromal lymphopoietin,Immunostaining,Innate immune system,Medical specialties,Biology,Immunology,Biochemistry,Cell biology]
author: De Silva | Rodrigo Zamith
---


Activated Th2 cells release IL-31 which stimulate itching by acting on IL-31 receptor A (IL-31RA) expressed on sensory nerve fibers and various immune cells, such as MCs, macrophages, DCs, eosinophils and basophils (20–26). The tissues from the AD-dogs were processed to obtain cryosections. However, since the co-localization study between the anti-CB2R and the -GPR55 antibodies showed that the inflammatory cells expressed both the markers (Supplementary Figure 4), it is plausible that CAL immunoreactive cells may also express CB2R-IR. In the present study, MCs expressed CB2R, GPR55, TRPV1, and TRPA1. The dendritic cells of the AD-dogs, although not differentiable from macrophages (since both cell types express IBA1 in dog skin) (51), showed immunoreactivity for all the receptors studied.

[Visit Link](https://www.frontiersin.org/journals/veterinary-science/articles/10.3389/fvets.2022.987132/full){:target="_blank rel="noopener"}

