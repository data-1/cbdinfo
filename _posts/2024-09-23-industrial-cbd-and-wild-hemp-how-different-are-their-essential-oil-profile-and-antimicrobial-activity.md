---
layout: post
title: "Industrial, CBD, and Wild Hemp: How Different Are Their Essential Oil Profile and Antimicrobial Activity?"
date: 2024-09-23
tags: [Cannabis,Cannabidiol,Gas chromatography–mass spectrometry,Cannabinoid,Caryophyllene]
author: Valtcho D. Zheljazkov | Vladimir Sikora | Ivayla Dincheva | Miroslava Kačániová | Tess Astatkie | Ivanka Semerdjieva | Dragana Četojević‐Simin
institutions: Institute of Field and Vegetable Crops | University of Novi Sad
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7587197/"
---


spontanea V.) accessions with 13 registered cultivars, eight breeding lines, and one cannabidiol (CBD) hemp strain belonging to C. sativa L. The first three groups had similar main essential oil (EO) constituents, but in different concentrations; the CBD hemp had a different EO profile. Essential oil constituents of registered hemp cultivars. Hemp–Cannabis sativa. [Google Scholar]  Articles from Molecules are provided here courtesy of Multidisciplinary Digital Publishing Institute (MDPI)  1. Hemp–Cannabis sativa.