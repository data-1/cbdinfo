---
layout: post
title: "A GPCR-based yeast biosensor for biomedical, biotechnological, and point-of-use cannabinoid determination"
date: 2024-08-25
tags: [Cannabinoid,JWH-018,G protein-coupled receptor,Biosensor,Cannabinoid receptor,Cannabinoid receptor 2,Receptor antagonist,Plate reader,Receptor (biochemistry),Biology,Biochemistry]
author: Lukas R | Iben E | Jens B | Sotirios C
---


The biosensor was able to detect both compounds. We constructed the luminescence reporter strain KM206 (Supplementary Table 1) by placing NanoLuc expression under the control of PFIG1 in the reporter module. Thus, we developed two portable biosensor devices, one for color measurements using the betalain reporter strain, and another for light measurement using the luminescence reporter strain, and developed workflows for the detection of cannabinoids in real-life samples. Whereas the colorimetric biosensor strain is well suited for the qualitative detection of cannabinoids from samples, some portable applications, such as regular monitoring of cannabinoids in samples from patients treated with medicinal cannabis, require the sensitive, semi-quantitative measurement of compounds and a shorter development time. Yeast strain construction  Yeast strains used in this study are listed in Supplementary Table 1.

[Visit Link](https://www.nature.com/articles/s41467-022-31357-6){:target="_blank rel="noopener"}

