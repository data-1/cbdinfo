---
layout: post
title: "Endocannabinoid System: Chemical Characteristics and Biological Activity"
date: 2023-02-27
categories:
- Endocannabinoid
author: Rezende, Alencar, Allan Kardec Nogueira, De Bem, Graziele Freitas, Fontes-Dantas, Fabrícia Lima, Montes, Guilherme Carneiro, Bismarck Rezende
tags: [Cannabinoid,Anandamide,Cannabinoid receptor,Tetrahydrocannabinol,Cannabinoid receptor type 2,2-Arachidonoylglycerol,Endocannabinoid system,Cannabinol,Cyclic adenosine monophosphate,Analgesic,Biochemistry,Neuroscience,Neurochemistry]
---


It involves a combination of cannabinoid receptors, endogenous cannabinoids (endocannabinoids), and enzymes responsible for the synthesis and degradation of endocannabinoids. The first studies started with the identification of receptors named type 1 and 2 cannabinoid receptors, or CB1R and CB2R [ 14 17 ]. Years later, CB1R was also cloned in human tissues [ 37 38 ] and mice [ 39 ], exhibiting 97–99% amino acid sequence identity between these species [ 40 ]. Studies involving the mapping of the rat brain suggest that the preferred location of CBR1 is in axons and nerve terminals and that its actions are related to the modulation of the release of neurotransmitters such as norepinephrine, dopamine, acetylcholine, glutamate, 5-hydroxytryptamine, γ-aminobutyric acid (GABA), and-aspartate [ 1 53 ]. In the gastrointestinal tract (GIT), CB1R is expressed both in the enteric nervous system and in non-neuronal cells such as the intestinal mucosa, including enteroendocrine cells, immune cells, and enterocytes.

<hr>

[Visit Link](https://www.mdpi.com/1424-8247/16/2/148){:target="_blank" rel="noopener"}


