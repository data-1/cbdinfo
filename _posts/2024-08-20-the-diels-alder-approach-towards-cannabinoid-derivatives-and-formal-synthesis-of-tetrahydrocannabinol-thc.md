---
layout: post
title: "The Diels-Alder Approach towards Cannabinoid Derivatives and Formal Synthesis of Tetrahydrocannabinol (THC)"
date: 2024-08-20
tags: [X-ray crystallography,Mineralogy,Physics,Chemical physics,Chemistry,Analytical chemistry,Phases of matter,Molecular physics,Crystallography,Physical sciences,Physical chemistry,Applied and interdisciplinary physics,Materials science,Condensed matter physics,Atomic, molecular, and optical physics,Materials,Atomic physics]
author: R. Mechoulam | Y. Gaoni | K. E. Fahrenholtz | M. Lurie | R. W. Kierstead | P. Braun | R. K. Razdan | H. C. Dalzell | G. R. Handrick | B. R. Martin
---


15 a: colorless crystals, C9H10O3, Mr=166.17, crystal size 0.30×0.10×0.05 mm, monoclinic, space group P21/c (No. 15 b: colorless crystals, C12H14O3, Mr=206.23, crystal size 0.50×0.30×0.20 mm, orthorhombic, space group Pbca (No. 16 a: yellow crystals, C13H18O3, Mr=222.27, crystal size 0.24×0.12×0.08 mm, monoclinic, space group P21/n (No. 17 a: colorless crystals, C13H16O3, Mr=220.26, crystal size 0.36×0.30×0.03 mm, triclinic, space group P-1 (No. 18 a: colorless crystals, C14H18O4, Mr=250.28, crystal size 0.30×0.20×0.15 mm, monoclinic, space group P21/n (No.

[Visit Link](https://chemistry-europe.onlinelibrary.wiley.com/doi/10.1002/open.202000343){:target="_blank rel="noopener"}

