---
layout: post
title: "Cannabis and cannabinoid medications for the treatment of chronic orofacial pain: A scoping review"
date: 2024-08-29
tags: [Cannabis,Cannabinoid,Oral medicine,Clinical medicine,Health,Medicine,Health care,Diseases and disorders,Medical specialties]
author: 
---


Data  We systematically screened for sources including a measure of effect of a cannabinoid compound on pain in COP patients that might be treated by our target specialists. Study Selection  Of 705 retrieved titles, 8 met inclusion/exclusion criteria and were included for review. Conclusions  Most sources concluded their respective cannabinoid treatments to provide some therapeutic benefit for COP (6 of 8) and all concluded their treatments to be safe. Patients and clinicians require more and higher quality evidence to make confident and informed decisions regarding treatment of COP with cannabis or cannabinoids. This review summarizes current evidence for patients, clinicians, and future researchers.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2772559623000019){:target="_blank rel="noopener"}

