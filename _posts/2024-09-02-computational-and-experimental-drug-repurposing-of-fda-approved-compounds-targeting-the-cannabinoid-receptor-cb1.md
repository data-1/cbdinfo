---
layout: post
title: "Computational and Experimental Drug Repurposing of FDA-Approved Compounds Targeting the Cannabinoid Receptor CB1"
date: 2024-09-02
tags: [Docking (molecular),2-Arachidonoylglycerol,Drug design,Ligand (biochemistry),Drug discovery,Virtual screening,Biochemistry,Biology]
author: Emanuele Criscuolo | Maria Laura De Sciscio | Angela De Cristofaro | Catalin Nicoara | Mauro Maccarrone | Filomena Fezza | De Sciscio | Maria Laura | De Cristofaro
---


Virtual Screening  To estimate the binding affinity of the investigated drugs towards CB1R, a molecular docking analysis was performed by means of the MOE (Molecular Operating Environment) software (Chemical Computing Group (CCG), Montreal, QC H3A 2R7, Canada). Drugs 1999, 57, 967–989. Drugs 2023, 83, 761–770. Cannabinoid Receptors and Their Ligands: Beyond CB1 and CB2. Drugs 2000, 9, 1553–1571.

[Visit Link](https://www.mdpi.com/1424-8247/16/12/1678){:target="_blank rel="noopener"}

