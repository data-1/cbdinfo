---
layout: post
title: "Influence of Fungicide Application on Rhizosphere Microbiota Structure and Microbial Secreted Enzymes in Diverse Cannabinoid-Rich Hemp Cultivars"
date: 2024-09-02
tags: [Ecosystem,Rhizosphere,Soil,Microbiota,Microorganism,Plant root exudates,Biodiversity,DNA sequencing,Soil ecology,Polymerase chain reaction,Dehydrogenase,Fungus,Cannabidiol,Enzyme,Biology]
author: Junhuan Xu | Tyson Knight | Donchel Boone | Muhammad Saleem | Sheree J. Finley | Nicole Gauthier | Joseph A. Ayariga | Rufus Akinrinlola | Melissa Pulkoski | Kadie Britt
---


by  Junhuan Xu  1,* ,  Tyson Knight  1,  Donchel Boone  1,  Muhammad Saleem  2 ,  Sheree J. Finley  3 ,  Nicole Gauthier  4 ,  Joseph A. Ayariga  1 ,  Rufus Akinrinlola  5,  Melissa Pulkoski  6,  Kadie Britt  7 ,  Tigist Tolosa  8,  Yara I. Rosado-Rivera  6,  Ibrahim Iddrisu  1 ,  Ivy Thweatt  1,  Ting Li  2 ,  Simon Zebelo  8,  Hannah Burrack  9,  Lindsey Thiessen  6,10,  Zachariah Hansen  5,11 ,  Ernest Bernard  5,  Thomas Kuhar  Thomas Kuhar  Thomas Kuhar, Ph.D., has been working as a Professor in Department of Entomology at (Virginia and [...]  Thomas Kuhar, Ph.D., has been working as a Professor in Department of Entomology at (Virginia Polytechnic Institute and State University) since 2008. The Abundance of Microbial Communities in Hemp Rhizosphere across Hemp Cultivars  The number of phyla that showed significant differences (P < 0.05) in the abundance of microbial communities among the three treatments in each of the four diverse cannabinoid-rich hemp cultivars is as follows: 14, 18, 15, and 4 in cultivars Otto II, BaOx, Wife, and Cherry Citrus, respectively ( ). The Abundance of Microbial Communities in Hemp Rhizosphere of Different Cultivars across Fungicide Treatments  The number of phyla that showed significant differences (P < 0.05) in the abundance of microbial communities among the four diverse cannabinoid-rich hemp cultivars in each of the three treatments is as follows: 19, 22, and 9 phyla in the treatments of fungal inoculation, fungicide treatment, and natural infection, respectively ( ). This figure provides insights into distinct bacterial populations in the rhizosphere of different hemp cultivars under varied treatment conditions, highlighting significant variations in bacterial abundance among cultivars across the treatments. Soil.

[Visit Link](https://www.mdpi.com/1422-0067/25/11/5892){:target="_blank rel="noopener"}

