---
layout: post
title: "In planta Female Flower Agroinfiltration Alters the Cannabinoid Composition in Industrial Hemp (Cannabis sativa L.)"
date: 2024-08-25
tags: [Cannabidiol,RNA interference,Agroinfiltration,Tetrahydrocannabinolic acid synthase,Cannabinoid,Cannabis,Molecular cloning,Real-time polymerase chain reaction,Gene expression,High-performance liquid chromatography,Tetrahydrocannabinol,Tetrahydrocannabivarin,Tetrahydrocannabinolic acid,Vector (molecular biology),Cannabinol,Gene,Health sciences,Biotechnology,Molecular biology,Life sciences,Genetics,Biochemistry,Biology]
author: Wayne R
---


The gene expression of two cannabinoid pathway genes, CBDAS and THCAS was measured using qRT-PCR (Deguchi et al., 2021). Notably, non-functional sequences present in the hemp genome were not isolated from any of THCAS, CBDAS and CBCAS, implying that truncated cannabinoid synthase genes are not expressed or expressed at an exceptionally low level. FIGURE 6  Cannabinoid Contents in Agroinfiltrated Female Flowers  In hemp female flowers, Δ9-THCA, CBDA, CBD, CBGA were detected, and Δ9-THC, Δ8-THC, CBG and CBC were below the limit of quantification. In this work, this protocol was applied to in planta agroinfiltration on female flowers, leading to a drastic increase in CBDAS expression (Figure 6A) and 54% higher CBDA content than control plants (Figure 7A). Moreover, in planta agroinfiltration to female flowers was highly effective to silence the THCAS gene via RNA interference (Figure 7B).

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2022.921970/full){:target="_blank rel="noopener"}

