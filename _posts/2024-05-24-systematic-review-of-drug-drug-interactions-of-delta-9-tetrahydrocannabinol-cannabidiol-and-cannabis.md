---
layout: post
title: "Systematic review of drug-drug interactions of delta-9-tetrahydrocannabinol, cannabidiol, and Cannabis"
date: 2024-05-24
categories:
- 
author: Jeffrey D, Paul T, Raup-Konsavage, Wesley M, Kent E
tags: [Cannabidiol,Cannabinoid,Pharmacogenomics,Tricyclic antidepressant,Warfarin,Cannabis (drug),Dronabinol,Medical cannabis,Valproate,Prothrombin time,Drug interaction,Medication,Dose (biochemistry),Cytochrome P450,Tetrahydrocannabinol,Adverse effect,Tacrolimus,Adverse drug reaction,Systematic review,CYP2C19,Pharmacology,Anticonvulsant,Anticoagulant,Clinical medicine,Drugs,Health care,Medicine,Health,Medical treatments,Chemicals in medicine,Medical specialties,Medicinal chemistry]
---


With this expanding use of cannabis and cannabinoids, clinicians, researchers, and patients require a better understanding of the potential for cannabinoids to interact with other medications through drug-drug interactions to produce unintended side effects and/or adverse drug reactions (ADRs). Anticoagulants  Seven case reports identified interactions between cannabinoids and warfarin; six out of seven subjects required warfarin dose adjustments and two experienced adverse effects from concomitant cannabis use. The authors did not report any interactions nor adverse events with valproate or phenobarbital and CBD ingestion. 61.5% (n = 8/13) and 46.2% (n = 6/13) of the studies reported drug and cannabinoid pharmacokinetics, respectively. Six out of seven case reports (85.7%) describe variable INR when patients on warfarin ingested cannabis.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2024.1282831/full){:target="_blank" rel="noopener"}


