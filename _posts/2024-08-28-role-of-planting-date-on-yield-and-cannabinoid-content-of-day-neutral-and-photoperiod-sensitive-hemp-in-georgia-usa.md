---
layout: post
title: "Role of Planting Date on Yield and Cannabinoid Content of Day-neutral and Photoperiod-sensitive Hemp in Georgia, USA"
date: 2024-08-28
tags: [Cannabis,Cannabis sativa,Hemp,Plants,Botany,Agriculture,Plants and humans,Cannabaceae,Entheogens,Kingdoms (biology),Archaeplastida,Ethnobotany]
author: Timothy Coolong | Kate Cassity-Duffey | Noelle Joy
---


Abstract  Industrial hemp (Cannabis sativa) cultivars used for flower, fiber, or seed production are usually considered short-day plants and flower in response to photoperiod. In 2021, 33,500 acres of hemp were planted for open field production in the United States, with ∼16,000 acres grown for the floral market and the remainder grown for seed and fiber production (USDA 2022). The juvenile phase has been reported to be as short as 13 d after emergence in some cultivars (Amaducci et al. 2008b). Although the impact of temperature and photoperiod has been investigated in hemp grown for fiber or seed production (Amaducci et al. 2008a, 2008b, 2012; Tang et al. 2016), there is less information on the impact of environmental conditions on flower yield in hemp. The objectives of this study were to determine if day-neutral hemp cultivars were suitable for planting in early spring in Georgia and how planting date may affect growth and development in hemp.

[Visit Link](https://journals.ashs.org/horttech/view/journals/horttech/33/1/article-p138.xml){:target="_blank rel="noopener"}

