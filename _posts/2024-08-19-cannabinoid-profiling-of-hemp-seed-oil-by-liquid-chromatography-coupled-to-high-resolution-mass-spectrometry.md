---
layout: post
title: "Cannabinoid Profiling of Hemp Seed Oil by Liquid Chromatography Coupled to High-Resolution Mass Spectrometry"
date: 2024-08-19
tags: [Cannabinoid,Cannabis,Mass spectral interpretation,Cannabidiol,High-performance liquid chromatography,Chromatography,Tetrahydrocannabinolic acid]
author: Maria Angela
---


FIGURE 1  Since very few works in the literature describe the fragmentation mechanism of the most common cannabinoids using an electrospray ionization source in both positive and negative mode, the first part of the work regarded the elucidation of the fragmentation patterns of the precursor ions [M+H]+ and [M–H]- of the cannabinoid standards (CBDA, CBGA, THCA, CBDV, CBD, CBG, CBN, Δ9-THC, Δ8-THC and CBC). Figure 7 shows an example of the total ion chromatograms of a hemp seed oil sample obtained in positive (A) and negative (B) ionization mode. FIGURE 7  In the present work, we report the identification of 32 cannabinoids in 10 commercial hemp seed oils obtained by organic farming. Such separation suggests that the chemical composition of the different hemp seed oils is different. Indeed, it is now a common belief that either THC or CBD alone are less effective than a combination of cannabinoids or of cannabinoids and other compounds in producing the final biological activity of hemp seed oil and other cannabis derived products (Crescente et al., 2018).

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2019.00120/full){:target="_blank rel="noopener"}

