---
layout: post
title: "Role of Caryophyllane Sesquiterpenes in the Entourage Effect of Felina 32 Hemp Inflorescence Phytocomplex in Triple Negative MDA-MB-468 Breast Cancer Cells"
date: 2022-09-01
categories:
- Cannabis
- terpene
author: Di Giacomo, Mariano, Gullì, Fraschetti, Vitalone, Filippi, Mannina, Scotto D Abusco, Di Sotto, Silvia Di Giacomo
tags: [Cannabinoid,Cannabis sativa,Cannabidiol,Cannabis,Gas chromatography,Caryophyllene,Tetrahydrocannabinol,Cannabinoid receptor type 2,Apoptosis,Cytotoxicity,Cannabinoid receptor type 1,Receptor (biochemistry),Biochemistry]
---


Considering the GC-MS results, the following concentrations of nonintoxicating phytocannabinoids and caryophyllane sesquiterpenes, in 50 μg/mL of FOJ and FOS, respectively, were tested: 4 and 17 μg/mL for cannabidiol, 0.2 and 6 μg/mL for cannabichromene, 0.05 and 0.07 μg/mL for α-humulene, 0.05 and 0.1 μg/mL for β-caryophyllene, and 0.03 and 0.1 μg/mL for β-caryophyllene oxide. β-Caryophyllene oxide was the less potent cytotoxic sesquiterpene in all the cancer cells tested: at the concentration of 50 µg/mL, it produced about a 45% and 60% cell viability lowering in Caco-2 and MDA-MB-468 cells respectively, with only slight effects in H358 cells ( Figure 1 G); by contrast, a higher than 80% inhibition was achieved at 100 µg/mL. Similarly, cancer cell viability was significantly lowered by pure compounds, with cannabidiol, cannabichromene, and β-caryophyllene being the most effective, especially in MDA-MB-468 cells. Our results highlighted that blocking CB1 receptors contributes to the inhibition of breast cancer cell growth, although these receptors were not targeted by our samples. Overall, present results highlight Felina32 hemp inflorescences to be a source of bioactive phytocomplexes, containing an interesting combination between nonintoxicating phytocannabinoids and caryophyllane sesquiterpenes to be exploited in cancer research and strengthen the importance of considering minor terpenes of the hemp metabolome, such as caryophyllane sesquiterpenes, due to their possible involvement in the inter-entourage effects.

<hr>

[Visit Link](https://www.mdpi.com/1420-3049/26/21/6688/htm){:target="_blank" rel="noopener"}


