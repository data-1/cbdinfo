---
layout: post
title: "Morphine Analgesia, Cannabinoid Receptor 2, and Opioid Growth Factor Receptor Cancer Tissue Expression Improve Survival after Pancreatic Cancer Surgery"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Opioid,Morphine,Pancreatic cancer,Analgesic,Cancer,Reverse transcription polymerase chain reaction,Μ-opioid receptor,Chemotherapy,Colorectal cancer,Clinical medicine,Medical specialties]
author: Lubomir Vecera | Petr Prasil | Josef Srovnal | Emil Berta | Monika Vidlarova | Tomas Gabrhelik | Pavla Kourilova | Martin Lovecek | Pavel Skalicky | Jozef Skarda
---


We also showed that piritramide opioid analgesia reduces the presence of CTCs in colon cancer patients after surgery, potentially affecting survival [13,14], and that the expression of the cannabinoid-2 receptor (CB2) in cancer tissues improves survival in small-cell lung cancer [15]. Cancer 2008, 122, 742–750. Cancers 2023, 15, 4038. https://doi.org/10.3390/cancers15164038  AMA Style  Vecera L, Prasil P, Srovnal J, Berta E, Vidlarova M, Gabrhelik T, Kourilova P, Lovecek M, Skalicky P, Skarda J, et al. Morphine Analgesia, Cannabinoid Receptor 2, and Opioid Growth Factor Receptor Cancer Tissue Expression Improve Survival after Pancreatic Cancer Surgery. Cancers. Morphine Analgesia, Cannabinoid Receptor 2, and Opioid Growth Factor Receptor Cancer Tissue Expression Improve Survival after Pancreatic Cancer Surgery Cancers 15, no.

[Visit Link](https://www.mdpi.com/2072-6694/15/16/4038){:target="_blank rel="noopener"}

