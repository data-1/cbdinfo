---
layout: post
title: "CB2 Cannabinoid Receptors Contribute to Bacterial Invasion and Mortality in Polymicrobial Sepsis"
date: 2022-11-27
categories:
- Cannabinoid
author: Balázs Csóka, Department Of Surgery, University Of Medicine, Dentistry Of New Jersey-New Jersey Medical School, Newark, New Jersey, United States Of America, Zoltán H. Németh, Morristown Memorial Hospital, Morristown
tags: [Cannabinoid receptor type 2,Interleukin 10,Immune system,Inflammation,Sepsis,Peritoneum,Apoptosis,Macrophage,White blood cell,Septic shock,Interleukin 6,Cytokine,Flow cytometry,T cell,Immunosuppressive drug,Cannabinoid receptor,Biochemistry,Medical specialties,Clinical medicine,Immunology,Cell biology,Biology]
---


Endocannabinoids that are produced excessively in sepsis are potential factors leading to immune dysfunction, because they suppress immune cell function by binding to G-protein-coupled CB 2 receptors on immune cells. As shows, the levels of IκBα were increased in the spleen of CB 2 KO as compared to WT mice, indicating decreased NF-κB activation in KO mice. We then determined the levels of macrophage-inflammatory protein-2 (MIP-2), a crucial chemokine that mediates inflammatory responses, in the plasma and peritoneal lavage fluid of CB 2 KO and WT mice subjected to CLP, and we found that CLP-induced concentrations of MIP-2 were diminished in CB 2 KO mice as compared with their WT counterparts when measured at 16 h after CLP ( ). To begin to study the role of CB 2 receptors, we first investigated the effect of CB 2 deficiency in CLP-induced septic peritonitis by monitoring the survival of CB 2 WT and KO mice. Using the CLP model of sepsis, we found that CB 2 receptor activation by endogenously released cannabinoids contributes to mortality, bacterial invasion, IL-10 production, and immune cell death in sepsis.

<hr>

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2712683/){:target="_blank" rel="noopener"}


