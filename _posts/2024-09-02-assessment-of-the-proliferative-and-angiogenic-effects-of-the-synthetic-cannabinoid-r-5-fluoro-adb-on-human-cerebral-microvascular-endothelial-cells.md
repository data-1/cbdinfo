---
layout: post
title: "Assessment of the proliferative and angiogenic effects of the synthetic cannabinoid (R)-5-fluoro ADB on human cerebral microvascular endothelial cells"
date: 2024-09-02
tags: [Angiogenesis,Angiopoietin,GSK-3,Vascular endothelial growth factor,Cannabinoid receptor 2,Cannabinoid receptor,Life sciences,Cell signaling,Biological processes,Biochemistry,Cell biology,Biology]
author: Laith Naser AL-Eitan | Saif Zuhair | Iliya Yacoub Khair | Mansour Abdullah Alghamdi
---


In the present study, we hypothesized that the synthetic cannabinoid (R)-5-Fluoro-ADB could modulate the endothelial cells’ viability and brain cells’ angiogenesis rate. Results  Synthetic cannabinoid (R)-5-fluoro-ADB increases cell viability of hBMECs  Cell viability following exposure to the synthetic cannabinoid (R)-5-Fluoro-ADB was assessed using the MTT assay. Synthetic cannabinoid (R)-5-Fluoro-ADB promotes the cell migration rate of hBMECs  Endothelial cell migration is a crucial and essential step to initiate angiogenesis. This study, has demonstrated significant effects on the angiogenesis of brain cells, as well as influencing various cellular processes such as protein and mRNA expression within the cells. Our results suggest that the proangiogenic factor VEGF may have a crucial role in facilitating the angiogenesis associated with cannabinoid receptor activation in the brain.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10849210/){:target="_blank rel="noopener"}

