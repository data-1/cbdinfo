---
layout: post
title: "Cannabidiol use and effectiveness: real-world evidence from a Canadian medical cannabis clinic - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Rapin, Research Department, Santé Cannabis, Montréal, Gamaoun, El Hage, Arboleda, Maria Fernanda, Prosk, Ste-Catherine O. Bureau
tags: [Cannabidiol,Medical cannabis,Tetrahydrocannabinol,Dose (biochemistry),Cannabinoid,Diseases and disorders,Health care,Health sciences,Clinical medicine,Medicine,Health]
---


This retrospective study explored the use of CBD-rich products in a medical cannabis clinical setting in Canada and associated effectiveness on a common symptom cluster presentation of pain, anxiety, depression, and poor sense of wellbeing, as measured by ESAS-r. In such patients, CBD treatments may have been targeted to other clinical symptoms not assessed in the current study. There is a probable placebo effect; however, there were no differences in initial CBD doses between the severity groups. Limitations  Limitations are common in real-world data (RWD), especially in retrospective studies. In this study, with no control group, no causality effect can be drawn between CBD-rich treatment and symptom improvement.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00078-w){:target="_blank" rel="noopener"}


