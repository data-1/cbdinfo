---
layout: post
title: "Efficient Synthesis for Altering Side Chain Length on Cannabinoid Molecules and Their Effects in Chemotherapy and Chemotherapeutic Induced Neuropathic Pain"
date: 2024-08-28
tags: [Cannabinoid,Tetrahydrocannabivarin,Cannabidiol,SCIEX,Mass spectrometry,Tetrahydrocannabinol]
author: Wesley M. Raup-Konsavage | Diana E. Sepulveda | Daniel P. Morris | Shantu Amin | Kent E. Vrana | Nicholas M. Graziane | Dhimant Desai | Raup-Konsavage | Wesley M | Diana E
---


by  Wesley M. Raup-Konsavage  1,† ,  Diana E. Sepulveda  1,2,†,  Daniel P. Morris  1,  Shantu Amin  1,  Kent E. Vrana  1,  Nicholas M. Graziane  1,2,* and  Dhimant Desai  1,*  1  Department of Pharmacology, Penn State College of Medicine, Hershey, PA 17033, USA  2  Department of Anesthesiology & Perioperative Medicine, Penn State College of Medicine, Hershey, PA 17033, USA  *  Authors to whom correspondence should be addressed. These compounds were then tested in an animal model of chemotherapeutic-induced neuropathic pain and to reduce colorectal cancer cell viability. [Google Scholar] [CrossRef] [PubMed]  Chemical, C. CBGV Cannabigerol. Biomolecules 2022, 12, 1869. https://doi.org/10.3390/biom12121869  AMA Style  Raup-Konsavage WM, Sepulveda DE, Morris DP, Amin S, Vrana KE, Graziane NM, Desai D. Efficient Synthesis for Altering Side Chain Length on Cannabinoid Molecules and Their Effects in Chemotherapy and Chemotherapeutic Induced Neuropathic Pain. 2022; 12(12):1869. https://doi.org/10.3390/biom12121869  Chicago/Turabian Style  Raup-Konsavage, Wesley M., Diana E. Sepulveda, Daniel P. Morris, Shantu Amin, Kent E. Vrana, Nicholas M. Graziane, and Dhimant Desai.

[Visit Link](https://www.mdpi.com/2218-273X/12/12/1869){:target="_blank rel="noopener"}

