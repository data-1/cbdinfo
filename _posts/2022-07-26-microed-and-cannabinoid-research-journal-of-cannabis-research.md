---
layout: post
title: "MicroED and cannabinoid research - Journal of Cannabis Research"
date: 2022-07-26
categories:
- cannabinoid
author: Filer, Crist N., Perkinelmer Health Sciences Inc., Waltham, Perkinelmer, Boston, Winter Street, Ma, Crist N. Filer, Albany St
tags: [X-ray crystallography,Nuclear magnetic resonance,Creative Commons license,Crystallography,Spectroscopy,Creative Commons,Physical sciences,Chemistry,Applied and interdisciplinary physics,Physical chemistry,Scientific techniques,Molecular physics]
---


Abstract MicroED has recently emerged as a convenient and powerful tool for the unequivocal structure determination of small molecules and it could likely be used in cannabinoid research as well. The structure of cannabidiol. Nannenga BL, Gonen T. The cryo-EM method microcrystal electron diffraction (MicroED). Nature Methods. Rosenqvist E, Ottersen T. The crystal and molecular structure of delta-9-tetrahydrocannabinolic acid B. Acta Chemica Scandinavica B.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00070-4){:target="_blank" rel="noopener"}


