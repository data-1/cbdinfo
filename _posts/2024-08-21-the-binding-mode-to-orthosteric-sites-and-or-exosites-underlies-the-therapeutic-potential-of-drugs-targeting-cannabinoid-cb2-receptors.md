---
layout: post
title: "The Binding Mode to Orthosteric Sites and/or Exosites Underlies the Therapeutic Potential of Drugs Targeting Cannabinoid CB2 Receptors"
date: 2024-08-21
tags: [Allosteric regulation,G protein-coupled receptor,Cannabinoid receptor 2,Ligand (biochemistry),Receptor (biochemistry),Functional selectivity,Cell signaling,Cannabinoid receptor 1,Agonist,Protein–protein interaction,Cannabinoid,G protein,Ligand binding assay,Biomolecules,Neurophysiology,Biology,Proteins,Nutrients,Biotechnology,Signal transduction,Macromolecules,Biophysics,Receptors,Molecular biology,Cell communication,Cell biology,Biochemistry,Neurochemistry]
author: Reyes-Resina
---


Although the potential of GPCRs as therapeutic targets is still considered to be high, there have been only a few recent approvals of drugs targeting these receptors. As would be expected from an allosteric mode of action, the binding of the compound to the allosteric site causes conformational changes in such a way that biases the effect of orthosteric agonists (Navarro et al., 2018a). In practice this means that if an allosteric compound is suspected on the basis of changes in affinity of radiolabeled compound to the orthosteric site, the orthosteric compound should modify the affinity of the binding of the allosteric compound to the allosteric site. Different Macromolecular Environments of the CB2R Impact agonist Binding and Effect  Can a given compound be more efficacious at targeting a cell that expresses CB2R in a particular conformation? Figure 2 shows the STRING analysis of the interactions of the receptor which indicates mandatory interactions with G proteins, and interactions with the CB1R and with other GPCRs.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2022.852631/full){:target="_blank rel="noopener"}

