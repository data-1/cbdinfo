---
layout: post
title: "Study Shows Cannabinoids May Help With Pathological Tremors"
date: 2022-07-25
categories:
- Cannabinoid
author: 
tags: [Astrocyte,Tremor,Cannabis (drug),Neuroscience,Clinical medicine,Nervous system,Medical specialties,Health,Medicine,Physiology]
---


In probing astrocytes to understand the biological effects of cannabis, the researchers take a novel approach as earlier studies have focused primarily on neurons.”  “One might imagine a new approach to medical cannabis for shaking, where you—during the development of cannabis-based medicinal products—target the treatment either at the spinal cord or the astrocytes—or, at best, the astrocytes of the spinal cord,” said Eva Carlsen, PhD, who worked on the project as part of her postdoctoral research. “Using this approach will avoid affecting the neurons in the brain responsible for our memory and cognitive abilities, and we would be able to offer patients suffering from involuntary shaking effective treatment without exposing them to any of the most problematic side effects of medical cannabis.”  Essentially, limb movements all start at the spinal cord, and that goes for voluntary and involuntary movements. This happens when the motor neurons in the spinal cord are activated to trigger the muscles. By looking into how motor neurons in the spinal cord work with cannabinoids, the research team was better able to understand the impacts of cannabis use and how it can help with tremors. This research is huge for the future of cannabis healing, as it shows new ways that the plant can impact those with specific, hard-to-treat conditions who are searching for relief.

<hr>[Visit Link](https://www.weedworthy.com/the-news/press-releases/study-shows-cannabinoids-may-help-with-pathological-tremors){:target="_blank" rel="noopener"}


