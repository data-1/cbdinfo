---
layout: post
title: "Cannabinoids as an Alternative Option for Conventional Analgesics in Cancer Pain Management: A Pharmacogenomics Perspective"
date: 2024-09-16
tags: [Cannabidiol,CYP2D6,Analgesic,Cannabinoid,Medical cannabis,Tetrahydrocannabinol,Opioid,Pharmacogenomics,Pain management,Nabilone,Dronabinol,Cannabinol,Paracetamol,Codeine,Cancer,Terminal illness,Health care,Drugs,Pharmacology,Health,Medical specialties,Medicine,Clinical medicine]
author: Anmi Jose | Levin Thomas | Gayathri Baburaj | Murali Munisamy | Mahadev Rao
institutions: Manipal Academy of Higher Education
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7017683"
---


This review organizes the current knowledge in the context of SNPs associated with opioids and nonopioids and its clinical consequences in pain management and pharmacogenetic targets of cannabinoids, for use in clinical practice. Multidimensional treatment of cancer pain. J Pain. Multidimensional treatment of cancer pain. J Pain.