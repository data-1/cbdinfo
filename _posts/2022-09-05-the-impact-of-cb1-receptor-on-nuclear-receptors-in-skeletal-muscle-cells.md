---
layout: post
title: "The Impact of CB1 Receptor on Nuclear Receptors in Skeletal Muscle Cells"
date: 2022-09-05
categories:
- cannabinoid
author: Haddad, Mansour Haddad
tags: [Cannabinoid receptor type 1,Cannabinoid receptor,Real-time polymerase chain reaction,Insulin resistance,Receptor (biochemistry),Myogenesis,Endocannabinoid system,Nuclear receptor 4A1,Cannabinoid receptor type 2,Cannabinoid,GLUT4,PDK4,Adipose tissue,Skeletal muscle,Glucose,Cell signaling,Insulin,Molecular biology,Biochemistry,Cell biology,Biotechnology,Biology,Cellular processes,Neurochemistry,Life sciences]
---


To our knowledge, it is the first research about the effect of ACEA and its cannabinoid CB1 receptor subtypes on nuclear receptors (NR4A) in skeletal muscles, and it characterizes a novel mechanism of signaling for the cannabinoid CB1 receptor’s role in the cells of skeletal muscles. In the current research, the functionality of CB1 receptor was examined by evaluating the effect of CB1 receptor agonism (ACEA) or antagonism (rimonabant) on the activation of significant genes (nuclear receptors; NR4A1, NR4A2 and NR4A3) that might be involved in mitogenic, inflammatory and metabolic functions in rat skeletal muscle cells. This conclusion does not conflict with a previous researcher [ 27 ], who found that—in adipose cells (3T3-L1 cells), but not in skeletal muscle—the activation of NR4A receptors is known to promote glucose utilization by enhancing the activity of insulin to stimulate glucose transport since this research study used a different cell line culture. In this current study, the activation of cannabinoid CB1 receptor has been found to enhance expression of NR4A in skeletal muscles. Because cannabinoid CB1 receptors increased NR4A mRNA gene expression in skeletal muscles and NR4A1 restrains inflammation, glucose transport, and insulin action [ 45 48 ], it is possible that cannabinoid CB1 receptors modulate skeletal muscle physiological roles including glucose and fatty acid metabolism.

<hr>

[Visit Link](https://www.mdpi.com/1873-149X/28/4/29/htm){:target="_blank" rel="noopener"}


