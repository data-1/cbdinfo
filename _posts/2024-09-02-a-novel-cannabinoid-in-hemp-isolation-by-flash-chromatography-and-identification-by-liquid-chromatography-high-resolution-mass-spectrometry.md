---
layout: post
title: "A novel cannabinoid in hemp: Isolation by flash chromatography and identification by liquid chromatography high resolution mass spectrometry"
date: 2024-09-02
tags: [Liquid chromatography–mass spectrometry,Chromatography,Techniques,Physical sciences,Analytical chemistry,Scientific techniques,Laboratory techniques,Chemistry,Instrumental analysis,Laboratories,Mass spectrometry,Spectrum (physical sciences),Mass,Analysis]
author: 
---


Abstract  A novel and major cannabinoid (epicannabidiol hydrate) present in hemp plants and oils was isolated and characterized by a combination of flash chromatography and liquid chromatography coupled with quadrupole time-of-flight mass spectrometry (LC/Q-TOF-MS). In this way, other major cannabinoids present in the samples, such as cannabidiol and Δ9-tetrahydrocannabinol, were separated and the focus was placed on the novel cannabinoid compound. Additional MS-MS analysis in negative ion mode revealed the position of the additional hydroxyl group in the molecule. She then worked as an Assistant Professor at the University of Almeria for 5 years, where her main focus was the development of advanced LC-MS methodologies for the analysis of pesticides in food samples, specially the use of time-of-flight techniques using accurate mass identification. She has more than 27 years experience on developing methods for emerging contaminants using LC-MS techniques.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2666831924000468){:target="_blank rel="noopener"}

