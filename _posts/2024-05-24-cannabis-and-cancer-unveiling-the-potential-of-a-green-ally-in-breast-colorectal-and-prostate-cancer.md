---
layout: post
title: "Cannabis and cancer: unveiling the potential of a green ally in breast, colorectal, and prostate cancer"
date: 2024-05-24
categories:
- 
author: Husam A, Sara Feras, Hazem Mohamed, Anas Hasan, Mohammad Fuad, Anas Zakarya, Aseel N
tags: [Cannabinoid,Apoptosis,Cannabinoid receptor 2,Cannabinol,HER2,Cannabinoid receptor,Autophagy,Endocannabinoid system,Anandamide,Reactive oxygen species,HTTP cookie,Cancer,Colorectal cancer,Metastasis,Cannabidiol,Tetrahydrocannabinol,Cannabinoid receptor 1,Biology,Clinical medicine,Cell biology]
---


A study found that ∆9-THC induces apoptosis and inhibits BC cell proliferation through the activation of CB2 receptor (Zhong et al. 2023). Study found that cannabinoid receptor agonists, JWH-133 and WIN-55,212-2, decreased cell viability and migration in BC cell lines, MDA-MB-231, and MDA-MB-468 cell lines (Khunluck et al. 2022). Jeong et al. conducted a study to demonstrate the effect of CBD on inducing autophagy in Oxaliplatin resistance colorectal cancer cell (CRC), they generated oxaliplatin-resistant cell lines, which didn’t respond to oxaliplatin treatment (Jeong et al. 2019). Anticancer effects of CBD against colorectal cancer  Effect on apoptosis  A recent study was conducted both in vivo and in vitro to demonstrate the mechanism of CBD in inducing apoptosis in colorectal cancer cells. To treat CRC metastasis, CBD can be used.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-024-00233-z){:target="_blank" rel="noopener"}


