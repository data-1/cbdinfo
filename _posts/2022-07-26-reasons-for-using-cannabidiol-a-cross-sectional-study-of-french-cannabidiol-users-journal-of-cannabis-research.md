---
layout: post
title: "Reasons for using cannabidiol: a cross-sectional study of French cannabidiol users - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Fortin, University Paris Sorbonne, Paris, Di Beo, Aix Marseille Univ, Inserm, Ird, Sesstim, Sciences Economiques, Sociales De La Santé
tags: [Cannabidiol,Tobacco smoking,Cannabis (drug),Creative Commons license,Smoking,Creative Commons,Health,Anxiety]
---


The sample was predominantly male (70%), with a median age of 36 (interquartile range 28–44) years (Table 1). Table 1 Sociodemographic and behavioral characteristics of cannabidiol users (n = 1166) and logistic regressions of cannabidiol users’ expected effects as outcomes in those who declared “well-being” to be the primary reason for using cannabidiol (n = 311) Full size table  In the sub-population which answered well-being (n = 311), the effects of CBD most expected by respondents were diminished stress (63% of that group), followed by improved sleep (60%), reduced anxiety/depression (43%), reduced pain or inflammation (41%), increased concentration (16%), and headache relief (16%). Over half the study sample (56%) had first used CBD more than a year prior to the survey, and half (50%) had used it at least 20 of the previous 30 days (Table 2). Table 2 Pattern of cannabidiol use in the whole study sample (n = 1166) Full size table  In the multivariable analysis performed on the whole sample (Table 1), in terms of tobacco cigarette smoking patterns and the likelihood of reporting well-being as the primary reason to use CBD, smokers were 27% less likely to report it (adjusted odds ratio (aOR) 95% confidence interval (CI): 0.73 [0.55–0.97], p = 0.031) than non-smokers, while e-cigarette users were 97% more likely to report it (1.97 [1.2–3.24], p = 0.007). Among those who reported well-being as their primary reason to use CBD, improved sleep was more frequently reported by individuals with lower self-reported income levels and those who were overweight, while individuals who did not use illegal cannabis and those with lower self-reported income levels were more likely to report reduced anxiety/depression as a specific expected effect of using CBD.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00102-z){:target="_blank" rel="noopener"}


