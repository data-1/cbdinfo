---
layout: post
title: "Cannabinoid Combination Induces Cytoplasmic Vacuolation in MCF-7 Breast Cancer Cells"
date: 2024-08-20
tags: [Paraptosis,Autophagy,Triple-negative breast cancer,Cannabinoid,Apoptosis,Unfolded protein response,Tumor microenvironment,MTORC1,Staining,Cannabinol,Lysosome,Interferon gamma,T helper cell,Endoplasmic reticulum,Biochemistry,Biology,Cell biology]
author: Recardia Schoeman | Natasha Beukes | Carminita Frost
---


Cells 2017, 171, 1678–1691.e1613. Cells 2019, 8, 1013. Cancer Cell 2019, 35, 17–32.e16. Cell 2005, 16, 2091–2105. Cell.

[Visit Link](https://www.mdpi.com/1420-3049/25/20/4682){:target="_blank rel="noopener"}

