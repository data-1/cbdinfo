---
layout: post
title: "Cannabis-derived food products in the New Era of Smarter Food Safety"
date: 2024-09-17
tags: []
author: Adam L. Friedlander
institutions: 
doi: "https://doi.org/10.1016/b978-0-12-818956-6.00009-9"
---


After defining and providing the legal foundation for cannabis, this chapter focuses on how the classification of cannabis impacts congressional and executive branch actions—including why cannabis is currently prohibited in food. Ultimately, the author outlines reasons and steps to building the future of cannabis food safety.