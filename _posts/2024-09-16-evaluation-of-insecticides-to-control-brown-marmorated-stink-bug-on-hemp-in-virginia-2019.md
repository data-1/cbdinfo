---
layout: post
title: "Evaluation of Insecticides to Control Brown Marmorated Stink Bug on Hemp in Virginia, 2019"
date: 2024-09-16
tags: [Brown marmorated stink bug]
author: Kadie E Britt | Thomas P. Kuhar
institutions: Virginia Tech
doi: "https://doi.org/10.1093/amt/tsaa055"
---


The objective of our experiment was to assess the efficacy of several natural insecticide products for control of BMSB, which is a common pest of grain hemp in Virginia. A small plot field experiment was conducted on a planting of ‘Felina 32’ hemp direct seeded with a grain drill at 30 lb seed per acre on 31 May 2019 at the Virginia Tech Kentland Farm. BMSB were placed 5 bugs per container and mortality was recorded at 1, 2, and 3 DAT. In the BMSB laboratory bioassay, a significant treatment effect occurred at 3 DAT, at which time, Azera had the highest percentage mortality at 60.0%, which was higher than all other treatments except Requiem (40.0%). BMSB mortality on plants treated with BoteGHA, Trilogy, and Entrust was not significantly different from the untreated check.1  © The Author(s) 2020.