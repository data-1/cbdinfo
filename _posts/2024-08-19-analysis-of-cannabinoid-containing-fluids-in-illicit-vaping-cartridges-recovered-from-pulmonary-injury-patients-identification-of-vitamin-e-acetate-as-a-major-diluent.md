---
layout: post
title: "Analysis of Cannabinoid-Containing Fluids in Illicit Vaping Cartridges Recovered from Pulmonary Injury Patients: Identification of Vitamin E Acetate as a Major Diluent"
date: 2024-08-19
tags: [2019–2020 vaping lung illness outbreak,High-performance liquid chromatography,Gas chromatography–mass spectrometry,Vaporizer (inhalation device),Electronic cigarette,Mass spectrometry,Gas chromatography,Cannabis (drug)]
author: Bryan Duffy | Lingyun Li | Shijun Lu | Lorie Durocher | Mark Dittmar | Emily Delaney-Baldwin | Deepika Panawennage | David LeMaster | Kristen Navarette | David Spink
---


When GC-MS analysis of a VEA standard was performed, there were retention time and mass spectral matches for the component in the vaporizer fluid to that of the VEA analytical standard. Nicotine, cannabinoids, and additives in vaporizer fluids samples. The illicit vaporizers fluids are usually <50% cannabinoids with as much as 58% VEA. In some cases, we observed both VEA and MCT in the same vaporizer fluid. Supplementary Materials  The following are available online at https://www.mdpi.com/2305-6304/8/1/8/s1, Figure S1: Analysis of VEA using GC-MS with selected-ion monitoring, Figure S2: Mass spectral analysis of VEA in a vaporizer fluid, Table S1: Accurate mass analysis of MCT in a vaporizer fluid, Figure S3: Analysis of commercial MCT using LC-HRMS/MS, Figure S4: Mass spectral analysis of squalane in a vaporizer fluid diluent, Figure S5: Mass spectral analysis of triethyl citrate in a vaporizer fluid diluent, Figure S6: Analysis of a vaporizer fluid thickener using GC-MS.

[Visit Link](https://www.mdpi.com/2305-6304/8/1/8){:target="_blank rel="noopener"}

