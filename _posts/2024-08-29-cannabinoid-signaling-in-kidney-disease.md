---
layout: post
title: "Cannabinoid Signaling in Kidney Disease"
date: 2024-08-29
tags: [Diabetic nephropathy,Endocannabinoid system,Cannabinoid receptor 2,Cannabinoid receptor 1,Kidney failure,2-Arachidonoylglycerol,Nephron,Kidney,Adenosine monophosphate,Chronic kidney disease,Cell signaling,Cannabinoid,Cell biology]
author: Liana Arceri | Thanh Khoa Nguyen | Shannon Gibson | Sophia Baker | Rebecca A. Wingert | Thanh Khoa | Rebecca A
---


[Google Scholar] [CrossRef]  Liu, L.Y. Expression of cannabinoid receptors in human kidney. Kidney J. Cannabinoid Signaling in Kidney Disease. Cannabinoid Signaling in Kidney Disease Cells 12, no.

[Visit Link](https://www.mdpi.com/2073-4409/12/10/1419){:target="_blank rel="noopener"}

