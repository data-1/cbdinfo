---
layout: post
title: "Cannabinoid receptor 2 signal promotes type 2 immunity in the lung"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Immune system,Innate lymphoid cell,Biochemistry,Medical specialties,Biology,Immunology,Cell biology,Clinical medicine,Cell signaling]
author: 
---


Type 2 immunity in the lung protects against pathogenic infection and facilitates tissue repair, but its dysregulation may lead to severe human diseases. Notably, cannabis usage for medical or recreational purposes has increased globally. However, the potential impact of the cannabinoid signal on lung immunity is incompletely understood. Here, we report that cannabinoid receptor 2 (CB2) is highly expressed in group 2 innate lymphoid cells (ILC2s) of mouse and human lung tissues. Of importance, the CB2 signal enhances the IL-33-elicited immune response of ILC2s.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2772892723000482){:target="_blank rel="noopener"}

