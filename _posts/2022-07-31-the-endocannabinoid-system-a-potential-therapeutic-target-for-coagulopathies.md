---
layout: post
title: "The Endocannabinoid System: A Potential Therapeutic Target for Coagulopathies"
date: 2022-07-31
categories:
- cannabinoid
- Endocannabinoid
author: Khayat, Lehmann, Wujood Khayat, Christian Lehmann
tags: [Cannabinoid receptor,Cannabinoid,Cannabinoid receptor type 2,Cannabinoid receptor type 1,Coagulation,Platelet,2-Arachidonoylglycerol,Tetrahydrocannabinol,Thrombosis,Stroke,Medical specialties,Biochemistry,Clinical medicine]
---


Blood coagulation disorders or coagulopathies are a group of disorders in which there is a hypercoagulative state leading to excessive abnormal blood clot formation and tissue ischemia, or hypocoagulation with impaired blood clot formation or extensive blood clot lysis, both of which can result in hemorrhage [ 43 ]. While CBD did not show any significant effect on thrombin activity, the other cannabinoids exhibited various degrees of significant inhibition with a 5-fold greater inhibition for THC compared to that of CBN. Anticoagulatory effects of cannabinoids have been reported in a few preclinical studies and clinical case reports. Consistent with these results, several cases reported an incidence of myocardial ischemia or infarction that is temporally associated with cannabinoid consumption [65,66,69, Although the previous two studies have shown no association between cannabinoid use and developing MI in trauma patients, numerous other studies demonstrate a link between cannabinoid consumption and triggering myocardial ischemia or infarction. A study examined the effects of the main endocannabinoids AEA, 2-AG, and virodhamide on platelet aggregation in human blood and platelet-rich plasma (PRP) samples using multiple electrode aggregometry showed that both 2-AG and virodhamide stimulated platelet aggregation in blood, and induced shape change and adenosine triphosphate (ATP) release that was followed by platelet aggregation in PRP, whereas AEA (600 µM) was inactive.

<hr>[Visit Link](https://www.mdpi.com/2218-1989/12/6/541/htm){:target="_blank" rel="noopener"}


