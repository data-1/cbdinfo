---
layout: post
title: "Metabolic Engineering of Nicotiana benthamiana to Produce Cannabinoid Precursors and Their Analogues"
date: 2024-08-27
tags: [Real-time polymerase chain reaction,Callus (cell biology),Cannabinoid,Cannabigerol,Vector (molecular biology),Polymerase chain reaction,Tetrahydrocannabinolic acid,Molecular cloning,Biochemistry,Biology,Life sciences,Biotechnology]
author: Vaishnavi Amarr Reddy | Sing Hui Leong | In-Cheol Jang | Sarojam Rajani | Vaishnavi Amarr | Sing Hui | In-Cheol
---


Production of olivetolic acid and divarinic acid, the universal precursors for major and minor cannabinoids, respectively, was observed in transgenic N. benthamiana plants. However, no peaks of CBGA were detected in any of the transgenic lines ( ) despite producing OA (Figure S3). Production of Olivetol in N. benthamiana Cell Lines  To test whether cell cultures would be a better platform than transgenics to produce cannabinoids, we developed N. benthamiana cell cultures from the transgenic N. benthamiana lines. Plant J. Plant J.

[Visit Link](https://www.mdpi.com/2218-1989/12/12/1181){:target="_blank rel="noopener"}

