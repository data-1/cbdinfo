---
layout: post
title: "Mechanisms of Cannabidiol (CBD) in Cancer Treatment: A Review"
date: 2022-07-31
categories:
- CBD
author: Heider, Camren G., Itenberg, Sasha A., Rao, Ma, Wu, Camren G. Heider, Sasha A. Itenberg, Jiajia Rao
tags: [DNA damage-inducible transcript 3,Apoptosis,Reactive oxygen species,Cannabidiol,Cannabinoid,Chemotherapy,Cancer,Metastasis,Bcl-2,Tetrahydrocannabinol,Lung cancer,Cannabinoid receptor type 2,Cannabis,Non-small-cell lung carcinoma,Cannabinoid receptor,Cell biology,Neurochemistry,Diseases and disorders,Biology,Cellular processes,Biochemistry,Clinical medicine,Biotechnology]
---


with high THC content is cultivated for medical applications and/or recreational use [ Cannabis plants are easy to cultivate, as they do not require a specific type of soil; however, the soil should be at a neutral pH or slightly alkaline [ Cannabis plants and contributes their THC content [ Cannabis sp. They can also be effectively grown in diverse climates and can be used in organic crop rotation [ Cannabis plants and used to obtain various potential health benefits—including cancer treatment and pain management [ Phytochemicals, including phytocannabinoids and non-phytocannabinoids, fromL. Many Cannabis strains contain higher amounts of CBD than THC [ CBD is a major bioactive, non-intoxicating cannabinoid inplants. Cannabinoids may exert their anticancer effects, as well as many of their other biological activities, primarily by binding to a group of G protein-coupled receptors, known as type 1 and 2 cannabinoid receptors (CB1 and CB2, respectively) [ 5 ]. THC exerts psychotropic effects due to its high affinity with cannabinoid receptor (CB1) in the brain, whereas non-psychotropic cannabinoids, such as CBD, have a lower affinity with CB1, which can be activated to promote the positive effects of CBD [ 10 ].

<hr>[Visit Link](https://www.mdpi.com/2079-7737/11/6/817/htm){:target="_blank" rel="noopener"}


