---
layout: post
title: "Case report: Cannabinoid therapy for discoid lupus erythematosus in a dog"
date: 2024-09-02
tags: [Inflammation,Interleukin 10,Cannabinoid,Medical specialties,Immunology,Clinical medicine]
author: da Silva | Maria Eduarda Schmitz | Marcy Lancia
---


Within a few weeks, the dog exhibited significant improvement in dermatological signs, accompanied by a concurrent improvement in liver function. Figure 2  The patient’s initial presentation included persistent depigmentation and scaling on the nasal plan, along with crusted lesions on the ears and vulva. Veterinarians are interested in the use of cannabinoid compounds derived from the Cannabis sativa plant as safe and effective alternatives for the treatment of DLE in dogs. However, no studies or reports have been conducted so far on the use of cannabis oil in the treatment of DLE in dogs. The treatment initiated with a daily cannabinoid dose of 0.08 mg/kg.

[Visit Link](https://www.frontiersin.org/journals/veterinary-science/articles/10.3389/fvets.2024.1309167/full){:target="_blank rel="noopener"}

