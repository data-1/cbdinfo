---
layout: post
title: "Drug–Cannabinoid Interactions in Selected Therapeutics for Symptoms Associated with Epilepsy, Autism Spectrum Disorder, Cancer, Multiple Sclerosis, and Pain"
date: 2024-09-02
tags: [Cannabidiol,Tamoxifen,Cannabinoid,Risperidone,Selective serotonin reuptake inhibitor,Drug interaction,Tetrahydrocannabinol,Sirolimus,Antidepressant,Antipsychotic,Nabilone,Amitriptyline,Valproate,Medication,Sertraline,Temozolomide,Stiripentol,Chemicals in medicine,Pharmacology,Drugs,Clinical medicine,Medicine,Medical treatments,Medicinal chemistry,Health]
author: Maria G. Campos | Maria China | Mariana Cláudio | Miguel Capinha | Rita Torres | Simão Oliveira | Ana Fortuna | Maria G
---


The assessment of potential interactions must be carried out in both directions: that is, both the effects of cannabinoids on other drugs and the effects of other drugs on cannabinoids or their derivatives should be studied. Furthermore, cannabinoids can interact with other drugs and targeted therapies used in this type of cancer treatment [17]. Among all patients, they described 90 interactions with 34 medications. These data help in finding evidence for additional studies that will provide more robust information about these clinical drug interactions with CBD in cancer patients [34]. Evaluation and discussion of clinical evidence regarding potential developed interactions between cannabidiol (CBD) and drugs used to treat cancer.

[Visit Link](https://www.mdpi.com/1424-8247/17/5/613){:target="_blank rel="noopener"}

