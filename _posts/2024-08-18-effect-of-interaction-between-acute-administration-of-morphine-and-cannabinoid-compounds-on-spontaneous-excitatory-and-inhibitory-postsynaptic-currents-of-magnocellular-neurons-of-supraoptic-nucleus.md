---
layout: post
title: "Effect of interaction between acute administration of morphine and cannabinoid compounds on spontaneous excitatory and inhibitory postsynaptic currents of magnocellular neurons of supraoptic nucleus"
date: 2024-08-18
tags: [Endocannabinoid system,Inhibitory postsynaptic potential,Chemical synapse,Cannabinoid receptor 2,Neurotransmitter,Supraoptic nucleus,Cannabinoid,Γ-Aminobutyric acid,Cannabinoid receptor 1,Cell signaling,Basic neuroscience research,Neurochemistry,Neurophysiology,Branches of neuroscience,Physiology,Biochemistry]
author: Mitra Yousefpour | Nima Naderi | Fereshteh Motamedi
---


Thus, the results of co-administration of morphine and cannabinoid compounds in this study showed that cannabinoids could modulate the morphine effects on spontaneous synaptic currents of MCNs. It has been shown that MCNs of the SON synthesize and release endocannabinoids (13, 27). Also, there are other receptor systems involved in pharmacological effects of cannabinoids such as the orphan G protein-coupled receptor GPR55, which is expressed in several tissues including some brain regions (35). Solinas M, Goldberg SR. Motivational effects of cannabinoids and opioids on food reinforcement depend on simultaneous activation of cannabinoid and opioid systems. Solinas M, Goldberg SR. Motivational effects of cannabinoids and opioids on food reinforcement depend on simultaneous activation of cannabinoid and opioid systems.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4951608/){:target="_blank rel="noopener"}

