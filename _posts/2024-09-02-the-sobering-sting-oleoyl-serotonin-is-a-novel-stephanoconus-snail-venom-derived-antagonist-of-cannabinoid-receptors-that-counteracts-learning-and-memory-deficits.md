---
layout: post
title: "The Sobering Sting: Oleoyl Serotonin Is a Novel Stephanoconus Snail Venom-Derived Antagonist of Cannabinoid Receptors That Counteracts Learning and Memory Deficits"
date: 2024-09-02
tags: [Endocannabinoid system,Cannabinoid receptor 2,Conidae,Cannabinoid receptor 1,Neurophysiology,Biology,Biochemistry,Neurochemistry,Cell biology]
author: Dongchen An | Guilherme Salgado Carrazoni | Rudi D’Hooge | Steve Peigneur | Jan Tytgat | Guilherme Salgado | Souto das Neves | Ben-Hur | D’Hooge
---


Regarding the structure, OS ( A) shares a similar structure with AEA (a partial agonist of CB1 receptors) and Org27569 (a positive allosteric modulator of CB1 receptors in binding, a negative allosteric modulator of CB1 receptors in function) (Figure S1), which indicates that it may act through CB receptors and, as seen in other studies that modulated CB receptors’ function, affect behavior and memory [1,2,3]. The effect of OS on non-injected oocytes, GIRK1/GIRK2 channels, and CB receptors. OS was directly applied to the oocytes expressing KV7.1 channels. Oleoyl Serotonin Blocks CB Receptors Activated by WIN55,212-2  The application of 1 μM WIN55,212-2 (a non-selective agonist of CB receptors) activated CB1 and CB2 receptors in CB-GIRK1/GIRK2-RGS4 coupling systems. The effect of OS compound on the chimeric CB1/2 receptor.

[Visit Link](https://www.mdpi.com/2227-9059/12/2/454){:target="_blank rel="noopener"}

