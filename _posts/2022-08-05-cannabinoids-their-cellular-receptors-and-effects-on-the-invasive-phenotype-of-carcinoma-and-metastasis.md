---
layout: post
title: "Cannabinoids, their cellular receptors, and effects on the invasive phenotype of carcinoma and metastasis"
date: 2022-08-05
categories:
- Cannabinoid
author: Judah Glogauer, Michael G. Degroote School Of Medicine, Mcmaster University Waterloo Regional Campus, Kitchener Ontario, Jonathan Blay, School Of Pharmacy, University Of Waterloo, Waterloo Ontario, Department Of Pathology, Dalhousie University
tags: [Metastasis,Cannabinoid receptor type 2,Endocannabinoid system,Cannabinoid receptor,Epithelialmesenchymal transition,Invasion (cancer),Cannabinoid receptor type 1,Cannabinoid,Cell signaling,Angiogenesis,Receptor (biochemistry),Cancer,MMP2,Anandamide,Signal transduction,Cell communication,Biotechnology,Cell biology,Cellular processes,Biochemistry,Neurochemistry]
---


60 Cannabinoids have been shown to have varying effects on the ability of tumors to establish a metastasis, although from much of the data it is unclear at which stage of metastasis this effect is occurring. CANNABINOID RECEPTOR ACTIVATION AND CANCER CELL INVASION  4.1. CB1 receptor activation and cancer cell invasion When studying the effects of cannabinoids in the context of cellular changes in neoplastic progression, determining which receptor is implicated in a particular pathway can be a vital first step. 68 , 69 While this contradictory evidence does not negate the previously observed effects, it does illustrate the failure of a one‐receptor‐one‐mechanism explanation for cannabinoid receptor signaling in the context of cancer cell migration, and emphasizes the importance of the concept of biased signaling and that the observed effect of CB1 ligands on cancer cell migration is context dependent. CB2 receptor activation and cancer cell invasion Although the CB2 receptor also shows complexity in its association with migration signaling pathways, it appears to display less phenotypic diversity than the CB1 receptor in the context of cancer cell migration.

<hr>

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8842690/){:target="_blank" rel="noopener"}


