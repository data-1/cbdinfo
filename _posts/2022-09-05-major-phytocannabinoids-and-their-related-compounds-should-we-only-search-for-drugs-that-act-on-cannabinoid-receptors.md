---
layout: post
title: "Major Phytocannabinoids and Their Related Compounds: Should We Only Search for Drugs That Act on Cannabinoid Receptors?"
date: 2022-09-05
categories:
- Cannabinoid
author: Filipiuc, Leontina Elena, Ababei, Daniela Carmen, Alexa-Stratulat, Pricope, Cosmin Vasilica, Bild, Stefanescu, Stanciu
tags: [Cannabinoid,Tetrahydrocannabinol,Cannabinoid receptor type 2,Cannabidiol,TRPV1,2-Arachidonoylglycerol,Anandamide,Cannabinoid receptor type 1,Cannabinoid receptor,Endocannabinoid system,TRPV2,Amphetamine,Clinical medicine,Biochemistry,Neurochemistry,Cell biology,Medical specialties,Neurophysiology,Neuroscience]
---


CBG seems to be a potent TRPM8 antagonist (IC50 = 0.16 ± 0.02), which suggests it may have a therapeutic effect in these types of cancers [ 72 ]. From this class of phytocannabinoids, CBGV remains the only one with questionable behavior regarding cannabinoid receptors because, in the same study, the authors concluded that CBGV has a complex behavior, acting as a potent agonist, through Gi and MAPK pathway activation, studied in vitro, but with the observation that, in vivo, CBGV acts as an inverse agonist of cannabinoid receptors [ 67 ]. It can be an agonist of transient receptors (TRPV1), of 5-HT1A receptor, and of PPARγ receptor, or it can modulate the activity of numerous ion channels and various enzymes as well as modulating G55 protein-coupled receptor (GPR55) [ 84 ]. This study showed that acute systemic administration of cannabidiol increases extracellular serotonin levels through activation of 5HT1A and CB1 in the ventromedial prefrontal cortex, involved in the regulation of emotional impairment, as cannabidiol may be proposed as a potential drug with therapeutic indications for the treatment of depressive disorders associated with chronic neuropathic pain [ 83 ]. [155] CBD CBDV HEK-293 cells stably overexpressing human TRPV1  HEK-293 cells encoding the rat TRPV2 and expressing high levels of TRPV2  HEK-293 cells over-  expressingTRPA1  HEK-293 encoding the rat TRPM8 and overexpressing high levels of TRPM8 Stimulates TRPV1 channels.

<hr>

[Visit Link](https://www.mdpi.com/1999-4923/13/11/1823/htm){:target="_blank" rel="noopener"}


