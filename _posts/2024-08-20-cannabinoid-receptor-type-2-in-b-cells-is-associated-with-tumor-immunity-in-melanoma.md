---
layout: post
title: "Cannabinoid Receptor Type-2 in B Cells Is Associated with Tumor Immunity in Melanoma"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Regulatory T cell,Melanoma,Fluorescence in situ hybridization,Endocannabinoid system,Tumor microenvironment,RNA-Seq,Flow cytometry,Cancer immunotherapy,The Cancer Genome Atlas,Immune system,Cancer,Single-cell sequencing,Health sciences,Biology,Medical specialties,Clinical medicine]
author: Thomas Gruber | Steve Robatel | Mirela Kremenovic | Lukas Bäriswyl | Jürg Gertsch | Mirjam Schenk
---


In this study, using data from The Cancer Genome Atlas (TCGA)’s melanoma cohort, human melanoma tissues and murine models, as well as single-cell RNA sequencing, we uncover a critical role of CB2Rs in B cells in cutaneous melanoma. Cell 2000, 101, 455–458. ; et al. IL-32γ potentiates tumor immunity in melanoma. Cell. Cannabinoid Receptor Type-2 in B Cells Is Associated with Tumor Immunity in Melanoma Cancers 13, no.

[Visit Link](https://www.mdpi.com/2072-6694/13/8/1934){:target="_blank rel="noopener"}

