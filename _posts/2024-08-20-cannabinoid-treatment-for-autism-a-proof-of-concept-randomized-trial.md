---
layout: post
title: "Cannabinoid treatment for autism: a proof-of-concept randomized trial"
date: 2024-08-20
tags: [Cannabidiol,Autism spectrum,Tetrahydrocannabinol,Cannabinoid,Cannabinoid receptor 1,Health,Clinical medicine]
author: F. Xavier
---


Treatments were: (1) oral placebo, (2) whole-plant cannabis extract containing CBD and THC at a 20:1 ratio, and (3) pure CBD and pure THC at the same ratio and concentration. Impact of cannabinoid treatment with BOL-DP-O-01-W (whole-plant extract) and BOL-DP-O-01 (pure cannabinoids) on behavior  The impact of cannabinoid treatment on behavioral problems was assessed using the HSQ-ASD [33], and the CGI-I [34] (co-primary outcome measures). None of these 3 measures (HSQ-ASD, CGI-I and APSI) differed significantly between participants who received whole-plant extract versus pure cannabinoids (Table 4). Severity of ASD core symptoms at baseline (as assessed by ADOS-2) and concomitant use of medications were not significantly associated with response to either pure cannabinoids or whole-plant extract, on any assessment. In a controlled study of 150 participants, we found that BOL-DP-O-01-W, a whole-plant extract which contains CBD and THC in a 20:1 ratio, improved disruptive behaviors on one of two primary outcome measures and on a secondary outcome, an index of ASD core symptoms, with acceptable adverse events.

[Visit Link](https://molecularautism.biomedcentral.com/articles/10.1186/s13229-021-00420-2){:target="_blank rel="noopener"}

