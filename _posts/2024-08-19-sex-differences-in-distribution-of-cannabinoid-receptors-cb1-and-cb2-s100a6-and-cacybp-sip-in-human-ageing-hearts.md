---
layout: post
title: "Sex differences in distribution of cannabinoid receptors (CB1 and CB2), S100A6 and CacyBP/SIP in human ageing hearts"
date: 2024-08-19
tags: [Real-time polymerase chain reaction,Apoptosis,Polymerase chain reaction,Cardiac muscle,Cannabinoid receptor 2,Muscle contraction,Calcium in biology,Sarcoplasmic reticulum,Cannabinoid,Biology,Biochemistry,Cell biology]
author: 
---


In the hearts of subjects over 50 (Fig. Computer image analysis confirmed visually perceived age-related changes in the intensity of the immunohistochemical reaction against CB1, CB2, S100A6 and CacyBP/SIP in the hearts of men and women (Table 2). The men older than 50 years had lower expression of CacyBP/SIP gene compared to age-matched women, while in the group of subjects under 50 years old, there were no sex differences in expression of CacyBP/SIP gene (Fig. The performed morphometric analysis demonstrated cardiomyocyte hypertrophy in men older than 50 years as compared to younger men (cardiomyocytes width 11.1 ± 0.32 and 9.2 ± 0.33, respectively), whereas in women over 50, the width of cardiomyocytes was comparable with younger women (9.9 ± 0.34 and 9.6 ± 0.32 respectively) (Table 4). Cannabinoids and protein S100A6 perform a significant role in the process of cardiac hypertrophy [4, 11].

[Visit Link](https://link.springer.com/article/10.1186/s13293-018-0209-3){:target="_blank rel="noopener"}

