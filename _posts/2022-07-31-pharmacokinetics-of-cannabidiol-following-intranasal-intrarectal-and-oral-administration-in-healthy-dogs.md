---
layout: post
title: "Pharmacokinetics of Cannabidiol Following Intranasal, Intrarectal, and Oral Administration in Healthy Dogs"
date: 2022-07-31
categories:
- CBD
author: Polidoro, Small Animal Department, Small Animal Teaching Hospital, Faculty Of Veterinary Medicine, Ghent University, Temmerman, Department Of Pathobiology, Pharmacology, Zoological Medicine, Devreese
tags: [Cannabidiol,Bioavailability,Dose (biochemistry),Pharmacokinetics,Cannabinoid,Area under the curve (pharmacokinetics),Route of administration,High-performance liquid chromatography,Mass spectrometry,Absorption (pharmacology),Liquid chromatographymass spectrometry,Tetrahydrocannabinol,Medical cannabis,Chemistry,Pharmacology,Pharmaceutical sciences,Pharmacy,Medicinal chemistry,Drugs]
---


In the veterinary medicine, therapeutic applications of CBD in dogs include osteoarthritis-associated pain (6, 25, 26), aggressive behavior (27), and epilepsy (7, 28). We hypothesized that CBD delivered via IN administration would avoid first-pass liver effect and CBD delivered via IR administration would partially avoid liver metabolization and therefore higher plasma concentrations and subsequent exposure would be achieved compared to the PO administration route. Due to the very low (below LOQ) plasma CBD concentrations (Figure 1) obtained after IR administration, no pharmacokinetic analysis was conducted for this administration route. Nevertheless, IN administration of CBD provided a faster blood absorption when compared to the PO and IR CBD administration. Pharmacokinetics of cannabidiol administered by 3 delivery methods at 2 different dosages to healthy dogs.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fvets.2022.899940/full){:target="_blank" rel="noopener"}


