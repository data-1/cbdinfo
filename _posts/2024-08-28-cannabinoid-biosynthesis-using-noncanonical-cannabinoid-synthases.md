---
layout: post
title: "Cannabinoid Biosynthesis Using Noncanonical Cannabinoid Synthases"
date: 2024-08-28
tags: [Tetrahydrocannabinolic acid synthase,Chemistry,Life sciences,Biochemistry,Biology,Biotechnology]
author: Maybelle Kho Go | Tingting Zhu | Kevin Jie Han Lim | Yossa Dwi Hartono | Bo Xue | Hao Fan | Wen Shan Yew | Maybelle Kho | Kevin Jie Han | Yossa Dwi
---


The hypothesis of the study follows that those enzymes “similar” to Q8GTB6 may catalyze the production of cannabinoids such as THCA (2); enzymes “different” from Q8GTB6 may catalyze the production of novel cannabinoids. The purified enzymes were used to determine the kinetic parameters of cannabinoid synthase activity using CBGA (1) as substrate ( ). Nevertheless, M4DIE5 has detectable cannabinoid synthase active to catalyze the production of CBE (7). The three enzymes discovered in this study are the first reported heterologously expressed BBEs that do not originate from the Cannabis plant yet can catalyze the production of cannabinoids using CBGA (1) as substrate. Production of Δ9-tetrahydrocannabinolic acid from cannabigerolic acid by whole cells of Pichia (Komagataella) pastoris expressing Δ9-tetrahydrocannabinolic acid synthase from Cannabis sativa L. Biotechnol.

[Visit Link](https://www.mdpi.com/1422-0067/24/2/1259){:target="_blank rel="noopener"}

