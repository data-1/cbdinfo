---
layout: post
title: "Structural basis of selective cannabinoid CB2 receptor activation"
date: 2024-08-29
tags: [Cannabinoid receptor 2,Ligand (biochemistry),Cannabinoid,High-performance liquid chromatography,G protein-coupled receptor,Liquid chromatography–mass spectrometry,Thin-layer chromatography,Receptor (biochemistry),Functional selectivity,EC50,Cannabinoid receptor 1,Biochemistry]
author: de Paus | Laura V | van der Horst | Sanjay Sunil | van den Berg | Antonius P. A | Zhi-Jie | van der Stelt | Laura H
---


LEI-102 did not bind CB1R, thereby showing at least 1000-fold selectivity (Supplementary Table 3). In G protein activation assays, LEI-102 activated the receptor as a partial agonist (Emax 76 ± 1%) with a pEC50 value of 6.9 ± 0.2 (Supplementary Table 2). LEI-102 had a RT of 16 min, which was around half that of APD371 (45 min) and CP55,940 (32 min), whereas HU308 had the longest RT at the receptor of 71 min (Supplementary Table 2). LEI-102 and APD371 require H952.65 for G protein activation in CB2R  To study the mechanism of CB2R activation, five residues in the binding pocket were further characterized based on the complex structures (Fig. Membrane protein concentrations were determined using a BCA protein determination assay as described by the manufacturer72.

[Visit Link](https://www.nature.com/articles/s41467-023-37112-9){:target="_blank rel="noopener"}

