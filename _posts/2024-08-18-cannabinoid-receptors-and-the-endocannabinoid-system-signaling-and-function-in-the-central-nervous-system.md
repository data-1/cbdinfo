---
layout: post
title: "Cannabinoid Receptors and the Endocannabinoid System: Signaling and Function in the Central Nervous System"
date: 2024-08-18
tags: [2-Arachidonoylglycerol,Cannabinoid receptor 1,Cannabinoid receptor 2,Chemical synapse,Endocannabinoid system,Synapse,Cannabinoid,Cell signaling,Retrograde signaling,G protein-coupled receptor,Neurotransmission,MAPK/ERK pathway,Long-term depression,NMDA receptor,Neurotransmitter,Arrestin,Depolarization-induced suppression of inhibition,Cannabidiol,Astrocyte,Appetite,Cell biology,Biochemistry,Neurochemistry,Neurophysiology,Basic neuroscience research,Biology]
author: Shenglong Zou | Ujendra Kumar
---


Not long after that, another cannabinoid receptor (CBR) was identified and cloned, later termed as the cannabinoid receptor 2 (CB2R) [8]. Cannabinoid receptors and their ligands: Beyond CB1and CB2. Species differences in cannabinoid receptor 2 (CNR2 gene): Identification of novel human and rodent CB2 isoforms, differential tissue expression and regulation by cannabinoid receptor ligands. [Google Scholar] [CrossRef] [PubMed]  Martin, B.R. Cannabinoid Receptors and the Endocannabinoid System: Signaling and Function in the Central Nervous System International Journal of Molecular Sciences 19, no.

[Visit Link](https://www.mdpi.com/1422-0067/19/3/833){:target="_blank rel="noopener"}

