---
layout: post
title: "Challenges and Opportunities in Preclinical Research of Synthetic Cannabinoids for Pain Therapy"
date: 2024-09-26
tags: [MTT assay,Cannabinoid receptor 2,Cannabinoid,Tetrahydrocannabinol,Cannabinoid receptor 1,Comet assay,Cannabidiol,Analgesic,Synapse,Medical cannabis,Cannabinol,Cyclodextrin]
author: Bogdan Ionel Tamba | Gabriela Dumitrița Stanciu | Cristina M. Uritu | Elena Rezuş | Raluca Ştefănescu | Cosmin Mihai | Andrei Luca | Gabriela Rusu-Zota | Maria‐Magdalena Leon‐Constantin | Elena Cojocaru | Bogdan Gafton | Teodora Alexa‐Stratulat
pubdate: "2020-01-09"
institutions: Grigore T. Popa University of Medicine and Pharmacy
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7023162"
---


In preclinical models, several types of cannabinoids have been shown to decrease neuropathic nociception; there are several on-going clinical studies that evaluate medicinal cannabis preparations for the treatment of neuropathic pain. Pain. Pain. Drugs. Therapeutic potential of cannabis in pain medicine.