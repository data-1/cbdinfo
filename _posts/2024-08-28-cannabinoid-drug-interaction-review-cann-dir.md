---
layout: post
title: "CANNabinoid Drug Interaction Review (CANN-DIR™)"
date: 2024-08-28
tags: [Dronabinol,Cannabidiol,Tetrahydrocannabinol,Pharmacology,Health care,Health,Medicine,Drugs,Pharmaceutical sciences,Pharmacy,Health care industry,Medicinal chemistry]
author: Paul T. Kocis | Samuel Wadrose | Ryan Lee Wakefield | Aqib Ahmed | Renata Calle | Rohan Gajjar | Kent E. Vrana
---


The CANNabinoid Drug Interaction Review (CANN-DIR™) is a free web-based platform that has been developed to screen for potential drug-drug interactions from the perspective of how a cannabinoid delta-9-tetrahydrocannabinol (THC), CBD, or a combination of THC/CBD may affect the metabolism of another prescribed medication. Database (OBJECT) Medications  In order to assure a comprehensive list of medications, whether there is a potential DDI identified at this time or not, the drug database contained within CANN-DIRTM comprised medications listed in the US FDA document “Drug Development and Drug Interactions: Table of Substrates, Inhibitors and Inducers” [14]. [PubMed] [Google Scholar]  9. dronabinol (Marinol ®) Full Prescribing Information [updated 8/2017 Available from https://www.rxabbvie.com/pdf/marinol_PI.pdf. Available from https://meps.ahrq.gov/mepstrends/hc_pmed/  Articles from Medical Cannabis and Cannabinoids are provided here courtesy of Karger Publishers  1. [PubMed] [Google Scholar] [Ref list]  9. dronabinol (Marinol ®) Full Prescribing Information [updated 8/2017 Available from https://www.rxabbvie.com/pdf/marinol_PI.pdf.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9940648/){:target="_blank rel="noopener"}

