---
layout: post
title: "Hormonal control of promoter activities of Cannabis sativa prenyltransferase 1 and 4 and salicylic acid mediated regulation of cannabinoid biosynthesis"
date: 2024-08-29
tags: [Plant hormone,Tetrahydrocannabinolic acid synthase,Ethylene (plant hormone),Biotic stress,Indole-3-acetic acid,Promoter (genetics),Terpene,Cytokinin,Auxin,Biology,Life sciences,Biotechnology,Molecular biology,Biochemistry]
author: Lauren B | Samuel R | Gerald A
---


Both promoters were activated after treatment with SA (Fig. Our study also indicates that the promoters of CsPT1 and CsPT4 are responsive to different hormones, except for ABA and SA (Figs. In our study, NAA did not activate CsPT4 pro (Fig. Samples, Fig. The predicted hormone responsive elements of CsPT4 pro and CsPT1 pro are shown in Supplementary Figs.

[Visit Link](https://www.nature.com/articles/s41598-023-35303-4){:target="_blank rel="noopener"}

