---
layout: post
title: "Cannabinoid crystal polymorphism - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabinoid
author: Filer, Crist N., Perkinelmer Health Sciences Inc, Waltham, Winter Street, Ma, Crist N. Filer, You Can Also Search For This Author In, Author Information, Corresponding Author
tags: [Cannabinoid,Cannabidiol,Polymorphism (materials science),Crystal,Tetrahydrocannabinol,Chemistry,Physical sciences,Materials,Applied and interdisciplinary physics]
---


Regarding polymorphism in general, it can be seen in Fig. This search was conducted not only on the large number of publications captured under “cannabinoids” in general but also the very specific cannabinoids: THCA-A, delta-9-tetrahydrocannabinol (THC), cannabidiolic acid (CBDA), CBD, cannabigerolic acid (CBGA), CBG, cannabinolic acid (CBNA), CBN, cannabichromenic acid (CBCA), and cannabichromene (CBC). However, there have been several intriguing clues in the literature that cannabinoid crystal polymorphism may well be possible (Fig. 1 Growth of crystal polymorphism publications Full size image  Fig. It was discovered 40 years ago that the synthetic 9-ketocannabinoid derivative nabilone [3] displayed crystal polymorphism.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00131-2){:target="_blank" rel="noopener"}


