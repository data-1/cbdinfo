---
layout: post
title: "In Vitro Model of Neuroinflammation: Efficacy of Cannabigerol, a Non-Psychoactive Cannabinoid"
date: 2024-08-18
tags: [Tumor necrosis factor,Inflammatory cytokine,Interleukin 1 beta,Neurodegenerative disease,Cytokine,Macrophage,Apoptosis,Nitric oxide synthase,Inflammation,Superoxide dismutase,Interleukin 6,Cell biology,Biology,Biochemistry]
author: Agnese Gugliandolo | Federica Pollastro | Gianpaolo Grassi | Placido Bramanti | Emanuela Mazzon
---


CBG Exerted an Antioxidant Action in NSC-34 Cells Treated with Medium of LPS-Stimulated Macrophages  As we said above, inflammation and oxidative stress are two correlated processes, indeed the treatment of NSC-34 motor neurons with the medium of LPS-stimulated RAW 264.7 induced oxidative stress, as demonstrated by the increase in nitrotyrosine levels evaluated by immunocytochemical assay. CBG reduced neuronal death in NSC-34 cells treated with the medium of LPS stimulated macrophages, reducing inflammation and oxidative stress. [Google Scholar] [CrossRef] [PubMed]  Giacoppo, S.; Gugliandolo, A.; Trubiani, O.; Pollastro, F.; Grassi, G.; Bramanti, P.; Mazzon, E. Cannabinoid cb2 receptors are involved in the protection of raw264.7 macrophages against the oxidative stress: An in vitro study. [Google Scholar] [CrossRef] [PubMed]  Liu, Y.; Lu, J.; Shi, J.; Hou, Y.; Zhu, H.; Zhao, S.; Liu, H.; Ding, B.; Yin, Y.; Yi, G. Increased expression of the peroxisome proliferator-activated receptor gamma in the immune system of weaned pigs after escherichia coli lipopolysaccharide injection. USA 2017, 114, E4676–E4685.

[Visit Link](https://www.mdpi.com/1422-0067/19/7/1992){:target="_blank rel="noopener"}

