---
layout: post
title: "Safety Considerations in Cannabinoid-Based Medicine"
date: 2024-08-20
tags: [Cannabis use disorder,Cannabinoid,Cannabis (drug),Nabiximols,Tetrahydrocannabinol,Cannabidiol,Substance abuse,Dronabinol,Pharmacology,Clinical medicine,Health,Psychoactive drugs,Medical specialties,Diseases and disorders,Drugs,Medicine,Health care]
author: Sven Gottschling | Oyedeji Ayonrinde | Arun Bhaskar | Marc Blockman | Oscar D’Agnone | Danial Schecter | Luis David Suárez Rodríguez | Sherry Yafai | Claude Cyr
---


Medicines with cannabinoids in them (cannabinoid-based medicines) either contain THC or CBD or both. In: Cannabis and Cannabinoids. Cannabis. In: Cannabis and Cannabinoids. Cannabis.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7720894/){:target="_blank rel="noopener"}

