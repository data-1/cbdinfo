---
layout: post
title: "Overbaked: assessing and predicting acute adverse reactions to Cannabis"
date: 2024-09-24
tags: [Cannabis (drug),Anxiety,Effects of cannabis,Mental disorder,Cannabis use disorder,Psychosis,Major depressive disorder,Paranoia,Dependent and independent variables,Medical cannabis,Correlation,Adverse effect,Hallucination,Research,Anxiety sensitivity,Affect (psychology),Addiction,Behavioural sciences,Psychology,Health]
author: Emily M. LaFrance | Amanda Stueber | Nicholas C. Glodosky | Dakota Mauzay | Carrie Cuttler
institutions: Washington State University
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7819287"
---


Some of the most common adverse reactions to cannabis intoxication include paranoia (Arendt et al. 2007), dry mouth (Sexton et al. 2019), memory problems (Sexton et al. 2019), and an altered sense of perception/time (Arendt et al. 2007; Sexton et al. 2019). Based on previous research (Arendt et al. 2007), we also hypothesized that less frequent cannabis use and higher levels of negative affect (e.g. anxiety, anxiety sensitivity, depression, and neuroticism) would predict higher frequency of adverse reactions to cannabis, as well as higher levels of distress produced by these reactions. As such, participants in Sexton’s (2019) study may have reported on which they more commonly experience rather than which they had ever experienced. The present study focused on largely different reactions than theirs and results from our study indicate that anxiety and hallucinations are experienced more frequently (on approximately 20–25% of all cannabis use sessions). In contrast, paranoia and anxiety were rated as ‘moderately distressing’ on average and were also highly prevalent and frequent.