---
layout: post
title: "Safety and efficacy of cannabidiol-cannabidiolic acid rich hemp extract in the treatment of refractory epileptic seizures in dogs"
date: 2022-07-31
categories:
- CBD
author: Garcia, Gabriel A., Department Of Clinical Sciences, University Of Florida College Of Veterinary Medicine, United States, Kube, Veterinary Neurology, Pain Management Center Of New England, Carrera-Justiz, Tittle
tags: [Cannabidiol,Epilepsy,Anticonvulsant,Tetrahydrocannabinolic acid,Tetrahydrocannabinol,Seizure,Potassium bromide,Cannabinoid,Dose (biochemistry),Clinical trial,Clinical medicine,Medical specialties,Medicine]
---


The aim of this study was two-fold: (1) determine whether CBD/CBDA rich hemp extract can help control canine refractory epileptic seizure patients who are only partially responsive to commonly utilized ASMs and (2) determine the adverse events of this treatment through owner survey, physical examination, serum biochemistry and ASM serum concentration evaluation during treatment in a 3 month cross over blinded clinical trial. Serum phenobarbital concentrations were not significantly different based on treatment, or over time and no alterations due to treatment over time (Table 3). Rather the epileptic seizure events during CBD/CBDA-rich hemp oil treatment was compared to the placebo treatment phase, whereby two dogs of the 14 dogs showed a 3-fold reduction in epileptic seizure events compared to placebo, as a full response (15). We found that a 50% reduction was observed in 6 of the 14 dogs during CBD/CBDA-rich hemp oil vs. placebo. It must also be noted that two of the dogs did have increases in their potassium bromide dosing during the trial, one during the CBD/CBDA-rich hemp oil treatment phase and one during the placebo treatment phase, which can take up to 3 months to observe consistent changes in serum bromide concentrations.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fvets.2022.939966/full){:target="_blank" rel="noopener"}


