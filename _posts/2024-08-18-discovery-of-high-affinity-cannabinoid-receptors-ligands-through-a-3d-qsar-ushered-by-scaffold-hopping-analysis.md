---
layout: post
title: "Discovery of High-Affinity Cannabinoid Receptors Ligands through a 3D-QSAR Ushered by Scaffold-Hopping Analysis"
date: 2024-08-18
tags: [Ligand (biochemistry),Cannabinoid receptor 2,Neurochemistry,Biochemistry]
author: Giuseppe Floresta | Orapan Apirakkan | Antonio Rescifina | Vincenzo Abbate
---


Introduction  The receptors of the endocannabinoid system are the cannabinoid receptors. The 3D visualizations of the QSAR models are shown in and , where the 3D-QSAR coefficients for the two models are superimposed to the most potent and the most selective compounds for the two cannabinoid receptors. Other information about the conformation calculation, the alignment, and the build of the model are reported in the Supplementary Materials (Figures S1–S6). Statistical analysis information for the model, Figures S1–S7, Tables S1–S16. Model.

[Visit Link](https://www.mdpi.com/1420-3049/23/9/2183){:target="_blank rel="noopener"}

