---
layout: post
title: "Pros and Cons of the Cannabinoid System in Cancer: Focus on Hematological Malignancies"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Anandamide,Graft-versus-host disease,Interleukin 10,Cannabinoid receptor 1,Cannabinoid,T helper cell,Angiopoietin,Neurochemistry,Biochemistry,Clinical medicine,Medical specialties,Cell biology]
author: Natasha Irrera | Alessandra Bitto | Emanuela Sant’Antonio | Rita Lauro | Caterina Musolino | Alessandro Allegra | Sant’Antonio
---


Marijuana. Cell 2019, 176, 459–467. The cannabinoid receptors. Cells 2021, 10, 1282. Cell.

[Visit Link](https://www.mdpi.com/1420-3049/26/13/3866){:target="_blank rel="noopener"}

