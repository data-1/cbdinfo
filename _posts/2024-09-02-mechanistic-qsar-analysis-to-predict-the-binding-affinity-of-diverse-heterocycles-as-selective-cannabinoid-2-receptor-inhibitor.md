---
layout: post
title: "Mechanistic QSAR analysis to predict the binding affinity of diverse heterocycles as selective cannabinoid 2 receptor inhibitor"
date: 2024-09-02
tags: [Quantitative structure–activity relationship,Orbital hybridisation,Docking (molecular),Protein,Cannabinoid receptor 2,Aromaticity,Molecule,Chemical bond,Protein secondary structure,Functional group,Cannabinoid,Amino acid,Alpha helix,Atom,Amide,Receptor (biochemistry),Substituent,Protein structure,Hydrogen bond,Ligand,Ligand (biochemistry),Conformational isomerism,Chemical compound,Molecular binding,Protein–protein interaction,Amine,Phenyl group,Beta sheet,Coordination complex,Binding site,Chemistry,Concepts in chemistry,Physical chemistry,Physical sciences]
author: Rahul D. Jawarkar | Magdi E. A. Zaki | Sami A. Al-Hussain | Abdullah Yahya Abdullah Alzahrani | Long Chiau Ming | Abdul Samad | Summya Rashid | Suraj Mali | Gehan M. Elossaily | Abdul  Samad
---


QSAR data are presented in the form of a molecular descriptor, sp2C_aroC_4B, which captured the same aromatic carbon atom as well as the C-8 atom located within 4 bonds from the sp2 hybridized carbon atom, thus supporting the reported findings. This may be seen in A. RMSD graphs before and after simulation reveal the stability of the compound 8-bound cannabinoid 2 receptor (PDB I.D. After simulating cannabinoid 2 receptor protein (PDB I.D. As a result of the fact that the same ligand may have several binding interactions with the same protein residue, it is possible to get values that are more than 1.0. During the course of the 100ns simulation, protein–ligand interactions were examined, and compound 8 was shown to bind with the cannabinoid 2 receptor.

[Visit Link](https://www.tandfonline.com/doi/full/10.1080/16583655.2023.2265104){:target="_blank rel="noopener"}

