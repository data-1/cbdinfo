---
layout: post
title: "The cannabinoid profile and growth of hemp (Cannabis sativa L.) is influenced by tropical daylengths and temperatures, genotype and nitrogen nutrition"
date: 2022-07-31
categories:
- Cannabis
author: 
tags: [Cannabis,Cannabis sativa,Hemp,Cannabidiol,Tetrahydrocannabinol,Plant,Cannabinoid,Soil,Photoperiodism,Hashish,Cannabis strain,Botany,Plants]
---


Cannabis sativa is a quantitative short-day plant where decreasing daylength (often) regulates flowering initiation (van der Werf et al., 1994). Some studies reported that shorter photoperiods induce early flowering (Amaducci et al., 2008b, Hall et al., 2012). Cannabinoid concentrations are affected mainly by plant genetics (Calzolari et al., 2017, Small and Marcus, 2003); however, they are also affected by environmental conditions (Small et al., 2003). Cultivating hemp on a broad scale in tropical locations needs testing across varieties to understand their performance and responses to a tropical climate. and, c) how do these environmental factors interact with plant genetics to affect cannabinoid concentrations in temperate and tropical hemp varieties?

<hr>[Visit Link](https://www.sciencedirect.com/science/article/abs/pii/S0926669022000887){:target="_blank" rel="noopener"}


