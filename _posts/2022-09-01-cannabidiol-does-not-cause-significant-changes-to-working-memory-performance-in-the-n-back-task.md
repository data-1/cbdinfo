---
layout: post
title: "Cannabidiol Does Not Cause Significant Changes to Working Memory Performance in the N-Back Task"
date: 2022-09-01
categories:
- CBD
author: Jones, Vlachou, Éamon Jones, Styliani Vlachou
tags: [Cannabinoid,Cannabidiol,Neuroscience]
---


Similarly, no statistically significant result was found regarding the strength of CBD used, suggesting that there is not a dose-dependent relationship with N-back performance. Salient to our findings, Bhattacharyya et al. [ 33 ] reported that CBD does not cause negative changes to the participant’s memory functioning and reported increased cortical activation in CBD users. The reliability of the N-back task as a measure of WM must also be considered as a possible limitation. Miller et al. [ 46 ] suggested that the N-back task is not a pure measure of WM but may in fact be useful in research to measure subtle differences between groups. Given that the literature surrounding the cognitive effects of CBD is so sparse, a significant advantage of this study was that it expanded the literature in a previously poorly studied area and supported the notion that this non-psychotropic compound does not cause harm or significant improvements to working memory functioning.

<hr>

[Visit Link](https://www.mdpi.com/1424-8247/14/11/1165/htm){:target="_blank" rel="noopener"}


