---
layout: post
title: "An answered call for aid? Cannabinoid clinical framework for the opioid epidemic"
date: 2024-09-02
tags: [Medical cannabis,Opioid,Dronabinol,Cannabidiol,Opioid epidemic,Cannabis (drug),Substance abuse,Harm reduction,Recreational drug use,Oxycodone,Opioid use disorder,Health care,Medical specialties,Drugs,Drugs acting on the nervous system,Medicine,Social aspects of psychoactive drugs,Clinical medicine,Psychoactive drugs,Health]
author: Lawton W | Kenneth M | Matthew W | Steven D | Henry Patrick | Adam S | EunBit G | David M
---


Now, about 80% of people who use heroin misused prescription opioids first [9] and most opioid overdose deaths involve synthetic opioids [10]. Observational research has explored the role that adult cannabis use plays for patients in pain management, substance use and mental health treatment, and harm reduction [39,40,41], especially among people who inject drugs (PWID) [42, 43]. A four-year longitudinal observational study of cannabis use for cancer-related pain showed no opioid-sparing effect or reduction in pain severity, but only 6.5% of the 1514 patients used cannabis regularly 21–31 days per month, quality and type of cannabis was not assessed, and all patients were using black-market cannabis due to illegality during the study period [53]. A 2018 study in the Journal of Clinical Oncology shed light on critical gaps in research, education, and policy, with its survey of 400 medical oncologists that found, while 46% recommended medical cannabis and almost 80% discussed it, only 30% of oncologists felt sufficiently informed to recommend medical cannabis [69]. Clinical framework for prescribing cannabinoids in clinical practice and research  The Board of Medicine aims to address the lack of provider knowledge of cannabis, heterogenous research interventions, and untapped harm-reduction potential with an evidence-based clinical framework for utilizing cannabinoids to treat chronic pain in patients who are on opioids, seeking alternatives to opioids, and tapering opioids.

[Visit Link](https://harmreductionjournal.biomedcentral.com/articles/10.1186/s12954-023-00842-6){:target="_blank rel="noopener"}

