---
layout: post
title: "Beliefs about cannabis at the time of legalization in Canada: results from a general population survey"
date: 2024-09-24
tags: [Survey methodology,Addiction,Cannabis in Canada,Sampling (statistics),Risk,Cannabis,Cannabis use disorder,Drug,Health]
author: John Cunningham
institutions: Centre for Addiction and Mental Health
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/6945714"
---


The objective of this study was to examine beliefs about cannabis use at the time of legalization among past year cannabis users, those who had used cannabis but not in the last year, and people who had never used cannabis. In 2018, a general population telephone survey was conducted in Canada as part of a larger project examining perceptions of drug use [10]. Beliefs about cannabis use were compared between participants who had used cannabis (ever and past year) to those who had never used cannabis. Finally, participants were asked about their own use of cannabis (never, prior to past year, in the past 12 months; 1.3% missing data). Table 1  Cannabis use statuspNever used (n = 491)Ever used (n = 225)Past year use (n = 97)Mean (SD) age51.8 (18.9)49.7 (19.2)38.7 (16.3).001% Male49.944.552.8.27% Married/common law65.664.857.3.22% Some post-secondary69.179.468.8.015% Household income < $30,00010.511.611.8.90% Full/part time employed53.269.674.0.001  There were no significant differences (p > .05) in participants’ opinions about cannabis before and after it was legally available for sale in Canada (i.e., before and after October 1, 2018).