---
layout: post
title: "Sub-optimal nutrient regime coupled with Bacillus and Pseudomonas sp. inoculation influences trichome density and cannabinoid profiles in drug-type Cannabis sativa"
date: 2024-08-29
tags: [Rhizobacteria,Cannabinoid,Cannabis (drug),Tetrahydrocannabinolic acid,Trichome,Tetrahydrocannabinol,Cannabidiol,Cutting (plant),Fertilizer,Soil]
author: Cailun A. S | Eric D | Donald L
---


TABLE 3  For the control and consortium treatment for the recommended nutrient regime, n was 44 whereas for Pseudomonas sp. treatments n was 48. n was 48 for all four treatments under the low nutrient regime. FIGURE 3  3.3 Cannabinoid content changes due to nutrient conditions and PGPR inoculations  In order to ascertain whether changes in trichome densities are linked to changes in cannabinoid production, quantifications of cannabinoid contents from ground inflorescences under PGPR treatments and both nutrient conditions were obtained. Under recommended nutrient conditions, none of the concentrations of the cannabinoids measured were found to be significantly different in plants inoculated with bacteria compared to plants without bacterial treatment (Figure 4); the test for the statistical interaction model (treatment*compound) under the optimal nutrient regime only was not statistically significant (p = 0.56). TABLE 4  4 Discussion  This study has demonstrated that while PGPR inoculations do not have a significant effect on stalked trichome densities on cannabis inflorescence organs when plants were grown under recommended nutrient conditions, the application of an environmental stress for the first half of plant development reveals a benefit to applying these microbes.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2023.1131346/full){:target="_blank rel="noopener"}

