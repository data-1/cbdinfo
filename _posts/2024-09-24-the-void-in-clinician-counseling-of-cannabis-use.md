---
layout: post
title: "The Void in Clinician Counseling of Cannabis Use"
date: 2024-09-24
tags: [Medical cannabis,Opioid,Cannabis (drug),Cannabidiol,Cannabinoid,Dronabinol,Pain management,Tetrahydrocannabinol,Opioid use disorder,Substance abuse,Cannabis edible,Cannabis use disorder,Analgesic,Drug overdose,Mental disorder,Medical specialties,Health,Clinical medicine,Health care,Diseases and disorders,Social aspects of psychoactive drugs,Medicine,Psychoactive drugs]
author: Susan L. Calcaterra | Chinazo O. Cunningham | Christian J. Hopfer
institutions: University of Colorado Anschutz Medical Campus
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7280395"
---


The United States Preventive Services Task Force (USPSTF) recommends that primary care clinicians perform screening and brief intervention for unhealthy alcohol and tobacco use.1, 2 Increasingly, patients use cannabis to treat a myriad of medical conditions despite limited scientific evidence about the efficacy of cannabis use.3 How should clinicians counsel patients on the risks or benefits of cannabis use? As with any other addictive substance, it is important that clinicians provide their patients with evidence-based recommendations to prevent or reduce adverse health effects related to cannabis use. EVIDENCE SUPPORTING MEDICAL CANNABIS USE  Despite the known harms associated with cannabis use, there is evidence for the medical benefit of cannabis. When counseling patients, physicians may provide observational data on associated harms with high-potency cannabis use,6 but to date, these products have not been studied for medical use. In a study of 400 Colorado cannabis dispensaries, 69% of dispensaries contacted recommended cannabis for pregnancy-related nausea despite evidence supporting an association between maternal cannabis use and neonatal morbidity or death.23, 24 Rigorously conducted research is urgently needed to study high-potency cannabis for medical use before clinicians can adequately counsel patients.