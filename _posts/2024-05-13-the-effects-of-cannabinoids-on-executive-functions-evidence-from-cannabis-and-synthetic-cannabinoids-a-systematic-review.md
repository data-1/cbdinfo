---
layout: post
title: "The Effects of Cannabinoids on Executive Functions: Evidence from Cannabis and Synthetic Cannabinoids—A Systematic Review"
date: 2024-05-13
categories:
- 
author: Cohen, Weinstein, Aviv Weinstein
tags: [Cannabinoid,Cannabis (drug),JWH-018,Cognitive flexibility,Tetrahydrocannabinol,Endocannabinoid system,Psychoactive drug,Cannabinoid receptor,Attention,Cannabinoid receptor 1,Cannabidiol,Neuroscience]
---


These unique features contribute to the growing numbers of recreational drug users who have used SCs [ 4 5 ]. The Psychoactive Ingredients of Synthetic Cannabinoid Products  22,69, Over than 140 products containing SC have been identified, although, the main psychoactive components of these products are different types of SCs which are categorized into four major groups including; (a) Aminoalkylindole or JWH series, (b) classical cannabinoids, (c) non-classical cannabinoids and (d) fatty acid amides (e.g., oleamide) [ 21 70 ]. The first generation of SC products mostly contain the series of 1-alkyl-3-(1-naphthoyl) indoles known as JWH compounds or aminoalkylinodels. In addition, SCs from the CP series act as CB1 receptors full agonists [ 67 ]. In addition, SCs from the CP series act as CB1 receptors full agonists [ 67 ].

[Visit Link](https://www.mdpi.com/2076-3425/8/3/40){:target="_blank" rel="noopener"}


