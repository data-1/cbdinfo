---
layout: post
title: "Effect of aerosolized nicotine on human bronchial epithelial cells is amplified after co-administration with cannabidiol (CBD): a pilot in vitro study"
date: 2024-09-23
tags: [Cannabidiol,Electronic cigarette,Nicotine,Cannabinoid]
author: Noel J. Leigh | Maciej Ł. Goniewicz
institutions: 
doi: "https://bmcpharmacoltoxicol.biomedcentral.com/articles/10.1186/s40360-020-00418-1"
---


Lab-made and lab-modified refill solutions  Refill solutions containing propylene glycol only (PG, solvent control, 99 + % Acros Organics), PG with nicotine only (1.7 mg/ml, NIC), PG with CBD only (1.7 mg/ml; CBD), PG with nicotine and CBD (1.7 mg/ml each; NIC + CBD) as well as flavored liquid (Easy Rider) with PG, nicotine and CBD (1.7 mg/ml each; NIC + CBD + Flavor) were tested (Fig. PG + nicotine+CBD (NIC + CBD) exposure  When examining the effects of exposure to aerosol containing both NIC + CBD, we found a significant decrease in metabolic activity and cell viability compared to the air control (both assays p < 0.0001, Fig. PG + nicotine+CBD + flavor (NIC + CBD + Flavor) exposure  Cell viability and metabolic activity of H292 cells decreased significantly after exposure to aerosols from all liquids that contained NIC + CBD + Flavor compared to air (both assays p < 0.0001, Fig. Exposure to NIC + CBD + Flavor aerosol resulted in increased production of IL-6, CXCL1 and CXCL10 (p < 0.0035, Fig. Additionally, the use of flavored e-cigarette liquids with NIC + CBD resulted in a significantly increased pro-inflammatory response as compared to the air and PG controls as well as compared to the NIC + CBD liquid without flavoring (IL-1β.