---
layout: post
title: "The Atypical Cannabinoid Abn-CBD Reduces Inflammation and Protects Liver, Pancreas, and Adipose Tissue in a Mouse Model of Prediabetes and Non-alcoholic Fatty Liver Disease"
date: 2024-08-20
tags: [Metabolic dysfunction–associated steatotic liver disease,Insulin resistance,Inflammation,Beta cell,Adiponectin,Interleukin 6,Adipose tissue,Prediabetes,Macrophage,Diabetes,Pancreatic islets,Insulin,Hyperinsulinemia,Cannabinoid,Leptin,High-density lipoprotein,Cannabidiol,Lipoprotein,Immunohistochemistry,Medical specialties]
author: Romero-Zerbo | Silvana Y | García-Fernández | Espinosa-Jiménez | Pozo-Morales | Escamilla-Sánchez | Sánchez-Salido | Cobo-Vuilleumier | Rojo-Martínez | Benoit R
---


TABLE 1  ABN-CBD Ameliorates Hyperinsulinemia, Protects Beta Cells From DIO-Induced Apoptosis, Decreases Inflammation, and Stimulates Beta Cell Proliferation  After 2 weeks of treatment with vehicle or Abn-CBD, HFD-vehicle mice were hyperinsulinemic compared to SD-fed mice (Figure 2A) while blood glucose levels were comparable (Figure 2B). While no significant differences were found in brown adipose tissue among groups (Figure 5F), HFD-vehicle mice showed a significant decrease in Ucp1 expression in VAT and SAT that was counteracted by Abn-CBD (Figures 5G,H). Liver from HFD-vehicle mice had increased p-NFκB compared to liver from SD-fed mice, while liver from HFD-Abn-CBD mice had significantly reduced p-NFκB levels comparable to those in liver from SD-fed mice (Figure 7A). Additionally, liver from HFD-vehicle mice showed a significant 3.3-fold increase in the number of macrophages (F4/80+ cells) compared to SD-fed mice (Figure 7B). Overall, subchronic treatment with Abn-CBD in this mouse model improved low-grade inflammation, reverted hyperinsulinemia and decreased liver fibrosis without altering body weight or ectopic accumulation of fat in the liver.

[Visit Link](https://www.frontiersin.org/journals/endocrinology/articles/10.3389/fendo.2020.00103/full){:target="_blank rel="noopener"}

