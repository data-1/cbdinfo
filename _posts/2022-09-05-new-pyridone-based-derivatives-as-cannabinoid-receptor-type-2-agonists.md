---
layout: post
title: "New Pyridone-Based Derivatives as Cannabinoid Receptor Type 2 Agonists"
date: 2022-09-05
categories:
- Cannabinoid
author: Faúndez-Parraguez, Alarcón-Miranda, Cho, Young Hwa, Pessoa-Mahana, Gallardo-Garrido, Chung, Faúndez, Manuel Faúndez-Parraguez, Carlos Alarcón-Miranda
tags: [Cannabinoid receptor type 2,Receptor antagonist,Agonist,Cannabinoid,Ligand (biochemistry),Anandamide,Cannabinoid receptor,Pyridine,Receptor (biochemistry),Biochemistry,Physical sciences,Chemistry]
---


This receptor was identified and cloned from HL60 cells [ 3 ] and was initially considered as ‘peripheral cannabinoid receptor’ due to its wide distribution in peripheral cells and tissues, particularly in those of the immune system [ 4 5 ]. Various studies have shown that the activation of CB2R can block activation of microglia cells but has little effect on the normal functioning of neurons within the CNS [ 7 8 ]. 18,19,20,21, Previous reports have explored the 2-pyridone scaffold in the cannabinoid system particularly in the CB2R with promising results ( Figure 1 ) [ 17 22 ]. According to their findings, the three residues, W194, F117 and W258 play an important role in distinguishing agonist and antagonist response together with ligand efficacy [ Additionally, the recently reported Cryo-EM structure of human CB2R bound to the selective agonist [ 23 ] revealed important insight into the lipophilic binding cavity and provided structural determinants to distinguish CB2R agonists from antagonists. In an effort to identify novel CB2R agonists, in the present work we report the synthesis, evaluation and molecular docking study of two series of pyridone derivatives with the aim of initiating a SAR exploration around the pyridone central scaffold and identify new high affinity pyridone-based derivatives as CB2R ligands.

<hr>

[Visit Link](https://www.mdpi.com/1422-0067/22/20/11212/htm){:target="_blank" rel="noopener"}


