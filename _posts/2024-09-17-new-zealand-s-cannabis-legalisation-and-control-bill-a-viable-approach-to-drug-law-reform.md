---
layout: post
title: "New Zealand’s Cannabis Legalisation and Control Bill: a viable approach to drug law reform"
date: 2024-09-17
tags: [Cannabis,Cannabidiol,Drug liberalization,Harm reduction,Cannabis in Canada,Individual psychoactive drugs,Cannabaceae,Entheogens,Psychoactive drugs,Social aspects of psychoactive drugs]
author: Joseph M. Boden
institutions: University of Otago
doi: "https://www.publish.csiro.au/ah/Fulltext/AHv44n6_ED3"
---


The Ministry of Justice developed the Cannabis Legalisation and Control Bill, which provided a framework for the control, sale and supply of cannabis in New Zealand. New Zealand ranks ninth in the world for cannabis consumption, and cannabis is easily available for purchase from the black market. These findings imply that reducing harms associated with cannabis use requires control of the supply of cannabis, which prohibition has never achieved. Overseas research suggests that this level of control does reduce levels of use in younger people.,  Another feature of the Bill is that it was designed to restrict growth of the cannabis industry, pre-emptively addressing the possibility that large corporations would attempt to grow the market and predate on new and existing users, which drives substance-related harm. Public health advocates will continue to press the government to move forward with drug law reform and improving health, justice and social outcomes for all of society.