---
layout: post
title: "Bod Australia to start long Covid-19 trial using medicinal cannabis in UK"
date: 2022-07-25
categories:
- Cannabis
author: Globaldata Report
tags: [COVID-19 pandemic,Clinical trial,Privacy,Risk,Medication,Personal data,Health,Health care,Medicine,Health sciences]
---


Bod Australia is to start a clinical trial of its medicinal cannabis product, MediCabilis 5%, to treat long Covid-19 after the UK Medicines and Healthcare Products Regulatory Agency (MHRA) granted the company a clinical trial authorisation (CTA). Free Report Reshape regional strategies to navigate global uncertainties The COVID-19 crisis triggered one of the worst peacetime recessions globally. In the trial, subjects will be given MediCabilis daily for six months. The combined data will establish the feasibility and safety of medicinal cannabis and MediCabilis to treat the condition. “We will utilise the data generated to gain a better understanding of whether MediCabilis can be used as a potential treatment and how we can expedite further product commercialisation, which will underpin ongoing sales growth.”  MediCabilis is currently used to treat conditions such as anxiety, chronic pain, and sleep disorders, which are also common in long Covid-19 patients.

<hr>[Visit Link](https://www.clinicaltrialsarena.com/news/bod-australia-cannabis-long-covid/){:target="_blank" rel="noopener"}


