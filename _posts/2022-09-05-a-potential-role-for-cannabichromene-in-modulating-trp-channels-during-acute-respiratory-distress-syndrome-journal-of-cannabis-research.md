---
layout: post
title: "A potential role for cannabichromene in modulating TRP channels during acute respiratory distress syndrome - Journal of Cannabis Research"
date: 2022-09-05
categories:
- CBC
author: Khodadadi, Department Of Oral Biology, Diagnostic Sciences, Dental College Of Georgia, Augusta University, Augusta, Center For Excellence In Research, Scholarship, Innovation, Salles
tags: [Acute respiratory distress syndrome,Cannabidiol,Transient receptor potential channel,Complete blood count,Cannabinoid,Inflammation,Tetrahydrocannabinol,TRPV1,TRPA1,COVID-19,Cytokine,Creative Commons license,Macrophage,Medical specialties,Medicine,Clinical medicine]
---


Background  Acute respiratory distress syndrome (ARDS) is a life-threatening clinical syndrome whose potential to become one of the most grievous challenges of the healthcare system evidenced by the COVID-19 pandemic. Mounting evidence including our findings supporting the notion that cannabinoids have potential to be targeted as regulatory therapeutic modalities in the treatment of inflammatory diseases. Methods  We used, for the first time, an inhalant CBC treatment as a potential therapeutic target in a murine model of ARDS-like symptoms. Further analysis showed that CBC may wield its protective effects through transient receptor potential (TRP) cation channels, TRPA1 and TRPV1, increasing their expression by 5-folds in lung tissues compared to sham and untreated mice, re-establishing the homeostasis and immune balance. Conclusion  Our findings suggest that inhalant CBC may be an effective alternative therapeutic target in the treatment of ARDS.

<hr>

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00101-0){:target="_blank" rel="noopener"}


