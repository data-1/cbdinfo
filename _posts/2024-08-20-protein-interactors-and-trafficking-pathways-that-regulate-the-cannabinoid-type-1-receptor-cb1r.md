---
layout: post
title: "Protein Interactors and Trafficking Pathways That Regulate the Cannabinoid Type 1 Receptor (CB1R)"
date: 2024-08-20
tags: [Cannabinoid receptor 1,Endocannabinoid system,G protein-coupled receptor,Protein targeting,Lipid raft,Cell signaling,Synapse,Arrestin,Endocytosis,Cannabinoid,Golgi apparatus,Chemical synapse,Endosome,Axon,Receptor (biochemistry),Cell membrane,Palmitoylation,Neuron,Signal peptide,Vesicle (biology and chemistry),Cell biology,Biotechnology,Neurochemistry,Biology,Molecular biology,Biochemistry]
author: Fletcher-Jones | Keri L | Ashley J | Jeremy M | Kevin A
---


Cannabinoid type 1 receptor surface expression in neurons is highly polarized to axons and presynaptic sites, where ligand binding results in the downregulation of presynaptic release through coupling to a variety of downstream signaling pathways (Figure 1). FIGURE 2  Downstream signaling of CB1R also modulates the activation of a number of kinases. Consequently, CB1R undergoes constitutive internalization, and this has been proposed as a mechanism for the polarized surface distribution of CB1R in neurons, since it has been reported that rapid rates of constitutive endocytosis in somatodendritic compartments leads to an accumulation of CB1R at the axonal plasma membrane (Leterrier et al., 2006; McDonald et al., 2007a; Simon et al., 2013). Importantly, however, axonal surface expression of CD4-ctCB1R was not completely polarized, suggesting that other localization motifs or signals potentially induced by agonist binding are required for full polarization. Proteins That Interact With ctCB1R  Although not as extensively characterized as many other neurotransmitter receptors, there have been a number of studies investigating the proteins that interact with the C-terminal domain of CB1R.

[Visit Link](https://www.frontiersin.org/journals/molecular-neuroscience/articles/10.3389/fnmol.2020.00108/full){:target="_blank rel="noopener"}

