---
layout: post
title: "Terpenoids From Cannabis Do Not Mediate an Entourage Effect by Acting at Cannabinoid Receptors"
date: 2024-08-20
tags: [Cannabinoid,Cannabinoid receptor 2,Cannabinol,Tetrahydrocannabinol,Cannabidiol,Biochemistry,Neurochemistry]
author: David B | Kathleen J
---


For binding assays, radioligand ([3H]-CP55,490, PerkinElmer, Waltham, MA, USA), non-radiolabelled drugs, and P2 membrane preparations were diluted in binding buffer (50 mM HEPES pH 7.4, 1 mM MgCl2, 1 mM CaCl2, 2 mg/ml NZ-origin BSA, MP Biomedicals, Santa Ana, CA, USA) and dispensed into 96‐well, polypropylene V‐well plates (Hangzhou Gene Era Biotech Co Ltd, Zhejiang, China) in a final reaction volume of 200 µl (membranes were dispensed last). FIGURE 1  FIGURE 2  For cAMP data, tests were performed for forskolin alone, and with each other drug (2-AG, Δ9-THC, CBD, terpene, or terpene mixture). Discussion  Overall, these data do not support the idea that any of the five terpenes tested in this study contribute to a putative entourage effect directly through the cannabinoid receptors. β-Caryophyllene was found to bind weakly to CB2 alone, but no other functional or binding effects were detected for the terpenes alone or in combination with CBD, or cannabinoid agonists 2-AG and Δ9-THC. However, in combination with Santiago et al. (2019), there is likely now sufficient data to rule out direct interactions with either cannabinoid receptor as being the mechanism by which an entourage effect is mediated, so attention must move to other types of effect.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2020.00359/full){:target="_blank rel="noopener"}

