---
layout: post
title: "α-Glucosidase inhibitory activity of cannabidiol, tetrahydrocannabinol and standardized cannabinoid extracts from Cannabis sativa"
date: 2022-08-13
categories:
- Cannabinoid
- CBD
- THC
author: 
tags: [Cannabidiol,Tetrahydrocannabinol,Cannabinoid,Cannabis sativa,Pharmacognosy,Drugs,Biochemistry,Medicinal chemistry,Pharmacology,Chemistry,Cannabis,Medical research,Medical treatments,Cannabaceae,Pharmaceutical sciences,Biotechnology]
---


Two major cannabinoids of cannabis, namely cannabidiol (CBD) and tetrahydrocannabinol (THC) have been reportedly used as alternative medicine for diabetes treatment in both pre-clinical and clinical research. However, their mechanisms of action still remain unclear. Therefore, this study aimed to evaluate the α-glucosidase inhibitory activity of THC, CBD and the standardized cannabinoid extracts. In addition, the enzyme based in vitro assay on α-glucosidase revealed that THC and CBD exhibited good inhibitory activity, with the IC 50 values of 3.0 ± 0.37 and 5.5 ± 0.28 μg/ml, respectively. Furthermore, two standardized cannabinoid extracts, SCE-I (C. sativa leaf extract) and SCE-II (C. sativa inflorescence extract) exhibited stronger inhibitory activity than THC and CBD, with the IC 50 values of 1.2 ± 0.62 and 0.16 ± 0.01 μg/ml, respectively.

<hr>

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2665927122001046){:target="_blank" rel="noopener"}


