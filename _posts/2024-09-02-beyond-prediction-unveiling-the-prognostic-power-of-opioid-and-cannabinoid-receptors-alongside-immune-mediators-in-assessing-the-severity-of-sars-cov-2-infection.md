---
layout: post
title: "Beyond prediction: unveiling the prognostic power of μ-opioid and cannabinoid receptors, alongside immune mediators, in assessing the severity of SARS-CoV-2 infection"
date: 2024-09-02
tags: [COVID-19,Inflammation,Cannabinoid receptor 2,Cytokine,Osteopontin,Immune system,Biomarker,Receiver operating characteristic,Interleukin 17,Virus,SARS-CoV-2,Interferon gamma,Respiratory syncytial virus,Angiotensin-converting enzyme 2,Immunology,Biochemistry,Medical specialties,Cell biology,Biology,Clinical medicine]
author: Tavakoli-Yaraki | Fatemeh Nejat | Seyed Bashir | Zahra Noorani | Mohammad Amin | Mokhtari-Azad
---


Conversely, activation of CB receptors during influenza infection led to reduced immune cell recruitment but increased viral replication, hinting at their involvement in viral replication processes [15]..  Additionally, studies in Balb/c mice infected with RSV post-vaccination demonstrated that blocking the opioid receptor resulted in heightened immune responses and exacerbated infection outcomes, while also enhancing vaccine efficacy in reducing reinfection rates [16].. Notably, all patients were evaluated at their first visit to clinics, and the samples were collected before hospitalization and none of the patients had received COVID-19 vaccine at the time of investigation and sampling. The association of CB2, MOR, lL-17, OPN, MCP-1 and IFN-γ with demographic features of patients and their assessing and predictive values  The assessing values of the measured variables in COVID-19 patients were assessed using ROC curve analysis. Additionally, we detected an elevated expression of MOR in both male and female patients, which was associated with disease severity. Activation of the CB2 receptor has been linked to the upregulation of OPN and other mediators in human periodontal ligament cells, suggesting a potential interaction between CB2 receptors and OPN [49]... Additionally, it has been theorized that SARS-CoV-2 infection may directly impact IL-17 through the binding of ORF8 to the IL-17 receptor, triggering IL-17-induced inflammatory responses [50]... Our data consistently showed increased circulating IL-17 levels in patients with severe infection, underscoring the significant role of IL-17 in this disease.

[Visit Link](https://bmcinfectdis.biomedcentral.com/articles/10.1186/s12879-024-09280-6){:target="_blank rel="noopener"}

