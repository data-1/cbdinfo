---
layout: post
title: "Optimizing Photoperiod Switch to Maximize Floral Biomass and Cannabinoid Yield in Cannabis sativa L.: A Meta-Analytic Quantile Regression Approach"
date: 2024-08-21
tags: [Regression analysis,Cannabis sativa,Linear regression,Quantile regression,Cannabis,Errors and residuals,Quantile,Tetrahydrocannabinolic acid,Cannabinoid,Cannabidiol]
author: Nishara Muthu | Lesley G
---


Is the optimal time spent under long-day lighting different for maximal floral biomass production vs. maximal cannabinoid concentration in inflorescences? Our inclusion criteria required that studies of C. sativa report: (1) harvested yield as floral biomass and/or cannabinoid concentration; (2) the number of days spent under long day length lighting during the vegetative growth stage; (3) the timing of a definitive switch between long day (≥18 h of light) to short-day lighting (≤12 h of light) conditions for C. sativa (rather than a gradual change in photoperiod as might occur outdoors). Using Regression Models for Describing the Relationship Between Lighting Duration and Yield Outcomes  When using long day lighting to predict yield outcomes, floral biomass is best described using linear regression models (either simple or quantile) and cannabinoid content is best described using a quadratic quantile regression model (Figures 2, 3; Tables 1, 2). Optimizing Floral Biomass and Cannabinoid Potency Using Best Photoperiodic Switch Practices  The optimal duration of long day lighting exposure to maximize floral biomass compared to THC and CBD concentration in C. sativa are different. Conclusion  In conclusion, our analysis predicts that floral biomass and cannabinoid concentration of C. sativa can be maximized by growing plants under different long day length lighting durations: floral biomass is optimized when long day lighting duration is minimized, THC concentration is optimized at 42 days of long day lighting, and CBD in low CBD plants is optimized a 49–50 days of long day lighting.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2021.797425/full){:target="_blank rel="noopener"}

