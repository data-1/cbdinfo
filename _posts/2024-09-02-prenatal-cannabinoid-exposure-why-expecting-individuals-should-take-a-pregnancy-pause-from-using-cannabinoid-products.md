---
layout: post
title: "Prenatal cannabinoid exposure: why expecting individuals should take a pregnancy pause from using cannabinoid products"
date: 2024-09-02
tags: [Cannabis (drug),Epigenetics,Anandamide,Endocannabinoid system,Prenatal development,Cannabidiol,Fetal programming,Nabilone,Vaping,Tetrahydrocannabinol,Pregnancy,Cannabinoid,Dronabinol,Synthetic cannabinoids,Cannabinoid receptor 1,Health sciences,Health]
author: Gelonia L | Zarena M | Leigh-Anne | Gabrielle L | Jessie R
---


Prevalence of perinatal cannabinoid Use  Has the proliferation of nCB contributed to the increased prevalence of perinatal cannabinoid use? The equivocal results of many studies of perinatal cannabinoid use, the increased potency of natural and synthetic cannabis products, the increased availability of cannabinoids due to ever-increasing legalization for medical and recreational uses, and the perceived innocuousness of cannabinoids have all contributed to an increased prevalence of pregnant individuals using cannabinoid products. Individuals were recruited from prenatal clinics at the major hospitals in Ottawa, and those with prenatal use of cannabinoids, alcohol, and tobacco were included as well as individuals with no prenatal use (79). Of the total sample, 9,312 pregnant individuals reported lifetime cannabinoid use resulting in 10,373 pregnancies. However, in only 272 pregnancies had participants used cannabinoids during pregnancy.

[Visit Link](https://www.frontiersin.org/journals/pediatrics/articles/10.3389/fped.2023.1278227/full){:target="_blank rel="noopener"}

