---
layout: post
title: "Converting Sugars into Cannabinoids—The State-of-the-Art of Heterologous Production in Microorganisms"
date: 2022-08-05
categories:
- Cannabinoid
author: Favero, Gabriel Rodrigues, De Melo Pereira, Gilberto Vinícius, De Carvalho, Júlio Cesar, De Carvalho Neto, Dão Pedro, Soccol, Carlos Ricardo
tags: [Cannabinoid,Tetrahydrocannabinol,Cannabidiol,Saccharomyces cerevisiae,Terpene,Cannabigerol,Metabolic engineering,Metabolism,Dronabinol,Cannabis sativa,Pichia pastoris,Biosynthesis,Biochemistry,Chemistry]
---


The cells are then sent to a settling tank (ST-101) in which ethyl acetate is used with a 2:1 ratio to resuspend the cells and subsequently promote liquid-liquid extraction. The biphasic mixture passes through a liquid–liquid separator (LS-101), wherein the upper (organic) phase contains cannabinoids, ethyl acetate, and the lower phase is composed of water, ethyl acetate, and nutrients/culture medium. 9-THCA oxidation into CBNA and other secondary reactions [ The filtrate is then sent to a set of multiple effects falling film evaporators (EV-01/02) to remove part of the solvent and prepare the product for the decarboxylation step. The last step is to remove the residual solvent in the product and promote the decarboxylation of Δ-THCA into Δ-THC. For this step, a decarboxylation vacuum oven (DO-101) is proposed, in which the mixture is dispersed into trays with temperature close to 120 °C for up to one hour [ 105 ].

<hr>

[Visit Link](https://www.mdpi.com/2311-5637/8/2/84/htm){:target="_blank" rel="noopener"}


