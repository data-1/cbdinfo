---
layout: post
title: "Partially substituting alfalfa hay with hemp forage in the diet of goats improved feed efficiency, ruminal fermentation pattern and microbial profiles"
date: 2024-05-24
categories:
- 
author: 
tags: [Rumen,Feed conversion ratio,Meat,Biology]
---


This study investigated the effects of partial substitution of alfalfa hay (AH) with hemp forage (HF) in growing goat diets on growth performance, carcass traits, ruminal fermentation characteristics, rumen microbial communities, blood biochemistry, and antioxidant indices. However, final BW, average daily gain, carcass traits, meat quality, and most blood biochemistry indices did not differ among treatments. The ruminal NH3-N concentration and blood urine nitrogen linearly increased (P < 0.01) with increasing substitution rate of HF, whereas the total volatile fatty acids concentration quadratically changed (P < 0.01). These results indicate that the HF could be used to partially substitute AH in goat diets, whereas the effects vary between substitution rates of HF for AH. Although no cannabinoid-related residues were detected in meat, the presence of cannabinoids residues in blood warrants further study of HF feeding to confirm the cannabinoids residues are not present in the animal products.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2405654524000052){:target="_blank" rel="noopener"}


