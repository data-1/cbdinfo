---
layout: post
title: "Analysis of Morphological Traits, Cannabinoid Profiles, THCAS Gene Sequences, and Photosynthesis in Wide and Narrow Leaflet High-Cannabidiol Breeding Populations of Medical Cannabis"
date: 2024-08-21
tags: [Cannabis,Cannabis indica,Cannabidiol,Cannabis sativa,Photosynthesis,Tetrahydrocannabinolic acid,Hemp,Primer (molecular biology),Cannabinoid,Biology]
author: Jan Jurij
---


Significant morphological differences were found between plants and chemotypes. These analyses were performed on five plants per breeding population (N = 5). The measurements were done on five plants per breeding population (N = 5). As recently reported in an excellent study in which Jin et al. (2021b) phenotypically characterised 21 cannabis cultivars covering three chemical phenotypes, morphological traits can be used reliably to distinguish among cannabis chemotypes, which facilitates taxonomic classification. High maximum photosynthetic rates indicate that the plants were grown under suitable conditions.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2022.786161/full){:target="_blank rel="noopener"}

