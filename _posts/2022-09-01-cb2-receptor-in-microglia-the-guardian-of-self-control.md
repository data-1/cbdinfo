---
layout: post
title: "CB2 Receptor in Microglia: The Guardian of Self-Control"
date: 2022-09-01
categories:
- Cannabinoid
author: Komorowska-Müller, Joanna Agnieszka, Schmöle, Joanna Agnieszka Komorowska-Müller, Anne-Caroline Schmöle
tags: [Cannabinoid receptor type 2,Microglia,Neuroinflammation,Amyloid beta,Amyloid plaques,Inflammation,Immune system,MAPKERK pathway,Memory,Neurofibrillary tangle,Cre-Lox recombination,Interleukin 13,Cell signaling,Neuroscience,Cell biology,Biochemistry,Neurochemistry,Biotechnology,Physiology,Biology,Medical specialties,Signal transduction]
---


This process is impaired during later stages of AD and the increased levels of tau lead to enhanced activation of microglia [ 59 ]. However, most of the studies investigate the overall effect of CB2R activation on AD pathology rather than the specific role in microglial activation. However, it improved the cognitive performance of mice. Deletion of the CB2R led to decreased microgliosis and reduced number of infiltrating cells from the periphery, which was subsequently accompanied by decreased neuronal loss and improved cognitive performance [ 51 73 ]. Interestingly, the results between pharmacological and genetic manipulation of the CB2R in AD mouse model differ regarding their effect on microglial activity and AD-induced neuroinflammation.

<hr>

[Visit Link](https://www.mdpi.com/1422-0067/22/1/19/htm){:target="_blank" rel="noopener"}


