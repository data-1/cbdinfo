---
layout: post
title: "1,2,3-Triazole derivatives as highly selective cannabinoid receptor type 2 (CB2) agonists"
date: 2022-08-27
categories:
- Cannabinoid
author: 
tags: [Cannabinoid receptor type 2,Ligand (biochemistry),Chemistry,Drugs,Cellular processes,Pharmacology,Biochemistry,Neurochemistry,Cell signaling,Biotechnology,Signal transduction,Cell biology,Cell communication,Neurophysiology,Receptors,Medicinal chemistry,Neuroscience]
---


The cannabinoid receptor 2 (CB2 receptor) has attracted considerable interest, mainly due to its potential as a target for therapeutics for treating various diseases that have a neuroinflammatory or neurodegenerative component while avoiding the adverse psychotropic effects that accompany CB1 receptor-based therapies. With the appreciation that CB2-selective ligands show marked functional selectivity, there is a renewed opportunity to explore this promising area of research from both a mechanistic as well as a therapeutic perspective. In this research, we are interested in the discovery of new chemotypes as highly selective CB2 modulators, which may serve as good starting points for further optimization towards the development of CB2 therapeutics. In search of new chemotypes as CB2 selective agents, we screened a series of triazole derivatives with interesting bioactive scaffolds, which led to the discovery of two novel and highly selective ligands for CB2 receptors. Compounds 6 and 11 produced a concentration-dependent inhibition of specific [3H]-CP55,940 (CB2) binding with K i ± SEM values of 105.3 ± 22.6 and 116.4 ± 19.5 nM, respectively, while no binding affinity towards CB1 receptors or opioid receptors was observed.

<hr>

[Visit Link](https://www.sciencedirect.com/science/article/pii/S1878535221005608){:target="_blank" rel="noopener"}


