---
layout: post
title: "In Vitro Effect of Δ9-Tetrahydrocannabinol and Cannabidiol on Cancer-Associated Fibroblasts Isolated from Lung Cancer"
date: 2022-07-31
categories:
- CBD
- THC
author: Milián, Monleón-Guinot, Sancho-Tello, Galbis, José Marcelo, Cremades, Almenar-Ordaz, Peñaroja-Martinez, Farras, Martín De Llano
tags: [Tumor microenvironment,Cannabidiol,Cannabinoid,Epithelialmesenchymal transition,Tetrahydrocannabinol,Cannabinoid receptor type 2,Transforming growth factor beta,Metastasis,Cancer,Biology,Biochemistry,Cell biology,Clinical medicine]
---


No effect on type I collagen protein expression was observed in cells cultured with A549 cell CM without TGFβ treatment ( Figure 6 B,F). We cultured A549 cells with NF or CAF CM treated with or without THC and/or CBD. On the other hand, cell density was significantly higher in A540 cells cultured with NF and CAF CM, in which both THC and CBD, alone or in combination, significantly inhibited these increases, except when exposed to NF CM treated with 30 µM THC alone. In the case of CAFs, THC did not generate any inhibitory effect, while CBD completely abolished the increase induced by TGFβ, both alone and in combination with THC, although substantially less inhibition was observed in this case (panel B). Once again, treatment with THC and CBD, alone or in combination, inhibited these changes, reinforcing the beneficial use of both drugs in combination, since we observed similar effects but using a dose three times lower of these compounds.

<hr>[Visit Link](https://www.mdpi.com/1422-0067/23/12/6766/htm){:target="_blank" rel="noopener"}


