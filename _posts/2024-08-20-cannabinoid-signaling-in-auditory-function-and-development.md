---
layout: post
title: "Cannabinoid Signaling in Auditory Function and Development"
date: 2024-08-20
tags: [Superior olivary complex,Endocannabinoid system,Chemical synapse,Cannabinoid receptor 2,Cannabinoid receptor 1,Synapse,Cannabinoid,Anandamide,Inhibitory postsynaptic potential,Cell biology,Basic neuroscience research,Neurochemistry,Neurophysiology,Biochemistry,Branches of neuroscience,Physiology]
author: Bradley J
---


Upon ligand binding, CB1 receptors can also induce PI3K/Akt activation in many cell types including glial cells where it promotes cell survival (Galve-Roperh et al., 2002; Gomez et al., 2011). In addition to CB1 and CB2 receptors, TRPV1, TRPV2, TRPV3, TRPV4, TRPA1, and TRPM8 have all been shown to respond to CB ligands (Muller et al., 2009). ECB Signaling in The Development of The Auditory System  Currently, there is limited information regarding cannabinoid signaling during inner ear development. However currently, we have rudimentary knowledge on the distribution and function of ECS components in the development or function of cochlear ribbon synapses. Loss of function mutations in ECS genes in zebrafish, mice, and human populations all suggest critical developmental roles, including important functions in the establishment and innervation of sensory epithelia and therefore it is critical that future studies more closely examine the role of ECS in these processes and in otic development and function.

[Visit Link](https://www.frontiersin.org/journals/molecular-neuroscience/articles/10.3389/fnmol.2021.678510/full){:target="_blank rel="noopener"}

