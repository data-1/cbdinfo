---
layout: post
title: "Subgroup trends in alcohol and cannabis co-use and related harms during the rollout of recreational cannabis legalization in Washington state"
date: 2024-09-16
tags: [2012 Washington Initiative 502,Cannabis in Canada,Cannabis (drug),Cannabis,Psychoactive drugs,Health,Cannabaceae,Individual psychoactive drugs,Social aspects of psychoactive drugs]
author: Meenakshi S. Subbaraman | William C. Kerr
institutions: Alcohol Research Group | Public Health Institute
doi: "https://www.sciencedirect.com/science/article/abs/pii/S0955395919301811?via%3Dihub"
---


Although legalized possession and amended DUI limits went into effect on December 9, 2012, licensed retail stores did not open until July 2014 (Washington State Liquor and Cannabis Board, 2017). Washington and Colorado were the first US states to legalize recreational cannabis, and few studies have examined trends in cannabis use in the post-legalization period within each state. Subgroups analyses of the NSDUH also show that the prevalence of cannabis use increased by 4% for men and 2.7% women from 2002 to 2014, with all of the increase occurring between 2007–2014 (Carliner et al., 2017). Similarly, a study of the 1984–2015 National Alcohol Surveys (NAS) using age-period-cohort models concluded that the increase in cannabis use since 2005 occurred across the whole population and is attributable to general period effects not linked specifically to liberalized cannabis legislation (Kerr, Lui, & Ye, 2018). Thus, we would expect that any changes in the prevalence of simultaneous cannabis and alcohol co-use in the general population would be accompanied by changes in the prevalence of alcohol-related harms.