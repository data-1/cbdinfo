---
layout: post
title: "Botanically-Derived &Delta;9-Tetrahydrocannabinol and Cannabidiol, and Their 1:1 Combination, Modulate Toll-like Receptor 3 and 4 Signalling in Immune Cells from People with Multiple Sclerosis"
date: 2022-08-05
categories:
- CBD
- THC
author: Fitzpatrick, Hackett, Costelloe, Hind, Downer, Eric J., John-Mark Fitzpatrick, Becky Hackett, Lisa Costelloe, William Hind
tags: [Toll-like receptor 4,Cannabinoid,Cannabidiol,Toll-like receptor,Tetrahydrocannabinol,Cannabinoid receptor type 2,Interferon,Inflammation,Cannabinoid receptor,Cell signaling,Tumor necrosis factor,Interferon type I,Cannabinoid receptor type 1,Gene expression,Immune system,Cytokine,Toll-like receptor 3,Biotechnology,Biology,Immunology,Biochemistry,Medical specialties,Cell biology,Signal transduction,Clinical medicine]
---


Given the inhibitory effects of THC:CBD on TLR3 signalling to CXCL10 and IFN-β in PBMCs, in addition to evidence linking TLR4 to MS pathogenesis [ 15 16 ], we next set out to determine the proclivity of THC and CBD, when delivered alone and in a 1:1 combination, to modulate TLR4-induced TNF-α protein expression in PBMCs isolated from HCs and pwMS. Indeed, THC:CBD inhibited poly(I:C)-induced IFN-β protein expression by 54% and 71% on average in immune cells from HCs and pwMS, respectively ( Figure 3 e). The inhibitory effect of the combination of phytocannabinoids on TLR3-induced IFN-β protein expression was more effective than administration of THC or CBD alone in PBMCs from both HCs and pwMS, again, indicating an additive effect of their combination ( Figure 3 e). This inhibitory effect of the combination of phytocannabinoids on TLR3-induced CXCL10 was significantly more effective than administration of either THC or CBD alone in PBMCs from both HCs and pwMS, indicating an additive effect of their combination ( Figure 2 e). To determine the impact of disease on TLR3/4 signalling in PBMCs isolated from the current cohort of study participants, PBMCs from HCs and pwMS were treated with poly(I:C) or LPS for 24 h, supernatants harvested and assessed for protein expression of CXCL10 or IFN-β (to assess TLR3 signalling) ( Table 3 ), and TNF-α (to assess TLR4 signalling) ( Table 4 ).

<hr>

[Visit Link](https://www.mdpi.com/1420-3049/27/6/1763/htm){:target="_blank" rel="noopener"}


