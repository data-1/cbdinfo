---
layout: post
title: "The preservation and augmentation of volatile terpenes in cannabis inflorescence - Journal of Cannabis Research"
date: 2022-07-26
categories:
- cannabis
- terpene
author: Bueno, Vireo Health, Minneapolis, Leuer, Kearney, Green, Edward H., Greenbaum, Eric A., Lagoon Ave
tags: [Cannabis sativa,Gas chromatographymass spectrometry,Terpene,Sampling (statistics)]
---


The differing rates of terpene loss from inflorescence of the same chemotype, but different harvests, is further illustrative of the inherent variability of cannabis inflorescence terpene content. Conversely, chemotype and storage time did not impact the novel’s system ability to preserve terpenes. Results illustrate the novel system was able to replenish the terpene content of the DjG inflorescence samples. Similar to preliminary results, utilizing the novel system with chemotype specific terpenes as the ‘external’ volatiles, is expected reduce terpene loss over time, and batch to batch variability of the terpene profile. Conversely, the ability to selectively adjust the terpene profiles of inflorescence samples to be dominated by individual terpenes was achieved with an accuracy of 95.4% for α-pinene and 89.0% for β-myrcene.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-020-00035-z){:target="_blank" rel="noopener"}


