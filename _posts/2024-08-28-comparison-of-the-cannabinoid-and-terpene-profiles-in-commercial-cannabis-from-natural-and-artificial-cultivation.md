---
layout: post
title: "Comparison of the Cannabinoid and Terpene Profiles in Commercial Cannabis from Natural and Artificial Cultivation"
date: 2024-08-28
tags: [Cannabinol,Cannabinoid,Cannabis (drug),Tetrahydrocannabinolic acid,Tetrahydrocannabinol,Cannabidiol,Gas chromatography–mass spectrometry,Cannabis,Metabolomics,Dronabinol,Terpene,Liquid chromatography–mass spectrometry]
author: Fereshteh Zandkarimi | John Decatur | John Casali | Tina Gordon | Christine Skibola | Colin Nuckolls
---


We analyzed three samples of each (n = 3) for both indoor and outdoor samples. We identified 9 monoterpenes and 14 sesquiterpenes in CP groups and 10 monoterpenes and 15 sesquiterpenes in RV samples (Tables S2 and S3). Constituents of Cannabis Sativa. Cannabis Cannabinoids 2018, 1, 19–27. [Google Scholar] [CrossRef]  Danziger, N.; Bernstein, N. Light matters: Effect of light spectra on cannabinoid profile and plant development of medical cannabis (Cannabis sativa L.).

[Visit Link](https://www.mdpi.com/1420-3049/28/2/833){:target="_blank rel="noopener"}

