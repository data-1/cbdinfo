---
layout: post
title: "Cell type specific cannabinoid CB1 receptor distribution across the human and non-human primate cortex"
date: 2024-08-25
tags: [Cannabinoid receptor 1,Chemical synapse,Inhibitory postsynaptic potential,Neurotransmitter]
author: Kenneth N | David A | Robert A
---


Given the paucity of such studies, the current work investigated normative distribution of cell type specific CB1R within two representative cortical regions using healthy postmortem human and non-human primate samples. In both monkey and human samples, we found that CB1R was present in both excitatory and inhibitory neuron populations within the PFC, primary auditory cortex, and association auditory cortex, with significantly higher levels within inhibitory populations. Within the excitatory boutons, mean CB1R intensity was consistent across brain regions in monkey samples, while it was lowest in the PFC compared to the auditory regions in human samples. Regional and cell type by region interactions of CB1R  In the current study, we identified the presence of CB1R within both excitatory and inhibitory neuronal populations in the human and non-human primate PFC and auditory cortices. In addition, we identified differences in cell type specific CB1R distribution across brain regions between non-human primate and human postmortem brain samples—namely a regional difference in excitatory bouton CB1R intensity in human samples that was not present in monkey samples.

[Visit Link](https://www.nature.com/articles/s41598-022-13724-x){:target="_blank rel="noopener"}

