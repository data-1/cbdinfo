---
layout: post
title: "Potential of NIRS Technology for the Determination of Cannabinoid Content in Industrial Hemp (Cannabis sativa L.)"
date: 2022-07-31
categories:
- Cannabinoid
- hemp
author: Jarén, Zambrana, Paula C., Pérez-Roncal, López-Maestresalas, Ábrego, Arazuri, Carmen Jarén, Paula C. Zambrana, Claudia Pérez-Roncal
tags: [Cannabidiol,Cannabis,Cannabinoid,Tetrahydrocannabinol,Cannabis sativa,Tetrahydrocannabinolic acid,High-performance liquid chromatography,Near-infrared spectroscopy,Spectroscopy,Hemp,Spectrophotometry,Infrared,Cross-validation (statistics),Chromatography]
---


In the prediction of total THC content, RPD values > 2 were obtained, which indicates the goodness of the models. The best model to predict CBD content was also obtained without applying any data pretreatment technique. As in this study, other authors have achieved good results for the prediction of THC and CBD by NIRS without any data pretreatment. As in this study, other authors have achieved good results for the prediction of THC and CBD by NIRS without any data pretreatment. PLS models were developed for both types of sample, obtaining good results with R2p and RMSEP values of 0.99% and 2.32% for THC, and 0.99% and 1.33% for CBD, respectively, for the flower samples, and R2p and RMSEP values of 0.95% and 3.79% for THC, and 0.99% and 1.44% for CBD, in the cannabis extract samples, respectively.

<hr>[Visit Link](https://www.mdpi.com/2073-4395/12/4/938/htm){:target="_blank" rel="noopener"}


