---
layout: post
title: "Control of glutamate release by complexes of adenosine and cannabinoid receptors"
date: 2024-09-24
tags: [Adenosine A2A receptor,Receptor antagonist,Cannabinoid receptor 1,Dopamine receptor D2,Cannabinoid,Synapse,Bimolecular fluorescence complementation,G protein-coupled receptor,Allosteric regulation,Cell signaling,Neurophysiology,Cell communication,Signal transduction,Biology,Nutrients,Proteins,Biochemistry,Neurochemistry,Cell biology,Molecular biology]
author: Attila Köfalvi | Estefanía Moreno | Arnau Cordomí | Ning-Sheng Cai | Víctor Fernández‐Dueñas | Samira Ferreira | Ramón Guixà-González | Marta Sánchez‐Soto | Hideaki Yano | Verònica Casadó-Anguera | Rodrigo A. Cunha | Ana M. Sebastião | Francisco Ciruela | Leonardo Pardo | Vicent Casadó | Sergi Ferré
institutions: 
doi: "https://bmcbiol.biomedcentral.com/articles/10.1186/s12915-020-0739-0#Sec9"
---


These results also indicated that the ability of A1R in the A1R-A2AR heteromer to mediate inhibition of glutamate release in the striatal glutamatergic terminals was not dependent on the canonical Gs-Gi antagonistic interaction at the AC level and, therefore, on the inhibition of A2AR-mediated AC-PKA signaling. The results in Fig. 2, the results in Fig. As expected from their respective coupling to Gi and Gs proteins, the CB1R agonist CP55940 (200 nM) decreased cAMP formation induced by forskolin (500 nM) and the selective A2AR agonist CGS21680 (500 nM) increased cAMP in cells transfected with CB1R or A2AR, respectively (Additional file 1: Figure S4a and S4b). That was the ability of the dopamine D4 receptor (D4R) to modify the constitutive activity in the D2R-D4R heteromer [32], also localized in striatal glutamatergic terminals, where it mediates a presynaptic dopaminergic inhibitory control of glutamate release [50,51,52].