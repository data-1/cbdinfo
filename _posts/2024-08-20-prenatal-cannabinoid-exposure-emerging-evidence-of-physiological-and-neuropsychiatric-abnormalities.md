---
layout: post
title: "Prenatal Cannabinoid Exposure: Emerging Evidence of Physiological and Neuropsychiatric Abnormalities"
date: 2024-08-20
tags: [Cannabis (drug),Prenatal development,Cannabinoid receptor 1,Cannabidiol,Hippocampus,Endocannabinoid system,Tetrahydrocannabinol,Psychosis,Cannabinoid,Cannabis in pregnancy,Sleep,Mitochondrion,Memory,Fetus,Pregnancy,Cohort study,Placenta,Schizophrenia,Mental disorder]
author: Mina G | Daniel B | Steven R
---


However, this effect is not always observed, with some studies reporting no effect on birth weight (37–42). These findings highlight the importance of investigating the impact of exogenous cannabinoid exposure on placental development. Importantly, Δ9–THC exposed rats also exhibited reduced body weight and pancreatic weight at birth, suggesting that the commonly observed clinical outcome of LBW may be associated with fetal glucometabolic dysregulation, an outcome that may disproportionately impact the long-term metabolic health of female offspring (35). Although in this study alterations were observed in exposed female, but not male, offspring, others have demonstrated an increase in the rewarding effect of morphine in exposed offspring of both sexes, with a stronger effect in males (105, 107). The most recent data also suggests an association between in utero exposure to cannabinoids and cognitive, behavioral, and neuropsychiatric aberrations.

[Visit Link](https://www.frontiersin.org/journals/psychiatry/articles/10.3389/fpsyt.2020.624275/full){:target="_blank rel="noopener"}

