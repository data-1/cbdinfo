---
layout: post
title: "Patients’ perspectives on prescription cannabinoid therapies: a cross-sectional, exploratory, anonymous, one-time web-based survey among German patients"
date: 2024-09-02
tags: [Medical cannabis,Prescription drug,Dronabinol,Pain,Physician,Chronic pain,Major depressive disorder,Health insurance,Attention deficit hyperactivity disorder,Tetrahydrocannabinol,Health,Medical specialty,Mental disorder,Opioid,Cannabinoid,Medicine,Health care,Clinical medicine]
author: Jan Moritz | Farid I | Laura Sophie | Christian S
---


Of all physicians contacted, 43 physicians indicated that they treated patients with cannabinoids and were interested in inviting them to participate in the study. TABLE 3  4 Discussion  The results of this cross-sectional study suggest that most of the surveyed outpatients treated with prescription cannabinoids in Germany subjectively experience health benefits and symptom reduction associated with these therapies. Most physicians stated that they did not prescribe cannabinoids and therefore could not invite patients to the study. Only a few individual physicians prescribed cannabinoids and of those only 34 informed their patients about this survey meaning, all study participants were prescribed cannabinoids by one of these 34 physicians. Most of the surveyed participants found therapy with cannabinoids to be effective compared to their treatment prior to medical cannabinoid prescription use.

[Visit Link](https://www.frontiersin.org/journals/medicine/articles/10.3389/fmed.2023.1196160/full){:target="_blank rel="noopener"}

