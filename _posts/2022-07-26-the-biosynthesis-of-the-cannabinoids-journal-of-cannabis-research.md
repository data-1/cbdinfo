---
layout: post
title: "The biosynthesis of the cannabinoids - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabinoid
author: Tahir, M. Nazir, Department Of Chemistry, Biochemistry, University Of Windsor, Windsor, Shahbazi, Rondeau-Gagné, Trant, John F.
tags: [Tetrahydrocannabinolic acid synthase,Tetrahydrocannabinol,Tetrahydrocannabinolic acid,Cannabinoid,Olivetol,Active site,Cannabidiol,Cannabis sativa,Cannabis,Biochemistry,Physical sciences,Chemistry]
---


In C. sativa, cannabinoids are biosynthesized as phytoprotectants: in fresh biomass, 95% of the THC, CBD, and CBC exist as their acidic parents: tetrahydrocannabinolic acid (THCA), cannabidiolic acid (CBDA), and cannabichromenic acid (CBCA) (United Nations Office on Drugs and Crime 2005). In 1969, Mechoulam reported on a second THC acid isomer, the 4-carboxy-THC (Fig. 2 Biosynthesis of cannabinoids. Kearsey et al. (2019) also confirmed that, in the absence of OAC, a nonenzymatic C2 → C7 decarboxylative aldol condensation of the tetraketide intermediate occurs forming olivetol instead of OLA (Fig. Kearsey et al. also investigated the crystal structure of TKS in the presence of CoA (Fig.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00062-4){:target="_blank" rel="noopener"}


