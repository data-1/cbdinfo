---
layout: post
title: "Human Astrocyte Spheroids as Suitable In Vitro Screening Model to Evaluate Synthetic Cannabinoid MAM2201-Induced Effects on CNS"
date: 2024-08-28
tags: [Cannabinoid receptor 1,Astrocyte,Cannabinoid receptor 2,Cadherin-1,Cannabinoid,Tetrahydrocannabinol,Synapse,Extracellular matrix,Biology,Cell biology]
author: Uliana De Simone | Patrizia Pignatti | Laura Villani | Luciana Alessandra Russo | Azzurra Sargenti | Simone Bonetti | Eleonora Buscaglia | Teresa Coccini | De Simone | Luciana Alessandra
---


Cell 2012, 148, 1039–1050. Cell 2010, 141, 859–871. Cell. Cell. Cell 2013, 155, 1154–1165.

[Visit Link](https://www.mdpi.com/1422-0067/24/2/1421){:target="_blank rel="noopener"}

