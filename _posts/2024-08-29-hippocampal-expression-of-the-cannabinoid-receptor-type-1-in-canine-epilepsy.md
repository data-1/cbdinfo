---
layout: post
title: "Hippocampal expression of the cannabinoid receptor type 1 in canine epilepsy"
date: 2024-08-29
tags: [Cannabinoid receptor 1,Hippocampus,Immunohistochemistry,Endocannabinoid system,Dentate gyrus,Hippocampus anatomy,Astrocyte,Hippocampus proper,Epileptogenesis,Synapse,Seizure,Cannabinoid,Chemical synapse,Epilepsy,Histopathology,Biology]
author: Freundt Revilla
---


Similar patterns were also observed in other regions of the hippocampus between structural and idiopathic epilepsy groups, however with no difference in respect to the control group. In the same regions, hippocampal tissue from dogs with idiopathic epilepsy displayed lower intensity of CB1R expression than the tissue of dogs with structural epilepsy (DG (p < 0.0001), CA3 (p < 0.0001) and hilus (p < 0.01)) (Fig. Evaluation of double CB1R staining with TUBB3 (Fig. It should be also considered that an increase in intensity only indicates an upregulation of CB1R in cells that also express the receptor at control conditions, whereas an increase in area indicates an induction in cells that normally do not express the receptor at levels above a detection threshold. Qualitative and quantitative tissue analysis  In each tissue sample, the evaluation of immunohistochemistry and immunofluorescence was performed at the level of the hippocampus (canine brain transections http://vanat.cvm.umn.edu/brainsect/).

[Visit Link](https://www.nature.com/articles/s41598-023-29868-3){:target="_blank rel="noopener"}

