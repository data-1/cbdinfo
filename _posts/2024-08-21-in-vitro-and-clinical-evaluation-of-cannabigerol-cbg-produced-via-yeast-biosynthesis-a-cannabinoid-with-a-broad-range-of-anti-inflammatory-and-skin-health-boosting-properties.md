---
layout: post
title: "In Vitro and Clinical Evaluation of Cannabigerol (CBG) Produced via Yeast Biosynthesis: A Cannabinoid with a Broad Range of Anti-Inflammatory and Skin Health-Boosting Properties"
date: 2024-08-21
tags: [Photoaging,Cannabinoid receptor 2,Cannabinoid,Fibronectin,Cannabidiol,Ultraviolet,Extracellular matrix,Acne,Reactive oxygen species,Antioxidant,Cell biology,Biochemistry,Biology]
author: Eduardo Perez | Jose R. Fernandez | Corey Fitzgerald | Karl Rouzard | Masanori Tamura | Christopher Savile | Jose R
---


Gene array analysis of CBG and CBD applied topically to a 3D human skin model demonstrates that CBG outperforms CBD, selectively targeting collagen, elastin and other key skin health and hydration genes. Moreover, in vitro studies in normal human epidermal keratinocytes (NHEKs) and human dermal fibroblasts (HDFs) show that CBG and CBD both possess strong antioxidant and anti-inflammatory properties, with CBG demonstrating equal if not better activity than CBD. In addition to these significant differences in overall gene regulation, CBG and CBD were found to modulate several key genes for skin aging, hydration, and inflammation. CBG Improves Skin Barrier Function and Reduces the Appearance of Redness in Human Subjects  Since CBG demonstrated antioxidant, anti-inflammatory, and skin health-boosting activity in vitro, we sought to determine whether it could provide similar benefits when applied topically to human skin. Moreover, CBD, CBG, and other cannabinoids demonstrate anti-inflammatory activity in skin cells versus several different inducers [2].

[Visit Link](https://www.mdpi.com/1420-3049/27/2/491){:target="_blank rel="noopener"}

