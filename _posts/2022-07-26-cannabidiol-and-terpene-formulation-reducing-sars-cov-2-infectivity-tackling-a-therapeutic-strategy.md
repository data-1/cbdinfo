---
layout: post
title: "Cannabidiol and Terpene Formulation Reducing SARS-CoV-2 Infectivity Tackling a Therapeutic Strategy"
date: 2022-07-26
categories:
- CBD
- terpenes
author: Santos, R D Innovation Department, Exmceuticals Portugal Lda, Cooperativa De Formação E Animação Cultural, Centre For Interdisciplinary Development, Research On Environment, Applied Management, Space, Cofac-Dreams -Universidade Lusófona, Sirsantoss Gmail.Com
tags: [Severe acute respiratory syndrome coronavirus 2,Cannabidiol,Virus,Antiviral drug,Angiotensin-converting enzyme 2,Cannabinoid,Inflammation,Real-time polymerase chain reaction,Immune system,Infection,Cytokine,Coronavirus spike protein,Cell culture,Tetrahydrocannabinol,Chromatography,Immunotherapy,Macrophage,Medical specialties,Biology]
---


Our group settled six formulations combining CBD and terpenes purified from Cannabis sativa L, Origanum vulgare , and Thymus mastichina . CBD is a partial agonist for cannabinoid receptor 2 (CN2R), widely expressed in the immune system (37–39). 3.2 Analysis by Cell Line and per Formulation  3.2.1 Caco-2  F3T is toxic at the concentration of 50 µM. The obtained data suggest these formulations to be exploited as new therapeutics targeting COVID-19, providing evidence that CBD and terpenes could be considered for further studies as effective anti-SARS-CoV-2 agents and potentially used for treatment or as adjuvants to conventional COVID-19 therapies. Pulm Pharmacol Ther (2021) 69:102047. doi: 10.1016/j.pupt.2021.102047 PubMed Abstract | CrossRef Full Text | Google Scholar  37.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fimmu.2022.841459/full){:target="_blank" rel="noopener"}


