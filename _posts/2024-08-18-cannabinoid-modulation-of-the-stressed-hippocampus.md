---
layout: post
title: "Cannabinoid Modulation of the Stressed Hippocampus"
date: 2024-08-18
tags: [Cannabinoid receptor 1,Anandamide,Hippocampus,Long-term potentiation,Memory,Cannabinoid,Memory consolidation,Endocannabinoid system,Cannabinoid receptor 2,Fear,Synapse,Hypothalamic–pituitary–adrenal axis,Neurochemistry,Neurophysiology,Branches of neuroscience,Basic neuroscience research,Neuroscience]
author: Franciele F | Vila-Verde | Vinícius L | Ferreira-Junior | Nilson C | Francisco S | Alline C
---


Stress Induces Changes in Cannabinoid-Mediated Hippocampal Neuroplasticity  Probably reflecting its role in several brain functions such as learning, memory and affective processing, the hippocampus is a very plastic brain structure. Similarly, the MAGL inhibitor, JZL184, reversed the effects of chronic stress in adult neurogenesis and blocked the stress-induced impairment of LTP induction in the lateral perforant pathway (Zhang et al., 2015). The previous exposure to stressors (restraint and water deprivation) could down-regulate CB1 receptors located in GABAergic terminals, facilitating CB1-mediated attenuation of glutamate release and resulting in an anxiolytic response (Campos et al., 2010; Ruehle et al., 2012). Additionally, in WYK rats, repeated treatment with a FAAH inhibitor induced antidepressant-like effect and increased levels of BDNF and AEA in the hippocampus (Vinod et al., 2012). A recent study from the same group found that repeated treatment with a FAAH inhibitor or citalopram also modified the functional states of 5-HT1A and 5-HT2A/C/receptors in the hippocampus (Bambico et al., 2016).

[Visit Link](https://www.frontiersin.org/journals/molecular-neuroscience/articles/10.3389/fnmol.2017.00411/full){:target="_blank rel="noopener"}

