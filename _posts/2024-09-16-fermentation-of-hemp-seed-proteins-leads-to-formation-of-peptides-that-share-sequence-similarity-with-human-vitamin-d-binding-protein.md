---
layout: post
title: "Fermentation of hemp seed proteins leads to formation of peptides that share sequence similarity with human vitamin D-binding protein"
date: 2024-09-16
tags: [Protein,Edestin,Cell culture,Peptide,Probiotic,Microorganism,Fermentation,Resazurin,Kefir,Cell biology,Biochemistry,Biology]
author: Marco Ruggiero | Stefania Pacini
institutions: 
doi: "https://doi.org/10.15761/mri.1000170"
---


Hemp seed protein extract (Switzerland and Europe). Hemp seed protein extract was dissolved in mineral water and heat-treated with the goal of inducing mild unfolding of the native structure of edestin, an hexameric globular protein, so to expose sites where proteolysis by microbial proteases could occur with the consequent formation of bioactive peptides. As high control, cells were treated with solvent.15][16]  Results and discussion  The protein/peptide content from the fermented hemp seed protein extract added to each well containing 2,500 cells was calculated as 0.6 µg/well, an amount that is of the same order of magnitude as that used by Gregory et al. to assess the effects of DBP-MAF on transformed prostate cell proliferation and migration [10]. We postulate that proteases secreted by the microbes of the probiotic formulation, together with the increase of acidity that is characteristic of fermentation, led to formation of peptides functionally very similar to DBP-MAF and, in particular, to the peptide TPTELAKLVNKRSE [18] that was identified by Gregory et al. as the active site of DBP-MAF [10]. To our knowledge this is the first demonstration that fermentation of edestin leads to formation of peptides that show biological activity in human cell cultures (Figure 1).
