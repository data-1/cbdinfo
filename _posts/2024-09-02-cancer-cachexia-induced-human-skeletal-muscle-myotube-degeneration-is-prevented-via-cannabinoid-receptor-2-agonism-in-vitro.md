---
layout: post
title: "Cancer-Cachexia-Induced Human Skeletal Muscle Myotube Degeneration Is Prevented via Cannabinoid Receptor 2 Agonism In Vitro"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Cannabinoid receptor 1,Cannabinoid,Myogenesis,Cachexia]
author: John Noone | Mary F. Rooney | Marilena Karavyraki | Andrew Yates | Saoirse E. O’Sullivan | Richard K. Porter | Mary F | O’Sullivan | Saoirse E | Richard K
---


In Vitro Human Skeletal Muscle Cancer Cachexia Cell Model Is Established Using Cancer Conditioned Media Which Induces Significant Degeneration of Mature Myotubes  Primary human skeletal muscle myoblasts (HSkMC) have previously been shown to successfully differentiate into mature myotubes [31,32] with the process taking approximately 10–12 days. In vitro human skeletal muscle cancer cachexia cell model. Cannabinoid Receptor Agonists, THC and JWH133, Provide a Protective Effect against Myotube Degeneration in the Cancer Cachexia Model, with SW480 and H1299 Cancers  Having established the in vitro human skeletal muscle cell model of cancer cachexia, our next objective was to exploit this model to test whether a selection of cannabinoid related drugs had any protective effects against myotube degeneration. In Vitro Cancer Cachexia Model  4.3.1. Figure S2: Optimization of conditioned media concentration for in vitro human skeletal muscle cancer cachexia cell model.

[Visit Link](https://www.mdpi.com/1424-8247/16/11/1580){:target="_blank rel="noopener"}

