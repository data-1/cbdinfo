---
layout: post
title: "Study reveals antibiotic potential of cannabis compound"
date: 2022-07-25
categories:
- CBG
author: Nikki Hancocks
tags: [Antibiotic,Bacteria,Methicillin-resistant Staphylococcus aureus,Cannabidiol,Health,Cannabinoid,Health sciences,Microbiology,Drugs,Medicine,Health care,Clinical medicine,Life sciences,Medical specialties]
---


Early studies have shown that some cannabinoids can slow the growth of gram-positive bacteria, such as S. aureus​, but not gram-negative bacteria, such as E. coli​. Eric Brown, a microbiologist at the department of biochemistry and biomedical sciences at McMaster University in Canada led the new research to test the antibacterial properties of several cannabinoids against both MRSA and gram-negative bacteria. The team also tested the ability of these substances to prevent the formation of biofilms on surfaces and to kill dormant persistor MRSA that are highly resistant to antibiotics. The researchers discovered that CBG targets the cell membrane of gram-positive bacteria, and by itself, it is not effective against gram-negative bacteria, which have an additional outer membrane. Brown says the next step is to reduce the toxicity of the CBG compound, possibly by combining it with another substance  Source: ACS Infectious Diseases​  Brown.

<hr>[Visit Link](https://www.nutraingredients.com/Article/2020/03/02/Study-reveals-antibiotic-potential-of-cannabis-compound){:target="_blank" rel="noopener"}


