---
layout: post
title: "Synthetic and Non-synthetic Cannabinoid Drugs and Their Adverse Effects-A Review From Public Health Prospective"
date: 2024-08-18
tags: [Cannabis (drug),Cannabinoid,Cannabidiol,Psychosis,Psychoactive drug,Tetrahydrocannabinol,Effects of cannabis,JWH-018,Schizophrenia,Driving under the influence,Psychoactive drugs,Health,Drugs acting on the nervous system,Drugs,Clinical medicine]
author: Aviv M
---


Psychoactive Ingredients of Cannabinoids  The main psychoactive ingredient in cannabis is THC, which is a CB1 and CB2 receptors partial-agonist and the most potent cannabinoid that is present in the organic forms of cannabis (8, 27). Adverse Health Effects of Cannabinoids  Acute Effects of Cannabinoids  The intoxication effects of cannabis are characterized by cannabis users as; mild euphoria, relaxation, and a general pleasant feeling (1, 7). These cognitive alterations increase the risk of road accidents if cannabis or SC users drive while intoxicated (1, 15). There is evidence indicating adverse effects of cannabis on cardiovascular function (1, 44, 46). While the psychotropic effects associated with natural cannabis are related to the presence of THC (1), SC products contain a wide range of high-potent full agonists of the cannabinoid receptors that induce “THC-like” effects, but they are more severe and enduring (10, 15, 29, 31).

[Visit Link](https://www.frontiersin.org/journals/public-health/articles/10.3389/fpubh.2018.00162/full){:target="_blank rel="noopener"}

