---
layout: post
title: "Evaluation of cannabinoid receptors type 1–2 in periodontitis patients"
date: 2024-08-27
tags: [Cannabinoid receptor 2,Inflammation,Periodontal disease,Endocannabinoid system,NF-κB,Periodontology,Cytokine,Inflammatory cytokine,Immune system,Anti-inflammatory,Cannabinoid,Immunology,Cell biology,Medical specialties,Biology,Biochemistry,Clinical medicine,Health sciences,Neurochemistry,Signal transduction,Cell signaling]
author: I. B | D. F | T. W | K. K | N. P | M. A | B. G | T. E | C. S | C. P
---


However, such advances in ECs and the introduction of the recent cannabinoid medications are a new pathway for preventing destructive inflammatory reactions such as periodontitis. 4 DISCUSSION  In the present study, the CB1 and CB2 gene expression in periodontitis showed a significant presence of CB2 in the gingival tissues, but not CB1. Therefore, most studies on cannabinoid receptors in other inflammatory conditions such as rheumatoid arthritis were limited to CB2 receptors and their agonists (Barrie et al., 2017). Nakajima et al. (2006) showed the presence of CB1 and CB2 receptors after bacterial stimulation; therefore, cannabinoid systems may play anti-inflammatory effects. As the EC system modulates inflammatory reactions, cannabinoid receptors can be targeted in periodontitis.

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1002/cre2.608){:target="_blank rel="noopener"}

