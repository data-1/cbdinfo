---
layout: post
title: "Interaction of Glia Cells with Glioblastoma and Melanoma Cells under the Influence of Phytocannabinoids"
date: 2022-08-05
categories:
- Cannabinoid
author: Hohmann, Walsleben, Ghadban, Kirchhoff, Dehghani, Urszula Hohmann, Christoph Walsleben, Chalid Ghadban, Frank Kirchhoff, Faramarz Dehghani
tags: [Cannabinoid,Astrocyte,Cannabinoid receptor type 2,Cannabidiol,MTOR,Tetrahydrocannabinol,Metastasis,Microglia,Cell adhesion,Tumor microenvironment,Transforming growth factor beta,Cell migration,Glioblastoma,Protein kinase B,Cannabinoid receptor type 1,Cell biology,Biochemistry]
---


All treatments did not affect spheroid size at 70 h. During the formation of 3D aggregates, the size and shape of spheroids have been shown to be primarily determined by cell–cell and cell–matrix adhesion and generated tension [ 70 72 ]. Consequently, the addition of astrocytes or microglia in the current study likely affects the formation of adhesion sites and/or the buildup of tension. Via such signaling, it seems plausible that astrocytes and microglia affected adhesion and tension and thus spheroid formation in this study. In this study, tumor cells and one specific glia cell type were co-cultured to analyze the impact of glia cells on interactions between tumor cells and glia. Indeed, in more complex organotypic models and tumor slice cultures, additional interactions will potentially yield a more complex behavior as a sum of effects will be detected caused by differential extracellular matrix composition and further cell types, such as e.g., infiltrating immune cells, endothelial cells, etc.

<hr>

[Visit Link](https://www.mdpi.com/2073-4409/11/1/147/htm){:target="_blank" rel="noopener"}


