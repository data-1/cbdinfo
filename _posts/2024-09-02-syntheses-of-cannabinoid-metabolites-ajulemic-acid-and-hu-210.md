---
layout: post
title: "Syntheses of Cannabinoid Metabolites: Ajulemic Acid and HU-210"
date: 2024-09-02
tags: [Cannabinoid,Proton nuclear magnetic resonance,Ethyl acetate,Chemistry,Concepts in chemistry]
author: Wenbin Shao | Pingyong Liao | Xiaoyan Zhang | Binbin Fan | Ruijia Chen | Xilong Chen | Xuejun Zhao | Wenbin Liu
---


General structural information for cannabinoid metabolites with oxygen at C-11 site. Considering the rapid change in the illicit drug market, a concise synthetic route ( ) where the combination of a simple replacement of the phenol protecting group and the optimization of the Riley oxidation condition give a better yield (43% overall yield). Therefore, a synthetic route for the synthesis of a key intermediate, which subsequently, can be used to synthesize the cannabinoid metabolites, was developed. The reaction was quenched by the addition of an aqueous solution of NH4Cl (10 mL), the MeOH in the resultant mixture was removed under vacuum, and the resultant residue was extracted with ethyl acetate (3 × 10 mL). [Google Scholar] [CrossRef] [PubMed]  Rhee, M.-H.; Vogel, Z.; Barg, J.; Bayewitch, M.; Levy, R.; Hanu, L.; Breuer, A.; Mechoulam, R. Cannabinoid derivatives: Binding to cannabinoid receptors and inhibition of adenylcyclase.

[Visit Link](https://www.mdpi.com/1420-3049/29/2/526){:target="_blank rel="noopener"}

