---
layout: post
title: "On the Role of Central Type-1 Cannabinoid Receptor Gene Regulation in Food Intake and Eating Behaviors"
date: 2024-08-20
tags: [Cannabinoid receptor 1,Orexin,Single-nucleotide polymorphism,Reward system,DNA methylation,Endocannabinoid system,Eating,Epigenetics,Biology,Biochemistry]
author: Mariangela Pucci | Elizabeta Zaplatic | Emanuela Micioni Di Bonaventura | Paolo De Cristofaro | Mauro Maccarrone | Carlo Cifani | Claudio D’Addario | Micioni Di Bonaventura | Maria Vittoria | De Cristofaro
---


Here, we focused mainly on the role of type-1 Cannabinoid Receptor gene (CNR1) gene, which encodes for CB1R, and its regulation in food intake and eating behaviors. Via CB1R, eCBs modulate homeostatic and rewarding neural circuitries, and regulates consequently eating behaviors and energy balance, according to food availability: activation of eCB signaling is favorable when access to food is restricted, whereas it promotes obesity and metabolic diseases when food is abundant. The orphan receptor GPR55 is a novel cannabinoid receptor. Gene 2012, 495, 194–198. The association of endocannabinoid receptor genes (CNR1 and CNR2) polymorphisms with depression.

[Visit Link](https://www.mdpi.com/1422-0067/22/1/398){:target="_blank rel="noopener"}

