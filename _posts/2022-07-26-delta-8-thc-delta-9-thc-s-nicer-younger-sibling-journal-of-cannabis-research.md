---
layout: post
title: "Delta-8-THC: Delta-9-THC’s nicer younger sibling? - Journal of Cannabis Research"
date: 2022-07-26
categories:
- THC
author: Kruger, Jessica S., Department Of Community Health, Health Behavior, University At Buffalo, Buffalo, Daniel J., Population Studies Center, Institute For Social Research, University Of Michigan
tags: [Medical cannabis,Delta-8-Tetrahydrocannabinol,Harm reduction,Cannabidiol,Tetrahydrocannabinol,Cannabis edible,Research,Cannabinoid,Cannabis (drug),Dose (biochemistry),Anxiety,Recreational drug use,Drug,Health,Psychoactive drugs]
---


Most participants (83%) also reported consuming delta-9-THC cannabis and products and reported substitution for delta-9-THC (57%) and pharmaceutical drugs (59%). Participants’ responses containing this theme included: “Delta 8 feels like Delta 9’s nicer younger sibling”; “It has all the positives and many fewer drawbacks/side effects. Delta 9 is better for sleep.”  The second most common theme was the therapeutic effect or benefit from delta-8-THC, participants’ responses containing this theme included: “It is like “lite” Delta 9. I can focus and work more with Delta 8 than Delta 9. The most frequent of these comments was that delta-8-THC edibles or tinctures were more powerful than when delta-8-THC was inhaled as a vape: “How Delta 8 is consumed plays a large role in the effects, when eaten or taken in a tincture it feels much closer to Delta 9 in effects compared to when vaping/dabbing Delta 8.”

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00115-8){:target="_blank" rel="noopener"}


