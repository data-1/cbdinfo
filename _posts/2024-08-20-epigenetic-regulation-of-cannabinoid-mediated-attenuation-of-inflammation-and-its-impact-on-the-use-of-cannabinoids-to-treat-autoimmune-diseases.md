---
layout: post
title: "Epigenetic Regulation of Cannabinoid-Mediated Attenuation of Inflammation and Its Impact on the Use of Cannabinoids to Treat Autoimmune Diseases"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Cannabinoid,Interleukin 10,Myeloid-derived suppressor cell,Inflammation,Inflammatory bowel disease,Apoptosis,Medical cannabis,Cannabinoid receptor 1,T helper 17 cell,T helper cell,Regulatory T cell,Ulcerative colitis,FOXP3,Cytokine,Experimental autoimmune encephalomyelitis,Inflammatory cytokine,MicroRNA,Cannabidiol,Tetrahydrocannabinol,Tumor necrosis factor,TRPV1,TBX21,Ligand (biochemistry),DNA methylation,Endocannabinoid system,Interleukin 17,Cannabinoid receptor,Epigenetics,Neuroinflammation,Interferon gamma,Anti-inflammatory,Receptor (biochemistry),Fatty-acid amide hydrolase 1,Cell biology,Biochemistry,Biology,Cell signaling,Immunology,Neurochemistry,Medical specialties]
author: Bryan Latrell Holloman | Mitzi Nagarkatti | Prakash Nagarkatti | Bryan Latrell
---


Cannabinoids 2013, 7, 11. Cell. [Google Scholar] [CrossRef] [PubMed] [Green Version]  Rieder, S.A.; Chauhan, A.; Singh, U.; Nagarkatti, M.; Nagarkatti, P. Cannabinoid-induced apoptosis in immune cells as a pathway to immunosuppression. Cell. [Google Scholar] [CrossRef]  Hegde, V.L.

[Visit Link](https://www.mdpi.com/1422-0067/22/14/7302){:target="_blank rel="noopener"}

