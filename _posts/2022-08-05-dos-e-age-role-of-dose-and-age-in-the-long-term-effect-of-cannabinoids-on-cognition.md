---
layout: post
title: "Dos(e)Age: Role of Dose and Age in the Long-Term Effect of Cannabinoids on Cognition"
date: 2022-08-05
categories:
- Cannabinoid
author: Zamberletti, Rubino, Erica Zamberletti, Tiziana Rubino
tags: [Cannabis (drug),Cannabinoid,Endocannabinoid system,Memory,Tetrahydrocannabinol,Self-administration,Cannabidiol,Adolescence,Cannabinoid receptor type 1,Dose (biochemistry),T-maze,Ageing,Prenatal development,Mental disorder,Neuroscience]
---


Almost 4% of the global population aged between 15 and 64 years used cannabis at least once in the last year, according to the 2021 World Drug Report [ 1 ]. Outside of teens, older adults aged ≥65 years currently represent the fastest-growing population of cannabis users. Recently, an increased prevalence of cannabis use has also been described among pregnant women. Accordingly, a recent study showed that pregnant women with nausea and vomiting during pregnancy had nearly 2–4 times greater odds of prenatal cannabis use [ 12 ]. The increased prevalence of cannabis use reported in these sensitive population subgroups highlights the need to examine the effects of cannabis use on cognitive function across ages.

<hr>

[Visit Link](https://www.mdpi.com/1420-3049/27/4/1411/htm){:target="_blank" rel="noopener"}


