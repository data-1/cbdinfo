---
layout: post
title: "Probing the Conformational Space of the Cannabinoid Receptor 2 and a Systematic Investigation of DNP"
date: 2024-09-02
tags: [G protein-coupled receptor,Cannabinoid receptor 2,Chemical shift,Receptor antagonist,Cell signaling,Protein secondary structure,Ligand (biochemistry),G protein,Nuclear magnetic resonance,Protein,Biochemistry,Cell biology]
author: Johanna Becker-Baldus | Alexei Yeliseev | Thomas T. Joseph | Snorri Th. Sigurdsson | Lioudmila Zoubak | Kirk Hines | Malliga R. Iyer | Arjen van den Berg | Sam Stepnowski | Jon Zmuda
---


The protein concentration in these samples was 0.25–0.5 mM. [PMC free article] [PubMed] [CrossRef] [Google Scholar]  d. Yeliseev A.; Gawrisch K. Expression and NMR Structural Studies of Isotopically Labeled Cannabinoid Receptor Type II. Angew Chem Int Ed Engl 2013, 52, 1222–1225. [PMC free article] [PubMed] [CrossRef] [Google Scholar]  d. Yeliseev A.; Gawrisch K. Expression and NMR Structural Studies of Isotopically Labeled Cannabinoid Receptor Type II. Angew Chem Int Ed Engl 2013, 52, 1222–1225.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10500644/){:target="_blank rel="noopener"}

