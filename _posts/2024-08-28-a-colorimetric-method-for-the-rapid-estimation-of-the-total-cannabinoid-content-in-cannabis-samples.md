---
layout: post
title: "A Colorimetric Method for the Rapid Estimation of the Total Cannabinoid Content in Cannabis Samples"
date: 2024-08-28
tags: [Cannabinoid,Cannabidiol,Cannabinol,Cannabis (drug),Absorbance,Accuracy and precision,Tetrahydrocannabinol,Cannabis]
author: Neus Jornet-Martínez | Josep Biosca-Micó | Pilar Campíns-Falcó | Rosa Herráez-Hernández | Jornet-Martínez | Biosca-Micó | Campíns-Falcó | Herráez-Hernández
---


The reaction and detection conditions have been established according to the results obtained for the individual cannabinoids Δ9-tetrahydrocannabidiol (THC), cannabidiol (CBD), and cannabinol (CBN), as well as for ethanolic extracts obtained from cannabis samples after ultrasonication. Image and spectra of the solutions obtained for a blank (water) and for the three cannabinoids tested after reaction with FBB in solution. No significant absorbances in the 400–600 nm interval were observed for the resulting mixtures when the volume of extract used was ≤50 µL, equivalent to a dilution factor of the extract of 1:12 (Figure S3). Spectra obtained for cannabinoids present in a cannabis sample: (A) extracts obtained with methanol and ethanol; (B) extracts obtained with two successive extractions with; (C) extracts obtained with ethanol by using ultrasonication and hand stirring for 10 min. Cannabis Cannabinoids 2019, 2, 1–13.

[Visit Link](https://www.mdpi.com/1420-3049/28/3/1303){:target="_blank rel="noopener"}

