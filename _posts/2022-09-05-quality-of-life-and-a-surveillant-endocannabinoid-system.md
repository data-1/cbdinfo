---
layout: post
title: "Quality of Life and a Surveillant Endocannabinoid System"
date: 2022-09-05
categories:
- Endocannabinoid
author: De Melo Reis, Ricardo Augusto, Laboratory Of Neurochemistry, Institute Of Biophysics Carlos Chagas Filho, Universidade Federal Do Rio De Janeiro, Isaac, Alinny Rosendo, Freitas, Hércules Rezende, Laboratory Of Neuroenergetics
tags: [Cannabis (drug),Cannabinoid receptor type 2,Cannabinoid,Endocannabinoid system,Cannabinoid receptor type 1,Neurodegenerative disease,Cannabinoid receptor,Exercise,Mitochondrion,Adipose tissue,Hippocampus,Neurotransmitter,Hypothalamicpituitaryadrenal axis,Cannabidiol,Astrocyte,Stress (biology),Brain-derived neurotrophic factor,Obesity,Memory,Epigenetics,Carnitine palmitoyltransferase I,Tetrahydrocannabinol,Anandamide,Effects of cannabis,Leptin,Neuron,Regulation of gene expression,DNA methylation,Docosahexaenoic acid,Anxiety,Eicosapentaenoic acid,Brain,Hypothalamus,Neurobiological effects of physical exercise,-Aminobutyric acid,Amyloid beta,Inflammation,Sleep,Gene expression,Dopamine,Nervous system,Receptor (biochemistry),Metabolism,Depression (mood),Schizophrenia,Cell signaling,Hypokinesia,Fat,Substance abuse,Health,Weight loss,Mindfulness,White adipose tissue,Anti-inflammatory,Adipocyte,Dementia,2-Arachidonoylglycerol,Substantia nigra,Homeostasis,Microglia,Promoter (genetics),Physiology,Neuroscience,Biochemistry]
---


This indicates that an increase in AEA during exercise might be one of the factors involved in the exercise-induced increase in peripheral BDNF levels and that AEA high levels during recovery might delay the return of BDNF to basal levels (Figure 4). How does Cannabis modulate the ECS? Finally, an important discussion is necessary when long-term effects on cognition of medical cannabis (MC) use is compared to recreational cannabis, especially in those with adolescent onset. The roles of the ECS regulating immune and cognitive functions also support its modulation as a potential novel therapeutic target in AD. Differential effects of exercise on brain opioid receptor binding and activation in rats.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fnins.2021.747229/full){:target="_blank" rel="noopener"}


