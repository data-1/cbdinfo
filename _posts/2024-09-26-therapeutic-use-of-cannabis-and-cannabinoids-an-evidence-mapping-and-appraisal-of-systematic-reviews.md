---
layout: post
title: "Therapeutic use of cannabis and cannabinoids: an evidence mapping and appraisal of systematic reviews"
date: 2024-09-26
tags: [Medical cannabis,Cannabidiol,Peripheral neuropathy,Randomized controlled trial,Diseases and disorders,Medicine,Medical specialties,Health,Clinical medicine]
author: Nadia Montero‐Oleas | Ingrid Arévalo-Rodríguez | Solange Núñez-González | Andrés Viteri-García | Daniel Simancas‐Racines
pubdate: "2020-01-15"
institutions: Universidad UTE
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7076827"
---


Keywords: Cannabis, Cannabinoids, Medical marijuana, Evidence mapping, Evidence synthesis  Background  Medical cannabis refers to the use of cannabis or cannabinoids for the treatment of a medical condition or to alleviate its associated symptoms [1, 2]. Systematic review and meta-analysis of Cannabis treatment for chronic pain. A systematic review. A systematic review. Systematic review and meta-analysis of Cannabis treatment for chronic pain.