---
layout: post
title: "A genome-wide association study identifies a possible role for cannabinoid signalling in the pathogenesis of diabetic kidney disease"
date: 2024-08-29
tags: [Genome-wide association study,Single-nucleotide polymorphism,Cannabinoid receptor 2,Diabetic nephropathy,Type 2 diabetes,Medical specialties,Genetics,Biology]
author: Amy Jayne | Alexander P | Al Safar
---


Replication analysis  Based on the top identified SNP in this cohort, further replication data was obtained from three datasets: DKD summary results of Biobank Japan32, the FinnGen Project33 (cohort name: finngen_R5_DM_NEPHROPATHY_EXMORE), and the UK Biobank34 (cohort name: E1122). In the analysis in this study, there were 823 DKD/ESRD cases and 903 controls. A replication analysis was conducted in this cohort on previously identified variants (SNPs and candidate genes) associated with DKD, totaling 49 SNPs in 42 gene loci. GWAS of DKD  To identify genetic variants that contribute to the susceptibility of DKD in the UAE population, we conducted a genome-wide association study (GWAS) using 258 cases with T2DM who developed DKD, and 938 control subjects who had T2DM but did not develop DKD. We have discovered an additional eleven variants (rs78595611 in PPP1R9A gene; rs4001941 inCYP2B7P; rs117074522 inCYP4F24P; rs275475 in RP11-332J15.1; rs11664515 in OR10H2; rs16836018 inSTX18-AS1; rs17349061 inRB1; rs9493286 inMOXD1; rs76361654 in PPP1R3A; rs141291445 in LINC00693; and rs11696648 inMACROD2) with suggestive association of DKD that will need to be replicated in a larger cohort, across multiple ancestral ethnic groups from Middle East populations.

[Visit Link](https://www.nature.com/articles/s41598-023-31701-w){:target="_blank rel="noopener"}

