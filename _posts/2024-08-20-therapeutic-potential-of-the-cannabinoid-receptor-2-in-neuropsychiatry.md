---
layout: post
title: "Therapeutic potential of the cannabinoid receptor 2 in neuropsychiatry"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Endocannabinoid system,Γ-Aminobutyric acid,Schizophrenia,Brain-derived neurotrophic factor,Serotonin,Hypothalamic–pituitary–adrenal axis,5-HT2A receptor,Hippocampus,Anandamide,Dizocilpine,Synaptic plasticity,2-Arachidonoylglycerol,Cannabinoid,Cannabinoid receptor 1,Antipsychotic,Neurotransmission,Cannabinoid receptor,Neuron,Astrocyte,Neuroplasticity,Neurophysiology,Neurochemistry]
author: Gasparyan | Jorge Manzanares | María S. García-Gutiérrez | Francisco Navarrete
---


This review summarizes the main findings highlighting the involvement of CB2R in different physiological processes such as stress response, emotional reactivity, and cognitive processing that are affected in different psychiatric disorders (anxiety, depression, schizophrenia, and bipolar disorder), as well as the evidence supporting its therapeutic potential in the pharmacological approach to these pathologies. Involvement of CB2R in anxiety-related disorders  The presence of CB2R in brain areas closely related to the response to stress and anxiety, such as the hippocampus and the amygdala, has led to the study of its possible involvement in these conditions [37, 39]. Interestingly, these effects are not found in the previous administration of a CB2R agonist (JWH133), supporting the involvement of CB2R in anxiety [47]. Advances in the identification of molecular mechanisms related to the involvement of CB2R in neuropsychiatry  CB2R was identified in different cell types (neurons, glia, and astroglia) of the as well as its interaction with targets classically related to the response to stress, anxiety, depression, and schizophrenia, such as the hypothalamic-pituitary-adrenal (HPA) axis, processes of neurogenesis, and neuronal plasticity, and the GABAergic, dopaminergic and serotonergic neurotransmitter systems. These effects are not observed with the administration of CB1R agonists, confirming the role of CB2R in the regulation of GABAergic transmission in this brain region.

[Visit Link](https://www.explorationpub.com/Journals/ent/Article/10046){:target="_blank rel="noopener"}

