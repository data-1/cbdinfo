---
layout: post
title: "Trends in prescription and cost of Sativex, a cannabinoid-based medicine, in treating patients with multiple sclerosis in England"
date: 2024-09-02
tags: [Nabiximols,Prescription drug,Cannabidiol,Medical cannabis,Pharmacology,Clinical medicine,Health,Health care,Medicine]
author: Farideh A. Javid | Anam Alam | Emily Williams | Sidhra Sajid Malik | Usama Mohayuddin | Syed Shahzad Hasan
---


Multiple Sclerosis Journal, 10(4), 425–433. Multiple Sclerosis Journal, 16(11), 1349–1359. Journal of Multiple Sclerosis, 1(2), 1–2. Multiple Sclerosis Journal, 16(11), 1349–1359. Multiple Sclerosis Journal, 10(4), 425–433.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11080669/){:target="_blank rel="noopener"}

