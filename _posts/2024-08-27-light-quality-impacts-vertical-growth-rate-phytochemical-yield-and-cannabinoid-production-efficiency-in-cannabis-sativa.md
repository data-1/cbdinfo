---
layout: post
title: "Light Quality Impacts Vertical Growth Rate, Phytochemical Yield and Cannabinoid Production Efficiency in Cannabis sativa"
date: 2024-08-27
tags: [Cannabidiol,Cannabis sativa,Cannabinoid,Cannabis,Visible spectrum,Tetrahydrocannabinol]
author: Victorio Morello | Vincent Desaulniers Brousseau | Natalie Wu | Bo-Sen Wu | Sarah MacPherson | Mark Lefsrud | Vincent Desaulniers | Bo-Sen
---


Plant J. Plants 2021, 10, 1866. Plants 2019, 8, 93. Plants 2021, 10, 1420. Plants 2021, 10, 1075.

[Visit Link](https://www.mdpi.com/2223-7747/11/21/2982){:target="_blank rel="noopener"}

