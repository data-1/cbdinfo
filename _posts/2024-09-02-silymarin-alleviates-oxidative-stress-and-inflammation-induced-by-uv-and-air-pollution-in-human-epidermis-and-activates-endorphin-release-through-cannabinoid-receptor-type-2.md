---
layout: post
title: "Silymarin Alleviates Oxidative Stress and Inflammation Induced by UV and Air Pollution in Human Epidermis and Activates β-Endorphin Release through Cannabinoid Receptor Type 2"
date: 2024-09-02
tags: [Aryl hydrocarbon receptor,NFE2L2,Cannabinoid receptor 2,Real-time polymerase chain reaction,Keratinocyte,Complementary DNA,NAD(P)H dehydrogenase (quinone 1),Interleukin 6,Reactive oxygen species,Cell biology,Biochemistry,Biology]
author: Cloé Boira | Emilie Chapuis | Amandine Scandolera | Romain Reynaud
---


These data confirm that SM has antioxidant properties in epidermal cells. SM Reduced ROS on UVA/B+Urban-Dust-Stressed RHEs  ROS were quantified in the RHEs in order to evaluate the oxidative stress induced by UVA/B and urban dust application and confirmed the antioxidant properties of SM. Both SM and CBD activated the receptor ( ). CBD activates antioxidant pathways regulating inflammation in keratinocytes, mediates epidermal differentiation through AHR signaling [37,38], and is considered as a potential therapeutic for inflammatory skin disorders [39]. Silymarin Alleviates Oxidative Stress and Inflammation Induced by UV and Air Pollution in Human Epidermis and Activates β-Endorphin Release through Cannabinoid Receptor Type 2 Cosmetics 11, no.

[Visit Link](https://www.mdpi.com/2079-9284/11/1/30){:target="_blank rel="noopener"}

