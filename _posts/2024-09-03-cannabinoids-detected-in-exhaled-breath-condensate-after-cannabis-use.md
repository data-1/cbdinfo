---
layout: post
title: "Cannabinoids detected in exhaled breath condensate after cannabis use"
date: 2024-09-03
tags: [Tetrahydrocannabinol,Cannabinoid,Tetrahydrocannabinolic acid,Cannabinol,Cannabis,Detection limit,PubMed Central,Cannabidiol,Aerosol]
author: Jennifer L Berry | Ashley Brooks-Russell | Cheryle N Beuning | Sarah A Limbacher | Tara M Lovestead | Kavita M Jeerage
---


Five different cannabinoids, including Δ9-tetrahydrocannabinol (THC), were detected in EBC collected from cannabis users. THC concentrations in EBC samples collected at 0.7 h showed no trend with sample metrics like mass or number of breaths. EBC samples were collected from 12 participants who inhaled cannabis and 2 control participants who did not use cannabis. In this study, THC was detected in EBC at 0.7 h from every participant that inhaled cannabis (figure ) and the THC concentration at this time point was always higher than the THC concentration measured prior to cannabis use (the presumed baseline). THC was detected in all EBC samples collected 0.7 h after cannabis use and decreased in concentration by 1.7 h. THC concentrations and the detection of non-THC cannabinoids including CBN and CBG were consistent with past studies using different breath collection devices.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11264354/){:target="_blank rel="noopener"}

