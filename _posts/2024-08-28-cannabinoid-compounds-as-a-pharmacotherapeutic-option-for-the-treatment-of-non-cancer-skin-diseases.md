---
layout: post
title: "Cannabinoid Compounds as a Pharmacotherapeutic Option for the Treatment of Non-Cancer Skin Diseases"
date: 2024-08-28
tags: [Cannabinoid,Anandamide,Cannabinoid receptor 2,Psoriasis,Interleukin 17,Melanocyte,Fatty-acid amide hydrolase 1,Endocannabinoid system,Interleukin 23,2-Arachidonoylglycerol,Cannabinoid receptor 1,Cannabinol,Cannabinoid receptor,Keratinocyte,Dermatitis,Atopic dermatitis,Tetrahydrocannabivarin,TRPV1,Wound healing,Anti-inflammatory,Janus kinase,Tumor necrosis factor,Inflammation,2-Arachidonyl glyceryl ether,Epidermis,Immunosuppressive drug,Itch,Interleukin 6,Tetrahydrocannabinol,Sebaceous gland,Pattern hair loss,Allergy,Healing,Macrophage,Neurochemistry,Cell biology,Biochemistry]
author: Robert Ramer | Burkhard Hinz
---


PPARγ was found to be activated by THC [76] and CBD [77]. In addition to the two cannabinoid receptors, human keratinocytes have also been shown to express TRPV1 [55]. Human keratinocytes have also been shown to express PPARα/γ/δ receptors and GPR55 [55]. Regulation of the Endocannabinoid System in Skin Diseases  3.3.1. In addition, AEA and 2-AG have been reported to be reduced in keratinocytes from psoriasis patients, while PEA levels are increased [7].

[Visit Link](https://www.mdpi.com/2073-4409/11/24/4102){:target="_blank rel="noopener"}

