---
layout: post
title: "A Comparison of Acute Neurocognitive and Psychotomimetic Effects of a Synthetic Cannabinoid and Natural Cannabis at Psychotropic Dose Equivalence"
date: 2024-08-25
tags: [Cannabis (drug),JWH-018,Drugs acting on the nervous system,Pharmacology,Drugs,Psychoactive drugs]
author: Eef Lien | Kim Paula Colette | Natasha Leigh | Johannes Gerardus
---


JWH-018 Study  Subjective and performance data after an acute dose of JWH-018 was taken from a previous study performed by our group (31, 32). Subjective high score demonstrated a significant effect of Drug [F(1, 46) = 329.1; p < 0.001, η2p = 0.88]. No significant effect of Study or Drug x Study was found, confirming that subjective high in both samples was comparable. Up until now, more than 190 SCs have been reported by the EMCDDA, and only for a few of them the acute effects have been studied in humans (29–32, 90, 91). Psychosis and synthetic cannabinoids.

[Visit Link](https://www.frontiersin.org/journals/psychiatry/articles/10.3389/fpsyt.2022.891811/full){:target="_blank rel="noopener"}

