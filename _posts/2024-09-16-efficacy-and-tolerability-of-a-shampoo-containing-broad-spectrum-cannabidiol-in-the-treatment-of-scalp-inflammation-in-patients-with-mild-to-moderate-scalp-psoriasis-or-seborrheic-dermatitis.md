---
layout: post
title: "Efficacy and Tolerability of a Shampoo Containing Broad-Spectrum Cannabidiol in the Treatment of Scalp Inflammation in Patients with Mild to Moderate Scalp Psoriasis or Seborrheic Dermatitis"
date: 2024-09-16
tags: [Psoriasis,Interleukin 2,Cytokine,Hair loss,Seborrhoeic dermatitis,Hair follicle,Inflammation]
author: Colombina Vincenzi | Antonellá Tosti
institutions: University of Miami
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7706496"
---


Scalp inflammation is commonly associated with scalp psoriasis or seborrheic dermatitis (SD). has also been described in scalp psoriasis [19]. Shampoo treatments are the most commonly used means of managing hair and scalp conditions and have proven to be effective in the treatment of psoriasis [2] and SD [24, 25, 26]. Psoriasis of the scalp. Psoriasis of the scalp.