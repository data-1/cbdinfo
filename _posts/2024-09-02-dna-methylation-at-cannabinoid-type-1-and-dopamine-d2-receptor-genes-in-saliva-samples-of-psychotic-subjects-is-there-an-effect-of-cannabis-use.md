---
layout: post
title: "DNA methylation at cannabinoid type 1 and dopamine D2 receptor genes in saliva samples of psychotic subjects: Is there an effect of Cannabis use?"
date: 2024-09-02
tags: [Psychosis,Dopamine receptor D2,Cannabinoid,DNA methylation,Cannabinoid receptor 1,Dopamine,Tetrahydrocannabinol,Schizophrenia,Genetics,Biology,Mental disorders]
author: 
---


Psychosis is a characterizing feature of many mental disorders that dramatically affects human thoughts and perceptions, influencing the ability to distinguish between what is real and what is not. Both genetic and environmental factors, such as stressful events or drug use, play a pivotal role in the development of symptomatology and therefore changes in the epigenome may be of relevance in modeling a psychotic phenotype. According to the well-documented dysregulation of endocannabinoid and dopaminergic system genes in schizophrenia, we investigated DNA methylation cannabinoid type 1 receptor (CNR1) and dopamine D2 receptor (DRD2) genes in saliva samples from psychotic subjects using pyrosequencing. The epigenetic mark was significantly higher and directly correlated for both genes in psychotic subjects compared to healthy controls. We also showed that these DNA methylation levels were lower in psychotic subjects reporting current delta-9-tetrahydrocannabinol (THC) consumption, a well-known risk factor for developing psychosis throughout the lifespan, resembling those of controls at least for the DRD2 gene.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S1043661824002883){:target="_blank rel="noopener"}

