---
layout: post
title: "Characterization and expression analysis of MATEs in Cannabis sativa L. reveals genes involving in cannabinoid synthesis"
date: 2024-08-27
tags: [Alternative splicing,Gene expression,Transcriptomics technologies,RNA-Seq,Conserved sequence,Protein isoform,Biotechnology,Molecular biology,Biochemistry,Biology,Cell biology,Genetics,Life sciences]
author: Atia Tul
---


Chromosomal location, gene structure, phylogenic tree, conserved motifs and cis-acting elements analysis of CsMATEs  Visualization of CsMATEs structures and chromosome locations were conducted using TBtools (Chen et al., 2020). FIGURE 2  Conserved motifs of CsMATEs and gene structure of CsMATEs  Conserved protein motifs are associated with gene function and protein subcellular localizations. Moreover, the seven genes exhibited similar expression pattern in different tissues as that of cannabinoid synthetic genes (Figure 4A), which further indicates the involvement CsMATEs in biosynthesis of cannabinoids. Notably, although overall variation of OA is similar to CBGA, accumulation of OA is opposed to CBGA at two hours treatment where OA content was decreased but CBGA contents was increased compared to zero hours treatment (Figure 6A). In addition, although CBGA content was not much affected under UV-B treatment at 12 hours, accumulations of THCA, CBDA and CBG were increased.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2022.1021088/full){:target="_blank rel="noopener"}

