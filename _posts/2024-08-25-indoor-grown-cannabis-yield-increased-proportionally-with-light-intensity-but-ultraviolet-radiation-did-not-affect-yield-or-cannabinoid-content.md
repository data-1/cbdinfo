---
layout: post
title: "Indoor grown cannabis yield increased proportionally with light intensity, but ultraviolet radiation did not affect yield or cannabinoid content"
date: 2024-08-25
tags: [Ultraviolet,Cannabinoid,Visible spectrum,Tetrahydrocannabinol,Ultraviolet index,Cannabidiol,Cannabinol,Cannabis,Photosynthetically active radiation,Tetrahydrocannabinolic acid,Ultraviolet–visible spectroscopy,Light-emitting diode]
author: A. Maxwell P
---


In one plot in each enclosure, two UVA LED bars were centered 0.24 m apart, at the same height of the LED Toplight fixtures. Foliar THC concentrations were higher in the UVA + UVB vs. the control and UVA treatments, but this did not affect the composite T-THC concentrations due to relatively high proportions of THCA in all treatments (Table 4). Further, since inflorescence tissues have substantially higher cannabinoid contents than other aboveground tissues (Richins et al., 2018), plants that produce proportionally higher inflorescence biomass under higher LI may also increase overall cannabinoid yield. Since none of the inflorescence cannabinoid levels were affected by the UV treatments, the eustress levels of UV radiation in this study did not have substantial effects on the secondary metabolite composition of the cannabis genotype used in this investigation. Given the negative effects of UV exposure in Rodriguez-Morrison et al. (2021b), positive effects in Lydon et al. (1987), and negligible effects in the present study relative to their total doses, it is still possible that there is a UV exposure protocol that provokes eustress over distress responses in some cannabis chemotypes.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2022.974018/full){:target="_blank rel="noopener"}

