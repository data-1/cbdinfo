---
layout: post
title: "Evaluation of cannabidiol’s inhibitory effect on alpha-glucosidase and its stability in simulated gastric and intestinal fluids - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Ma, Bioactive Botanical Research Laboratory, Department Of Biomedical, Pharmaceutical Sciences, College Of Pharmacy, University Of Rhode Island, Kingston, Li, Liu, Seeram
tags: [Cannabidiol,Acarbose,Alpha-glucosidase inhibitor,Chemistry,Biochemistry]
---


In addition, given that α-glucosidase inhibitors including CBD would have to survive gastrointestinal tract digestion to exert their desired inhibitory effects on α-glucosidase, the stability of CBD was also evaluated in simulated gastric and intestinal fluids. CBD was fairly stable in both simulated gastric and intestinal fluids for two hours with a remaining CBD level of 90.1 and 90.9%, respectively, as compared to the control group (Fig. 1 Inhibitory effect of CBD on α-glucosidase activity at concentrations of 10, 19, 38, 76, 152, 304, 608, and 1216 μM. Molecular forces formed between CBD and α-glucosidase protein and predicted binding energies (C) Full size image  Fig. Nevertheless, it should be noted that CBD at the higher test concentrations (i.e. 608 and 1216 μM) in the yeast α-glucosidase enzymatic assay showed promising anti-α-glucosidase activity but its efficacy as a α-glucosidase inhibitor in animals or human remains unknown.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00077-x){:target="_blank" rel="noopener"}


