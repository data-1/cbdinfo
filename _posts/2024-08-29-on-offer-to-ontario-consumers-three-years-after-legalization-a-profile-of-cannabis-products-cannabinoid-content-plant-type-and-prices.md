---
layout: post
title: "On offer to Ontario consumers three years after legalization: A profile of cannabis products, cannabinoid content, plant type, and prices"
date: 2024-08-29
tags: [Cannabis (drug),Cannabis edible,Cannabis in Canada,Cannabis smoking,Cannabidiol,Electronic cigarette,Cannabis,Individual psychoactive drugs,Entheogens,Cannabaceae]
author: Di Ciano
---


TABLE 1  TABLE 2  THC and CBD potency  A large proportion of inhalation products were categorized as very strong THC products (percent of products with ≥20%/g THC and average THC): dried flower: 94%, 22%/g; thread cartridges: 96%, 74%/g; resin: 100%, 71%/g (Table 2). Inhalation products had an average price of 6.49 $/g while ingestible products had an average price of 2.77 $/unit. The average THC potency in the current market is higher than the average potency of 10-13% reported by Health Canada prior to legalization (29), and higher still than the average 6–10% reported in the legal medical market that existed pre-legalization of non-medical cannabis (30). The average dried flower prices was lower than those reported in studies of the retail cannabis market in Canada pre-legalization and at the time of legalization (29, 30). In comparison to self-reported legal cannabis prices in Canada from 2019, regardless of quantity, the average price of dried flower in 2022 is also lower, ranging from $23.16/1g to $9.95/3.5 g and $9.95/27.9 g in 2019 (25).

[Visit Link](https://www.frontiersin.org/journals/psychiatry/articles/10.3389/fpsyt.2023.1111330/full){:target="_blank rel="noopener"}

