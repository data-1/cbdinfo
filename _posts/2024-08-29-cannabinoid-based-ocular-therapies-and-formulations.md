---
layout: post
title: "Cannabinoid-Based Ocular Therapies and Formulations"
date: 2024-08-29
tags: [Dronabinol,Medical cannabis,Glaucoma,Anandamide,Cannabinoid receptor 2,Biofilm,Cannabinoid,Contact lens,Clinical medicine]
author: Sofia M. Saraiva | Lucía Martín-Banderas | Matilde Durán-Lobato | Sofia M | Martín-Banderas | Durán-Lobato
---


However, due to cannabinoids’ unfavorable physicochemical properties and adverse systemic effects, along with ocular biological barriers to local drug administration, drug delivery systems are needed. Therapeutic Potential of Cannabinoids on Glaucoma and Limitations of Conventional Formulations  Cannabis seems to be an alternative for the treatment of glaucoma, since some studies have demonstrated its capacity to decrease IOP [16]. Regarding ocular applications, these drugs have shown interesting pharmacological properties that could lead to alternative treatments for several diseases in need of new or improved therapies, including glaucoma, uveitis, DR, keratitis and the prevention of Pseudomonas aeruginosa infection. Cannabinoids and the Eye. [Google Scholar] [CrossRef] [Green Version]  Szczesniak, A.M.; Kelly, M.E.M.

[Visit Link](https://www.mdpi.com/1999-4923/15/4/1077){:target="_blank rel="noopener"}

