---
layout: post
title: "Cannabigerol Action at Cannabinoid CB1 and CB2 Receptors and at CB1–CB2 Heteroreceptor Complexes"
date: 2024-08-18
tags: [Cannabinoid,Cannabinoid receptor 2,Cannabinoid receptor 1,Ligand (biochemistry),Receptor antagonist,Functional selectivity,Biochemistry,Neurochemistry,Cell biology,Cell signaling]
author: Reyes-Resina | Sánchez de Medina | Rivas-Santisteban | Sánchez-Carnerero Callado | Ferreiro-Vera | Enric I | Pier Andrea
---


Competition Binding Experiments  [3H]-CP-55940 competition binding experiments were performed incubating 0.3 nM of radioligand and different concentrations of the tested compounds with membranes obtained from CHO cells expressing human CB1 or CB2 receptors (10 μg protein/sample) for 90 min (CB1R) or 60 min (CB2R) at 30°C. TABLE 1  FIGURE 2  CBG Binds to the Orthosteric Site of Cannabinoid CB2R at Nanomolar Concentrations  Competition experiments were performed using 20 nM of a fluorophore-conjugated selective CB2R agonist (CM-157) and a homogeneous non-radioactive method performed in living cells expressing SNAP-CB2R (details in Martínez-Pinilla et al., 2016; Figure 2C). These results indicate that in cells expressing both cannabinoid receptors, CB1 and CB2, CBG shows higher affinity for cannabinoid CB2R. To characterize the CBG effect, experiments were performed in HEK-293T cells expressing the two cannabinoid receptors. At 1 μM the effect of CBG on receptor activation by other agonists was similar to that exerted by 100 nM (Figure 4) thus suggesting that the effective affinity in living cells is that obtained in HTRF non-radioactive-based assays.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2018.00632/full){:target="_blank rel="noopener"}

