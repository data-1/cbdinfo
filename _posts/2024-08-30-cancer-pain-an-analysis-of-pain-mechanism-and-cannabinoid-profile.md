---
layout: post
title: "cancer pain: an analysis of pain mechanism and cannabinoid profile"
date: 2024-08-30
tags: [Cannabidiol,Medical cannabis,Cannabinoid,Pain,Chronic pain,Tetrahydrocannabinol,Pain management,Cannabis edible,Chronic condition,Cannabis,Health care,Diseases and disorders,Medicine,Clinical medicine,Health]
author: Erin Prosk | Charles Sun | Alain Watier | Michael Dworkind | Lucile Rapin | Maria Fernanda Arboleda
---


For participants taking multiple cannabis products, the cannabinoid profile was determined by the overall THC:CBD concentration of all products. There is no association between pain mechanism and baseline treatment recommendations [χ2(6) = 1.32, P < 0.97] which indicates that pain mechanism is not a primary consideration in treatment recommendations relative to other factors, such as pain severity and previous cannabis use. BPI-SF pain severity average scores for baseline and FUP3M, according to pain mechanism and initial cannabinoid profile. The ADRs severity, causality, outcome, and action taken according to the initial cannabinoid profile are reported in Table 4. Among patients who had previously tried both dried flower and/or ingested cannabis oil extracts, there was a higher proportion of no ADRs (75%) vs. at least one ADR (25%) reported.

[Visit Link](https://www.explorationpub.com/Journals/em/Article/1001148){:target="_blank rel="noopener"}

