---
layout: post
title: "Granite dust application to hemp – variety-specific impacts on growth and cannabinoid production"
date: 2024-09-02
tags: [Cannabis,Hemp,Cannabis sativa,Cannabidiol,High-performance liquid chromatography,Cannabinoid,Soil,Fertilizer,Plant,Leaf]
author: N. K
---


Results: plant growth experiments  Soil amendment with granite dust at 5% w/w and 10% w/w dosages and applied to different varieties of C. sativa yielded selective improvement to growth characteristics versus control plants of the same varieties (Tables 1 and 2). Katani and Fibranova varieties did not contain significant amounts of CBDA and are therefore not included, and Cherry Blossom plants subjected to rose granite dust treatment were not tested for CBD or CBDA content. Soil amendment improved flower growth in CFX-2 variety C. sativa plants. In terms of quantitative changes in cannabinoid production, the application of granite dust as soil amendment significantly increased the production of CBD and CBDA only for CFX-2. Growth and cannabinoid production of Cherry Blossom high CBD-variety plants were not significantly improved by soil amendment with granite dust.

[Visit Link](https://www.nature.com/articles/s41598-023-49529-9){:target="_blank rel="noopener"}

