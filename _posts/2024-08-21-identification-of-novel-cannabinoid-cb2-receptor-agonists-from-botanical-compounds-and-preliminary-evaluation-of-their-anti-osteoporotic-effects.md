---
layout: post
title: "Identification of Novel Cannabinoid CB2 Receptor Agonists from Botanical Compounds and Preliminary Evaluation of Their Anti-Osteoporotic Effects"
date: 2024-08-21
tags: [Cannabinoid receptor 2,Ligand (biochemistry),Docking (molecular),Osteoblast,Bone,Stacking (chemistry),Protein structure,Cyclic adenosine monophosphate,Osteoclast,Protein,Cannabinoid receptor 1,Biology,Cell biology,Biotechnology,Biochemistry]
author: Si-Jing Hu | Gang Cheng | Hao Zhou | Qi Zhang | Quan-Long Zhang | Yang Wang | Yi Shen | Chen-Xia Lian | Xue-Qin Ma | Qiao-Yan Zhang
---


(d) The agonistic effect of HU308 on CB2R in HEK293-CB2 cells. Determination of Proliferation, ALP Activities, and Bone Mineralized Nodules of Osteoblasts  The BMSCs cells were inoculated in a 96-well plate at a density of 1 × 104 cells per well and cultured at 37 °C for 24 h. The cells were treated with 1 μM HU308 and the nine screened natural CB2 receptor agonists (dihydromethysticin, desmethoxyyangonin, flavokawain A, echinatin, mangiferin, 11-keto-beta-boswellic acid, flavokawain C, orientin, and asperuloside) for 48 h, and 10 μL CCK-8 solution was added to the culture well and incubated for 30 min. [Google Scholar] [CrossRef] [PubMed] [Green Version]  Tian, F.; Yang, H.; Huang, T.; Chen, F.; Xiong, F. Involvement of CB2 signalling pathway in the development of osteoporosis by regulating the proliferation and differentiation of hBMSCs. Cell. [Google Scholar] [CrossRef] [PubMed] [Green Version]  Lee, D.; Yu, J.; Huang, P.; Qader, M.; Manavalan, A.; Wu, X.; Kim, J.-C.; Pang, C.; Cao, S.; Kang, K.; et al.

[Visit Link](https://www.mdpi.com/1420-3049/27/3/702){:target="_blank rel="noopener"}

