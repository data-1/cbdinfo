---
layout: post
title: "The Medicinal Natural Products of Cannabis sativa Linn.: A Review"
date: 2022-07-31
categories:
- Cannabis
author: Odieka, Anwuli Endurance, Obuzor, Gloria Ukalina, Oyedeji, Opeoluwa Oyehan, Gondwe, Hosu, Yiseyon Sunday, Adebola Omowunmi
tags: [Medical cannabis,Tetrahydrocannabinol,Cannabinoid,Solvent,Tetrahydrocannabinolic acid,Mass spectrometry,Cannabidiol,Dronabinol,Chromatography,High-performance liquid chromatography,Nabilone,Cannabis sativa,Chemistry]
---


The applications of plants as medicines predates human history. One notable medicinal plant that has continued to garner attention over the years, and in recent times, is  Cannabis sativa L. is known for its medicinal uses since ancient times, because of its rich supply of phytochemicals [ C. sativa and its variants, which are of the family Cannabaceae [9-tetrahydrocannabinol; C 12 H 30 O 2 ) [ L. is known for its medicinal uses since ancient times, because of its rich supply of phytochemicals [ 4 ], hence the quest for harnessing its pharmacological potential by scientists. 9, C. sativa are associated with fewer side effects and can be used for several industrial applications [ Cannabis sativa contains essential oils of a high value, which can also improve the effectiveness of cannabinoids in pharmaceutical formulations [ However, in multiple countries today, its cultivation and usage are regulated by laws [ 6 8 ]. The Office of Medical Cannabis Research (OMC), a Dutch government agency in Europe, became the first organization to obtain the exclusive right to supply medical cannabis to research institutes and pharmacies, and, under the Single Convention on Narcotic Drugs of 1961, to import and export cannabis extracts and resin for medical purposes. In addition,contains essential oils of a high value, which can also improve the effectiveness of cannabinoids in pharmaceutical formulations [ 6 ].

<hr>[Visit Link](https://www.mdpi.com/1420-3049/27/5/1689/htm){:target="_blank" rel="noopener"}


