---
layout: post
title: "Editorial: Appropriateness and safety of using cannabinoid and psychedelic medicines as treatments for psychiatric disorders"
date: 2024-08-29
tags: [Psychedelic drug,Randomized controlled trial,Mental disorder,Psilocybin,Research,Psychiatry,Health,Clinical medicine,Mental disorders,Medicine,Health care,Medical specialties,Mental health]
author: Blest-Hopley | Ben H | O'Neill | Simon G. D
---


This paper provides a framework for ensuring consistent and quality analysis of such real-world evidence, which can highlight therapeutic potential for further investigation in more rigorous RCTs and inform the clinical use of these substances in the real world (Schlag et al.). MacCallum et al. report on psilocybin outlined mechanisms and use cases for the substance and emphasized the importance of caution and risk mitigation in future studies and real-world use (MacCallum et al.). The common themes in these papers highlight the need for more research to make robust claims on the efficacy of cannabinoid and psychedelic substances for psychiatric disorders. One of the key themes is the utilization of real-world evidence to identify therapeutic areas that can be explored in rigorously designed trials, but also practical considerations for appropriateness, efficacy, and safety in a population. Therefore, researchers, regulators, and the interested wider population should be cognizant of this need.

[Visit Link](https://www.frontiersin.org/journals/psychiatry/articles/10.3389/fpsyt.2023.1191970/full){:target="_blank rel="noopener"}

