---
layout: post
title: "Perturbation of 3D nuclear architecture, epigenomic aging and dysregulation, and cannabinoid synaptopathy reconfigures conceptualization of cannabinoid pathophysiology: part 2—Metabolome, immunome, synaptome"
date: 2024-09-02
tags: [CGAS–STING cytosolic DNA sensing pathway,Mitochondrion,Synapse,Lactate dehydrogenase,Gliotransmitter,Retrotransposon,Cellular senescence,Long-term potentiation,Chemical synapse,Astrocyte,Basal ganglia,Brain-derived neurotrophic factor,Synaptic plasticity,Neuron,Inflammation,Epigenetics,Senescence-associated secretory phenotype,Immune system,Carcinogenesis,Chromatin remodeling,Cell signaling,DNA repair,Striatum,Stimulator of interferon genes,Signal transduction,Biology,Molecular biology,Cellular processes,Biotechnology,Biochemistry,Physiology,Cell biology]
author: Albert Stuart Reece | Gary Kenneth Hulse
---


Mitochondria have over 1,000 different proteins. In general terms, these effects occur either because of lactate as a metabolic substrate, via lactate signaling, or from lactylation of key proteins (107). In dendritic cells, lactate induces an acidic environment, reduces CD1a and increases CD14 expression, and activates GPR81 signaling and lactate, which is important via SLC16A. Cancer  cGAS-STING surveils cancer cells. Inflammatory signaling can either stimulate or inhibit ECS signaling via CB1R and CB2R signaling or by changing the levels of activity of eCB metabolic enzymes (141).

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10579598/){:target="_blank rel="noopener"}

