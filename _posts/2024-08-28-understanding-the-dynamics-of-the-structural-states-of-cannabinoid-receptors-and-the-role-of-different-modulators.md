---
layout: post
title: "Understanding the Dynamics of the Structural States of Cannabinoid Receptors and the Role of Different Modulators"
date: 2024-08-28
tags: [Cannabinoid receptor 2,Receptor antagonist,G protein-coupled receptor,Ligand (biochemistry),Cannabinoid receptor 1,Cannabinoid,Cell biology,Cell signaling,Neurochemistry,Biochemistry]
author: Anjela Manandhar | Mona H. Haron | Michael L. Klein | Khaled Elokely | Mona H | Michael L
---


by  Anjela Manandhar  1,  Mona H. Haron  2,  Michael L. Klein  1 and  Khaled Elokely  1,*  1  Institute for Computational Molecular Science and Department of Chemistry, Temple University, Philadelphia, PA 19122, USA  2  National Center for Natural Products Research, University of Mississippi, Oxford, MS 38677, USA  *  Author to whom correspondence should be addressed. Cannabinoid Receptor 1 (CB1R) and Cannabinoid Receptor 2 (CB2R) are the two types of human cannabinoid receptors currently identified. Discussion  Here, we modelled the active, inactive, and intermediate states of CBRs and investigated the structural changes upon the binding of different modulators –agonists, antagonists, and inverse agonists. [Google Scholar] [CrossRef] [PubMed]  Zou, S.; Kumar, U. Cannabinoid receptors and the endocannabinoid system: Signaling and function in the central nervous system. [Google Scholar] [CrossRef]  Friesner, R.A.; Murphy, R.B.

[Visit Link](https://www.mdpi.com/2075-1729/12/12/2137){:target="_blank rel="noopener"}

