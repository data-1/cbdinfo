---
layout: post
title: "Hexahydrocannabinol (HHC) and Δ9-tetrahydrocannabinol (Δ9-THC) driven activation of cannabinoid receptor 1 results in biased intracellular signaling"
date: 2024-09-02
tags: [Cannabinoid receptor 1,Endocannabinoid system,Cell signaling,Cannabinoid,Cannabidiol,G protein-coupled receptor,Nervous system,Receptor (biochemistry),Biochemistry]
author: 
---


Cannabis plant extracts include many compounds in addition to Δ9-THC and CBD. Further, β-arrestin initiates internalization of the receptor and, at the same time, facilitates activation of signaling pathways such as ERK1/2 or JNK319,20. The resulting mixture of ∆8-THC and MeOH was used for the reduction without further purification. G protein activation induced by CB1R stimulation  To test whether HHC induces signaling via CB1R, we first measured the G protein activation in the transfected cells. Overall, the results indicate that the (9R)-HHC epimer stimulates GRK3-CB1R and β-arrestin2-CB1R interactions more effectively than Δ9-THC or the (9S)-HHC epimer.

[Visit Link](https://www.nature.com/articles/s41598-024-58845-7){:target="_blank rel="noopener"}

