---
layout: post
title: "Distribution of Cannabinoid Receptors in Keratinocytes of Healthy Dogs and Dogs With Atopic Dermatitis"
date: 2024-08-25
tags: [Keratinocyte,Epidermis,Cannabinoid receptor 2,Skin,Endocannabinoid system,TRPV1,T helper cell,Cannabinoid,Cannabinoid receptor 1,Inflammation,Interleukin 31,Innate immune system,Cytokine,Immune system,Cell biology,Biology]
author: De Silva | Rodrigo Zamith
---


The tissues from the CTRL- and the AD-dogs were processed to obtain cryosections. AD dogs. Cannabinoid and Cannabinoid-Related Receptor Immunolabelling in Keratinocytes  All the receptors studied showed some degree of immunoreactivity (IR) in the basal and suprabasal layers whereas none of the receptors studied were expressed in the stratum corneum. In the present study, PPARα was identified in the keratinocytes of the basal and the suprabasal layers of the skin of both the CTRL- and the AD-dogs. While TRPV1 antagonists could play a potential role in the treatment of humans AD (71, 75), nothing can be said about dog AD yet, also given that in the current study the keratinocytes of the AD-dogs did not show significant upregulation of TRPV1-IR in the suprabasal layer cells when compared to the CTRL-dogs.

[Visit Link](https://www.frontiersin.org/journals/veterinary-science/articles/10.3389/fvets.2022.915896/full){:target="_blank rel="noopener"}

