---
layout: post
title: "Neuronal Cannabinoid CB1 Receptors Suppress the Growth of Melanoma Brain Metastases by Inhibiting Glutamatergic Signalling"
date: 2024-08-29
tags: [NMDA receptor,Anandamide,Cannabinoid receptor 1,2-Arachidonoylglycerol,Metastasis,Endocannabinoid system,Melanoma,Glutamate receptor,Cell signaling,Biology,Biochemistry,Neurochemistry]
author: Carlos Costas-Insua | Marta Seijo-Vila | Cristina Blázquez | Sandra Blasco-Benito | Francisco Javier Rodríguez-Baena | Giovanni Marsicano | Eduardo Pérez-Gómez | Cristina Sánchez | Berta Sánchez-Laorden | Manuel Guzmán
---


Cell 2020, 181, 219–222. Cell 2022, 185, 2899–2917.e31. Cell 2013, 153, 86–100. Cell 2015, 161, 1681–1696. Cell Rep. 2018, 24, 1523–1535.

[Visit Link](https://www.mdpi.com/2072-6694/15/9/2439){:target="_blank rel="noopener"}

