---
layout: post
title: "Cannabis hyperemesis syndrome: an update on the pathophysiology and management"
date: 2024-09-16
tags: [Endocannabinoid system,Cannabis (drug),2-Arachidonoylglycerol,Cannabinoid receptor 2,Cannabinoid hyperemesis syndrome,Tetrahydrocannabinol,TRPV1,Cannabinoid,Nausea,Rome process,Cannabinoid receptor 1,Cell signaling,Clinical medicine]
author: Abhilash Perisetti
institutions: University of Arkansas for Medical Sciences
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7599351/"
---


Pharmacology of cannabinoid CB1 and CB2 receptors. Venkatesan T, Levinthal DJ, Li BU, et al. Role of chronic cannabis use: cyclic vomiting syndrome vs cannabinoid hyperemesis syndrome. Cannabinoid hyperemesis syndrome: a paradoxical cannabis effect. Cannabinoid hyperemesis syndrome. Venkatesan T, Levinthal DJ, Li BU, et al. Role of chronic cannabis use: cyclic vomiting syndrome vs cannabinoid hyperemesis syndrome.