---
layout: post
title: "A focused review on CB2 receptor-selective pharmacological properties and therapeutic potential of β-caryophyllene, a dietary cannabinoid"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Pharmacology,Endocannabinoid system,Cannabinoid receptor 1,Health,Medicine,Medical research,Neurochemistry,Pharmaceutical sciences,Clinical medicine,Drugs,Medicinal chemistry,Medical treatments,Biochemistry]
author: 
---


The endocannabinoid system (ECS), a conserved physiological system emerged as a novel pharmacological target for its significant role and potential therapeutic benefits ranging from neurological diseases to cancer. Among both, CB1 and CB2R types, CB2R have received attention for its pharmacological effects as antioxidant, anti-inflammatory, immunomodulatory and antiapoptotic that can be achieved without causing psychotropic adverse effects through CB1R. The ligands activate CB2R are of endogenous, synthetic and plant origin. In recent years, β-caryophyllene (BCP), a natural bicyclic sesquiterpene in cannabis as well as non-cannabis plants, has received attention due to its selective agonist property on CB2R. The focus of the present manuscript is to represent the CB2R selective agonist mediated pharmacological mechanisms and therapeutic potential of BCP.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S0753332221004248){:target="_blank rel="noopener"}

