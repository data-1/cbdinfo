---
layout: post
title: "Impact of Different Phytohormones on Morphology, Yield and Cannabinoid Content of Cannabis sativa L."
date: 2024-08-20
tags: [Cannabidiol,Medical cannabis,Cannabis,Apical dominance,Plant hormone,Auxin,Tetrahydrocannabinol,Botany,Plants]
author: Lisa Burgel | Jens Hartung | Daniele Schibano | Simone Graeff-Hönninger | Graeff-Hönninger
---


Cannabis Cannabinoids 2020, 1–8. Cannabis Cannabinoids 2018, 1, 19–27. Plant J. Plant J. Plant 2011, 4, 616–625.

[Visit Link](https://www.mdpi.com/2223-7747/9/6/725){:target="_blank rel="noopener"}

