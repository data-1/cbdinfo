---
layout: post
title: "Non-Canonical Cannabinoid Receptors with Distinct Binding and Signaling Properties in Prostate and Other Cancer Cell Types Mediate Cell Death"
date: 2024-08-21
tags: [Ligand (biochemistry),Receptor antagonist,Cannabinoid receptor 2,Receptor (biochemistry),G protein-coupled receptor,G protein,Cannabinoid receptor 1,Cell signaling,Biochemistry,Neurochemistry,Biology,Cell biology]
author: Amal M. Shoeib | Lance N. Benson | Shengyu Mu | Lee Ann MacMillan-Crow | Paul L. Prather | Amal M | Lance N | MacMillan-Crow | Lee Ann | Paul L
---


In any case, when all receptor binding data are taken as a whole, it is relatively clear that CBRs expressed in the cancer cells examined exhibit very distinct receptor binding and functional properties relative to canonical cannabinoid receptors. Effect of Chronic Cannabinoid Ligand Treatment on CBR Density in Prostate Cancer Cells  These studies were conducted to determine the effect of chronic exposure of PC-3 and DU-145 cells to cannabinoid ligands with high affinity for non-canonical CBRs on receptor expression. Cancer 2009, 101, 940–950. Cancer 2012, 12, 436–444. Cell.

[Visit Link](https://www.mdpi.com/1422-0067/23/6/3049){:target="_blank rel="noopener"}

