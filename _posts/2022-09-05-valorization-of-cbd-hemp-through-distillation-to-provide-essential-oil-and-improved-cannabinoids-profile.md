---
layout: post
title: "Valorization of CBD-hemp through distillation to provide essential oil and improved cannabinoids profile"
date: 2022-09-05
categories:
- Cannabis
- Hemp
author: Zheljazkov, Valtcho D., Crop, Soil Science Department, Oregon State University, Corvallis, Maggi, School Of Pharmacy, University Of Camerino, Camerino
tags: [Cannabis,Cannabis sativa,Cannabinoid,Gland (botany),Hemp,Cannabidiol]
---


In 2014, section 7606 of the U.S. Congress Agricultural Act of 2014, the “Farm Bill”, authorized pilot programs on cultivation of industrial hemp, defined as “the plant Cannabis sativa L. and any part of such plant, whether growing or not, with a delta-9 tetrahydrocannabinol (THC) concentration of not more than 0.3% on a dry weight basis”. Most of the hemp grown in the U.S. is for production of high-value chemicals such as cannabinoids and terpenes. There has been a debate on whether hemp is a single species or include other species such as Cannabis indica Lam. Most of the hemp chemicals are produced in multicellular glandular trichomes, which can be sessile glands (with very short stalks), or long-stalk secretory glands (Figs. The top of these glands is a cavity covered by a waxy cuticle, where the resin (a mix of cannabinoids and terpenes) is accumulated.

<hr>

[Visit Link](https://www.nature.com/articles/s41598-021-99335-4){:target="_blank" rel="noopener"}


