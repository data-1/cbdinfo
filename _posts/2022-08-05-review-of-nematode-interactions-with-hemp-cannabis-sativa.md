---
layout: post
title: "Review of Nematode Interactions with Hemp (Cannabis Sativa)"
date: 2022-08-05
categories:
- Hemp
author: Ernest C. Bernard, Entomology, Plant Pathology, The University Of Tennessee, Plant Biotechnology, Building, E J Chapman Drive, Knoxville, Angel G. Chaffin, Pope'S Plant Farm
tags: [Cannabis,Root-knot nematode,Ditylenchus dipsaci,Soybean cyst nematode,Cannabis sativa,Cannabidiol,Plants,Botany,Kingdoms (biology),Organisms]
---


The primary literature was searched for hemp-nematode papers, resulting in citations from 1890 through 2021. Keywords: Cannabis sativa, Cyst nematodes, Ditylenchus, Hemp, Heterodera humuli, Host-parasite relationships, Management, Meloidogyne, Plant extracts, Pratylenchus, Review, Root chemistry, Root-knot nematodes, Stem nematodes  Materials and methods Hemp literature Articles were gathered from the Web of Science database using the terms “Cannabis × nemato*” and “hemp × nemato*”. To determine the total number of records for root-knot nematodes, the terms “Meloidogyne”, “Heterodera radicicola” and “Heterodera marioni” were searched. In a more complicated case, Goodey et al. (1965) listed Ditylenchus dipsaci as a parasite of hemp, citing Steiner and Buhrer (1932). The present literature search yielded 38 primary citations of nematodes as parasites of hemp and 23 citations on uses of hemp for management of nematodes.

<hr>

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8975275/){:target="_blank" rel="noopener"}


