---
layout: post
title: "Interaction between cannabinoid and nucleotide systems as a new mechanism of signaling in retinal cell death"
date: 2024-08-19
tags: [Retina,Cannabinoid,2-Arachidonoylglycerol,Cannabinoid receptor 1,Endocannabinoid system,Retinopathy,Neurodegenerative disease,Inflammation,Anandamide,Macular degeneration,Cell biology,Neurochemistry,Biochemistry,Biology]
author: Hércules R. Freitas | Ricardo A. M. Reis | Ana L. M. Ventura | Guilherme R. França
---


Albeit these protective effects, however, recent data are implicating cannabinoid receptors in cell death in the retina, both in the early developing (Freitas et al., 2019) and diseased tissue (Matias et al., 2006; El-Remessy et al., 2011; Chen et al., 2018). In the developing retina, purinergic signaling is critical to trigger eye and retina development through the activation of distinct P2Y receptor subtypes that regulate neurogenesis and cell migration as well as the activation of P2X7 receptors (P2X7Rs) that mediates neuronal cell death in this tissue (Ventura et al., 2018). P2X7R is the major nucleotide receptor involved in retinal cell death and its activation by ATP in the developing tissue induces the death of neurons. These findings, together with the observation that ATP-mediated increase in intracellular calcium is observed in the cultures after the treatment of the cells with the cannabinoid agonist, strongly suggest that activation of cannabinoid receptors promotes P2X7R-mediated signaling in retinal progenitors in culture ( ). Since activation of P2X7Rs is implicated in the death of retinal cells in several types of retinal injuries, an interesting possibility to explain the deleterious effects of cannabinoids in the retina would be that in certain conditions, cannabinoids modulated the expression/function of P2X7Rs, resulting in increased calcium signaling induced by these nucleotide receptors.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6788250/){:target="_blank rel="noopener"}

