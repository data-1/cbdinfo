---
layout: post
title: "Non-destructive assessment of cannabis quality during drying process using hyperspectral imaging and machine learning"
date: 2024-05-24
categories:
- 
author: Hyo In, Su Hyeon, Soo Hyun, Je Hyeong, Ho-Youn, Jung-Seok
tags: [Tetrahydrocannabinolic acid,Receiver operating characteristic,Precision and recall,Cannabinoid,Cannabis sativa,Cannabidiol,Support vector machine,Hyperspectral imaging,K-nearest neighbors algorithm,Statistical classification,Machine learning]
---


According to Chen et al. (2021), hot-air drying increased CBDA conversion rate and decreased drying time as the temperature increased from 40°C to 90°C. Figure 3  Figure 4  3.3 Changes in cannabinoids of cannabis flowers during drying process  Cannabinoids, such as CBDA, CBD, THCA, and THC, underwent significant changes during the drying process (Figure 5). The developed models were used to monitor the cannabis quality during the drying process. 4.3 Hyperspectral imaging analysis with spectral pre-processing and machine learning  We established a hyperspectral imaging-based model for evaluating cannabis quality during the drying process, including dryness, CBDA conversion, THCA conversion, and CBD : THC ratio. The classification model for the total CBD content was predicted with an accuracy of 0.741 by the RF model combined with 2nd Der, and the model for the total THC content was predicted with an accuracy of 0.833 by the LR model without spectral preprocessing.

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2024.1365298/full){:target="_blank" rel="noopener"}


