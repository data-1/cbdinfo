---
layout: post
title: "Differentiating Cannabis Products: Drugs, Food, and Supplements"
date: 2022-07-31
categories:
- Cannabinoid
- Cannabis
author: Salehi, Department Of Pharmacognosy, Puchalski, Ric Scalzo Institute For Botanical Research, United States, Shokoohinia, Zolfaghari, Asgary, Isfahan Cardiovascular Research Center, Carl R. Lupica
tags: [Cannabidiol,Cannabis,Dronabinol,Medical cannabis,Tetrahydrocannabinol,Cannabinoid,Cannabis sativa,Tetrahydrocannabinolic acid,Cannabis (drug),Hemp,Nabiximols,Drug,Cannabis strain,Gas chromatography,Drugs]
---


Common hemp seed food products include bulk or packaged raw hulled hemp seeds, hemp seed oil, hemp protein “concentrates” or powders, and hemp milk, among others. Google Scholar  Buchbauer, G. (2010). doi:10.1111/jse.12552 CrossRef Full Text | Google Scholar  Johnson, R. (2019). Google Scholar  Johnson, R. (2018). Extraction of Cannabinoids from Cannabis Sativa L. (Hemp)-Review.

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2022.906038/full){:target="_blank" rel="noopener"}


