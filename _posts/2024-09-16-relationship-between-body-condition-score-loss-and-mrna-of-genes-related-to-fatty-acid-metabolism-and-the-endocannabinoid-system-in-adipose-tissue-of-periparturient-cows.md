---
layout: post
title: "Relationship between body condition score loss and mRNA of genes related to fatty acid metabolism and the endocannabinoid system in adipose tissue of periparturient cows"
date: 2024-09-16
tags: [Cattle,Dairy cattle,Fatty acid metabolism,Fatty acid,Adipose tissue,Acyl-CoA,Biomolecules,Biochemistry]
author: E. Dirandeh | M. Ghorbanalinia | A. Rezaei-Roodbari | M.G. Colazo
institutions: Agriculture Food and Rural Development
doi: "https://doi.org/10.1017/s1751731120000476"
---


The aim of this study was to determine the relationship between body condition score (BCS) loss and the mRNA abundance of genes related to fatty acid metabolism and the ECS in the subcutaneous adipose tissue (AT) of dairy cows. Real-time PCR was used to determine mRNA abundance of key genes related to fatty acid metabolism, inflammation and ECS in AT. The overall mean plasma haptoglobin and NEFA concentrations were greater in MBL and HBL groups compared with the LBL group (P < 0.05). The mRNA abundance of TNF-α, Interleukin-6 (IL-6) and IL-1β was greatest at 21 and 42 days post-calving in HBL, intermediate in MBL and lowest in LBL groups, respectively. However, mRNA abundance of fatty acid amide hydrolase was lower at 21 and 42 days post-calving in HBL cows than in LBL cows (P < 0.05).