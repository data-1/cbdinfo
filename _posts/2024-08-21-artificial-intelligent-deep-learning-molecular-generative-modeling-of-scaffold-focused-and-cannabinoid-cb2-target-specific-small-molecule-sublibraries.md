---
layout: post
title: "Artificial Intelligent Deep Learning Molecular Generative Modeling of Scaffold-Focused and Cannabinoid CB2 Target-Specific Small-Molecule Sublibraries"
date: 2024-08-21
tags: [Cannabinoid receptor 2,Drug discovery,Training, validation, and test data sets,Simplified molecular-input line-entry system,Allosteric regulation,Long short-term memory,Ligand (biochemistry),Deep learning,Functional group,Pharmacology,Chemistry]
author: Yuemin Bian | Xiang-Qun Xie | Xiang-Qun
---


From the training molecules, generative models study and summarize a probability distribution to sample new molecules that are similar to the training data [24,25]. The training process results in a probability distribution of the next SMILES character given the input string. The validity, uniqueness, and novelty of sampled indole molecules under different epochs and sampling temperatures of the g-DeepMGM. Table S3: The validity, uniqueness, and novelty of sampled purine molecules under different epochs and sampling temperatures of the g-DeepMGM. Generative chemistry: Drug discovery with deep learning generative models.

[Visit Link](https://www.mdpi.com/2073-4409/11/5/915){:target="_blank rel="noopener"}

