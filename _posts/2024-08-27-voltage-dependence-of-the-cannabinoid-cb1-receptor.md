---
layout: post
title: "Voltage dependence of the cannabinoid CB1 receptor"
date: 2024-08-27
tags: [Receptor (biochemistry),G protein-coupled receptor,Cannabinoid receptor 1,Ligand (biochemistry),Synapse,Cannabinoid,EC50,2-Arachidonoylglycerol,Voltage-gated ion channel,Membrane potential,Cannabinoid receptor,Agonist,Anandamide,Cannabinoid receptor 2,Cell signaling,Basic neuroscience research,Neurophysiology,Biochemistry,Cell biology,Neurochemistry,Signal transduction,Cell communication]
author: Ben-Chaim
---


For several GPCRs, it has been proposed that voltage dependence is also agonist specific (Sahlholm et al., 2008b; Navarro-Polanco et al., 2011; Rinne et al., 2015; Ruland et al., 2020); namely, depolarization, that reduces the binding of one agonist to a given receptor, will have no effect, or will even have an opposite effect, on the binding of other agonists. To further evaluate the voltage dependence of the potency of 2-AG toward the CB1 receptor we measured the activation of the receptor by 2-AG at several holding potentials. On the other hand, when AEA was the activating ligand, the decay of the GIRK current following washout occurred at similar rate at both membrane potentials (mean time constant was 42.5 ± 14.8 s at -80 mV and 41.1 ± 17.4 s at +40 mV, p = 0.6, paired t-test), again consistent with the observation that the affinity of AEA is not voltage dependent. It is seen that the potency of THC in activating the CB1 receptor is voltage dependent. Here, we show that the cannabinoid CB1 receptor is also voltage dependent.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2022.1022275/full){:target="_blank rel="noopener"}

