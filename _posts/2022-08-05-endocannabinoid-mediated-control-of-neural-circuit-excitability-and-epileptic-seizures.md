---
layout: post
title: "Endocannabinoid-Mediated Control of Neural Circuit Excitability and Epileptic Seizures"
date: 2022-08-05
categories:
- Cannabinoid
- Endocannabinoid
author: Sugaya, Department Of Neurophysiology, Graduate School Of Medicine, The University Of Tokyo, International Research Center For Neurointelligence, Wpi-Ircn, The University Of Tokyo Institutes For Advanced Study, Utias, Kano, Edward S. Ruthazer
tags: [Epileptogenesis,Cannabinoid receptor type 1,Epilepsy,Hippocampus,Endocannabinoid system,Cannabidiol,Cannabinoid,Inhibitory postsynaptic potential,Chemical synapse,Spike-and-wave,Seizure,Neurotransmitter,Cannabinoid receptor type 2,Synapse,Brain,Hippocampus anatomy,Anticonvulsant,Hippocampus proper,TRPV1,Receptor (biochemistry),Neuron,2-Arachidonoylglycerol,Dentate gyrus,Cell signaling,Kindling (sedativehypnotic withdrawal),Clobazam,-Aminobutyric acid,Hippocampal sclerosis,Physiology,Cell biology,Biochemistry,Neurophysiology,Neurochemistry,Neuroscience]
---


Involvement of CB 1 and CB 2 Receptors  It has also been reported that, as with DGLα, expression of the CB 1 receptor is affected by epileptogenesis (Figure 2). Taken together, these results indicate that CB 1 receptor signaling at excitatory synaptic terminals in the hippocampus has a suppressive effect on kainate-induced seizures (Table 1). These results indicate that CB 1 receptor signaling in the epileptic brain suppresses the occurrence of spontaneous seizures in the SE model and the absence seizure model (Figure 2 and Table 1). Therefore, CBD might suppress seizures by increasing the levels of AEA and/or 2-AG. Seizure 57, 22–26.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fncir.2021.781113/full){:target="_blank" rel="noopener"}


