---
layout: post
title: "Overexpression of cannabinoid receptor 2 is associated with human breast cancer proliferation, apoptosis, chemosensitivity and prognosis via the PI3K/Akt/mTOR signaling pathway"
date: 2024-08-30
tags: [Cannabinoid receptor 2,PI3K/AKT/mTOR pathway,Cannabinoid receptor,Chemotherapy,Protein kinase B,Endocannabinoid system,Cancer,Immunohistochemistry,Apoptosis,MTOR,Akt/PKB signaling pathway,Cell biology,Life sciences,Clinical medicine,Biology,Biochemistry,Biotechnology,Health sciences]
author: Bifulco M | Di Marzo V | Grimaldi C | Capasso A | Ramer R | Hinz B | Perez-Gomez E | Andradas C | Blasco-Benito S | Alenabi A
---


The results confirmed that CB2 expression was significantly reduced in BC tissues than paracancerous tissues (Figure 1B,C). Meanwhile, CB2 protein expression was lower in BC tissues (Figure 1D; Figure S1B). 3.3 CB2 agonists affected the proliferation and apoptosis of BC cells  We examined the effect of CB2 on breast cancer cells using the specific agonist JWH-015. JWH-015 inhibited BC cell proliferation, as shown by CCK-8 assay (Figure 3A; Figure S4C), and promoted apoptosis of BC cell lines (Figure 3B–E). Ki-67 IHC staining of tumor sections was used to evaluate their proliferative capacity, and Ki67 expression was significantly lower in CB2-overexpressing cells (Figure 5D,E).

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1002/cam4.6037){:target="_blank rel="noopener"}

