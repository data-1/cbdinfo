---
layout: post
title: "Metabolomic Analysis of Cannabinoid and Essential Oil Profiles in Different Hemp (Cannabis sativa L.) Phenotypes"
date: 2024-08-20
tags: [Cannabis,Cannabidiol,Gas chromatography,Terpene,Monoterpene,Cannabinoid]
author: Marjeta Eržen | Iztok J. Košir | Miha Ocvirk | Samo Kreft | Andreja Čerenak | Iztok J
---


Chemical Analysis of the Essential Oil of Hemp (Cannabis sativa L.)  Briefly, 11 phenotypes from “Carmagnola” selected (CS), “Tiborszallasi” (TS), and “Finola” selection (FS) contained 0.09–3.38 mL of essential oil per 100 g of air-dried flower (1.34 mL/100 g, on average). To our knowledge, this is the first research to study differences in cannabinoid and essential oil content in various phenotypes within different varieties of hemp. Leaf petioles had strong anthocyanin coloration, Table S1: Average essential oil (EO) content (mL/100 g of air-dried hemp) in the inflorescences, average composition (%) of essential oil, and groups (a, b, c, and d) from statistical analysis performed by analysis of variance (ANOVA) from different phenotypes of Carmagnola selected, Tiborszallasi, and Finola selection. Cannabis sativa. Metabolomic Analysis of Cannabinoid and Essential Oil Profiles in Different Hemp (Cannabis sativa L.) Phenotypes Plants 10, no.

[Visit Link](https://www.mdpi.com/2223-7747/10/5/966){:target="_blank rel="noopener"}

