---
layout: post
title: "Cannabinoid Quinones—A Review and Novel Observations"
date: 2024-08-20
tags: [HU-331,High-performance liquid chromatography,MTT assay,Reversed-phase chromatography,Cannabinoid,Neurodegenerative disease,Cannabidiol,Neuroprotection]
author: Natalya M. Kogan | Maximilian Peters | Raphael Mechoulam | Natalya M
---


This molecule was found to be active against some cancer cell lines, especially M14 melanoma. To investigate whether the position at which the rings are attached to each other is important for the anticancer activity of these compounds, the quinone of abnCBD (HU-1001) was synthesized by the same method as the quinone HU-331. A CBD derived quinone in which the position 2′ of the quinone ring is blocked by a methyl group, was prepared by oxidation of the known [51] 2′-methyl CBD to quinone HU-1010. However, the fact that this compound is able to kill cancer cells, though at much higher concentrations than HU-331, suggests that there is some additional mechanism of cancer cell death by cannabinoid quinones, not only topoisomerase II inhibition. The antioxidants, MPG and NAC, which are able to inhibit the anticancer activity of HU-331, bind directly to the molecule in this position, as shown by HPLC coupled to UV and MS methods and NMR, further proving the importance of position C2’.

[Visit Link](https://www.mdpi.com/1420-3049/26/6/1761){:target="_blank rel="noopener"}

