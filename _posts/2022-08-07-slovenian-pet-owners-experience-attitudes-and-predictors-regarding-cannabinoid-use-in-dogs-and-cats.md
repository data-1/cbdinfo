---
layout: post
title: "Slovenian Pet Owners' Experience, Attitudes, and Predictors Regarding Cannabinoid Use in Dogs and Cats"
date: 2022-08-07
categories:
- cannabinoid
- Cannabis
- CBD
author: Tomsič, Veterinary Faculty, University Of Ljubljana, Rakinić, Faculty Of Social Sciences, Seliškar, Mitsuaki Ohta, Alejandra Mondino, Roswitha Merle
tags: [Cannabidiol,Survey methodology,Dependent and independent variables,Medical cannabis,Alternative medicine,Pet,Likert scale,Research,Multilevel model,Prediction,Cannabinoid,Statistics]
---


The aim of this study was to assess the personal experience and attitudes of Slovenian pet owners regarding cannabinoid (CBD) use and to identify the predictors of the first use and reuse of CBDs in dogs and cats. The questionnaire consisted of six sections related to demographic data and personal experience with CBD use, information about the participant's animal, experience with CBD use in the participant's animal, reasons for not using CBDs in their animal, attitudes toward CBD use in dogs and cats, and postmodern health values. The overall experience of personal CBD use was positive, as reported in other studies (28–30), and our participants were very likely to use CBDs again in the future. The dog and cat owners in our survey used CBD products in their pets mainly as supportive therapy. The authors also thank the dog and cat owners who completed the questionnaire.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fvets.2021.796673/full){:target="_blank" rel="noopener"}


