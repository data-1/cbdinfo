---
layout: post
title: "New Insights in the Involvement of the Endocannabinoid System and Natural Cannabinoids in Nicotine Dependence"
date: 2022-08-27
categories:
- Cannabinoid
- Endocannabinoid
author: Saravia, Ten-Blanco, Pereda-Pérez, Berrendero, Rocio Saravia, Marc Ten-Blanco, Inmaculada Pereda-Pérez, Fernando Berrendero
tags: [Cannabinoid,Nicotine,Nicotinic acetylcholine receptor,Anandamide,Neurotransmitter,Endocannabinoid system,Relapse,Dopamine,Self-administration,Addiction,Chemical synapse,2-Arachidonoylglycerol,Tobacco smoking,Ventral tegmental area,Substance dependence,Synapse,Drug withdrawal,Neurophysiology,Neuroscience,Psychoactive drugs,Health,Neurochemistry]
---


Indeed, several studies have revealed the health risks of using these devices, as well as traditional cigarettes and any other tobacco product [ 26 29 ]. On the contrary, more recent studies have shown that activation of CB2R inhibits the rewarding effects of nicotine. Systemic administration of this compound dose-dependently inhibited nicotine self-administration in rats and mice. However, other study showed no changes in the somatic signs of withdrawal due to URB597 treatment [ 146 ]. Given the relevance of the endocannabinoid system in memory and learning [ 158 159 ], its involvement in the cognitive deficits associated with nicotine withdrawal has been investigated.

<hr>

[Visit Link](https://www.mdpi.com/1422-0067/22/24/13316/htm){:target="_blank" rel="noopener"}


