---
layout: post
title: "Cannabinoid signaling and risk in Huntington's disease"
date: 2024-08-25
tags: [Chemical synapse,Synapse,Cannabinoid receptor 1,Neurotransmitter,Inhibitory postsynaptic potential,Γ-Aminobutyric acid,GABA receptor,Excitatory postsynaptic potential,AMPA receptor,Glutamic acid,Striatum,Dendrite,Medium spiny neuron,Neuron,Branches of neuroscience,Neurophysiology,Neurochemistry,Physiology,Basic neuroscience research,Neuroscience,Biochemistry]
author: James R
---


Huntington's disease (HD) is a neurodegenerative disease with a progressive decline in motor and cognitive function. The dysfunction of eCB signaling observed in HD models is not constant across all terminals. In the model, we found that eCB signaling can function as a homeostatic controller of glutamatergic terminals. In the final steady state, eCB signaling had homeostatically controlled the release of glutamate such that few terminals released large excess glutamate, and most released small excess glutamate. Cannabinoids increase type 1 cannabinoid receptor expression in a cell culture model of striatal neurons: Implications for Huntington's disease.

[Visit Link](https://www.frontiersin.org/journals/computational-neuroscience/articles/10.3389/fncom.2022.903947/full){:target="_blank rel="noopener"}

