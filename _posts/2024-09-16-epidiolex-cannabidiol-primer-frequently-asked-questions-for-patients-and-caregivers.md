---
layout: post
title: "Epidiolex (Cannabidiol) Primer: Frequently Asked Questions for Patients and Caregivers"
date: 2024-09-16
tags: [Cannabidiol,Chemicals in medicine,Health care,Clinical medicine,Drugs,Medical treatments,Pharmacology,Medicine,Medical specialties,Pharmaceutical sciences,Diseases and disorders,Medicinal chemistry,Health]
author: Renad Abu‐Sawwa | Caitlin Stehling
institutions: Children's Healthcare of Atlanta | Augusta University | University of Georgia
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/6938286"
---


On June 25, 2018, the first plant-derived, purified pharmaceutical-grade cannabidiol (CBD) medication, Epidiolex (Greenwich Biosciences Inc, Carlsbad, CA), was approved in the United States by the US Food and Drug Administration (FDA). Epidiolex is for patients 2 years and older with Dravet syndrome (DS) or Lennox-Gastaut syndrome (LGS), and has revolutionized the treatment of medically refractory seizures in these patients.1–4 The road to widespread therapeutic use of CBD and its recent approval for seizure disorders has been a long one.1–4  Four pivotal randomized, double-blind, multicenter clinical trials evaluated the use of CBD in patients with DS and LGS with regard to the efficacy and safety in convulsive and drop seizure, respectively (GWPCARE 1–4).4 All trials demonstrated a significant absolute reduction in seizure frequency.1–4 The most common adverse drug effects (≥10%) depicted in the trials included somnolence, fatigue, rash, decreased appetite, diarrhea, insomnia, infection, and elevated transaminases.1–4