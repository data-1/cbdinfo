---
layout: post
title: "In situ visualization of opioid and cannabinoid drug effects using phosphosite-specific GPCR antibodies"
date: 2024-08-29
tags: [G protein-coupled receptor,Immunohistochemistry,Immunostaining,Receptor (biochemistry),G protein-coupled receptor kinase,Cannabinoid receptor 1,Protein phosphatase 1,Biology,Cell biology,Molecular biology,Life sciences,Biochemistry]
author: 
---


Key to this new protocol was the observation that receptor phosphorylation is highly unstable during routine immunohistochemistry and that for many GPCRs inclusion of protein phosphatase inhibitors may be an absolute requirement during perfusion, post-fixation and cryoprotection of tissues. Second, robust phospho-GPCR immunostaining was only apparent when appropriate protein phosphatase inhibitors were included in all steps of fixation and staining procedures. We observed that three out of four antibodies for MOP and one out of three antibodies for CB1 produced specific phospho-immunostaining in tissues suggesting that each phosphosite-specific GPCR antibody has to be validated individually for immunohistochemical applications. Antibodies  Primary antibodies  The phosphorylation state-specific rabbit MOP antibodies pT370-MOP (7TM0319B), pS375-MOP (7TM0319C), pT376-MOP (7TM0319D), pT379-MOP (7TM0319E), the phosphorylation state-specific rabbit CB1 antibody pS425-CB1 (7TM0056A) as well as the phosphorylation-independent rabbit antibody np-MOP (7TM0319N) and phosphorylation-independent guinea pig antibody np-MOP (7TM0319N-GP) were provided by 7TM Antibodies (www.7tmantibodies.com). A total of 35 mice (n = 4–6 per treatment condition) were used.

[Visit Link](https://www.nature.com/articles/s42003-023-04786-2){:target="_blank rel="noopener"}

