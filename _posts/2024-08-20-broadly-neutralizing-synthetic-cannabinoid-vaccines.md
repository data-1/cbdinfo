---
layout: post
title: "Broadly Neutralizing Synthetic Cannabinoid Vaccines"
date: 2024-08-20
tags: [ELISA,High-performance liquid chromatography,Hapten,Antibody,Chromatography,Thin-layer chromatography,Column chromatography]
author: Mingliang Lin | Jinny Claire Lee | Steven Blake | Beverly Ellis | Lisa M. Eubanks | Kim D. Janda
---


Moreover, the replacement of an alkyl chain to a phenyl containing linker on the hapten of vaccine 7 caused a threefold affinity enhancement compared to vaccine 6, which is an effect that has been observed in a previous study.32 The most promising results came from vaccines 3, 4, 8, and 9, which showed the best affinities at a low micromolar range with the broadest cross reactivity against 5–6 drugs (>10%). In general, the IC50 values measured by competitive ELISA tended to be inflated; thus, the corresponding Kd values for antisera generated from vaccines 8, 9, and 10 were expected to be in the nanomolar range.36 The antibody concentrations of these three vaccine groups were determined by interpolated titer ELISA (Figures S17).37 Antibody concentrations ranged from 1.5–2.5 mg/mL and were comparably higher than those reported from methamphetamine and nicotine vaccines (Figure S18).38,39  Promising hapten candidates were implemented in further studies to observe their abilities to bind with primary metabolites. Forty microliters of diluted sera was then added to the drug plates, resulting in 200 μM to 1 nM competitive range. The temperature was recorded at 15, 30, 60, 90, and 120 min after drug administration. Aerosol was generated for 5 s (65 μL) every 1 min in a 5 min inhalation period.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8395583/){:target="_blank rel="noopener"}

