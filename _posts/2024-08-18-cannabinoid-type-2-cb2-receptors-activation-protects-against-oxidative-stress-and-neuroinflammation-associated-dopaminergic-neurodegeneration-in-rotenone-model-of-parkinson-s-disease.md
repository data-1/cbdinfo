---
layout: post
title: "Cannabinoid Type 2 (CB2) Receptors Activation Protects against Oxidative Stress and Neuroinflammation Associated Dopaminergic Neurodegeneration in Rotenone Model of Parkinson's Disease"
date: 2024-08-18
tags: [NF-κB,Neurodegenerative disease,Reactive oxygen species,Cannabinoid receptor 2,Neuroinflammation,RELA,Tumor necrosis factor,Interleukin 6,Superoxide dismutase,Cannabinoid,Antioxidant,Microglia,Inflammation,Biology,Cell biology,Biochemistry]
author: M. Emdadul | Shreesh K
---


In the present study, we have investigated the neuroprotective effects of BCP in ROT induced rat model of PD, via CB2 receptor-dependent mechanism by using a selective CB2 receptor antagonist AM630 (Figure 1). Briefly, the activity of SOD was estimated by adding samples or standards (10 μl) to a 96-well plate. Interestingly, this protective effect of BCP was significantly (p < 0.05) reversed by the prior administration of CB2 antagonist AM630 to rats administered with ROT and BCP. FIGURE 5  CB2 Receptors Improve the Level of Proinflammatory Cytokines in the Midbrain  ROT administration created a significant (p < 0.05) increase in the level of proinflammatory cytokines such as IL-1β (Figure 6A), IL-6 (Figure 6B), and TNF-α (Figure 6C) as compared to the control group while treatment with BCP significantly (p < 0.05) attenuated the rise of these proinflammatory cytokines in ROT-injected rats (Figures 6A–C). Similar to NF-κB p65, the expression level of COX-2 and iNOS increased in ROT-challenged rats as compared to control rats (COX-2: 213.48 vs. 100% control; iNOS: 251.56 vs. 100% control).

[Visit Link](https://www.frontiersin.org/journals/neuroscience/articles/10.3389/fnins.2016.00321/full){:target="_blank rel="noopener"}

