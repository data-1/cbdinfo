---
layout: post
title: "Role of the CB2 Cannabinoid Receptor in the Regulation of Food Intake: A Systematic Review"
date: 2024-09-02
tags: [Endocannabinoid system,Anandamide,Cannabinoid receptor 2,2-Arachidonoylglycerol,Reward system,Cannabinoid receptor 1,Neurochemistry,Neurophysiology]
author: Luis Miguel Rodríguez-Serrano | María Elena Chávez-Hernández | Rodríguez-Serrano | Luis Miguel | Chávez-Hernández | María Elena
---


In summary, these studies identified a role for the CB2 receptor in food intake through depletion of the receptor and exposure to several diets, which resulted in increased CB2 receptor expression and food intake. The results show consistent evidence from animal studies that the CB2 receptor could play a role in the rewarding effects of palatable food through distinct neuronal mechanisms and that the intake of palatable food induces increased expression of CB2 in the NAC and VTA [71]. The Endocannabinoid System and the Brain. Role of the CB2 Cannabinoid Receptor in the Regulation of Food Intake: A Systematic Review. Role of the CB2 Cannabinoid Receptor in the Regulation of Food Intake: A Systematic Review.

[Visit Link](https://www.mdpi.com/1422-0067/24/24/17516){:target="_blank rel="noopener"}

