---
layout: post
title: "Introduction to the special issue: the two sides of hemp: medical and industrial"
date: 2024-09-03
tags: [Cannabidiol,Medical cannabis,Cannabis,Δ-8-Tetrahydrocannabinol,Hemp,Cannabinoid,Tetrahydrocannabinol,Biology]
author: 
---


Clear and consistent regulations are essential to support the growth of the hemp industry while ensuring consumer safety and compliance with the law. Studies conducted in the early 1940’s identified that Minnesota wild hemp served a critical role not only as a crucial fiber plant during World War II but also as a key botanical source for cannabinoid chemistry discoveries. Mazza (2021) assessed the efficacy and adverse events of short- and long-term medical cannabis treatment for fibromyalgia syndrome, in patients resistant to conventional therapy, which were treated with medical cannabis of various THC and CBD contents. The results suggest medical cannabis as an alternative treatment for patients unresponsive to conventional therapy. Topical CBD administration was well tolerated by this population and resulted in only minor adverse effects, and there was a significant improvement in pain levels and pain-related disability.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-024-00237-9){:target="_blank rel="noopener"}

