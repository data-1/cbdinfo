---
layout: post
title: "The phytochemical diversity of commercial Cannabis in the United States"
date: 2022-08-13
categories:
- Cannabis
author: Christiana J. Smith, Independent Researcher, Seattle, Washington, United States Of America, Daniela Vergara, Department Of Ecology, Evolutionary Biology, University Of Colorado Boulder, Boulder
tags: []
---


The legal Cannabis industry heavily markets products to consumers based on widely used labeling systems purported to predict the effects of different “strains.” We analyzed the cannabinoid and terpene content of commercial Cannabis samples across six US states, finding distinct chemical phenotypes (chemotypes) which are reliably present. To visualize how patterns of terpene profile variation map to the major THC:CBD chemotypes shown in Fig 1 , we plotted PCA scores for all samples along the first three principal components, with each sample color-coded by its THC:CBD chemotype ( Fig 5B–5D ). To further understand any systematic patterns of variation in cannabinoid profiles beyond THC and CBD levels, we performed Principal Component Analysis (PCA) on all samples that contained measurements for total THC, CBD, CBG, CBC, CBN, and THCV content. Samples across these clusters display similar total THC distributions, while Cluster III is associated with modestly higher CBG levels (Fig 7). If it is true that different chemotypes of THC-dominant Cannabis reliably produce distinct psychoactive or medicinal effects, then a sensible starting point is to design studies comparing the effects of common, distinctive commercial chemotypes, such as those described by our cluster analysis (Figs 6 and 7).

<hr>

[Visit Link](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0267498#sec009){:target="_blank" rel="noopener"}


