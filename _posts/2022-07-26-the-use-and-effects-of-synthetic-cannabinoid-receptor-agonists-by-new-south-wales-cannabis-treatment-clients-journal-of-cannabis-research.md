---
layout: post
title: "The use and effects of synthetic cannabinoid receptor agonists by New South Wales cannabis treatment clients - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabis
author: Jackson, Melissa A., Drug, Alcohol Clinical Services, Hunter New England Local Health District, Newcastle, Brown, Amanda L., Johnston, University Centre For Rural Health
tags: [Cannabis (drug),Substance abuse,Psychoactive drug,Tetrahydrocannabinol,Effects of cannabis,Drug,Synthetic cannabinoids,Cannabinoid,Mental disorder,Substance dependence,Anxiety,Cannabis use disorder,Psychosis,Health,Addiction,Psychoactive drugs,Clinical medicine,Health sciences]
---


The SCRA + cannabis group were largely male, although somewhat older [Mdn (IQR) = 32(17)] than those described in earlier research (Winstock and Barratt 2013b; Barratt et al. 2013). Here, those suffering fatal harm (n = 55) were male (91%) and older age [M (SD) = 37(12)] with over two-thirds having a documented history of cannabis use. Reasons to continue SCRA use centred on the strength of their psychoactive effects: this response has not been highlighted in SCRA literature but does support a descriptor of NPS consumers as an innovative group of people who use substances and actively seek altered states of consciousness (Sutherland et al. 2016). It is possible that the current sample, who were already experiencing problems with substance use, would be more likely to seek treatment than those who use substances and respond to internet surveys. Several other factors could also be accountable, such as age differences between survey and study respondents [Mdn (IQR) = 23(9) vs 32(17) respectively] or time differences between data collections (2011 vs 2015 respectively), and changes in SCRA diversity in that time.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00091-z){:target="_blank" rel="noopener"}


