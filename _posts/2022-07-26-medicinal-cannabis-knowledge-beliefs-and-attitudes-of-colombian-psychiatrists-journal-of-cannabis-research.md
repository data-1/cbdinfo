---
layout: post
title: "Medicinal cannabis: knowledge, beliefs, and attitudes of Colombian psychiatrists - Journal of Cannabis Research"
date: 2022-07-26
categories:
- cannabis
author: Orjuela-Rojas, Juan Manuel, Pontificia Universidad Javeriana, Bogotá, García Orjuela, Universidad Nacional De Colombia, Ocampo Serna, Juan Manuel Orjuela-Rojas, Sabina Ocampo Serna, Xiomara García Orjuela
tags: [Cannabidiol,Psychiatry,Medical cannabis,Mental disorder,Tetrahydrocannabinol,Psychosis,Major depressive disorder,Health care,Medicine,Clinical medicine,Health,Health sciences,Diseases and disorders,Medical specialties,Causes of death]
---


The survey showed that the majority of the sample of Colombian psychiatrists believe that MC should be available for different medical conditions and expressed a desire to be able to prescribe it. This situation opens the doors for optimizing education and training in MC in mental health workers. In different countries of the world, including Colombia, there are many myths and misleading advertising of MC that generates false expectations in patients, for example, that it cures cancer (Shi et al., 2019). An interesting finding of the survey was that the majority of the psychiatrists agreed with the use of MC for nonpsychiatric medical conditions, with cancer-related chronic pain receiving the highest approval with 87.6%, a figure much higher than the highest approval rate for a psychiatric symptom, which was for insomnia (35.2%). There were several limitations of this study.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00083-z){:target="_blank" rel="noopener"}


