---
layout: post
title: "The cannabinoid receptors system in horses: Tissue distribution and cellular identification in skin"
date: 2024-08-25
tags: [Dermis,Cannabinoid,Skin,Epidermis,Gene expression,Anandamide,Endocannabinoid system,Cannabinoid receptor,Epithelium,Palmitoylethanolamide,TRPV1,Axon,Hair follicle,Cell biology,Biochemistry,Biology]
author: Ryberg E | Larsson N | Sjogren S | Stasiulewicz A | Znajdek K | Grudzien M | Pawinski T | Sulkowska AJI | Blazquez C | Carracedo A
---


In all skin compartments, CBRs immunoreactivity was observed in the cytoplasm, whereas signals were rarely observed in the nuclei of single cells in the whole skin tissue (Figures 2 and 3). 3.5 In-vitro expression analysis of CBRs in primary skin-derived keratinocyte and dermal-derived cell cultures  Confocal microscopy indicated that epidermal keratinocytes and dermal cells, mainly fibroblasts, are the most common cells to express CBRs (Figures 2 and 3), whereas WB analysis additionally confirmed that CBRs occurred in the whole skin tissue samples (Figure 4). For CBR1, we observed 2 immunoreactive bands, 55 kDa, visible in keratinocytes and fibroblasts, as well as the less visible 50 kDa band in keratinocytes, completely absent in fibroblasts. Keratinocytes showed high Pan-CK expression, but its detectable levels also were observed in single primary skin dermal-derived cells, suggesting that the culture conditions used are appropriate for other cells of epithelial origin, such as hair follicle cells. Gene Biological Material Kruskal-Wallis ANOVA Brain Skin Keratinocytes Fibroblasts Relative gene expression (2−ΔCT ± SD) Cnr1 1.96E−02 ± 1.53E−03 1.14E−04 ± 4.17E−05 1.09E−04 ± 1.06E−05 5.40E−05 ± 3.06E−05 P = .007* Cnr2 1.16E−02 ± 3.11E−03 5.75E−04 ± 1.96E−04 3.25E−04 ± 1.16E−04 1.52E−04 ± 1.40E−04 P = .0005*  Although the transcript levels of Cnr1 and Cnr2 in skin, fibroblasts, and keratinocytes were low, they were still detectable, confirming mRNA presence of the investigated genes in those samples.

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1111/jvim.16467){:target="_blank rel="noopener"}

