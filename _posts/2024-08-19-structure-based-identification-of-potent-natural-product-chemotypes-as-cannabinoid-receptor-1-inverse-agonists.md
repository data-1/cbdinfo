---
layout: post
title: "Structure-Based Identification of Potent Natural Product Chemotypes as Cannabinoid Receptor 1 Inverse Agonists"
date: 2024-08-19
tags: [Cannabinoid receptor 2,Ligand (biochemistry),Docking (molecular),Cannabinoid receptor 1,Biochemistry]
author: Pankaj Pandey | Kuldeep K. Roy | Haining Liu | Guoyi Ma | Sara Pettaway | Walid F. Alsharif | Rama S. Gadepalli | John M. Rimoldi | Christopher R. McCurdy | Stephen J. Cutler
---


Further binding analysis for the three compounds 7, 15, and 17, exhibiting ≥50% displacement at the CB1 receptor when tested at 10 μM, revealed that the binding curves of the compounds plateaued well before reaching 100% receptor occupancy, indicating no true affinity for the CB1 receptor (Figure S2). Radioligand Receptor Binding Studies  Competitive binding assays were performed with a modified rapid filtration assay described by Ma et al. (2007) [63] and Felder et al. (1992) [64]. Supplementary Materials  The following are available online, Figure S1: Chemical structures of all 18 selected hits used for in vitro study; Figure S2: Binding curves for compounds 7, 15, and 17 obtained from the radioligand competitive binding assay; Table S1: Percentage displacement of radioligand at CB1 and CB2 receptors for all 18 screened compounds; Table S2: Physicochemical and other selected properties related to drug-likeness for all 18 tested hits. Molecules 2018, 23, 2630. https://doi.org/10.3390/molecules23102630  AMA Style  Pandey P, Roy KK, Liu H, Ma G, Pettaway S, Alsharif WF, Gadepalli RS, Rimoldi JM, McCurdy CR, Cutler SJ, et al. Structure-Based Identification of Potent Natural Product Chemotypes as Cannabinoid Receptor 1 Inverse Agonists. 2018; 23(10):2630. https://doi.org/10.3390/molecules23102630  Chicago/Turabian Style  Pandey, Pankaj, Kuldeep K. Roy, Haining Liu, Guoyi Ma, Sara Pettaway, Walid F. Alsharif, Rama S. Gadepalli, John M. Rimoldi, Christopher R. McCurdy, Stephen J. Cutler, and et al. 2018.

[Visit Link](https://www.mdpi.com/1420-3049/23/10/2630){:target="_blank rel="noopener"}

