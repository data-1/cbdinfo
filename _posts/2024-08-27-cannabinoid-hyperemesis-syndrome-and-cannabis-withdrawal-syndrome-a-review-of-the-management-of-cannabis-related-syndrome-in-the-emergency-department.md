---
layout: post
title: "Cannabinoid hyperemesis syndrome and cannabis withdrawal syndrome: a review of the management of cannabis-related syndrome in the emergency department"
date: 2024-08-27
tags: [Cannabinoid hyperemesis syndrome,Nausea,Cannabinoid,Cannabis (drug),Hyperemesis gravidarum,Diseases and disorders,Health,Medicine,Clinical medicine,Medical specialties]
author: Aristomenis K | Vincent Della | Eric P
---


Results  Although often presenting with similar symptoms such as abdominal pain and vomiting, cannabinoid hyperemesis syndrome (CHS) and cannabis withdrawal syndrome (CWS) are the result of two differing pathophysiological processes. In terms of symptoms, the combination of abdominal pain and vomiting occurs more frequently in CHS than CWS (85.1% versus 8.3%), though this is unspecific. Hot showers, for reasons described above, exclude the diagnosis of CWS, being present in 92.3% of patients with CHS (versus 0% in CWS) [14, 15]. In addition to its antiemetic properties, this pharmaceutical class has the added advantage of reducing agitation, which may also be present in CHS patients presenting to acute care. If present, abdominal pain should be addressed with capsaicin.

[Visit Link](https://intjem.biomedcentral.com/articles/10.1186/s12245-022-00446-0){:target="_blank rel="noopener"}

