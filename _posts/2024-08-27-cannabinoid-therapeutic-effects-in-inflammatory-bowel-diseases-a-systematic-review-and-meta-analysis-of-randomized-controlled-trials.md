---
layout: post
title: "Cannabinoid Therapeutic Effects in Inflammatory Bowel Diseases: A Systematic Review and Meta-Analysis of Randomized Controlled Trials"
date: 2024-08-27
tags: [Inflammatory bowel disease,Meta-analysis,Randomized controlled trial,Clinical medicine,Medicine,Health]
author: Antonio Vinci | Fabio Ingravalle | Dorian Bardhi | Nicola Cesaro | Sara Frassino | Francesca Licata | Marco Valvano
---


Objectives  The primary objective of the present study is to understand if cannabinoid supplementation to standard therapy is favorable, in terms of clinical outcome, in IBD patients, investigating if any difference is found between CD and UC patients. Study Characteristics  Five retrieved studies were selected for synthesis in meta-analysis. [Google Scholar] [CrossRef]  Harbord, R.M. [Google Scholar] [CrossRef]  Sterne, J.A.C. Cannabinoid Therapeutic Effects in Inflammatory Bowel Diseases: A Systematic Review and Meta-Analysis of Randomized Controlled Trials Biomedicines 10, no.

[Visit Link](https://www.mdpi.com/2227-9059/10/10/2439){:target="_blank rel="noopener"}

