---
layout: post
title: "Production of Feminized Seeds of High CBD Cannabis sativa L. by Manipulation of Sex Expression and Its Application to Breeding"
date: 2022-09-05
categories:
- Cannabis
author: Flajšman, Department Of Agronomy, Biotechnical Faculty, University Of Ljubljana, Slapnik, Murovec, Donald L. Smith, Zamir Punja, Jordi Petit Pedró, Ayelign M. Adal
tags: [Cannabis,Cannabis sativa,Flower,Cannabidiol,Plant hormone,Medical cannabis,Plant,Botany,Plants,Biology]
---


Treatments of the first experiment of male flowers induction on female plants of Cannabis sativa. Number of male flowers per plant and number of germinated pollen cells in vitro after the induction of male flowering on female plants of MX-CBD-707. In our experiment, 23 different treatments (chemical using STS or colloidal silver; hormonal using GA; and physiological by intensive cutting) were used for induction of male flowers on female plants, in order to evaluate their influence on sex expression in medical cannabis. The hormonal control of sex differentiation in dioecious plants of hemp (Cannabis sativa). Induction of female flowers on male plants of Cannabis sativa L. by 2-chlorethanephosphoric acid.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fpls.2021.718092/full){:target="_blank" rel="noopener"}


