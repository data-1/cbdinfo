---
layout: post
title: "Trends of emergency department visits for cannabinoid hyperemesis syndrome in Nevada: An interrupted time series analysis"
date: 2024-09-02
tags: [Cannabis (drug),Cannabinoid hyperemesis syndrome,Psychoactive drugs,Health care,Health,Clinical medicine,Medicine]
author: Jaeseung Soh | Yonsu Kim | Jay Shen | Mingon Kang | Stefan Chaudhry | Tae Ha Chung | Seo Hyun Kim | Yena Hwang | Daniel Lim | Adam Khattak
---


The State of Nevada legalized cannabis for medical use and recreational use in 2001 and 2016, respectively. Results  Trends of CHS ED visits in study population  presents trends of observed and projected rates of CHS ED visits in Nevada, showing an increase in CHS ED visits after commercialization for recreational use in 2017. Table 1  TreatmentParameterEstimateS.E.p-value2017 Q3Intercept-9.43583.68610.015Quarter0.00050.00010.008Treatment-10.53975.42840.061Quarter*Treatment0.00040.00030.0642017 Q4Intercept-11.47973.28720.001Quarter0.00060.00020.001Treatment-12.49295.32720.025Quarter*Treatment0.00060.00030.0292018 Q1Intercept-11.09273.06260.001Quarter0.00060.00020.001Treatment-14.76345.59050.013Quarter*Treatment0.00070.00030.014  Comparison between those with CHS and those without CHS among patients visiting ED for cannabis-related disorders  We compared characteristics of CHS patients with those of non-CHS patients visiting ED during the study period ( ). Our study might have underestimated the prevalence of CHS ED visits because it could not consider synthetic cannabinoid use. Time since first cannabis use and 12-month prevalence of cannabis use disorder among youth and emerging adults in the United States.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11135771/){:target="_blank rel="noopener"}

