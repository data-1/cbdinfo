---
layout: post
title: "Plant-Based Cannabinoids for the Treatment of Chronic Neuropathic Pain"
date: 2024-05-13
categories:
- 
author: Casey, Sherelle L., Vaughan, Christopher W., Sherelle L. Casey, Christopher W. Vaughan
tags: [Cannabinoid,Cannabidiol,Tetrahydrocannabinol,Analgesic,Nabiximols,Pain,Pain management,Cannabis,Neuropathic pain,Clinical medicine,Health,Medicine,Drugs,Health care]
---


It is a severe abnormal pain syndrome, generally characterized by spontaneous pain (pain in the absence of any stimulus). The findings of a 2005 observational study suggest that cannabis is useful for the treatment of neuropathic pain and is better tolerated by patients when compared to prescription medication [ 8 ]. These findings are supported by a study evaluating efficacy of smoked cannabis for neuropathic pain, with 3.5% and 7% THC cigarettes providing analgesia superior to placebo [ 18 ]. Of note is the finding in many of these studies that low-dose THC is equally as effective as medium and high doses, with reduced frequency and intensity of side effects in the low dose groups, suggesting that it is possible to provide analgesia without adverse side effects. 26, Given that nabiximols (oromucosal sprays containing both THC and cannabidiol) have received regulatory approval in many countries, the safety and convenience of the oromucosal administration route, and the evidence for modulation of adverse THC side effects by cannabidiol, there is a body of clinical research evaluating THC and cannabidiol in combination.

[Visit Link](https://www.mdpi.com/2305-6320/5/3/67){:target="_blank" rel="noopener"}


