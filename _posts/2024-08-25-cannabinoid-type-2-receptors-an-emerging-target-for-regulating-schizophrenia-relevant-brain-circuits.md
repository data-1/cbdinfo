---
layout: post
title: "Cannabinoid type-2 receptors: An emerging target for regulating schizophrenia-relevant brain circuits"
date: 2024-08-25
tags: [Cannabinoid receptor 2,Cannabinoid,Schizophrenia,Dopamine,Hippocampus,Endocannabinoid system,Psychosis,Ventral tegmental area,Excitatory postsynaptic potential,Dopaminergic pathways,Cannabinoid receptor 1,Microglia,Memory,Allosteric modulator,Long-term potentiation,Pyramidal cell,Receptor antagonist,Receptor (biochemistry),Inhibitory postsynaptic potential,Allosteric regulation,Brain,Striatum,Pharmacology,Cannabinoid receptor,Neuron,Cell signaling,Neurophysiology,Neurochemistry,Neuroscience,Basic neuroscience research,Biochemistry]
author: Anthony S | Daniel J
---


The complex pharmacology of these compounds has made it challenging to elucidate the mechanisms through which delta-9-THC, CBD, and other phytocannabinoids mediate changes in neurotransmission and behavior. As discussed below, recently developed tools are allowing detailed studies into the role CB2 receptors play in regulating circuits that are hyperactive in schizophrenia including the dopaminergic system, hippocampus, and prefrontal cortex. This is especially interesting in light of recent reports demonstrating that CB2 receptors are expressed in key brain circuits related to schizophrenia including dopaminergic neurons and the hippocampus. Collectively, these studies provide solid evidence that CB2 is functionally expressed in dopamine neurons where the net effect of CB2 receptor activation on these neurons is a reduction in excitability (Figure 1). While more studies are needed to determine if CB2 receptors can influence schizophrenia-related inflammation, the studies above suggest that CB2 receptor activation could potentially have important effects through modulation of microglia function, providing an additional mechanism of action through which CB2 receptors could alter schizophrenia-related pathological changes.

[Visit Link](https://www.frontiersin.org/journals/neuroscience/articles/10.3389/fnins.2022.925792/full){:target="_blank rel="noopener"}

