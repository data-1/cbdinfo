---
layout: post
title: "Modulation of type 1 cannabinoid receptor activity by cannabinoid by-products from Cannabis sativa and non-cannabis phytomolecules"
date: 2024-08-25
tags: [Cannabinoid,Cannabinoid receptor 1,Ligand (biochemistry),Receptor antagonist,Cannabinol,Cannabidiol,Biochemistry,Neurochemistry]
author: Robert B
---


TABLE 1  Results  Radioligand binding  When tested alone, the cannabinoids (∆8-THC, ∆6a,10a-THC, 11-OH-∆9-THC, and CBN) reduced [3H]CP55,940 binding to hCB1R to some extent (Figure 2A). FIGURE 3  Among the other phytomolecules tested, only PEA produced weak partial agonist inhibition of cAMP accumulation, having potency and efficacy significantly lower than either CP55,940 or ∆9-THC (Figure 3C; Table 1). Similar to its activity in the cAMP inhibition assay, PEA slightly increased βarrestin2 recruitment in the presence of 100 nM ∆9-THC, consistent with the notion that PEA may be an allosteric agonist of hCB1R (Figure 4D; Table 1). ∆8-THC produced an anti-nociceptive effect that was not different from ∆9-THC, but CBN and ∆6a,10a-THC did not produce such effect (Figure 6C). Therefore, the observed in vivo effects of PEA here may be due to PEA acting on other ligand targets not present in our in vitro model.

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2022.956030/full){:target="_blank rel="noopener"}

