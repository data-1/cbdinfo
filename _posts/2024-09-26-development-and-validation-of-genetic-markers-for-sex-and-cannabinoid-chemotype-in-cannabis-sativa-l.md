---
layout: post
title: "Development and validation of genetic markers for sex and cannabinoid chemotype in Cannabis sativa L."
date: 2024-09-26
tags: [Cannabis,Cannabidiol,Cannabinoid,Zygosity,Tetrahydrocannabinolic acid synthase,Tetrahydrocannabinolic acid,Tetrahydrocannabinol,Biology,Genetics,Cannabaceae]
author: J. Tóth | George M. Stack | Ali R. Cala | Craig H. Carlson | Rebecca L. Wilk | Jamie Crawford | D. R. Viands | Glenn Philippe | Christine D. Smart | Jocelyn K. C. Rose | Lawrence B. Smart
pubdate: "2020-01-11"
institutions: Cornell University
doi: "https://doi.org/10.1111/gcbb.12667"
---


Cannabis sativa is usually dioecious, having male and female flowers produced on separate plants. Some Cannabis plants are monoecious, producing both male and female flowers on the same plant (Menzel, 1964). About 270 plants genetically scored as male from four hemp cultivars were allowed to flower in greenhouse conditions, and all were phenotypically male (Table 2). 3.4 Cannabinoid chemotype assay validation  Two hundred and seventeen plants from 14 hemp cultivars grown for CBD in two locations were tested with the cannabinoid chemotype (CCP-1) assay and phenotyped for cannabinoids using HPLC. Some studies have found sequence and copy number variation in CBDAS and THCAS and correlated them to differences in cannabinoid production (McKernan et al., 2015; Weiblen et al., 2015).