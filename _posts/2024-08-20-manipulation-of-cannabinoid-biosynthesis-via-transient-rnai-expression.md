---
layout: post
title: "Manipulation of Cannabinoid Biosynthesis via Transient RNAi Expression"
date: 2024-08-20
tags: [RNA interference,Small interfering RNA,Gene silencing,Real-time polymerase chain reaction,RNA-induced silencing complex,DNA sequencing,Cannabinoid,Gene knockdown,Cannabis,Cannabinol,Gene,Genetics,Nucleic acids,Molecular genetics,Branches of genetics,Biotechnology,Molecular biology,Biology,Health sciences,Biochemistry,Life sciences]
author: Matchett-Oates | German C | Noel O. I
---


Transient expression systems are widely used as a valuable tool for vector construct evaluation, all the while being fast and inexpensive with specific protocols in cannabis already developed (Schachtsiek et al., 2019; Deguchi et al., 2020) exploring dsRNA and virus-induced gene silencing mechanisms, with significantly downregulated targeted gene expression levels observed. The high levels of sequence similarity of the CBDAS homologs (Table 2) at the DNA level, and regardless of the size of the PCR insert for siRNA generation, sequence homology is too significant to identify one best-fit homolog for vector design, and thus, a single homolog of CBDAS was chosen, identified as CBDAS-like#1 within the Cannbio-2 genome (Supplementary Data), for pRNAi-GG-CBDAS vector construction. siRNA predicted from the amplified THCAS sequence were more effective in downregulating the CBDAS transcripts, comparatively, to THCAS and CBCAS, which are more highly sequence homologous (>96%) than CBDAS is to THCAS (92%). Within the genomic sequences and alignment of these two genes and their high level of sequence similarity, it could be expected that the siRNA generated would not contain greater affinity for THCAS, but instead downregulate CBCAS further due to increased target sites. The 95 predicted siRNAs had a total of 329 exact matches between the CBCAS homologs and THCAS and only 94 matches within the CBDAS homologs resulting increase of 13% in transcript levels of THCAS and 76% increase of CBCAS transcript levels and a decrease of 39% in CBDAS (Figure 3D).

[Visit Link](https://www.frontiersin.org/journals/plant-science/articles/10.3389/fpls.2021.773474/full){:target="_blank rel="noopener"}

