---
layout: post
title: "Relationship between cannabis use frequency and major depressive disorder in adolescents: findings from the National Survey on Drug Use and Health 2012–2017"
date: 2024-09-30
tags: [Major depressive disorder,Mental disorder,Adolescence,Substance abuse,Psychology,Health,Mental health,Mental disorders,Behavioural sciences,Clinical medicine,Abnormal psychology,Causes of death,Human diseases and disorders,Diseases and disorders,Epidemiology]
author: Natalie Gukasyan | Eric C. Strain
pubdate: "2020-01-16"
institutions: Johns Hopkins Medicine | Johns Hopkins University
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7039755"
---


Compared to never users, adolescents with any history of cannabis use were older (15.8 ± 0.01 vs. 14.3 ± 0.01 years), were less likely to be female (47.6% vs. 49.2%), and had a higher prevalence of any past year alcohol use (73.5 vs. 14.3%) or use of illicit substances other than cannabis (29.0 vs. 4.4%). Given that adolescents who have used any cannabis are also more likely to have depression, one would expect that this would then demonstrate some form of a dose relationship – that is, that there would be increased prevalence or severity of depression associated with more cannabis use. The results could be interpreted to show either: 1) that there is a spurious relationship between the frequency of cannabis use and depression (as well as severity of depression) for adolescents that have used cannabis; 2) that the limitations of NSDUH mean we are unable to determine the relationship between cannabis use and depression in adolescents; or, 3) the relationship between cannabis use and depression in adolescents is complicated and more nuanced than a simple positive linear relationship between cannabis use and depression. Consistent with other studies, our analyses identified use of alcohol and illicit drugs other than cannabis as significant risk factors for all depression-related outcomes. While the present findings raise further questions about the relationship between severity of cannabis use and severity of depression among adolescents with cannabis use, clinicians should be aware that even remote cannabis use in an adolescent suggests a higher likelihood of depression.