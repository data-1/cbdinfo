---
layout: post
title: "Pharmacological Evaluation of Cannabinoid Receptor Modulators Using GRABeCB2.0 Sensor"
date: 2024-09-02
tags: [Receptor antagonist,Cannabinoid receptor 1,Endocannabinoid system,Ligand (biochemistry),IC50,Cannabinoid receptor 2,Cannabinoid,EC50,Allosteric regulation,Biochemistry,Neurochemistry]
author: Samay Shivshankar | Josephine Nimely | Henry Puhl | Malliga R. Iyer | Malliga R
---


Effect of 3-arm antagonists in modulating GRABeCB2.0 fluorescence in presence of 300 nM CP55940. We also tested the antagonists to inhibit agonist fluorescence in the presence of CP55940 (300 nM). For reported allosteric modulators, the binding assay was modified to test the ligand for basal activity, followed by the addition of the agonist, similar to the antagonist assay. [Google Scholar] [CrossRef] [PubMed]  Rajesh, M.; Bátkai, S.; Kechrid, M.; Mukhopadhyay, P.; Lee, W.-S.; Horváth, B.; Holovac, E.; Cinar, R.; Liaudet, L.; Mackie, K.; et al. Cannabinoid 1 receptor promotes cardiac dysfunction, oxidative stress, inflammation, and fibrosis in diabetic cardiomyopathy. [Google Scholar] [CrossRef] [PubMed]  Nguyen, T.; Thomas, B.F.; Zhang, Y. Overcoming the Psychiatric Side Effects of the Cannabinoid CB1 Receptor Antagonists: Current Approaches for Therapeutics Development.

[Visit Link](https://www.mdpi.com/1422-0067/25/9/5012){:target="_blank rel="noopener"}

