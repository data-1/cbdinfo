---
layout: post
title: "Season‐long characterization of high‐cannabinoid hemp (Cannabis sativa L.) reveals variation in cannabinoid accumulation, flowering time, and disease resistance"
date: 2024-08-20
tags: [Cannabis,Cannabinoid,Cannabidiol,Tetrahydrocannabinolic acid]
author: de Meijer | E. P. M | K. M | C. J | J. P | S. G | J. A | M. A | M. R. G | X.-Y
---


To replicate and extend work in the existing literature, the objectives of the replicated field trials involving 30 high-CBD cultivars in this experiment were to:  Assess variation in cultivar height and growth rate throughout the growing season;  Evaluate genetic segregation for chemotype in cultivars;  Quantify variation in flowering time within and between cultivars;  Model cannabinoid accumulation by cultivar using a time series of shoot tip samples;  Rate cultivars for susceptibility to powdery mildew; and  Quantify yield components by cultivar including biomass yield and cannabinoid content in stripped biomass. Value Formula Growth rate (Weekn height − Weekn−1 height)/# days between measurements Total cannabinoid % CBDA % + CBD % + THCA % + Δ9-THC % + CBGA % + CBG % + CBCA % + CBC % + THCVA % + THCV % + CBDVA % + CBDV % + CBN% Total CBD % CBD % + (CBDA %*0.877) Total THC % Δ9-THC % + (THCA %*0.877) Total CBG % CBG % + (CBGA %*0.878) Total CBC % CBC % + (CBCA %*0.877) Total THCV % THCV % + (THCVA %*0.867) Total CBDV % CBDV % + (CBDVA %*0.867) % Dry matter total dry biomass/total wet biomass*100% % Floral tissue dry floral biomass/total dry biomass*100% CBD yield per plant dry floral biomass*% CBD in biomass subsample Biomass:Shoot ratio % CBD in biomass sample/% CBD in last shoot sample CBD:THC ratio Total CBD %/(Total THC % + 1.013*CBN %)  2.4 Flowering surveys  All plants were rated weekly for evidence of flowering. Group 1 2 3 4 5  3.2 Chemotype and cannabinoid ratios  Several of the cultivars in the trial were segregating at the B locus and produced the expected CBD:THC ratios (Table 5; Table S2). For some cultivars, disease severity by cultivar varied by site: 'NY Cherry' had no signs of powdery mildew in the Ithaca trial, but nearly 20% mean leaf area with powdery mildew in the Geneva trial. Cultivar, but not site, had a significant effect on the CBD:THC ratio.

[Visit Link](https://onlinelibrary.wiley.com/doi/10.1111/gcbb.12793){:target="_blank rel="noopener"}

