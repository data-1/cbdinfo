---
layout: post
title: "Cannabis sativa terpenes are cannabimimetic and selectively enhance cannabinoid activity"
date: 2024-08-20
tags: [Cannabinoid receptor 1,Cannabinoid,Cannabinoid receptor 2,Caryophyllene,Cell signaling,Analysis of variance,Receptor (biochemistry),Agonist,Receptor antagonist,Ligand binding assay,Lipid raft,Western blot,In vitro,Allosteric regulation,G protein-coupled receptor,Cell biology,Neurochemistry,Biochemistry]
author: Justin E | John M
---


3A), and had no effect on the other terpenes (Fig. 3A) while having no effect on the other terpenes (Fig. Overall, our mechanistic studies suggest that the terpenes tested generally although selectively increase the behavioral effects of the cannabinoid WIN55,212-2, supporting the potential modulation of cannabinoids by terpenes. Terpenes activate the CB1 in vitro  Our behavioral results suggested that the terpenes potentially interact with the CB1 receptor, and likely others. In antagonist experiments, cells were pretreated for 5 min before agonist treatment for 5 min.

[Visit Link](https://www.nature.com/articles/s41598-021-87740-8){:target="_blank rel="noopener"}

