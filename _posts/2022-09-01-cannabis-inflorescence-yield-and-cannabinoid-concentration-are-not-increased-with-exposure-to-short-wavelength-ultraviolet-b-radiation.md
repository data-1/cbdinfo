---
layout: post
title: "Cannabis Inflorescence Yield and Cannabinoid Concentration Are Not Increased With Exposure to Short-Wavelength Ultraviolet-B Radiation"
date: 2022-09-01
categories:
- Cannabis
author: Rodriguez-Morrison, School Of Environmental Sciences, University Of Guelph, Llewellyn, Zheng, Donald L. Smith, Juan L. Valenzuela, Gianpaolo Grassi
tags: [Ultraviolet,Cannabis sativa,Leaf,Radiation exposure,Plant,Tetrahydrocannabinol,Electromagnetic spectrum,Photosynthetically active radiation,Ultraviolet index,Light-emitting diode,Radiation,Cannabis,Cannabidiol]
---


The effects of UV-PFD (μmol⋅m–2⋅s–1) applied during the flowering stage on aboveground tissue moisture content (%), stem dry weight (DW; g⋅m–2), and cannabinoid and terpene concentrations [mg⋅g–1 (DW) ] in the mature, air-dried apical inflorescence tissues of Cannabis sativa ‘Low Tide’ and ‘Breaking Wave.’  Responses of Inflorescence Yield, Apparent Quality, Cannabinoid and Terpene Concentrations to UV  The most discernable UV exposure effects on inflorescences were differences in the size of the apical inflorescences (Figure 10). Plant. Plant. Cannabis Cannabinoids 1, 19–27. Plant.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fpls.2021.725078/full){:target="_blank" rel="noopener"}


