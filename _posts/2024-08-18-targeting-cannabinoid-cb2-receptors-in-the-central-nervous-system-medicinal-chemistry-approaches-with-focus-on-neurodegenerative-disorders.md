---
layout: post
title: "Targeting Cannabinoid CB2 Receptors in the Central Nervous System. Medicinal Chemistry Approaches with Focus on Neurodegenerative Disorders"
date: 2024-08-18
tags: [Cannabinoid receptor 2,Cannabinoid receptor 1,Microglia,Cannabinoid,G protein-coupled receptor,Neurodegenerative disease,Receptor (biochemistry),Positron emission tomography,Cannabinoid receptor,Allosteric regulation,Cell biology,Biochemistry,Neurochemistry]
author: Rodríguez-Cueto | Fernández-Ruiz
---


Therefore, the potential of CB1Rs as targets for diseases of the CNS, and also peripheral disorders, has been limited by the psychoactive side effects derived from their agonists, and for the need to consider the risk-benefit balance. Accordingly, fewer side effects are expected when drugs are targeting receptors with restricted expression than when drugs are targeting receptors widely expressed in the CNS. CB2R may be also expressed by CNS neurons. The neuroprotective potential of compounds targeting the CB2R is, first of all, the logical consequence of their location in key cell types (e.g., in specific neuronal subsets, activated astrocytes, reactive microglia, perivascular microglia, oligodendrocytes, and neural progenitor cells), and also in some structures (e.g., the blood-brain barrier (BBB)) that are critical for the maintenance of the CNS integrity (Amenta et al., 2012; Chung et al., 2016) (Figure 2A). Expectations are that new formulations of selective CB2R ligands active at the orthosteric binding site, or acting as allosteric modulators, used alone or in combination with other licensed medicines, will be available to combat devastating neurological disorders such as Alzheimer's disease, Parkinson's disease, ataxias or amyotrophic lateral sclerosis.

[Visit Link](https://www.frontiersin.org/journals/neuroscience/articles/10.3389/fnins.2016.00406/full){:target="_blank rel="noopener"}

