---
layout: post
title: "MD Simulations Revealing Special Activation Mechanism of Cannabinoid Receptor 1"
date: 2024-08-21
tags: [G protein-coupled receptor,Biophysics,Receptors,Neurochemistry,Cell communication,Cell signaling,Cell biology,Biochemistry,Signal transduction,Nutrients,Molecular biology]
author: Zhi-Jie
---


We used two indicators to analyze the movement of this part (Figure 2B): 1) the distance between Y2945.58 and Y3977.53, the two conserved residues in class A GPCRs that lay far apart in the inactive state but form interactions in the active state (10.5 and 3.4 Å, respectively, for CB1, marked by side-chain Oη atoms), displaying the distortion of TM7; and 2) the distance between the cytoplasmic ends of TM3 and TM6, as loosening of the two segments is a key movement upon activation (8.2 Å in the inactive state and 13.8 Å in the active state for CB1, marked by R2143.50 and A3426.34 Cα atoms). Interestingly, only run 2, the run with the local movement of the cytoplasmic part, revealed remarkable conformational changes of the “twin toggle switch” residues (Figure 2C and Supplementary Figure S2). During simulations of runs 1–2, the RMSD of Cα atoms to the inactive structure decreased to less than that to the active structure, which increased during simulations, thus showing that the overall structure gradually shifted from the active state to the inactive state (Figure 3A). Transitions of the other three indicators occurred at ∼400 ns, and around this time, the overall structure became more similar to the inactive structure than to the active structure (Figure 3A). FIGURE 4  Our results show that the full agonist CP55,490 stabilized the “twin toggle switch” residues in the active state (Figure 4A)—F2003.36 χ1 only shifted temporarily in two runs, while W3566.48 χ2 did not change at all, suggesting no fluctuation was observed in all the five runs.

[Visit Link](https://www.frontiersin.org/journals/molecular-biosciences/articles/10.3389/fmolb.2022.860035/full){:target="_blank rel="noopener"}

