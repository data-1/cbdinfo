---
layout: post
title: "Measurement uncertainty revealed: The impacts of Certified Reference Material (CRM) on cannabinoid concentrations in the cannabis testing industry"
date: 2024-09-02
tags: [Chemist,Physical sciences,Chemistry]
author: 
---


Chief Science Officer Kaycha Labs  Mr. Goldman joined PhytaTech in 2015 (the predecessor company to Kaycha Labs Colorado). His primary areas of focus are the Colorado Lab Operations, Research & Development, New Test Development, and Nationwide Standardization. Prior to joining Kaycha Mr. Goldman served as an analytical chemist at the CLIA and CAP certified Forensic Laboratories, was a chemist for Novartis (Sandoz), Kemin Industries, Genentech, and served as a contract chemist. Mr. Goldman is a member of the American Institute of Chemical Engineers, American Association of Pharmaceutical Scientists and the American Chemical Society, Cannabis Chemistry Subdivision. Education: University of Kansas, MS Pharmaceutical Chemistry, University of Iowa, BS

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2666831924000584){:target="_blank rel="noopener"}

