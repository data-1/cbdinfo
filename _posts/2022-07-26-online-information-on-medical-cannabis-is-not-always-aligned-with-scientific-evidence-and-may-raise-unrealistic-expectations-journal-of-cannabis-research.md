---
layout: post
title: "Online information on medical cannabis is not always aligned with scientific evidence and may raise unrealistic expectations - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabis
author: Macedo, Arthur Cassa, Universidade Federal De Minas Gerais, Belo Horizonte, De Faria, André Oliveira Vilela, Bizzi, Universidade Federal Do Rio Grande Do Sul, Porto Alegre, Moreira
tags: [Cannabidiol,Health On the Net Foundation,Tetrahydrocannabinol,Randomized controlled trial,Medical cannabis,Clinical trial,Health care,Health,Medicine,Health sciences,Clinical medicine]
---


This study shows that over half of the webpages containing information about medical cannabis are from news websites, which indicates the newsworthiness of this topic. Therefore, the general picture is that there is a partial mismatch between the indications mentioned on the Web for cannabis-based products and the regulatory approval, particularly for the treatment of pain. Often, an indication is frequently mentioned on the web despite there being few RCTs listed in the Cochrane database, and this is, for instance, the case of nausea, cancer, and anxiety disorders. In the present study, while regulatory issues were often mentioned, only 22% of websites mentioned the potential side effects, and this was due, in particular, to the paucity of this type of information in news outlets and commercial websites. This limitation, however, could have been at least partially circumvented by evaluating a large number of webpages.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00145-w){:target="_blank" rel="noopener"}


