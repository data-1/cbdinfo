---
layout: post
title: "A Cannabinoid 2-Selective Agonist Inhibits Allogeneic Skin Graft Rejection In Vivo"
date: 2022-08-05
categories:
- Cannabinoid
author: Jayarajan, Center For Substance Abuse Research, United States, Meissler, Joseph J., Adler, Martin W., Eisenstein, Meliha Karsak, María Gómez-Cañas
tags: [Cannabinoid receptor type 2,Immunosuppressive drug,Interleukin 10,Regulatory T cell,T cell,Transplant rejection,Immune system,Macrophage,Cell biology,Immunology,Biology,Medicine,Clinical medicine,Medical specialties,Biochemistry]
---


When dissociated spleen cells were placed in culture ex vivo and stimulated with C3HeB/FeJ cells in an MLR, the cells from the O-1966 treated mice were significantly suppressed in their proliferative response to the allogeneic cells. The present studies examined the effect of injecting a CB2 selective agonist on in vivo immune responses, including skin graft rejection in mice, and cytokine and Treg levels in treated, as compared to, animals receiving vehicle. Percent graft survival of mice treated with (A) 1 mg/kg O-1966 ( ), or vehicle ( ), (B) 5 mg/kg O-1966 ( ) or vehicle ( ), or (C) 10 mg/kg O-1966 ( ) or vehicle ( ). As shown in Figure 5, splenocytes from mice grafted with C3HeB/FeJ skin and treated with O-1966 in vivo had significantly decreased proliferation in response to ex vivo stimulation with the C3HeB/FeJ cells. 4 Discussion  The results of this study extend published in vitro studies using the MLR assay (Robinson et al., 2013; Robinson et al., 2015) to show efficacy of a CB2 selective agonist, O-1966, in vivo, in retarding rejection of skin grafts in mice and production of an immunosuppressive phenotype in the spleens of grafted animals injected with the cannabinoid.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2021.804950/full){:target="_blank" rel="noopener"}


