---
layout: post
title: "Plant-Derived Trans-β-Caryophyllene Boosts Glucose Metabolism and ATP Synthesis in Skeletal Muscle Cells through Cannabinoid Type 2 Receptor Stimulation"
date: 2024-08-20
tags: [Citric acid cycle,Glycolysis,Pyruvate dehydrogenase complex,Insulin resistance,Mitochondrion,Metabolism,Insulin,Cellular respiration,Cannabinoid receptor 2,Glucose,Skeletal muscle,Adenosine triphosphate,Metabolic pathway,GLUT4,Biology,Cell biology,Biochemistry,Underwater diving physiology]
author: Federica Geddo | Susanna Antoniotti | Giulia Querio | Iris Chiara Salaroglio | Costanzo Costamagna | Chiara Riganti | Maria Pia Gallo | Iris Chiara | Maria Pia
---


CB2 Receptor Mediates Glucose Uptake in C2C12 Myotubes  As we have previously demonstrated that BCP improves glucose uptake in C2C12 muscle cells [23], here, we investigated how this effect requires CB2 receptor stimulation; for this purpose, we employed both AM630 and SR144528 as CB2 receptor antagonists in the glucose-uptake measurements. It is well proven that, after increasing the glucose uptake, insulin promotes glucose metabolism through glycolysis by increasing the activity of PFK2, GAPDH, enolase, and PK [28,29]; this was confirmed, as expected, in our experiments on C2C12 skeletal muscle cells stimulated for 30 min with Insulin 25 nM, a concentration in the physiological range ( ). Plant-Derived Trans-β-Caryophyllene Boosts Glucose Metabolism and ATP Synthesis in Skeletal Muscle Cells through Cannabinoid Type 2 Receptor Stimulation. Plant-Derived Trans-β-Caryophyllene Boosts Glucose Metabolism and ATP Synthesis in Skeletal Muscle Cells through Cannabinoid Type 2 Receptor Stimulation. Plant-Derived Trans-β-Caryophyllene Boosts Glucose Metabolism and ATP Synthesis in Skeletal Muscle Cells through Cannabinoid Type 2 Receptor Stimulation Nutrients 13, no.

[Visit Link](https://www.mdpi.com/2072-6643/13/3/916){:target="_blank rel="noopener"}

