---
layout: post
title: "Investigating Relationships Between Alcohol and Cannabis Use in an Online Survey of Cannabis Users: A Focus on Cannabinoid Content and Cannabis for Medical Purposes"
date: 2024-08-20
tags: [Cannabis (drug),Cannabis edible,Cannabidiol,Alcohol (drug),Long-term effects of alcohol,Cannabinoid,Medical cannabis,Tetrahydrocannabinol,Cannabinoid receptor 1,Psychoactive drugs,Health,Individual psychoactive drugs]
author: Hollis C | Raeghan L | Chrysta C | Kent E
---


Further, alcohol consumption has decreased significantly in states with legalized medical cannabis (10), and medical users have been shown to drink less and have fewer alcohol-related problems than recreational users (11). Creation of Variables for Analysis  Survey participants were cannabis users who were categorized into groups based on whether they (1) use cannabis to treat a medical condition (CTT) or whether their cannabis use is not intended to treat a medical condition (NCTT), and (2) according to the average THC/CBD ratio in the edible and flower cannabis that they typically use. Note that some individuals reported only flower (no edible) use; they were only included in the analyses using the flower groupings and comparing CTT to NCTT groups. Some individuals reported only edible (no flower) use; they were included only in analyses using the edible groupings and comparing CTT to NCTT. Also note that in response to this question, all three cannabinoid groups reported drinking less alcohol on cannabis use days on average (see Table 2; note that a “1” response to this question corresponds to “much less alcohol” and a “2” corresponds to “a little less alcohol”), and no participant across the entire sample endorsed drinking “much more alcohol.” This suggests that cannabis users in this study are not at risk for drinking much more alcohol on the days that they use cannabis, regardless of the cannabinoid content of their typical products and whether or not they are using cannabis to treat a medical condition.

[Visit Link](https://www.frontiersin.org/journals/psychiatry/articles/10.3389/fpsyt.2020.613243/full){:target="_blank rel="noopener"}

