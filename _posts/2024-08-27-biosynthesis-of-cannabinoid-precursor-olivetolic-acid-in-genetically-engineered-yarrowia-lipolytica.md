---
layout: post
title: "Biosynthesis of cannabinoid precursor olivetolic acid in genetically engineered Yarrowia lipolytica"
date: 2024-08-27
tags: [Cannabidiol,Cannabis,Tetrahydrocannabinol,Cannabinoid,Life sciences,Biotechnology,Biochemistry,Biology]
author: 
---


To produce OLA in Y. lipolytica, the plasmid pYLXP’-CsOLS-CsOAC encoding the codon-optimized CsOLS and CsOAC was transformed into Y. lipolytica. Among these chosen hexanoyl-CoA synthetases, we found the overexpression of PpLvaE with CsOLS and CsOAC (strain YL110) resulted in the highest OLA production, an eightfold increase in OLA titer (1.07 mg/L) (Fig. When the OA pathway (CsOLS-CsOAC-PpLvaE) was transformed into the strain YL111 with ylDGA2 knockout and YL112 strain with ylDGA1 and ylDGA2 double knockout, the resulting strains YL113 and YL114 showed a dramatic decline in OLA production (Fig. The engineered strain YL115 with overexpression of ylACC1 produced 1.9 mg/L of OLA, with a 1.8-fold improvement (Fig. Single-gene and multi-genes expression vectors construction  In this work, the YaliBrick plasmid pYLXP’ was used as the expression vector42.

[Visit Link](https://www.nature.com/articles/s42003-022-04202-1){:target="_blank rel="noopener"}

