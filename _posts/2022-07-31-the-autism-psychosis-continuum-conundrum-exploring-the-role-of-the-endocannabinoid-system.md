---
layout: post
title: "The Autism–Psychosis Continuum Conundrum: Exploring the Role of the Endocannabinoid System"
date: 2022-07-31
categories:
- endocannabinoid
author: Colizzi, Bortoletto, Costa, Bhattacharyya, Balestrieri, Marco Colizzi, Riccardo Bortoletto, Rosalia Costa, Sagnik Bhattacharyya, Matteo Balestrieri
tags: [Schizophrenia,Autism,Mental disorder,Autism spectrum,Psychosis,Neurexin,Tetrahydrocannabinol,Cannabinoid receptor type 1,Medical cannabis,Spectrum disorder,Cannabidiol,DNA methylation,Regulation of gene expression,Cannabinoid receptor type 2,Cannabinoid receptor,Neuroscience]
---


Altered transcripts following THC exposure were found in a substantial number of genes linked to autism (80 genes) and intellectual disability (167 genes), with fewer overlapping with schizophrenia [ 33 ]. Using a similar approach, another study investigated impaired social behavior as a phenotype associated with both autism and schizophrenia [ 27 ]. The human study investigated the effect of cannabidiol (CBD)-rich medical cannabis in 60 children with severe autism spectrum disorders (ASD) [ 32 ]. Such findings were corroborated by independent evidence of shared genetic vulnerability between autism, schizophrenia, and cannabis use [ 31 36 ], the latter being implicated in increasing the risk for psychosis [ 44 45 ] as well as inducing the exogenous disruption of the eCB system [ 46 ] with effects on brain function and related behavior [ 47 48 ]. Furthermore, a developmental trajectory between autism and schizophrenia was supported by evidence that THC exposure, a valid model of psychosis [ 49 50 ], results in schizophrenia-related phenotypes among individuals with severe forms of autism [ 32 35 ], possibly by altering genes involved in several neurodevelopmental pathways including both autism- and schizophrenia-related ones [ 33 ].

<hr>[Visit Link](https://www.mdpi.com/1660-4601/19/9/5616/htm){:target="_blank" rel="noopener"}


