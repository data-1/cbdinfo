---
layout: post
title: "THC and CBD affect metabolic syndrome parameters including microbiome in mice fed high fat-cholesterol diet - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
- THC
author: Gorelick, Eastern Regional R D Center, Kiryat Arba, Assa-Glazer, The Robert H. Smith Faculty Of Agriculture, Food, Environment, The Hebrew University Of Jerusalem, Rehovot, Zandani
tags: [Non-alcoholic fatty liver disease,Cannabidiol,Cannabinoid,Tetrahydrocannabinol,Gut microbiota,Real-time polymerase chain reaction,Fatty liver disease,Microbiota,16S ribosomal RNA,Dysbiosis,Ribosomal RNA,Complementary DNA,HE stain,Polymerase chain reaction,Medical specialties,Biochemistry,Biology]
---


This seeming contradiction may at least partially be due to other confounding factors within the context of metabolic disorders such as inflammation which can influence the microbiome (Silvestri et al. 2020). Our observations support previous studies in which patients with persistent NAFLD displayed significantly lower levels of Ruminococcaceae compared with healthy individuals (Zhu et al. 2013; Kim et al. 2019). What remains to be determined is whether alterations of the microbiome are actually the cause of the other changes observed or only associated effects. The results from our study suggest that CBD may be more effective than THC at ameliorating NAFLD. Unfortunately, in the majority of studies involving cannabis, especially observational ones, the levels of THC and CBD are not defined.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00137-w){:target="_blank" rel="noopener"}


