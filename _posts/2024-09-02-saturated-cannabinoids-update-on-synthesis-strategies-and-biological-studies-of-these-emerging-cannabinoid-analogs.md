---
layout: post
title: "Saturated Cannabinoids: Update on Synthesis Strategies and Biological Studies of These Emerging Cannabinoid Analogs"
date: 2024-09-02
tags: [Cannabinoid,Ligand (biochemistry),Cannabinoid receptor 2,Endocannabinoid system,Cannabidiol,Anandamide,Fatty-acid amide hydrolase 1,Tetrahydrocannabinol,Hexahydrocannabinol,Diels–Alder reaction,Cannabinoid receptor 1,Baeyer–Villiger oxidation,Receptor (biochemistry),Cannabinoid receptor,Organic chemistry,Concepts in chemistry,Chemistry]
author: Maite L. Docampo-Palacios | Giovanni A. Ramirez | Tesfay T. Tesfatsion | Alex Okhovat | Monica Pittiglio | Kyle P. Ray | Westley Cruces | Docampo-Palacios | Maite L | Giovanni A
---


Since its discovery in 1940, through catalytic hydrogenation of THC and cannabinoid derivatives, hydrogenated cannabinoids have been synthesized; only H4CBD and HHC have been of interest as they are the hydrogenated scaffolds of THC and CBD [8]. 3-chlorobenzoperoxoic acid was used for the oxidation reaction to obtain compounds 1 and 7 with a 56% and 40% yield, respectively, from the last two steps. Hydrogenation reaction of HHCA-27. (A) Compound 166 against the CB1 receptor; (B) compound 166 against the CB2 receptor [103]. Cannabinoids.

[Visit Link](https://www.mdpi.com/1420-3049/28/17/6434){:target="_blank rel="noopener"}

