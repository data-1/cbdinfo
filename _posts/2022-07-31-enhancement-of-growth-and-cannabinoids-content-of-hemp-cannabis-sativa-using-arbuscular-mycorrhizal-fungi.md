---
layout: post
title: "Enhancement of growth and Cannabinoids content of hemp (Cannabis sativa) using arbuscular mycorrhizal fungi"
date: 2022-07-31
categories:
- Cannabis
author: Seemakram, Department Of Microbiology, Faculty Of Science, Khon Kaen University, Paluka, Natural Products Research Unit, Department Of Chemistry, Center Of Excellence For Innovation In Chemistry, Suebrasri, Department Of Medical Science
tags: [Arbuscular mycorrhiza,Cannabis,Mycorrhiza,Cannabinoid,Cannabidiol,Cannabis sativa,Tetrahydrocannabinolic acid,Plant,Tetrahydrocannabinol,Soil]
---


Plants of Cannabis sativa KKU05 grown under different conditions: T1, control without AMF inoculum; T2, Non-mycorrhizal plants with synthetic NPK fertilizer; T3, Plants inoculated with Rhizophagus. We found that inoculation with certain species of AMF promotes growth of hemp and increases cannabinoid contents more than the use of synthetic fertilizer, an effect that has not yet been previously reported. Google Scholar  Asrar, A. Google Scholar  Imo, M. (2012). Google Scholar  Jackson, M. L. (1967).

<hr>[Visit Link](https://www.frontiersin.org/articles/10.3389/fpls.2022.845794/full){:target="_blank" rel="noopener"}


