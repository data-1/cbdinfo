---
layout: post
title: "Early Blockade of CB1 Receptors Ameliorates Schizophrenia-like Alterations in the Neurodevelopmental MAM Model of Schizophrenia"
date: 2022-08-27
categories:
- 
author: Stark, Iannotti, Fabio Arturo, Di Martino, Di Bartolomeo, Ruda-Kucerova, Piscitelli, Wotjak, Carsten T., D Addario
tags: [Real-time polymerase chain reaction,Neuroscience,Biochemistry]
---


However, still little is known whether prenatal MAM exposure could affect ECS signaling in the early postnatal days. A total of 20 males from each group (i.e., CNT and MAM, maximum of 2 pups per litter) were used for postnatal assessment of neurobehavioral development by a single examiner that was blind to the treatment conditions. As adults (from PND 100), these animals were submitted to the behavioral tests, which were conducted in battery with 3–4 days between two consecutive tests in the following order: the spontaneous locomotor activity in the open field test (OFT), the social activity in the social interaction (SI) test, and the cognitive performance in the novel object recognition (NOR) test [ 33 ]. In the whole brain of MAM rats, we found a significant increase of 2-AG content (t = 2.448,< 0.05), but not of AEA (t = 1.909,> 0.05), PEA (t = 0.8293,> 0.05) or OEA (t = 1.125,> 0.05) concentrations ( Figure 2 I,J) at PND 10. Overall, our data support the idea that altered ECS signaling at neonatal age could negatively affect the maturational processes within the CNS leading to abnormal neurotransmission, which could, in turn, underlie the social and cognitive deficits in adulthood ( Figure 3 ).

<hr>

[Visit Link](https://www.mdpi.com/2218-273X/12/1/108/htm){:target="_blank" rel="noopener"}


