---
layout: post
title: "Cannabinoids—Characteristics and Potential for Use in Food Production"
date: 2022-08-27
categories:
- Cannabinoid
author: Kanabus, Bryła, Roszko, Modrzewska, Pierzgalski, Joanna Kanabus, Marcin Bryła, Marek Roszko, Marta Modrzewska, Adam Pierzgalski
tags: [Tetrahydrocannabinol,Cannabinoid,Cannabidiol,Cannabis sativa,Cannabis,Emulsion,Flavonoid,Hemp,Gas chromatographymass spectrometry,Chemistry]
---


With hemp balm, the content of CBDA amounted to 44.7–80.4 mg/g of the product, CBD amounted to 7.6–13.8 mg/g, while CBGA and CBG were present in considerably lower concentrations, 2.0–3.9 mg/g and 0.4 mg/g, respectively. The dominant cannabinoids in hemp seed oils were CBDA and CBD, while Δ9-THC and Δ9-THCA were present in small amounts. Of the 30 food samples containing cannabinoids, CBD was detected in 15 samples in amounts ranging from 70 to 31.31 mg/kg. The Δ9-THC content in 12 food products was 0.08–98.62 mg/kg. CBD was not detected in the seeds, whereas a high concentration was detected in the liqueur.

<hr>

[Visit Link](https://www.mdpi.com/1420-3049/26/21/6723/htm){:target="_blank" rel="noopener"}


