---
layout: post
title: "Neural Substrates of Incidental Associations and Mediated Learning: The Role of Cannabinoid Receptors"
date: 2024-08-20
tags: [Classical conditioning,Cannabinoid receptor 1,Endocannabinoid system,Learning,Hippocampus,Cannabinoid,Second-order conditioning,Psychosis,Memory,Brain,Behavioural sciences,Neuroscience,Mental processes,Cognitive science,Cognition]
author: Busquets-Garcia
---


Sensory preconditioning in particular represents the most common behavioral protocol for studying incidental associations among relatively neutral or low-salience stimuli. Through this process, often termed mediated or representation-mediated learning, presentation of S2 during the second phase of training activates a mental representation of S1, so that that this associatively retrieved memory might become further associated with the experience of the US. With the hippocampus being a key brain region for sensory preconditioning, we addressed the role of hippocampal CB1R in these processes. Further experiments revealed that CB1Rs in hippocampal GABAergic neurons are indeed crucial for incidental learning, demonstrating a physiological link between hippocampal GABAergic signaling and associative memory between low-salience events. From Incidental Learning to Reality Testing: A Role for CB1 Receptor Signaling  Contrary to classical conditioning between a conditioned stimulus and an unconditioned stimulus that generally produces solid and long-lasting responses, an elemental characteristic of incidental associations between stimuli is that they are intrinsically weak (McDannald and Schoenbaum, 2009).

[Visit Link](https://www.frontiersin.org/journals/behavioral-neuroscience/articles/10.3389/fnbeh.2021.722796/full){:target="_blank rel="noopener"}

