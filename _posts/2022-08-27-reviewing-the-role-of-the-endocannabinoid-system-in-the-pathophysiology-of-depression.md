---
layout: post
title: "Reviewing the Role of the Endocannabinoid System in the Pathophysiology of Depression"
date: 2022-08-27
categories:
- Endocannabinoid
author: Gallego-Landin, Neurobiology Of Behaviour Research Group, Grenec Neurobio, Department Of Experimental, Health Sciences, Universitat Pompeu Fabra, García-Baos, Castro-Zavala, Valverde, Neuroscience Research Programme
tags: [Major depressive disorder,Hypothalamicpituitaryadrenal axis,Cannabinoid receptor type 2,Endocannabinoid system,Cannabinoid receptor type 1,Cannabinoid receptor,Microglia,Mental disorder,Anandamide,Stress (biology),Inflammation,Neuroinflammation,Hippocampus,Antidepressant,Cannabinoid,Interleukin 6,2-Arachidonoylglycerol,Subgranular zone,Adult neurogenesis,Cannabidiol,Mood disorder,Synapse,Depression (mood),NF-B,Attention deficit hyperactivity disorder,Brain-derived neurotrophic factor,Serotoninnorepinephrine reuptake inhibitor,Receptor (biochemistry),Neurogenesis,Cannabinoid receptor antagonist,TRPV1,Cytokine,Serotonin,Interleukin 10,Neurotransmitter,Post-traumatic stress disorder,Neuroscience,Neurochemistry,Biochemistry]
---


Although, some results suggest that certain effects of cannabinoids are not regulated by CB1 or CB2 (Brown, 2007), which has generated some debate regarding the potential existence of a third cannabinoid receptor, nonetheless this remains merely a hypothesis. The figure also includes the main synthesis and degradation processes of both endocannabinoids: 2-AG and AEA. Interestingly, the pharmacological effects of CBD go beyond endocannabinoid receptors. Researchers reported that the patient was administered CBD upon treatment with antidepressants, after which they showed improvement regarding depressive and anxiety symptoms. CB2 Receptors in the Brain: Role in Central Immune Function.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fphar.2021.762738/full){:target="_blank" rel="noopener"}


