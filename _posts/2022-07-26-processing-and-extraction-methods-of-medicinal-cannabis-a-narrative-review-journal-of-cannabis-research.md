---
layout: post
title: "Processing and extraction methods of medicinal cannabis: a narrative review - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabis
author: Lazarjani, Masoumeh Pourseyed, Drug Delivery Research Group, School Of Science, Faculty Of Health, Environmental Sciences, Auckland University Of Technology, Auckland, New Zealand, Young
tags: [Cannabis sativa,Solvent,Liquidliquid extraction,Cannabinoid,Supercritical fluid,Freeze-drying,Solubility,Tetrahydrocannabinolic acid,Ethanol,Supercritical fluid extraction,Hashish,Medical cannabis,Tetrahydrocannabinol,Carbon dioxide,Cannabis,Supercritical carbon dioxide,Soxhlet extractor,Water,Petroleum,Oil,Vegetable oil,Distillation,Phase (matter),Chemistry,Physical sciences,Chemical substances]
---


It is also believed that curing can increase cannabis potency as the number of cannabinoids such as THC and CBN will increase by curing. The use of ethanol for maceration extraction of cannabinoids was found to produce the highest yield when used twice compared to other methods of extractions, for instance, ultrasonic-assisted extraction (UAE) or supercritical fluid extraction (SFE) (Fathordoobady et al. 2019). The experimentation demonstrated the optimal conditions for the highest yield of cannabinoids using ultrasonication to be 50 min at 60 °C with ethanol as a solvent. This study also found that ethanol extract yield was 3 to 4 times higher than olive oil extract (De Vita et al. 2020). The optimum yield of these cannabinoids was achieved by using ethanol as co-solvent at 55 °C and 34 MPa (Fathordoobady et al. 2019).

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00087-9){:target="_blank" rel="noopener"}


