---
layout: post
title: "Cannabinoid Control of Olfactory Processes: The Where Matters"
date: 2024-08-20
tags: [Cannabinoid receptor 1,Endocannabinoid system,Mitral cell,Sense of smell,Synapse,Chemical synapse,Inhibitory postsynaptic potential,Neuroscience,Basic neuroscience research,Neurophysiology,Branches of neuroscience,Neurochemistry,Physiology,Brain]
author: Geoffrey Terral | Giovanni Marsicano | Pedro Grandes | Edgar Soria-Gómez | Soria-Gómez
---


Besides the studies regarding the functions of the ECS in primary olfactory structures, it is important to take into account that CB1 receptors are present and modulate associated olfactory areas (i.e., amygdala, orbitofrontal cortex, hippocampus or periaqueductal gray; [41,42,43,44]), suggesting that olfactory processing that involves the control of different brain structures might also be modulated by the ECS. [Google Scholar] [CrossRef] [PubMed]  Marsicano, G.; Lafenêtre, P. Roles of the endocannabinoid system in learning and memory. [Google Scholar] [CrossRef] [Green Version]  Herkenham, M.; Lynn, A.; Johnson, M.; Melvin, L.; de Costa, B.; Rice, K. Characterization and localization of cannabinoid receptors in rat brain: A quantitative in vitro autoradiographic study. [Google Scholar] [CrossRef]  Lotsch, J.; Hummel, T. Cannabinoid-related olfactory neuroscience in mice and humans. [Google Scholar] [CrossRef] [PubMed]  © 2020 by the authors.

[Visit Link](https://www.mdpi.com/2073-4425/11/4/431){:target="_blank rel="noopener"}

