---
layout: post
title: "Regulation and marketing of cannabidiol-containing products in European countries. Pharmacists’ knowledge in Bulgaria"
date: 2024-09-16
tags: [Cannabidiol,Medical cannabis,Cannabinoid,Cannabis,Nabiximols,Health,Pharmaceutical sciences,Drugs,Psychoactive drugs,Pharmacy,Pharmacology,Medicinal chemistry,Chemicals in medicine,Cannabaceae,Clinical medicine,Medical treatments,Medicine,Health care]
author: Bogdan Kirilov | Margarita Zhelyazkova | Elina Petkova-Gueorguieva | Georgi Momekov
institutions: Medical University of Sofia
doi: "https://doi.org/10.1080/13102818.2020.1824620"
---


presents the available CBD-containing products/preparations in 19 European countries, divided by us into four groups on the basis of their different regulatory status, pharmaceutical procedures and applications: approved drugs (Sativex®, Epidiolex®), magistral preparations, standardized preparations and CBD as an investigational new drug. Generally, these schemes mimic the regulatory approach for medicinal CBD products: they must be in oral formulations and must have a standardized CBD and THC content. Study: CBD in Bulgaria  We investigated CBD as a supplement in three aspects: availability of CBD products on the pharmacy market, their use for medical conditions and pharmacists’ knowledge, awareness and attitudes towards CBD. CBD products were not available in less than 10% of the pharmacies included in this survey. These results correspond to the medical use of CBD in other European countries, although in some of these countries the emphasis is on pharmacologically active products/preparations and their prescription by a licensed medical doctor.