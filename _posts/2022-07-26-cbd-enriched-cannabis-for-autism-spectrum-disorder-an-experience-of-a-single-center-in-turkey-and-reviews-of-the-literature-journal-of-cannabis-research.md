---
layout: post
title: "CBD-enriched cannabis for autism spectrum disorder: an experience of a single center in Turkey and reviews of the literature - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Bilge, Department Of Pediatric Neurology, Çukurova Medical School, Balcalı, Ekici, Pediatric Neurology Clinics, Istanbul, Serap Bilge, Barış Ekici, You Can Also Search For This Author In
tags: [Cannabinoid receptor type 1,Cannabidiol,Cannabinoid,Autism,Autism spectrum,Dose (biochemistry),Anandamide,Cannabinoid receptor,Mental disorder,Antipsychotic,Cannabinoid receptor type 2,Clinical medicine,Neurochemistry,Health,Neuroscience]
---


The main improvements of the treatment were as follows: a decrease in behavioral problems was reported in 10 patients (32.2%), an increase in expressive language was reported in 7 patients (22.5%), improved cognition was reported in 4 patients (12.9%), an increase in social interaction was reported in 3 patients (9.6%), and a decrease in stereotypes was reported in 1 patient (3.2%). Restlessness was the only reported side effect in 7 (22%) out of 31 patients who continued treatment for at least three months, and the CBD-enriched cannabis dose was reduced in these patients. Aran et al. (2019) were the first to retrospectively assess CBD-enriched cannabis effects on 60 children with ASD and severe behavioral problems using an open-label cohort study. Significant improvement was reported in 48.7% of patients, moderate improvement was reported in 31.1% of patients, and no change was reported in 14.3% of patients. These patients were on cannabis treatment.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00108-7){:target="_blank" rel="noopener"}


