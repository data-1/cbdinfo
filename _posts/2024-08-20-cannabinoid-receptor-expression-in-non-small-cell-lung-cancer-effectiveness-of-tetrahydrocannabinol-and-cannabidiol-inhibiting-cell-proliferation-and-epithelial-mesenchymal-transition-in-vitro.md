---
layout: post
title: "Cannabinoid receptor expression in non-small cell lung cancer. Effectiveness of tetrahydrocannabinol and cannabidiol inhibiting cell proliferation and epithelial-mesenchymal transition in vitro"
date: 2024-08-20
tags: [Cannabinoid receptor 2,Cannabinoid,Epithelial–mesenchymal transition,Cannabidiol,Biology,Cell biology,Biochemistry]
author: Javier Alcacer | María Oliver | María Sancho-Tello | Carlos Camps | José Galbis | Julian Carretero | Carmen Carda | Lara Milian | Manuel Mata
---


Both cannabinoid agonists inhibited the proliferation and expression of EGFR in lung cancer cells, and CBD potentiated the effect of THC. THC and CBD inhibited the proliferation and expression of EGFR in the lung cancer cells studied. To assess the antitumor effect of THC and CBD, cells were exposed to 10–100 μM THC or CBD, individually or in combination (1:1 ratio) for 48 hours. https://doi.org/10.1371/journal.pone.0228909.g001  THC/CBD inhibited the proliferation and EGFR expression in lung cancer cells  Next, we assessed the anti-proliferative effects of THC and CBD on A549, H460 and H1792 cells in vitro. The expression of CDH1 was significantly downregulated (0.25 ± 0.15, 0.30 ± 0.27 and 0.46 ± 0.19-fold for A549, H460 and H1792 cells, respectively), while that of CDH2 (6.45 ± 1.36, 3.86 ± 1.20 and 2.75 ± 0.97-fold for A549, H460 and H1792 cells, respectively) and VIM (7.59 ± 0.69, 4.15 ± 0.20 and 3.91 ± 0.39-fold for A549, H460 and H1792 cells, respectively) were upregulated in cells stimulated with TGF-β compared to control cells.

[Visit Link](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0228909){:target="_blank rel="noopener"}

