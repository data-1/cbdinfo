---
layout: post
title: "Cannabinoid and nicotine exposure during adolescence induces sex-specific effects on anxiety- and reward-related behaviors during adulthood"
date: 2024-05-13
categories:
- 
author: Anna N. Pushkin, Department Of Neurobiology, Behavior, University Of California, Irvine, Ca, United States Of America, Angeline J. Eugene, Valeria Lallai, Alan Torres-Mendoza
tags: [Cannabis (drug),Elevated plus maze,Nicotine,Cannabinoid receptor 2,Tobacco smoking,Nucleus accumbens,Cannabinoid receptor 1,Reward system,Cigarette]
---


Fowler, University of California Smoke and Tobacco Free Fellowship (https://www.ucop.edu/risk-services/loss-prevention-control/uc-smoke-tobacco-free.html) to A.N. Further, given that significant differences were found in behavioral measures at the moderate dose of the cannabinoid agonist, a second study was then conducted to examine whether these effects would be maintained with a lower dose of the cannabinoid agonist. The moderate dose of WIN (2 mg/kg) was selected based on prior studies demonstrating altered neural function with adolescent exposure in mice and rats [26, 27], and the low dose of WIN (0.2 mg/kg) was selected since this amount of drug has been shown to sustain daily reinforcing self-administration behavior in adolescent rats (~16 infusions/day at 0.0125 mg/kg/infusion = ~0.2 mg/kg per day) [28]. With regard to nicotine alone, opposing effects have been found in male Sprague-Dawley rats with increased depression-associated behaviors, but no difference in anxiety-associated behaviors, during adulthood [15]. Along these lines, it is interesting to note that in the current study, mice were at a satiated level (not food restricted) during sucrose consumption, during which time the opposing differences were found in males and females exposed to adolescent WIN.

[Visit Link](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0211346#sec026){:target="_blank" rel="noopener"}


