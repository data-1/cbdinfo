---
layout: post
title: "JWH-182: a safe and effective synthetic cannabinoid for chemotherapy-induced neuropathic pain in preclinical models"
date: 2024-09-02
tags: [Cannabinoid,Cannabinoid receptor 2,Cannabinoid receptor 1,Synapse,Neuron]
author: Leontina-Elena | Creangă-Murariu | Bogdan-Ionel | Daniela-Carmen | Răzvan-Nicolae | Gabriela-Dumitrița | Silviu-Iulian | Ivona-Maria | Alexa-Stratulat | Marinela-Carmen
---


Repeated dose 28-day toxicity of JWH-182  Daily monitoring during the 4 weeks of the experiment of the two groups with intraperitoneal administration (control group—receiving vehicle solution, dosed group—receiving JWH-182 at dose 2.627 mg/kg) did not show significant changes regarding the animal’s general state of health. The motor activity evaluation revealed a significant increase in general activity in the JWH-182 treated group compared to the control group. DRG tissues isolated from 6–8 weeks C57BL/6/Swiss mice. The results were normalized with those from the control group. The reference controls were: DRG neurons cultured in normal growth media and neurons treated with only 1 µM Paclitaxel.

[Visit Link](https://www.nature.com/articles/s41598-024-67154-y){:target="_blank rel="noopener"}

