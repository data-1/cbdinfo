---
layout: post
title: "Effects of Cannabidiol on Exercise Physiology and Bioenergetics: A Randomised Controlled Pilot Trial - Sports Medicine - Open"
date: 2022-07-26
categories:
- CBD
author: Sahinovic, Lambert Initiative For Cannabinoid Therapeutics, The University Of Sydney, Sydney, Brain, Mind Centre, School Of Psychology, Faculty Of Science, Irwin, School Of Health Sciences
tags: [VO2 max,Cannabidiol,Effect size,Blood pressure,Cannabinoid,Exercise,Heart rate,Placebo,Respiratory system,Vasodilation,Dose (biochemistry),Clinical trial,Interleukin 6,Health]
---


Study Design  Participants completed two treatment sessions involving the oral administration of CBD (300 mg) or a placebo in a randomised, double-blind, crossover design. Each session involved seven consecutive blocks of testing: Baseline (pre-treatment), Pre-RUN 1 (+ 60–90 min post-treatment), RUN 1 (+ 90–150 min), Post-RUN 1 (+ 155–170 min), RUN 2 (+ 180– approx. Participants were not permitted to consume fluid during exercise but received up to 500 mL of water on completion of the Post-RUN 1 and 2 assessments; again, individual intakes were recorded and replicated across sessions. Respiratory gases were sampled continuously between 24–32 (24 EX ), 37–45 (37 EX ) and 50–58 (50 EX ) min of exercise. Respiratory gases were sampled during the final ~ 5 min of exercise (i.e. from the point at which HR exceeded ~ 90% HR max , as determined during the initial V̇O 2max test).

<hr>[Visit Link](https://sportsmedicine-open.springeropen.com/articles/10.1186/s40798-022-00417-y){:target="_blank" rel="noopener"}


