---
layout: post
title: "Antimicrobial and Antiviral (SARS-CoV-2) Potential of Cannabinoids and Cannabis sativa: A Comprehensive Review"
date: 2022-08-27
categories:
- Cannabinoid
- Cannabis
author: Mahmud, Md Sultan, Hossain, Mohammad Sorowar, Ahmed, A. T. M. Faiz, Islam, Md Zahidul, Sarker, Md Emdad
tags: [Antibiotic,Methicillin-resistant Staphylococcus aureus,Tetrahydrocannabinol,Antimicrobial resistance,Cannabidiol,Virus,Inflammation,Kaposis sarcoma-associated herpesvirus,Cannabinoid,Biofilm,Cannabinoid receptor type 2,Microbiology,Medicine,Biology,Biochemistry,Immunology,Clinical medicine,Medical specialties]
---


Plasmids contain genes to express resistance to antibiotics. When a biofilm forms, bacterial cells acquire 10–1000 times more resistance to antibiotics [ 46 ]. The oral administration of THC and CBD in humans showed a significant reduction of TNF-α [ 186 ]. Extraction of active compounds from fibers reduces their antimicrobial activity, and the reduction depends on extraction time [ 209 ]. Hemp seed extract has potential as an antibacterial agent in food plants to fight MRSA biofilms because it can inhibit virulent biofilms at low concentration [ 101 ].

<hr>

[Visit Link](https://www.mdpi.com/1420-3049/26/23/7216/htm){:target="_blank" rel="noopener"}


