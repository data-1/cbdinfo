---
layout: post
title: "Characterization of Endocannabinoid System and Interleukin Profiles in Ovine AEC: Cannabinoid Receptors Type-1 and Type-2 as Key Effectors of Pro-Inflammatory Response"
date: 2024-08-20
tags: [Endocannabinoid system,Cannabinoid receptor 2,Interleukin 10,Fatty-acid amide hydrolase 1,Implantation (embryology),Anandamide,Inflammation,Interleukin 12,Interleukin 4,Cell biology,Biochemistry,Medical specialties,Biology]
author: Luana Greco | Valentina Russo | Cinzia Rapino | Clara Di Germanio | Filomena Fezza | Nicola Bernabò | Paolo Berardinelli | Alessia Peserico | Domenico Fazio | Mauro Maccarrone
---


Then, the role of cannabinoid receptors 1 and 2 (CB1 and CB2) on interleukin expression and release was investigated in middle stage AEC using selective agonists and antagonists. Endocannabinoid system (ECS) gene expression modulation in amniotic epithelial cells (AEC) collected at different stages of gestation. ECS Controlled the Immune Activities of AEC at Middle Stage of Gestation  In order to evaluate the influence of extracellular CB modulation on constitutive (CTR) and LPS-induced (LPS) interleukin expression and release, AEC derived from the middle stage of gestation were exposed to CB1 and CB2 selective agonists (ACEA and JWH-015, 1 μM) or antagonists (AM281 and SR1445282, 1 μM). Cell. Cells.

[Visit Link](https://www.mdpi.com/2073-4409/9/4/1008){:target="_blank rel="noopener"}

