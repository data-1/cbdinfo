---
layout: post
title: "Cannabinoid Conference 2022"
date: 2024-08-27
tags: [Cannabidiol,Cannabinoid,Triple-negative breast cancer,Tetrahydrocannabinol,Catechol-O-methyltransferase,Medical cannabis,Tamoxifen,Selective estrogen receptor modulator,Estrogen receptor,Breast cancer,Mesenchymal stem cell,Lipopolysaccharide,Inflammation,Dose (biochemistry),Cancer,Dementia,Pancreatic cancer,Cannabis,Reactive oxygen species,Ligand (biochemistry),Health,Health sciences,Medicine,Medical specialties,Clinical medicine]
author: 
---


Methods  For these purposes, we performed a dose-response treatment with CBD by exposing male adult rats to single and repeated CBD treatments at 5, 15 and 30 mg/kg, to investigate both the rapid modulation of BDNF after acute treatment, as well as a potential drug-free time point following 7 days of treatment. Keywords  Cannabinoids, cannabidiol, cannabidiol acid, breast cancer, proliferation. 2021 DOI: 10.1021/acschemneuro.1c00086 [PMC free article] [PubMed] [Google Scholar]  [3] Rodriguez-Soacha DA, et al. Development of an Indole-Amide-Based Photoswitchable Cannabinoid Receptor Subtype 1 (CB1R) «Cis-On» Agonist. Reference  [1] Nagarkatti P, et al. Cannabinoids as novel anti-inflammatory drugs. Keywords  Cannabidiol, human PMN, inflammation, pain.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9729861/){:target="_blank rel="noopener"}

