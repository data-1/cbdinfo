---
layout: post
title: "The role of cannabinoids in epilepsy treatment: a critical review of efficacy results from clinical trials"
date: 2024-09-16
tags: [Epilepsy,Seizure,Cannabidiol,Clinical trial,Valproate,Generalized tonic–clonic seizure,Clinical medicine,Health,Medicine,Medical specialties,Health care,Diseases and disorders,Medical treatments,Health sciences]
author: Rima Nabbout | Elizabeth A. Thiele
institutions: Massachusetts General Hospital
doi: "https://onlinelibrary.wiley.com/doi/10.1684/epd.2019.1124"
---


The primary endpoint was the median percentage change in convulsive seizure frequency from the four-week baseline period compared to the 14-week treatment period among patients who received CBD, compared with placebo. Serious AEs were more common in the CBD group than in the placebo group (16% vs. 5%), and AEs led to withdrawal in eight patients in the CBD group compared with one in the placebo group. In this study, 171 patients were randomized (86 to CBD and 85 to placebo). In the same treatment period, patients had a 49% median reduction in non-drop seizures vs. 23% in the placebo group (p=0.004). In patients from GWPCARE1 Part B, median reduction from baseline in monthly seizure frequency, assessed in 12-week periods up to Week 48, ranged from 38% to 44% for convulsive seizures and 39% to 51% for total seizures.