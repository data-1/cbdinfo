---
layout: post
title: "Impaired Ethanol-Induced Sensitization and Decreased Cannabinoid Receptor-1 in a Model of Posttraumatic Stress Disorder"
date: 2024-08-12
tags: [Endocannabinoid system,Cannabinoid receptor 1,Anandamide,Dopamine receptor D2,Striatum,Post-traumatic stress disorder,Cannabinoid,Neurophysiology,Neurochemistry]
author: Jessica J. Matchynski-Franks | Laura L. Susick | Brandy L. Schneider | Shane A. Perrine | Alana C. Conti
---


Cannabinoid receptor-1 (CB1) is sensitive to the effects of ethanol (EtOH) and traumatic stress, and is a critical regulator of striatal plasticity. The impaired behavioral response observed in mice exposed to both mSPS and EtOH was associated with a decrease in striatal CB1 receptors. Additionally, increases in striatal PSD95 and decreases in striatal D2 receptor proteins induced by mSPS were reversed in mice exposed to mSPS and EtOH. Alcohol Alcohol. Alcohol Alcohol.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4871361/){:target="_blank rel="noopener"}

