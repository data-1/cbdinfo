---
layout: post
title: "Potential perioperative cardiovascular outcomes in cannabis/cannabinoid users. A call for caution"
date: 2024-09-02
tags: [Cannabinoid,Cannabis (drug),Cannabinol,Cannabidiol,Medical cannabis,Anticoagulant,Warfarin,Synthetic cannabinoids,Tetrahydrocannabinol,Dronabinol,Effects of cannabis,Atherosclerosis,Stroke,Heart failure,Myocardial infarction,Anesthesia,Bradycardia,Takotsubo cardiomyopathy,Arrhythmia,Medical specialties,Clinical medicine]
author: Echeverria-Villalobos | Tristan E
---


Cardiovascular events are among the risks of cannabis and cannabinoid consumption, as both natural and synthetic cannabinoids induce changes in the cardiovascular system of humans and animals (18). Consequently, we conducted a literature review to first identify the possible mechanisms underlying the harmful cardiovascular effects of cannabinoids; second, to review the pharmacological interactions of cannabinoids with commonly prescribed drugs, as well as anesthetic agents, in patients with heart disease; and third, to highlight the perioperative cardiovascular consequences associated with cannabis and cannabinoid consumption based on the most recently published evidence. The reported perioperative cardiovascular effects associated with the consumption of cannabis and synthetic cannabinoids are related not only to the consumed dose and route of administration of these drugs but also to their pharmacological interactions with cardiovascular medications, platelet antiaggregants, and anesthetic drugs. Table 3  3.9 Preoperative considerations of Cannabis users  Most of the evidence related to perioperative cardiovascular complications in cannabis users comes from case reports and small studies showing a low incidence of serious adverse events (181, 182). They found that cannabis users had a greater rate of arrhythmias than non-cannabis users (2.7% vs. 1.6%) (185).

[Visit Link](https://www.frontiersin.org/journals/cardiovascular-medicine/articles/10.3389/fcvm.2024.1343549/full){:target="_blank rel="noopener"}

