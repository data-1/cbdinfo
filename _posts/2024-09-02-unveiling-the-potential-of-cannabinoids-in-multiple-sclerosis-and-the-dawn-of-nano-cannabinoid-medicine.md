---
layout: post
title: "Unveiling the Potential of Cannabinoids in Multiple Sclerosis and the Dawn of Nano-Cannabinoid Medicine"
date: 2024-09-02
tags: [Endocannabinoid system,Cannabinoid,Multiple sclerosis,Nanoparticles for drug delivery to the brain,Nabilone,Cannabidiol,Cannabinoid receptor,Blood–brain barrier,Glatiramer acetate,Management of multiple sclerosis,Tetrahydrocannabinol,Anandamide,Neurotransmitter,Cannabinoid receptor 2,Interferon beta-1b,Biology,Medical specialties,Clinical medicine]
author: Roua A. Nouh | Ahmed Kamal | Oluwaseyi Oyewole | Walaa A. Abbas | Bishoy Abib | Abdelrouf Omar | Somaia T. Mansour | Anwar Abdelnaser | Roua A | Walaa A
---


Clinical studies on the use of cannabinoids for the management of multiple sclerosis. Treatment of Multiple Sclerosis: A Review. Multiple Sclerosis. Cannabis Cannabinoid. Delta-9-THC in the treatment of spasticity associated with multiple sclerosis.

[Visit Link](https://www.mdpi.com/1999-4923/16/2/241){:target="_blank rel="noopener"}

