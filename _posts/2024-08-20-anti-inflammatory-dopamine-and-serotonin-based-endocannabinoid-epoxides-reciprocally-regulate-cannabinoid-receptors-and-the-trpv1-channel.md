---
layout: post
title: "Anti-inflammatory dopamine- and serotonin-based endocannabinoid epoxides reciprocally regulate cannabinoid receptors and the TRPV1 channel"
date: 2024-08-20
tags: [TRPV1,Receptor antagonist,Cannabinoid receptor 2,Liquid chromatography–mass spectrometry,Enzyme inhibitor,Cannabinoid receptor 1,Enzyme kinetics,Ion source,Receptor (biochemistry),Cytochrome P450,Cannabinoid,Cannabinoid receptor,Cell biology,Biochemistry]
author: William R | Lauren N | Javier L
---


Co-metabolism of AEA with NADA or NA5HT  We next determined the co-substrate kinetics of AEA with the eVDs to determine if we can explain the observed effects of AEA on BV2-mediated metabolism. Overall, the potentiation of eVD metabolism by AEA and the altered AEA regioselectivity in the presence of eVDs demonstrate that eVDs and AEA are binding to CYP2J2 at multiple sites. None of these antagonized 50 nM CP55940 activation of these receptors (Supplementary Fig. 29) and the potentiation of eVD metabolism (Figs. Antagonism experiments for NA5HT, 14′,15′-epoNA5HT, and AMG-9810 were determined by adding varying concentrations of antagonist in 100 μL of HTB 15 min prior to stimulating with 250 nM CAP.

[Visit Link](https://www.nature.com/articles/s41467-021-20946-6){:target="_blank rel="noopener"}

