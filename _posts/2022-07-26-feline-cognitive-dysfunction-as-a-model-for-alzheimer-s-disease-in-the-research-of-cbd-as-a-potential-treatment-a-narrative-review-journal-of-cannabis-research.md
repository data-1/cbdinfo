---
layout: post
title: "Feline cognitive dysfunction as a model for Alzheimer’s disease in the research of CBD as a potential treatment—a narrative review - Journal of Cannabis Research"
date: 2022-07-26
categories:
- CBD
author: Zadik-Weiss, Amsterdam, The Netherlands, Ritter, Tel Aviv, Hermush, Department Of Geriatrics, Skilled Nursing, Laniado Medical Center, Netanya
tags: [Cannabidiol,Dementia,Amyloid beta,Alzheimers disease,Animal disease model,Tau protein,Neurofibrillary tangle,Disease,Model organism,Clinical trial,Veterinary medicine,Animal testing,Ageing,Clinical medicine,Health sciences,Medicine,Health,Medical specialties]
---


Transgenic animal models do not express all the factors present in the human disease, including pathological changes and disease progression; they also do not reflect the etiology of the human disease which is multifactorial and includes genetic and environmental risk factors (Franco and Cedazo-Minguez 2014). Humans and domestic animals share the same environment (including environmental contaminant), common stressors, and many genetic traits (Christopher 2015). Cats and dogs share pathological changes with the human AD and share the human environment which makes them both good candidate models for naturally occurring AD (Takeuchi et al. 2008). While dogs share many similarities with the human AD, cats are one of the only species which displays naturally occurring tau pathologies (Chambers et al. 2015). These facts make cats, at the very least a complementary model to dogs in researching naturally occurring AD.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-020-00054-w){:target="_blank" rel="noopener"}


