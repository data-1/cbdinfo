---
layout: post
title: "Cannabinoids in Medicine: A Multifaceted Exploration of Types, Therapeutic Applications, and Emerging Opportunities in Neurodegenerative Diseases and Cancer Therapy"
date: 2024-05-13
categories:
- cbd
author: Voicu, Brehar, Toader, Covache-Busuioc, Corlatescu, Antonio Daniel, Bordeianu, Costin, Horia Petre, Bratu
tags: [Anandamide,Cannabinoid,Cannabinoid receptor 2,Endocannabinoid system,Cannabis (drug),Cannabidiol,Tetrahydrocannabinol,Cannabinoid receptor 1,2-Arachidonoylglycerol,Inflammatory bowel disease,Neurodegenerative disease,Cannabinoid receptor,Fatty-acid amide hydrolase 1,Medical cannabis,Apoptosis,Nabiximols,Neuroprotection,Chemotherapy,Pharmacology,Cell biology,Clinical medicine,Neurochemistry,Biochemistry]
---


The therapeutic potential of Cannabis sativa, particularly its bioactive components like cannabinoids and terpenes, has garnered substantial attention in contemporary research. Limonene displayed significant cytotoxicity against T24 human bladder cancer cells by inducing G2/M-phase cell cycle arrest, decreasing migration and invasion, increasing apoptosis rates, and upregulating Bax/caspase-3 expression levels while attenuating Bcl-2 [ 181 ]. D-limonene demonstrated similar results in colon cancer cells, where it inhibited cell viability by inducing apoptosis through intrinsic pathway activation and suppressing PI3K/Akt activity [ 182 ]. Specifically, noteworthy upregulations were observed for cannabinoid CB1 receptor (CNR1), G-protein-coupled receptor 55 (GPR55), and 5-HT1a receptor (5HTR1A) when compared to vehicle-treated controls, with an approximately 50-fold increase in the expression of GPR55. Moreover, no clinical trials have assessed the potential antitumoral effects of cannabinoids in the treatment of pediatric cancer.

[Visit Link](https://www.mdpi.com/2218-273X/13/9/1388){:target="_blank" rel="noopener"}


