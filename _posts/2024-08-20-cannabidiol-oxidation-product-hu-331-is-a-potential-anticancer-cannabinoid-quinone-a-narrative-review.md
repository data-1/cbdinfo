---
layout: post
title: "Cannabidiol oxidation product HU-331 is a potential anticancer cannabinoid-quinone: a narrative review"
date: 2024-08-20
tags: [HU-331,Topoisomerase,Chemotherapy,Anthracycline,Angiogenesis,Doxorubicin,Cancer,Angiogenesis inhibitor,Cannabidiol,Clinical medicine,Biochemistry,Medical specialties,Health sciences,Biology]
author: J. Myles | Joseph E
---


1), which has been examined as a potential anticancer therapeutic (Mechoulam et al. 1968; Kogan et al. 2004; Kogan et al. 2007a; Kogan et al. 2007b). HU-331 has been shown to be active against the enzyme human topoisomerase II (TOP2), a known cancer drug target (Kogan et al. 2007b; Regal et al. 2014; Wilson et al. 2018). Further, HU-331 has also been tested in several cancer cell lines and mouse model systems (Kogan et al. 2004; Kogan et al. 2007a; Kogan et al. 2007b; Waugh et al. 2020). In 2007, Kogan et al. compared the anticancer activity and adverse event profile (general toxicity) of HU-331 vs doxorubicin in vivo using additional mouse model systems (Kogan et al. 2007a). Thus, HU-331 has the potential to be an effective cancer treatment option while potentially avoiding some of the toxic effects of agents like doxorubicin.

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00067-z){:target="_blank rel="noopener"}

