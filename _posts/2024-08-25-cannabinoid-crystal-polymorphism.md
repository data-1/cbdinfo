---
layout: post
title: "Cannabinoid crystal polymorphism"
date: 2024-08-25
tags: [Cannabinoid,Cannabidiol,Cannabinol,Crystal polymorphism,Cannabichromene,Cannabis,Cannabis research,Cannabinoids,Cannabaceae]
author: Crist N
---


Of the many cannabinoids in Cannabis, only a few have been characterized by X-ray crystal analysis and they include: delta-9-tetrahydrocannabinolic acid A (THCA-A) (Skell et al. 2021), delta-9-tetrahydrocannabinolic acid B (THCA-B) (Rosenqvist and Ottersen 1975), cannabidiol (CBD) (Mayr et al. 2017; Jones et al. 1977; Ottersen et al. 1977a), cannabinol (CBN) (Ottersen et al. 1977b), and cannabigerol (CBG) (Fettinger et al. 2020). An exhaustive literature review was also performed to learn if any cannabinoid crystal polymorphs have been described? Also, the synthetic heterocycle rimonabant [2] is an inverse agonist at the type 1 cannabinoid receptor (CB-1) and has also demonstrated crystal polymorphism (Fours et al. 2015). It was discovered 40 years ago that the synthetic 9-ketocannabinoid derivative nabilone [3] displayed crystal polymorphism. Furthermore, the THC naphthoyl ester derivative [4] was just reported to exist in as many as eight different crystal polymorphic forms, designated A-H (Hallow et al. 2021).

[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00131-2){:target="_blank rel="noopener"}

