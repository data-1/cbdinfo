---
layout: post
title: "Reversing the Psychiatric Effects of Neurodevelopmental Cannabinoid Exposure: Exploring Pharmacotherapeutic Interventions for Symptom Improvement"
date: 2024-08-20
tags: [Ventral tegmental area,Cannabis (drug),Brain-derived neurotrophic factor,Neuregulin 1,Endocannabinoid system,Schizophrenia,Hippocampus,Branches of neuroscience,Neuroscience]
author: Marta De Felice | Steven R. Laviolette | De Felice | Steven R
---


Although no clinical studies to our knowledge have yet reported long-lasting consequences on GABAergic and glutamatergic systems following adolescent cannabis exposure, aberrations have been observed in young chronic smokers [36] and in cannabis users in an early stage of psychosis [37]. Specifically, pharmacological agonist activation of GABAA receptors in the PFC at adulthood reversed short-term and social memory impairments as well as reduced anxiety and alterations in exploratory behavior induced by THC exposure during adolescence. What can rats tell us about adolescent cannabis exposure? Effects in rats of adolescent exposure to cannabis smoke or THC on emotional behavior and cognitive function in adulthood. [Google Scholar] [CrossRef] [PubMed]  Lewis, D.A.

[Visit Link](https://www.mdpi.com/1422-0067/22/15/7861){:target="_blank rel="noopener"}

