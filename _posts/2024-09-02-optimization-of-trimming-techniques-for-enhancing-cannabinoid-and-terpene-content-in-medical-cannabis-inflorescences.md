---
layout: post
title: "Optimization of Trimming Techniques for Enhancing Cannabinoid and Terpene Content in Medical Cannabis Inflorescences"
date: 2024-09-02
tags: [Cannabinoid,Cannabis,Biosynthesis,Metabolic pathway,Cannabidiol,Tetrahydrocannabinolic acid,Cannabis sativa,Tetrahydrocannabinol,Metabolism]
author: Nimrod Brikenstein | Matan Birenboim | David Kenigsbuch | Jakob A. Shimshoni
---


Inflorescences of medicinal cannabis are used worldwide for their medicinal properties for various medical conditions such as cancer, pain management, and rheumatic diseases [1–3]. In addition, the cannabinoid and terpene profiles are significantly influenced by postharvest processes [19]. Two primary methods of trimming can be distinguished in cannabis postharvest processing: “wet trimming” and “dry trimming” [19]. Postharvest operations of cannabis and their effect on cannabinoid content: a review. Postharvest operations of cannabis and their effect on cannabinoid content: a review.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11249524/){:target="_blank rel="noopener"}

