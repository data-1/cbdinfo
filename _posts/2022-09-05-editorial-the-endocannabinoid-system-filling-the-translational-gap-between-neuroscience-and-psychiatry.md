---
layout: post
title: "Editorial: The Endocannabinoid System: Filling the Translational Gap Between Neuroscience and Psychiatry"
date: 2022-09-05
categories:
- Endocannabinoid
author: De Gregorio, Neurobiological Psychiatry Unit, Department Of Psychiatry, Mcgill University, Division Of Neuroscience, Vita-Salute San Raffaele University, Ruggeri, Section Of Psychiatry, Department Of Neurosciences, Biomedicine
tags: [Addiction,Mental disorder,Psychiatry,Cannabinoid,Psychosis,Neuroscience,Effects of cannabis,Cannabidiol,Health,Medicine,Mental health,Mental disorders,Clinical medicine,Health sciences]
---


Editorial on the Research Topic  The Endocannabinoid System: Filling the Translational Gap Between Neuroscience and Psychiatry  Translational research has become a priority in every branch of medicine. In their review, Navarrete et al. summarize the evidence regarding the specific effects of cannabis use during the prenatal and postnatal periods, indicating behavioral and neurobiological aberrancies that can potentially persist throughout childhood and adolescence, and increased risk for the development of psychiatric disorders later in life, especially affective and substance use disorders (SUD). High use of cannabis during pregnancy, underestimation of its risk, progressive increase in cannabis potency in terms of Δ-9-THC concentration over the last two decades, and long-lasting neurodevelopmental consequences have been reported. To date, there is interest in the possibility that cannabidiol (CBD), the most studied compound in cannabis after Δ-9-THC, may modulate reward, decisional and sensorimotor processes, being a viable treatment for behavioral addictions. Finally, Kayser et al. explore in their methods article the translational potential and limitations of human laboratory studies of the effects of cannabinoids.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fpsyt.2021.771442/full){:target="_blank" rel="noopener"}


