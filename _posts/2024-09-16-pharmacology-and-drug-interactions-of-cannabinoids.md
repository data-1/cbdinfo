---
layout: post
title: "Pharmacology and drug interactions of cannabinoids"
date: 2024-09-16
tags: [Cannabidiol,Drug interaction,Clobazam,Tetrahydrocannabinol,Pharmacokinetics,Dose (biochemistry),Valproate,Plasma protein binding,Pharmacogenomics,CYP2C19,Stiripentol,Bioavailability,Pharmacy,Drugs acting on the nervous system,Clinical medicine,Pharmaceutical sciences,Medical treatments,Medicinal chemistry,Pharmacology,Drugs,Chemicals in medicine]
author: Cecilie Johannessen Landmark | Ulrich Brandl
institutions: Jena University Hospital
doi: "https://onlinelibrary.wiley.com/doi/10.1684/epd.2019.1123"
---


Possible pharmacokinetic interactions with other AEDs that affect the metabolism of CBD are not yet documented but might be of clinical relevance, such as clobazam. The combination of CBD and stiripentol or valproate is being studied in a Phase 2 trial which may provide answers regarding possible interactions at the level of metabolism as well as protein binding. If a significant increase in benzodiazepine levels is observed, an adequate decrease in clobazam dose is recommended. In the controlled trials, 8% of the patients showed significantly increased liver enzymes at 10 mg/kg/day and 16% at 20 mg/kg/day in combination with valproate (Devinsky et al., 2018b). CBD demonstrates a challenging pharmacokinetic profile with low bioavailability, significant protein binding, and interactions with various metabolic pathways in the liver, including CYPs that are susceptible to pharmacogenetic variability and drug interactions.