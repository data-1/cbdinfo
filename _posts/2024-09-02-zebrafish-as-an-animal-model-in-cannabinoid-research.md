---
layout: post
title: "Zebrafish as an Animal Model in Cannabinoid Research"
date: 2023-09-02
tags: [Cannabinoid,Cannabinoid receptor 1,Cannabinoid receptor 2,Zebrafish,Endocannabinoid system,Dronabinol]
author: Joanna Lachowicz | Aleksandra Szopa | Katarzyna Ignatiuk | Katarzyna Świąder | Anna Serefko
---


Behavioral studies with the use of cannabinoids in the zebrafish model. Use of cannabinoids in the treatment of disease models induced in zebrafish. Though literature data does not mention to which extent results from mammalian studies related to cannabinoids are similar to the results from zebrafish studies, there are reports confirming that the abovementioned genetic, physiological, metabolic, and pharmacologic conservation allows us to think of the zebrafish model as of a valuable tool to search for new drug targets, discover novel drugs, and model human disorders. Cannabis Cannabinoid Res. Zebrafish 2018, 15, 349–360.

[Visit Link](https://www.mdpi.com/1422-0067/24/13/10455){:target="_blank rel="noopener"}

