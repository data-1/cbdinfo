---
layout: post
title: "Enhancing Endocannabinoid Control of Stress with Cannabidiol"
date: 2022-08-27
categories:
- CBD
author: Henson, Jeremy D., Vitetta, Quezada, Hall, Jeremy D. Henson, Luis Vitetta, Michelle Quezada, Sean Hall
tags: [Stress (biology),Cannabinoid receptor type 1,Endocannabinoid system,2-Arachidonoylglycerol,Cannabidiol,Anxiety disorder,Hypothalamicpituitaryadrenal axis,Synapse,Neurotransmitter,Tetrahydrocannabinol,Chemical synapse,Cannabinoid receptor type 2,Cannabinoid,Occupational stress,Neuroscience,Physiology,Health,Biochemistry]
---


Although THC is a weak partial agonist of CBand CB, as far as the stress response is concerned, THC appears to act as a competitive inhibitor of AEA and 2-AG at CB, and THC increases basal- and stress-induced glucocorticoids [ 13 ]. CBD also acts to reduce stress and its manifestations by non-endocannabinoid receptors ( Figure 3 ). Reviews of 49 clinical trials of CBD, including intravenous, inhalation, and oral routes of administration and oral dose ranges of 10–1500 mg per day, found that CBD was well tolerated with a good safety profile [ 65 77 ]. Increased CBand CBsignaling due to FAAH inhibition by CBD attenuated stress-associated anxiety behaviors, prevented the chronic stress-associated decrease in hippocampal neurogenesis, and prevented the persistence of fear [ 37 64 ]. Again, the actions of CBD were stress specific with no effect seen on anxiety measures before the stress response [ 27 ].

<hr>

[Visit Link](https://www.mdpi.com/2077-0383/10/24/5852/htm){:target="_blank" rel="noopener"}


