---
layout: post
title: "Polypharmacological Approaches for CNS Diseases: Focus on Endocannabinoid Degradation Inhibition"
date: 2022-08-13
categories:
- Endocannabinoid
author: Papa, Pasquini, Contri, Gemma, Campiani, Butini, Varani, Vincenzi, Alessandro Papa, Silvia Pasquini
tags: [Cannabinoid receptor type 2,Cannabinoid receptor type 1,Cannabinoid receptor,Endocannabinoid system,Cannabinoid,Enzyme inhibitor,2-Arachidonoylglycerol,TRPV1,Cyclooxygenase,Synapse,Chemical synapse,Tetrahydrocannabinol,Receptor (biochemistry),Dopamine,IC50,G protein-coupled receptor,Neurotransmitter,Neuron,Neurodegenerative disease,Eicosanoid,Cell biology,Biotechnology,Neurophysiology,Neurochemistry,Neuroscience,Biochemistry]
---


These data showed how derivate JZL195 determines antinociceptive effects through CB 1 receptors [1 (30 mg/kg), a selective FAAH inhibitor URB597 (IC 50 r FAAH = 33 nM) (reported in 1/2 agonist WIN55212 ( 1/2 agonist produced side effects at the same doses at which allodynia was also detected [ In the last few years, academic and industrial efforts have been strongly focused on the development of selective FAAH or MAGL inhibitors with potential therapeutic application in several diseases such as MS, epilepsy, neuropathic pain, and chronic pain disorders [ 86 156 ]. On these bases, Dong et al. examined the role of ECS in depressive behavior, measuring AEA, 2-AG, and BDNF levels after the administration of JZL195 in WKY female rats (a rat model of depression condition) to evaluate its antidepressant activity [ 168 ]. administration of all compounds produce dose-dependent antipruritic effects, designating FAAH, MAGL, and dual FAAH/MAGL inhibitors as promising therapeutic agents for the treatment of pruritic diseases [ 170 ]. Biphenyl oxirane derivate (±) Table 1 ) (MAGL IC= 4.1 µM, rat brain FAAH IC= 5.1 µM) and 4-benzylphenyl derivate (±)(Table X) (MAGL IC= 16 µM, rat brain FAAH IC= 0.28 µM) both tested as racemic mixture, showed a good inhibition profile against FAAH and MAGL enzymes [ 172 ]. Except for the 1,2,4-triazole derivative Table 2 ), other leaving groups tested on methylene-3,4-dioxyphenyl piperidine scaffold, did not show relevant results.

<hr>

[Visit Link](https://www.mdpi.com/2073-4409/11/3/471/htm){:target="_blank" rel="noopener"}


