---
layout: post
title: "Cannabinoid receptor type-1: breaking the dogmas"
date: 2024-08-18
tags: [Endocannabinoid system,Cannabinoid receptor 1,Cannabinoid,Receptor (biochemistry),Cannabinoid receptor,Cannabinoid receptor 2,Chemical synapse,Cell signaling,Synapse,Neuron,Allosteric regulation,Receptor antagonist,Cell communication,Neurochemistry,Biochemistry,Neurophysiology,Cell biology,Basic neuroscience research,Physiology,Signal transduction,Branches of neuroscience]
author: Arnau Busquets Garcia | Edgar Soria-Gomez | Luigi Bellocchio | Giovanni Marsicano | Busquets Garcia | Soria-Gomez
---


Interestingly, recent findings have shown how CB1 receptors can modulate microglia activation, suggesting its presence in this cell type49. However, further studies and more direct, specific, and powerful tools are needed to investigate the role of mitochondrial or other intracellular CB1 receptors on synaptic transmission, brain functions, and behavior. As cannabinoid ligands present an interesting therapeutic profile91, the development of new and safer drugs such as CB1 receptor allosteric modulators is needed. Although numerous studies have fully characterized the chemical and signaling properties of these new synthetic or natural compounds97,98, the in vivo effects of all these drugs modulating physiological or pathological conditions constitutes an emerging area in the cannabinoid field. Moreover, combinations of drugs able to modulate glutamatergic or GABAergic neurotransmission with cannabinoid agonists have been shown to promote specific effects of CB1 receptors and inhibit others116.

[Visit Link](https://f1000research.com/articles/5-990/v1){:target="_blank rel="noopener"}

