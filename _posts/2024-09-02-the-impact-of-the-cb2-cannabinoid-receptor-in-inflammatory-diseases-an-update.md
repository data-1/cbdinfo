---
layout: post
title: "The Impact of the CB2 Cannabinoid Receptor in Inflammatory Diseases: An Update"
date: 2024-09-02
tags: [Cannabinoid receptor 2,Metabolic dysfunction–associated steatotic liver disease,Inflammatory bowel disease,Macrophage,Inflammation,Cirrhosis,T helper cell,Neuroinflammation,Tumor necrosis factor,Immunoglobulin E,2-Arachidonyl glyceryl ether,Interleukin 13,Macrophage polarization,Biochemistry,Cell communication,Immune system,Signal transduction,Biology,Cell signaling,Neurochemistry,Immunology,Medical specialties,Cell biology]
author: Volatiana Rakotoarivelo | Thomas Z. Mayer | Mélissa Simard | Nicolas Flamand | Vincenzo Di Marzo | Thomas Z | Di Marzo
---


[Google Scholar] [CrossRef] [PubMed]  Martin, A.B. [Google Scholar] [CrossRef] [PubMed]  Klein, T.W. [Google Scholar] [CrossRef] [PubMed]  Schatz, A.R. Brain cannabinoid receptor 2: Expression, function and modulation. [Google Scholar] [CrossRef] [PubMed]  Park, H.-J.

[Visit Link](https://www.mdpi.com/1420-3049/29/14/3381){:target="_blank rel="noopener"}

