---
layout: post
title: "Considerations for Cannabinoids in Perioperative Care by Anesthesiologists"
date: 2022-08-13
categories:
- Cannabinoid
author: Laudanski, Wain, Krzysztof Laudanski, Justin Wain
tags: [Anandamide,Cannabidiol,Anesthesia,Postoperative nausea and vomiting,Tetrahydrocannabinol,2-Arachidonoylglycerol,Cannabinoid receptor type 2,Cannabinoid receptor,Dronabinol,Cannabinoid,Cannabinoid receptor type 1,Nabilone,Medical cannabis,Pain management,Endocannabinoid system,Platelet-activating factor,Cannabis (drug),Clinical medicine,Medical specialties,Physiology,Health,Drugs,Medicine]
---


19,35,78,83,200,14,87,107,117,156,167,169,26,44,46,71,72, A formidable challenge is gauging the intake of cannabinoids, as the dosages and potency of different cannabinoid preparations vary in strength and composition [ 18 37 ]. 338,337,340,341,286,49,89, An individual may present in five potential states related to cannabinoid use: acute intoxication, chronic/habitual intake, withdrawal from cannabinoids substances, positive urine toxicology screen, or taking FDA-approved medication. Finally, exposure to cannabinoids may occur in an insidious/second-hand way, impacting the ability to consent [ 66 346 ]. Overall, the effect of cannabinoids on the cardiovascular system is hypotension combined with tachycardia. Of note, increased risk of a coronary event is prolonged and occurs after even a single intake of cannabinoid compounds [ 204 ].

<hr>

[Visit Link](https://www.mdpi.com/2077-0383/11/3/558/htm){:target="_blank" rel="noopener"}


