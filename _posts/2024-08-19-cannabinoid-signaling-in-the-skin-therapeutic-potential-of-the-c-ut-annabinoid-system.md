---
layout: post
title: "Cannabinoid Signaling in the Skin: Therapeutic Potential of the “C(ut)annabinoid” System"
date: 2024-08-19
tags: [Cannabinoid receptor 2,Endocannabinoid system,Cannabinoid receptor,Cannabinoid,Fatty-acid amide hydrolase 1,Cannabinoid receptor 1,Inflammation,Hair follicle,Wound healing,Melanocyte,Keratinocyte,RELA,TRPV1,Cell signaling,Receptor antagonist,UVB-induced apoptosis,Adenosine A2A receptor,Receptor (biochemistry),Transforming growth factor beta,Tetrahydrocannabivarin,Anti-inflammatory,Cytokine,Anandamide,Ultraviolet,Fatty acid-binding protein,Biology,Biochemistry,Signal transduction,Neurochemistry,Cell biology]
author: Kinga Fanni Tóth | Dorottya Ádám | Tamás Bíró | Attila Oláh | Kinga Fanni
---


have been shown to be expressed on human epidermal keratinocytes [45,91,92,95,111,133], and the functional activity of the putative EMT was also demonstrated on these cells [170]. Finally, as mentioned above, CBD (10 μM; A2A receptor dependent action) [120], as well as CBG, CBGV, CBC, CBDV, and THCV (all in 0.1 μM) [126] were found to exert anti-inflammatory effects in human sebocytes, whereas CBD (0.1 μM; adenosine receptor dependent action) was also shown to be effective in alleviating poly-(I:C)- induced pro-inflammatory response in cultured human plucked HF-derived outer root sheath (ORS) keratinocytes [140]. Interestingly, CBD (3–10 μM) and THC (15 μM) were found to trigger activation of RBL-2H3 cells via inducing Ca2+-influx. First of all, expression of CB1 and CB2 was already demonstrated in human dermal fibroblasts. Wound Healing  Considering that cannabinoid signaling regulates fibroblast functions, proliferation and differentiation of epidermal keratinocytes, as well as cutaneous inflammation, it is not surprising that it influences the complex [341,342,343,344,345] process of cutaneous wound healing as well.

[Visit Link](https://www.mdpi.com/1420-3049/24/5/918){:target="_blank rel="noopener"}

