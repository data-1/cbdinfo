---
layout: post
title: "Sequence and annotation of 42 cannabis genomes reveals extensive copy number variation in cannabinoid synthesis and pathogen resistance genes"
date: 2024-09-24
tags: [Cannabis,DNA methylation,Gene,Single-nucleotide polymorphism,DNA sequencing,Copy number variation,CpG site,Genome,Genetics,Gene expression,Life sciences,Molecular biology,Biochemistry,Biology,Biotechnology]
author: Kevin McKernan | Yvonne Helbert | Liam T. Kane | Heather Ebling | Lei Zhang | Biao Liu | Zachary Eaton | Stephen McLaughlin | Sarah B. Kingan | Primo Baybayan | Gregory T. Concepcion | Mark C. Jordan | Alberto Riva | W. Brad Barbazuk | Timothy T. Harkins
institutions: 
doi: "https://www.biorxiv.org/content/10.1101/2020.01.03.894428v1.full"
---


Cannabinoid and terpene production by plants is linked to both attraction of pollinators and responses to plant pathogens (Penuelas et al. 2014; Andre et al. 2016; Allen et al. 2019; Lyu et al. 2019). Copy number variation across the genomes  Copy number variation (CNV) in cannabinoid synthase genes has been reported previously (van Bakel et al. 2011; McKernan 2015; Weiblen et al. 2015; Pisupati et al. 2018; Vergara 2019). Twenty-three TLPs, 35 chitinases, and 24 MLO genes were found in the Jamaican Lion reference genome and were evaluated for gene expression in 5 parental tissues and genomic copy number variation across the 40 genomes. Using the genomic DNA coverage maps across THCA synthase (THCAS), cannabidiolic acid synthase (CBDAS) and cannabichromenic acid synthase (CBCAS) (genes found on contigs 741, 1772, 756), we were able to classify plant primary cannabinoid expression into Type I, II, and III plants (Figure 9). S17) in the JL5 genome.