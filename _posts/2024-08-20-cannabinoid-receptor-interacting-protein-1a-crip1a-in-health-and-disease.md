---
layout: post
title: "Cannabinoid Receptor Interacting Protein 1a (CRIP1a) in Health and Disease"
date: 2024-08-20
tags: [Granule cell,Cannabinoid receptor 1,Excitatory postsynaptic potential,Striatum,Dentate gyrus,Mossy fiber (hippocampus),Schizophrenia,Hippocampus,Endocannabinoid system,Dopaminergic pathways,Chemical synapse,Gene expression,Synapse,Ventral tegmental area,Cannabinoid receptor 2,Cell biology,Biochemistry,Biology]
author: Emily E. Oliver | Erin K. Hughes | Meaghan K. Puckett | Rong Chen | W. Todd Lowther | Allyn C. Howlett | Emily E | Erin K | Meaghan K | W. Todd
---


Cell. Cell. Cell. Genes Cells 2015, 20, 324–339. Cell.

[Visit Link](https://www.mdpi.com/2218-273X/10/12/1609){:target="_blank rel="noopener"}

