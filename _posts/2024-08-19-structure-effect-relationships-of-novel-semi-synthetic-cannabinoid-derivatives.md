---
layout: post
title: "Structure–Effect Relationships of Novel Semi-Synthetic Cannabinoid Derivatives"
date: 2024-08-19
tags: [Cannabinoid receptor 2,Cannabinoid receptor 1,Receptor antagonist,Tumor necrosis factor,Macrophage,Interleukin 6,Real-time polymerase chain reaction,Monocyte,Cannabidiol,Cell signaling,Cell biology,Biochemistry]
author: Marcus R | Juan A | Fernández-Ruiz | Bernd L | García-Toscano | Gómez-Cañas | Maria R
---


The membranes were incubated for 90 min at 30°C with the radioligand and different concentrations of the different derivatives. Anti-Inflammatory Effect on Human Monocytes  To test the anti-inflammatory effect of the test compounds, monocytes were incubated for 30 min with increasing doses of the test compounds. Trend tests were performed using the Jonckheere–Terpstra test. The CBD-derivatives 2-HEC (1), 2-HPC (2), GCBD (3), CHC (4), and NMSC (6) activated the CB2-receptor and acted as agonists (Figures 1 and 4). 2-HEC (1), 2-HPC (2), GCBD (3), and NMSC (6) significantly inhibited PMA/IO-induced NFAT activation of Jurkat T-cells (Figure 6) in a dose-dependent fashion (trend tests: p = 0.038, p = 0.005, p = 0.006, and p = 0.06, respectively).

[Visit Link](https://www.frontiersin.org/journals/pharmacology/articles/10.3389/fphar.2019.01284/full){:target="_blank rel="noopener"}

