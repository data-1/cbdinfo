---
layout: post
title: "Effects of Cold Temperature and Acclimation on Cold Tolerance and Cannabinoid Profiles of Cannabis sativa L. (Hemp)"
date: 2024-08-25
tags: [Cannabis,Cannabidiol,Cannabinoid,Tetrahydrocannabinol,High-performance liquid chromatography,Tetrahydrocannabinolic acid,Analysis of variance,Hemp]
author: Andrei Galic | Heather Grab | Nicholas Kaczmar | Kady Maser | William B. Miller | Lawrence B. Smart | William B | Lawrence B
---


Electrolyte leakage as a result of cold intensity treatments (ELDL) differed between cultivars (F = 17.108, p < 0.0001), plant age groups (F = 3.870, p < 0.05), and detached-leaf cold stress exposures (F = 12.316, p < 0.0001) ( ). Plant J. Plant. Plants. **Cultivar × Cold Acclimation × Plant Agen.s.

[Visit Link](https://www.mdpi.com/2311-7524/8/6/531){:target="_blank rel="noopener"}

