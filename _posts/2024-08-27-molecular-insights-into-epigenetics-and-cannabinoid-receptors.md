---
layout: post
title: "Molecular Insights into Epigenetics and Cannabinoid Receptors"
date: 2024-08-27
tags: [Cannabinoid receptor 2,Cannabinoid receptor 1,MicroRNA,Gene expression,Epigenetics,2-Arachidonoylglycerol,DNA methylation,Regulation of gene expression,Anandamide,Histone,H3K9ac,Promoter (genetics),MECP2,Endocannabinoid system,Transcription (biology),Histone deacetylase,H3K4me3,Methyltransferase,H3K9me2,H3K27me3,DNA methyltransferase,Five prime untranslated region,Biotechnology,Life sciences,Macromolecules,Cellular processes,Genetics,Biochemistry,Cell biology,Biology,Molecular biology,Molecular genetics]
author: Balapal S. Basavarajappa | Shivakumar Subbanna | Balapal S
---


DNA methylation and CB1 receptor gene (Cnr1) expression. [Google Scholar] [CrossRef]  Basavarajappa, B.S. Brain cannabinoid receptor 2: Expression, function and modulation. [Google Scholar] [CrossRef]  Basavarajappa, B.S. [Google Scholar] [CrossRef] [PubMed] [Green Version]  Laprairie, R.; Kelly, M.; Denovan-Wright, E. The dynamic nature of type 1 cannabinoid receptor (CB1) gene transcription.

[Visit Link](https://www.mdpi.com/2218-273X/12/11/1560){:target="_blank rel="noopener"}

