---
layout: post
title: "Identification and Characterization of Cannabimovone, a Cannabinoid from Cannabis sativa, as a Novel PPARγ Agonist via a Combined Computational and Functional Study"
date: 2024-08-20
tags: [Gene expression,GLUT4,Real-time polymerase chain reaction,Docking (molecular),3T3-L1,Protein kinase B,Biology,Cell biology,Biochemistry,Molecular biology,Biotechnology]
author: Fabio Arturo Iannotti | Fabrizia De Maio | Elisabetta Panza | Giovanni Appendino | Orazio Taglialatela-Scafati | Luciano De Petrocellis | Pietro Amodeo | Rosa Maria Vitale | Fabio Arturo | De Maio
---


Chemical structure of cannabimovone (CBM) with functional groups colored in red. Therefore, to further explore the pharmacological activity of CBM on PPARγ, we induced 3T3-L1 pre-adipocytes to differentiate for 10 days in the presence of rosiglitazone at 1 µM, and CBM at both 10 and 30 µM, to further explore the pharmacological activity of CBM on PPARγ. Effect of rosiglitazone and CBM in differentiating 3T3-L1 cells. CBM Improves Insulin Sensitivity in Differentiating 3T3-L1 cells  Finally, we explored whether, in differentiated 3T3-L1 cells, the insulin signaling impairment induced by palmitate could be prevented by CBM via PPARγ activation. To validate the computational results, luciferase assays on both PPARγ and PPARα were carried out, confirming the higher potency of CBM toward PPARγ in comparison to PPARα ( ) with a dose-dependent activation of Gal4-PPARγ in the former case.

[Visit Link](https://www.mdpi.com/1420-3049/25/5/1119){:target="_blank rel="noopener"}

