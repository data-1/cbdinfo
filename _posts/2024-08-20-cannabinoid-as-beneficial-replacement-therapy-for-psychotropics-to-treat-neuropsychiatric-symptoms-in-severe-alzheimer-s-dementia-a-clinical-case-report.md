---
layout: post
title: "Cannabinoid as Beneficial Replacement Therapy for Psychotropics to Treat Neuropsychiatric Symptoms in Severe Alzheimer’s Dementia: A Clinical Case Report"
date: 2024-08-20
tags: [Dementia,Antipsychotic,Cannabinoid,Alzheimer's disease,Tetrahydrocannabinol,Clinical medicine,Health,Medicine,Health care,Diseases and disorders,Medical specialties]
author: 
---


In June 2015, the patient progressed to severe dementia stage and behavioral symptoms (agitation, disruptive behavior, anxiety) worsened again. In the course of treatment with dronabinol, no side effects or adverse events occurred. Although there are no FDA-approved drugs for NPS (with the exception of short-term treatment with risperidone for severe aggression), it is common clinical practice to prescribe psychotropic medication such as antipsychotic drugs or sedatives to control symptoms in the long term. In this case report, oral dronabinol turned out as promising candidate for treating behavioral symptoms such as agitation, aggression and anxiety even in a severe stage of AD (11–14). Prior studies reported that especially lower doses of THC may have anxiolytic effects (17, 18).

[Visit Link](https://www.frontiersin.org/journals/psychiatry/articles/10.3389/fpsyt.2020.00413/full){:target="_blank rel="noopener"}

