---
layout: post
title: "A bibliometric analysis of the cannabis and cannabinoid research literature - Journal of Cannabis Research"
date: 2022-07-26
categories:
- Cannabinoid
- Cannabis
author: Ng, Jeremy Y., Michael G. Degroote Centre For Learning, Discovery, Department Of Health Research Methods, Evidence, Impact, Faculty Of Health Sciences, Mcmaster University, Hamilton
tags: [Cannabis sativa,Cannabis (drug),Cannabidiol,Cannabis,Tetrahydrocannabinol,Health]
---


One analysis provided a breakdown of this collective funding by country, finding that more than $1.4 billion funded researchers in the USA; the UK at $39.9 million, and Canada at $36.1 million represented a distant second and third respectively (Hellth n.d.-a). Systematic reviews and meta-analyses have generally found a consistent association between cannabis use and psychosis or other mental health conditions that cause psychotic symptoms (Matheson et al. 2011; Minozzi et al. 2010; Moore et al. 2007). One systematic review conducted by Belendiuk et al. (2015) provides a review of the literature exploring the use of cannabis as a treatment for cancer and multiple sclerosis. Belendiuk et al. (2015) also explored the use of cannabis as a treatment for multiple sclerosis. Strengths and limitations  This bibliometric study contained several notable strengths including the fact that the characteristics of 29 802 publications published in 5474 journals were captured, representing the largest of all bibliometric analyses on this topic to date.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-022-00133-0){:target="_blank" rel="noopener"}


