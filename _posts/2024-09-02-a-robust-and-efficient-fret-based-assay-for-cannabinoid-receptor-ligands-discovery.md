---
layout: post
title: "A Robust and Efficient FRET-Based Assay for Cannabinoid Receptor Ligands Discovery"
date: 2024-09-02
tags: [Ligand (biochemistry),Ligand binding assay,Cannabinoid receptor 2,Biochemistry]
author: Gemma Navarro | Eddy Sotelo | Iu Raïch | María Isabel Loza | Jose Brea | Maria Majellaro | María Isabel
---


These wavelengths are compatible with the emission spectra of Terbium [24], the lanthanide used for CBRs labeling in the developed Tag-lite® binding assay. CELT-335 HTRF Assay Validation in hCB2R Expressing Adherent Cells  As in the case of CB1R, the Kd obtained through the Tag-lite® saturation binding assay (24.2 nM) confirmed the previously observed affinity of CELT-335 for CB2R and demonstrated high FRET between Terbium and the fluorescent ligand (HTRF signal, Ratio 665/620, see ). In an effort to advance drug research in this field, we validated a dual hCB1/hCB2Rs fluorescent ligand (CELT-335) in the Tag-lite® competition binding assay, demonstrating its suitability for screening new compounds with diverse chemical structures, functional activities, and ranges of affinity for both subtypes of cannabinoid receptors. Evaluation of Binding in a Transfected Cell Line Expressing a Peripheral Cannabinoid Receptor (CB2): Identification of Cannabinoid Receptor Subtype Selective Ligands. Development of Selective, Fluorescent Cannabinoid Type 2 Receptor Ligands Based on a 1,8-Naphthyridin-2-(1: H)-One-3-Carboxamide Scaffold.

[Visit Link](https://www.mdpi.com/1420-3049/28/24/8107){:target="_blank rel="noopener"}

