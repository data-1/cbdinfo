---
layout: post
title: "Association of early onset of cannabis, cigarette, other drug use and schizophrenia or psychosis"
date: 2024-09-16
tags: [Schizophrenia,Psychosis,Substance abuse,Cannabis (drug),Post-traumatic stress disorder,Behavioural sciences,Mental health,Mental disorders,Health,Clinical medicine,Abnormal psychology,Diseases and disorders,Human diseases and disorders,Causes of death,Epidemiology,Social issues]
author: Jennie E. Ryan | Philip Veliz | Sean Esteban McCabe | Sarah A. Stoddard | Carol J. Boyd
institutions: University of Michigan–Ann Arbor
doi: "https://www.ncbi.nlm.nih.gov/pmc/articles/7035998"
---


To address this limitation, Davis et al. (2013) used data derived from the 2004-2005 National Epidemiologic Survey on Alcohol and Related Conditions (NESARC-II) and found that cannabis use was related to schizophrenia/psychosis in a dose dependent manner, however the authors did not adjust for possible covariates including cigarette, alcohol, and polysubstance use. The AUDADIS-5 includes questions regarding drug specific use for cannabis, cigarettes, alcohol and nine other classes of drugs (See , and supplementary 2). Individuals with schizophrenia/psychosis also were significantly more likely to have used cannabis (38% vs 22%, p=0.001), cigarettes (36% vs 22%, p<0.001) and alcohol (37% vs 28%, p=0.023), and other drugs before age 16 years (21% vs 8%, p<0.001). In models of early cannabis + cigarette + alcohol use (models 6, 13) only cigarette use was associated with schizophrenia/psychosis (aOR 1.69, CI 1.21-2.36, p= 0.002 for cigarettes; aOR 1.12, CI 0.76-1.63, p=0.565 for cannabis; aOR 1.27, CI 0.66-2.45, p=0.468 for alcohol). This study highlights the importance of considering cofounding factors such as polysubstance use when describing the relationship between cannabis use in early adolescence and schizophrenia or psychosis, and the need for more prospective research.