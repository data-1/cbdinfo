---
layout: post
title: "Involvement of the endocannabinoid system in the inhibition of Sindbis virus replication: a preliminary study - Journal of Cannabis Research"
date: 2022-07-26
categories:
- endocannabinoid
author: Rodriguez, Juan L., Biology Department, Colorado State University-Pueblo, Pueblo, Lopez, Joseph A., Steel, J. Jordan, Department Of Biology
tags: [Cannabinoid receptor type 2,Cannabinoid receptor,Cannabinoid receptor type 1,Virus,Chikungunya,Alphavirus,Receptor (biochemistry),Receptor antagonist,Infection,Biochemistry,Life sciences,Medical specialties,Biotechnology,Cell biology,Biology]
---


Despite the millions of people infected with CHIKV and chronic arthralgia, there are no specific treatments or drugs commercially available for individuals infected with Alphaviruses and it is necessary to evaluate plausible treatment options, one of which, that targets the endocannabinoid system. There are two different cannabinoid (CB) receptors that have been found on human cells: CB1 and CB2 (Tuccinardi et al. 2006; Ai and Chang 2012). These CB receptors bind to endogenous, exogenous, or synthetic cannabinoids and trigger specific cellular responses (Tuccinardi et al. 2006). The binding and stimulation of the CB receptors seems to be the key for the clinical and physiological responses induced during cannabinoid use. In this study, we have verified that SINV infects liver cells and have investigated the impact of cannabinoid receptor stimulation on Sindbis virus (alphavirus) replication.

<hr>[Visit Link](https://jcannabisresearch.biomedcentral.com/articles/10.1186/s42238-021-00068-y){:target="_blank" rel="noopener"}


