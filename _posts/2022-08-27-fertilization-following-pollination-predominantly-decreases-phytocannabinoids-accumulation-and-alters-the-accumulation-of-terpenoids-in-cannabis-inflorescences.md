---
layout: post
title: "Fertilization Following Pollination Predominantly Decreases Phytocannabinoids Accumulation and Alters the Accumulation of Terpenoids in Cannabis Inflorescences"
date: 2022-08-27
categories:
- Cannabis
author: Lipson Feder, The Laboratory Of Cancer Biology, Cannabinoid Research, Faculty Of Biology, Technion-Israel Institute Of Technology, Cohen, Agricultural Research Organization, Aro, Volcani Center, Institute Of Plant Sciences
tags: [Terpene,Cannabinoid]
---


Fertilized flowers are considerably less potent, likely due to changes in the contents of phytocannabinoids and terpenoids; therefore, this study examined the effect of fertilization on metabolite composition by crossbreeding (-)-Δ 9 - trans -tetrahydrocannabinol (THC)- or cannabidiol (CBD)-rich female plants with different male plants: THC-rich, CBD-rich, or the original female plant induced to develop male pollen sacs. Therefore, it is likely that the content of secondary metabolites such as phytocannabinoids and terpenoids changes following the pollination and fertilization of Cannabis inflorescences. In order to gain more insight into the Cannabis metabolite regulation pathway, this work studied the effect of flower fertilization on the plant’s secondary metabolite accumulation. Results  Phytocannabinoids Quantity Predominantly Decreases After Fertilization  Mature inflorescences (6 or 8 weeks post flower induction) from female Cannabis plants of two distinct chemovars (Figures 1A,B), THC-rich (Type I) and CBD-rich (Type III), were subjected to fertilization by three different male Cannabis plants: THC-rich (Figures 1C,D), CBD-rich (Figures 1E,F) or the original female plant induced to develop male pollen sacs by application of ethylene inhibitor (Figure 1). Discussion  The present study was designed to examine the influence of flower fertilization on the accumulation of Cannabis secondary metabolites.

<hr>

[Visit Link](https://www.frontiersin.org/articles/10.3389/fpls.2021.753847/full){:target="_blank" rel="noopener"}


