---
layout: post
title: "Clinical and cognitive outcomes in first-episode psychosis: focus on the interplay between cannabis use and genetic variability in endocannabinoid receptors"
date: 2024-09-03
tags: [Psychosis,Wechsler Adult Intelligence Scale,Schizophrenia,Cannabis (drug)]
author: Maitane Oscoz-Irurozqui | Maria Guardiola-Ripoll | Carmen Almodóvar-Payá | Amalia Guerrero-Pedraza | Noemí Hostalet | María Isabel Carrion | Salvador Sarró | JJ Gomar | Edith Pomarol-Clotet | Mar Fatjó-Vilas
---


Cannabinoid receptor gene polymorphisms and cognitive performance in patients with schizophrenia and controls. Brain cannabinoid CB2 receptor in schizophrenia. The role of cannabis in cognitive functioning of patients with schizophrenia. Cannabinoid receptor gene polymorphisms and cognitive performance in patients with schizophrenia and controls. The role of cannabis in cognitive functioning of patients with schizophrenia.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC11348434/){:target="_blank rel="noopener"}

