---
layout: post
title: "Down-Regulation of Cannabinoid Type 1 (CB1) Receptor and its Downstream Signaling Pathways in Metastatic Colorectal Cancer"
date: 2024-08-19
tags: [Metastasis,MAPK/ERK pathway,Real-time polymerase chain reaction,Endocannabinoid system,Apoptosis,Extracellular signal-regulated kinases,Cancer,Cannabinoid receptor 2,Angiogenesis,Cannabinoid receptor 1,Cell signaling,Colorectal cancer,Gene expression,Biology,Cell biology,Biochemistry,Life sciences]
author: Valeria Tutino | Maria Gabriella Caruso | Valentina De Nunzio | Dionigi Lorusso | Nicola Veronese | Isabella Gigante | Maria Notarnicola | Gianluigi Giannelli | Maria Gabriella | De Nunzio
---


Previously, we demonstrated a significant reduction in CB1 receptor gene expression levels in cancer tissue compared to normal surrounding mucosa of patients with colorectal cancer (CRC), confirming that the negative modulation of cell proliferation, mediated by CB1 receptor, is lost in cancer [11]. The CB1 receptor protein levels were measured using a specific CB1 receptor antibody [22,23], observing statistically significant differences of CB1 receptor protein expression between patients with and without metastases, both in intestinal normal mucosa and tumor tissue ( b,c). Statistically significant differences were also observed for p38 MAPK protein expression between normal mucosa and cancer obtained from patients with and without metastasis ( b,c). shows the western blotting analysis of ERK1/2 and p-ERK1/2 protein expression, demonstrating a significant decrease of p-ERK1/2/ERK1/2 ratio, overall in normal mucosa. [Google Scholar] [CrossRef] [PubMed] [Green Version]  Di Marzo, V.; Bifulco, M.; De Petrocellis, L. The endocannabinoid system and its therapeutic exploitation.

[Visit Link](https://www.mdpi.com/2072-6694/11/5/708){:target="_blank rel="noopener"}

