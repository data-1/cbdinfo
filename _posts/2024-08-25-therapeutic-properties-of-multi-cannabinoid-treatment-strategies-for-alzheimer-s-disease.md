---
layout: post
title: "Therapeutic properties of multi-cannabinoid treatment strategies for Alzheimer’s disease"
date: 2024-08-25
tags: [Endocannabinoid system,Cannabinoid,Cannabinol,Cannabinoid receptor 2,Cannabidiol,Medical cannabis,Cannabis (drug),Alzheimer's disease,Tetrahydrocannabinol,Amyloid beta,Dementia,Anandamide,Tetrahydrocannabinolic acid,Biochemistry]
author: Steiner-Lim | Genevieve Z
---


for mice). Treatment effects of cannabinoid combinations and cannabis extracts in Alzheimer’s disease  Research suggests that cannabinoid treatments involving a combination of THC and CBD and other cannabinoids can produce greater therapeutic outcomes and less adverse effects than treatment with purified cannabinoid isolates. Chronic treatment of 6-month-old male APP/PS1 mice with either a CBD-rich (64.8% CBD, 2.3% THC), THC-rich (67.1% THC, 0.3% CBD), or 1:1 CBD:THC combination extract (all 0.75 mg/kg for each cannabinoid) reversed object recognition memory deficits of AD transgenic mice. Importantly, further research is required including clinical studies for multi-cannabinoid combination therapy in AD. Conflict of interest  GZS and TK have received funding from medicinal cannabis companies to conduct research on medicinal cannabis products, outside of this study.

[Visit Link](https://www.frontiersin.org/journals/neuroscience/articles/10.3389/fnins.2022.962922/full){:target="_blank rel="noopener"}

