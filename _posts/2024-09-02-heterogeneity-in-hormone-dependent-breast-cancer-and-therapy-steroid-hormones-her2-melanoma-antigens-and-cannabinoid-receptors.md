---
layout: post
title: "Heterogeneity in hormone-dependent breast cancer and therapy: Steroid hormones, HER2, melanoma antigens, and cannabinoid receptors"
date: 2024-09-02
tags: [Cancer,Breast cancer,Metastasis,HER2,Health sciences,Biology,Neoplasms,Causes of death,Medical specialties,Diseases and disorders,Clinical medicine]
author: 
---


Breast cancer is the most frequently diagnosed cancer and the leading cause of death by cancer among women worldwide. The prognosis of the disease and patients’ response to different types of therapies varies in different subgroups of this heterogeneous disease. The response to the therapy can be also affected by the concurrent use of alternative therapy, e.g., cannabinoid use is popular among breast cancer patients. In this review, we summarize the role of ER, PR, and HER2 in hormone-dependent breast cancer; provide current knowledge of MAGEs and cannabinoid receptors in breast cancer; ultimately discuss the potential interlacement of their signaling paths which may underlay diverse responses to therapies in breast cancer patients simultaneously using cannabinoids. These interactions are poorly understood but critical for the advancement of conventional and complementary treatment options for patients, particularly the ones with metastatic disease.

[Visit Link](https://www.sciencedirect.com/science/article/pii/S2667394022000600){:target="_blank rel="noopener"}

