---
layout: post
title: "Medicinal Cannabis and Synthetic Cannabinoid Use"
date: 2024-08-20
tags: [Medical cannabis,Cannabidiol,Cannabinoid,Psychoactive drugs,Medical treatments,Pharmacology,Drugs,Medicine,Clinical medicine,Health]
author: Simona Pichini | Alfredo Fabrizio Lo Faro | Francesco Paolo Busardò | Raffaele Giorgetti | Lo Faro | Alfredo Fabrizio | Francesco Paolo
---


Mammana et al. started this Special Issue with a research article on the medical use of cannabis products. In their original investigation, Tejedor-Cabrera et al. explored the risk of drug abuse associated with marijuana/hashish and alcohol use was assessed in young nursing students. [Google Scholar] [CrossRef] [PubMed]  Pellegrini, M.; Marchei, E.; Papaseit, E.; Farré, M.; Zaami, S. UHPLC-HRMS and GC-MS screening of a selection of synthetic cannabinoids and metabolites in urine of consumers. Medicina 2020, 56, 453. https://doi.org/10.3390/medicina56090453  AMA Style  Pichini S, Lo Faro AF, Busardò FP, Giorgetti R. Medicinal Cannabis and Synthetic Cannabinoid Use. Medicinal Cannabis and Synthetic Cannabinoid Use Medicina 56, no.

[Visit Link](https://www.mdpi.com/1648-9144/56/9/453){:target="_blank rel="noopener"}

