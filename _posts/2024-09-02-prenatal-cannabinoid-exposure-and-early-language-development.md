---
layout: post
title: "Prenatal cannabinoid exposure and early language development"
date: 2024-09-02
tags: [Cannabinoid,Prenatal development,Endocannabinoid system,Pregnancy,Cannabis (drug),Cannabinoid receptor,Health]
author: Talavera-Barber | Maria M | Amy J
---


In this study, we used data from a large prospective cohort study, the Safe Passage Study conducted by the Prenatal Alcohol and SIDS and Stillbirth (PASS) Network, of mother-infant dyads in the Northern Plains to examine the association between PCE and the timing of exposure and MSEL scores in children 12 months of age. Simple and multiple linear regression models were used to determine whether infants exposed to prenatal cannabis use were significantly associated with differences in MSEL scores compared to infants unexposed to prenatal cannabis. TABLE 2  Among infants who were exposed late in pregnancy, we observed higher MSEL scores for early composite, expressive language, and receptive language when compared to unexposed infants after adjusting for covariates. Our results demonstrated that infants exposed to cannabis later in pregnancy scored higher in overall early composite score, expressive, and receptive language compared to unexposed infants, and infants exposed to cannabis in early pregnancy scored higher in gross motor scores compared to unexposed infant. We based this hypothesis on preclinical evidence suggesting that THC directly impacts fetal brain development.

[Visit Link](https://www.frontiersin.org/journals/pediatrics/articles/10.3389/fped.2023.1290707/full){:target="_blank rel="noopener"}

