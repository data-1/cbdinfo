---
layout: post
title: "Perturbation of 3D nuclear architecture, epigenomic dysregulation and aging, and cannabinoid synaptopathy reconfigures conceptualization of cannabinoid pathophysiology: part 1–aging and epigenomics"
date: 2024-09-02
tags: [Cellular differentiation,DNA repair,Epigenomics,Transcription (biology),Enhancer (genetics),Promoter (genetics),Histone,Endocannabinoid system,Super-enhancer,Chromatin,Birth defect,Nucleosome,Atresia,Epigenetics,DNA,CTCF,Synapse,Branched-chain amino acid,Cell signaling,N6-Methyladenosine,DNA methylation,RAD51,Cannabinoid,Gene expression,T helper cell,Gene,Congenital heart defect,Carcinogenesis,Mitochondrion,Biochemistry,Genetics,Biotechnology,Cellular processes,Biology,Biological processes,Life sciences,Health sciences,Molecular biology,Cell biology]
author: Albert Stuart Reece | Gary Kenneth Hulse
---


Table 1  N Group Disorder Genes identified in the Schrott database Genes implicated % Genes implicated in Schrott Reference 1Brain disordersSchizophrenia59768587.10%Trubetskoy V. Nature 2022; 604(7906): 502–5082Congenital anomaliesOocytic Zar1 activation16220877.88%Cheng S, Science, 2022; 378 (6617)3Congenital anomaliesSperm2,9744,93060.34%Chen Y, Cell Res. Epigenetic effects of cannabis exposure. Cell. Cells. Aging Cell.

[Visit Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10507876/){:target="_blank rel="noopener"}

