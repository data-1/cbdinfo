---
layout: post
title: "Cannabinoids for the Treatment of Dermatologic Conditions"
date: 2022-07-31
categories:
- Cannabinoid
author: 
tags: [Cannabinoid receptor type 2,Tetrahydrocannabinol,Clinical medicine,Health,Medicine,Medical specialties,Diseases and disorders,Medical treatments,Drugs,Neurochemistry]
---


In recent years, cannabinoid (CB) products have gained popularity among the public. The anti-inflammatory properties of CBs have piqued the interest of researchers and clinicians because they represent promising avenues for the treatment of autoimmune and inflammatory skin disorders that may be refractory to conventional therapy. A primary literature search was conducted in October 2020, using the PubMed and Embase databases, for all articles published from 1965 to October 2020. Review articles, studies using animal models, and nondermatologic and pharmacologic studies were excluded. From 248 nonduplicated studies, 26 articles were included. Although promising, additional research is necessary to evaluate efficacy and to determine dosing, safety, and long-term treatment guidelines.

<hr>[Visit Link](https://www.sciencedirect.com/science/article/pii/S2667026722000017){:target="_blank" rel="noopener"}


